//
//  AccountSettings.swift
//  Droppin
//
//  Created by Rene Reyes on 10/28/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation


enum UserPropertyKey: String {
    case currentUserId = "currentUserId"
    case isAccountCreated = "accountCreated"
    case isTester = "isTester"
    case token = "token"
    case isFirstTimeViewingARequestLoaded = "isFirstTimeViewingARequestLoaded"
    case isFirstTimeViewShown = "isFirstTimeViewShown"
    case isFirstTimeCreatingAPin = "isFirstTimeCreatingAPin"
}

struct AccountSettings {
    private static let FIRST_TIME_VIEW_SHOW = "signed_show"
    private static let FIRST_TIME_VIEW_HIDE = "signed_hide"
    
    static let profileImageData = "profileImageData"
    
    static func isVisibleFirstTimeSignedIn() -> Bool {
        guard let value = UserDefaults.standard.string(forKey: UserPropertyKey.isFirstTimeViewShown.rawValue) else { return false }
        return value == FIRST_TIME_VIEW_SHOW
    }
    
    static func setFirstTimeViewAs(as shown: Bool) {
        let value = shown ? FIRST_TIME_VIEW_SHOW : FIRST_TIME_VIEW_HIDE
        UserDefaults.standard.set(value, forKey: UserPropertyKey.isFirstTimeViewShown.rawValue)
    }
    
    static func removeFirstTimeSignedIn() {
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.isFirstTimeViewShown.rawValue)
    }
    
    static func set(_ value: Any?, for property: UserPropertyKey) {
        UserDefaults.standard.set(value, forKey: property.rawValue)
    }
    
    static func get(property: UserPropertyKey) -> Any? {
        return UserDefaults.standard.object(forKey: property.rawValue)
    }
}


//UserDefaults.standard.removeObject(forKey: "currentUserId")
