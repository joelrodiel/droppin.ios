//
//  BasicAlert.swift
//  Droppin
//
//  Created by Madson on 9/27/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class BasicAlert: UIAlertController {

    private var completion: ((String) -> Void)?

    convenience init(title: String, message: String? = nil) {
        self.init(title: title, message: message, preferredStyle: .alert)
        setupAlert()
    }

    func present(from viewController: UIViewController) {
        viewController.present(self, animated: true, completion: nil)
    }
}

extension BasicAlert {
    private func setupAlert() {
        self.addAction(self.okAction)
    }

    private var okAction: UIAlertAction {
        return UIAlertAction(title: "OK", style: .default)
    }
}
