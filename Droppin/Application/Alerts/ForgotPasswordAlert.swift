//
//  ForgotPasswordAlert.swift
//  Droppin
//
//  Created by Madson on 9/26/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class ForgotPasswordAlert: UIAlertController {

    private var completion: ((String) -> Void)?
    private var placeholder: String?

    convenience init(placeholder: String?) {
        let title = "Forgot password?"
        let message = "Enter your e-mail address below"
        self.init(title: title, message: message, preferredStyle: .alert)
        self.placeholder = placeholder
        setupAlert()
    }

    func present(from viewController: UIViewController, _ completion: @escaping ((String) -> Void)) {
        self.completion = completion
        viewController.present(self, animated: true, completion: nil)
    }
}

extension ForgotPasswordAlert {
    private func setupAlert() {
        self.addTextField {
            $0.text = self.placeholder?.trimmed
            $0.keyboardType = .emailAddress
        }
        self.addAction(self.cancelAction)
        self.addAction(self.sendAction)
    }

    private var cancelAction: UIAlertAction {
        return UIAlertAction(title: "Cancel", style: .cancel)
    }

    private var sendAction: UIAlertAction {
        return UIAlertAction(title: "Send", style: .default) { (action) in
            guard let email = self.textFields?.first?.text else { return }
            self.completion?(email)
        }
    }
}
