//
//  ConnectionApi.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum ConnectionEndpoints: String  {
    case create = "connection"
    case getAllByConnectorId = "connection/by-connector/:id"
    case getByConnecteeId = "connection/by-connectee/:id"
    case updateStatus = "connection/:id/status"
    case delete = "connection/:id"
}

protocol ConnectionApi {
    var api: DroppinApi {get}
    
    func createConnection(connecteeId: String, completion: @escaping (DIResult<ConnectionResponse>) -> ())
    func getConnection(byConnecteeId connecteeId: String, completion: @escaping (DIResult<ConnectionResponse>) -> ())
    func getConnections(byConnectorId connectorId: String, search: String?, completion: @escaping (DIResult<ConnectionResponse>) -> ())
    func updateStatus(forConnection connectionId: String, status: ConnectionStatus, completion: @escaping (DIResult<ConnectionResponse>) -> ())
    func delete(connection connectionId: String, completion: @escaping (DIResult<ConnectionResponse>) -> ())
}

extension ConnectionApi {
    
    func createConnection(connecteeId: String, completion: @escaping (DIResult<ConnectionResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let parameters: Parameters = ["connecteeId": connecteeId]
        let endpoint =  api.getEndpoint(ConnectionEndpoints.create.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "alamofire response")
            
            let diResult: DIResult<ConnectionResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func getConnection(byConnecteeId connecteeId: String, completion: @escaping (DIResult<ConnectionResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(ConnectionEndpoints.getByConnecteeId.rawValue.replacingOccurrences(of: ":id", with: connecteeId))
        
        Alamofire.request(endpoint, method: .get, encoding: URLEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "alamofire response")
            
            let diResult: DIResult<ConnectionResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func getConnections(byConnectorId connectorId: String, search: String?, completion: @escaping (DIResult<ConnectionResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(ConnectionEndpoints.getAllByConnectorId.rawValue.replacingOccurrences(of: ":id", with: connectorId))
        var parameters: Parameters = [:]
        
        if let search = search {
            parameters["search"] = search
        }
        
        Alamofire.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "alamofire response")
            
            let diResult: DIResult<ConnectionResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func updateStatus(forConnection connectionId: String, status: ConnectionStatus, completion: @escaping (DIResult<ConnectionResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(ConnectionEndpoints.updateStatus.rawValue.replacingOccurrences(of: ":id", with: connectionId))
        let params: Parameters = ["status": status.rawValue]
        
        Alamofire.request(endpoint, method: .put, parameters: params, encoding: JSONEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "alamofire response")
            
            let diResult: DIResult<ConnectionResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func delete(connection connectionId: String, completion: @escaping (DIResult<ConnectionResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(ConnectionEndpoints.delete.rawValue.replacingOccurrences(of: ":id", with: connectionId))
        
        Alamofire.request(endpoint, method: .delete, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "alamofire response")
            
            let diResult: DIResult<ConnectionResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
}
