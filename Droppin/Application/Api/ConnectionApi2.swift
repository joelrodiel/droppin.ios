//
//  ConnectionApi2.swift
//  Droppin
//
//  Created by Madson on 10/29/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire

typealias ConnectionsCompletion = (Error?, ConnectionResponse2?) -> Void

struct ConnectionApi2 {

    private static let endpoint = Config.env.baseURL + "/connection"

    static func getConnections(_ userId: String, completion: @escaping ConnectionsCompletion) {
        let url = endpoint + "/by-connector/" + userId
        let encoding = URLEncoding.default

        Alamofire.request(url, method: .get, parameters: nil, encoding: encoding).validate().response {
            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }

            do {
                let response = try JSONDecoder.shared.decode(ConnectionResponse2.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }
}

struct Connection2: Codable {
    var connectionId: String?
    var userOneId: User2?
    var userTwoId: User2?
    var actionUserId: String?
    var status: ConnectionStatus?
    var createdAt: Date?
}

struct ConnectionResponse2: Codable {
    var metaData: [Connection2]?
}
