//
//  ConversationApi.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Alamofire
import SwiftyJSON

enum ConversationEndpoints: String  {
    case conversation = "conversation"
    case findOrCreate = "conversation/by-receiver/:id"
}

protocol ConversationApi {
    var api: DroppinApi {get}
    
    func findOrCreateConversation(byReceiverId userId: String, completion: @escaping (DIResult<ConversationResponse>)->())
    func getConversations(completion: @escaping (DIResult<ConversationResponse>)->())
}

extension ConversationApi {
    
    func findOrCreateConversation(byReceiverId userId: String, completion: @escaping (DIResult<ConversationResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(ConversationEndpoints.findOrCreate.rawValue.replacingOccurrences(of: ":id", with: userId))
        
        Alamofire.request(endpoint, method: .get, encoding: URLEncoding.default).validate().responseJSON { response in
            let result: DIResult<ConversationResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func getConversations(completion: @escaping (DIResult<ConversationResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint =  api.getEndpoint(ConversationEndpoints.conversation.rawValue)
        
        Alamofire.request(endpoint, method: .get).validate().responseJSON { response in
            let result: DIResult<ConversationResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
}
