//
//  CoreApi.swift
//  Droppin
//
//  Created by Rene Reyes on 11/29/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// @TODO how to handle when the network is not avaialble: NetworkReachabilityManager

protocol CoreApi {
    var api: DroppinApi {get}
    func signIn(userId: String, password: String, completion: @escaping (DIResult<SignInRequestResult>)->())
    func logout(completion: @escaping (DIResult<RequestResult>)->())
}

enum CoreEndpoints: String {
    case signIn = "sign-in"
    case logout = "logout"
}

extension CoreApi {
    
    func signIn( userId: String, password: String,  completion: @escaping (DIResult<SignInRequestResult>)->()) {
        //        print("[DN][signIn] ->start")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        //        let defaults = UserDefaults.standard
        //        let token = defaults.string(forKey: "token") as! String
        
        var parameters: Parameters = [ "userId": userId, "password": password]
        if let token = UserDefaults.standard.string(forKey: UserPropertyKey.token.rawValue)  {
            parameters["token"] = token
        }
        
        let endpoint =  api.getEndpoint(CoreEndpoints.signIn.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Received"))
            let diResult: DIResult<SignInRequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func logout(completion: @escaping (DIResult<RequestResult>)->()) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        
        let token = UserDefaults.standard.string(forKey: UserPropertyKey.token.rawValue) ?? ""
        
        let parameters: Parameters = ["token": token]
        let endpoint =  api.getEndpoint(CoreEndpoints.logout.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Received"))
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
}
