//
//  DIResult.swift
//  Droppin
//
//  Created by Rene Reyes on 11/22/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

// Inspired by Alamofire Result
enum DIResult<T> {
    case success(T)
    case failure(T?, DiApiError)
    
//    var data: T {
//        switch self {
//        case .success(let data):
//            return data
//        case .failure(let data, let error):
//            print(error)
//            return data
//        }
//    }
    
//    var error: Error? {
//        switch self {
//        case .success:
//            return nil
//        case .failure( _ , let error):
//            return error
//        }
//    }
}
