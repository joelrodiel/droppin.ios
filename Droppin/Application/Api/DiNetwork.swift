import UIKit
import Alamofire
import SwiftyJSON

final class DiNetwork {
    
    
    static let api = DroppinApi()
//    static let forcedHideActivityIndicatorTimeInterval: TimeInterval = 30
    //    class func getScreenRect() -> CGRect {
    //        return UIScreen.mainScreen().bounds
    //    }
    
    //    class func verifyCodeLength() -> Int {
    //        return 4
    //    }
    
    class func authentication( sendToSignInIfNotAuthenticated error: DiApiError ) {
//        if let err = error as? DiApiError {
//        if let err = error  {
            if error == .dAuthenticationError  {
                print("401 error. Changing view to Intial View Controller")
//                let defaults = UserDefaults.standard
//                defaults.set(false, forKey: "userLoggedIn")
                UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
                UserDefaults.standard.removeObject(forKey: UserPropertyKey.isTester.rawValue)
                UserDefaults.standard.removeObject(forKey: UserPropertyKey.token.rawValue)
                let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                
                
            }
//        }
    }
    
    class func createDiResult<T:RequestResult>(_ response: DataResponse<Any> )  -> DIResult<T> {
        
        
        switch response.result {
        case .success(let data):
            let json = JSON(data)
            var loggableJson = JSON(data)
            
            //            if loggableJson["pins"].exists() {
            //                for (key, subJson) in loggableJson["pins"] {
            //                    if loggableJson["pins"][key]["photo"].exists() {
            //                        loggableJson["pins"][key]["photo"] = "removed"
            //                    }
            //                }
            //            }
            //            print("[PinApi][createDiResult][response] ->JSON: \(loggableJson)")
            
            do {
                let requestResult = try T.init(json)
                let diResult = DIResult.success( requestResult )
                
                return diResult
            } catch let error as DiApiError {
                let requestResult: T?  = nil
                let diResult = DIResult.failure(requestResult, error)
                return diResult
            } catch let error {
                let requestResult: T?  = nil
                let diResult = DIResult.failure(requestResult, DiApiError.dError(error))
                return diResult
            }
            
            
            
        case .failure(let afError):
//            api.authentication(sendTo18IfNotAuthenticated: afError) // Chage Droppin API to be DiApi with class variables
            
//            do {
            
                print("[DiNetwork][createDiResult][response] -> failure: " + afError.localizedDescription)
                let json = JSON(response.data as Any)
                guard let requestResult = try? T.self.init(json) else {
                    let rr: T?  = nil
                    let diResult = DIResult.failure(rr, DiApiError.dAlamofireError(afError))
                    
                    return diResult
                }
            
                let errorCode = requestResult.errorCode ?? DiApiError.dNoApiErrorFound
                let diResult = DIResult.failure(requestResult, errorCode)
                return diResult
                
//                if let err = afError as? AFError {
//                    switch err {
//                    case .responseValidationFailed(let reason):
//                        switch reason {
//                        case .unacceptableStatusCode(let code):
//                            if code == 401 {
//                                print("401 error. Changing view to Intial View Controller")
//                                let defaults = UserDefaults.standard
//                                defaults.set(false, forKey: "userLoggedIn")
//                                UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
//                                UserDefaults.standard.removeObject(forKey: UserPropertyKey.isTester.rawValue)
//            UserDefaults.standard.removeObject(forKey: UserPropertyKey.token.rawValue)
//                                let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
//                                appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
//                            }
//                        default:
//                            print("reason defautl")
//                        }
//                    default:
//                        print("err default")
//                    }
//                }
                
                
//                print("[PinApi][createDiResult][response] -> error: " + afError.localizedDescription)
//                let json = JSON(response.data as Any)
//                let requestResult = try T.self.init(json)
                
                
                
//            } catch {
//                let requestResult: T?  = nil
//                let diResult = DIResult.failure(requestResult, DiApiError.dAlamofireError(afError))
//                return diResult
//            }
        }
    }
}
