import Foundation
import Alamofire
import SwiftyJSON

enum DiNotificationEndpoints: String  {
    case getNotifications = "notification"
    case updateStatus = "notification/:id/status"
    case readNotifications = "notification/read"
}

protocol DiNotificationApi {
    var api: DroppinApi {get}
    
    func getNotifications(type: DiNotificationType?, completion: @escaping (DIResult<DiNotificationsApiResponse>) -> ())
    func updateNotification(notificationId: String, status: DiNotificationStatus, completion: @escaping (DIResult<RequestResult>) -> ())
    func readNotifications(by type: DiNotificationType?, completion: @escaping (DIResult<RequestResult>) -> ())
}

extension DiNotificationApi {
    
    func getNotifications(type: DiNotificationType?, completion: @escaping (DIResult<DiNotificationsApiResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "getNotifications")
        
        let endpoint =  api.getEndpoint(DiNotificationEndpoints.getNotifications.rawValue)
        
        var parameters: Parameters?
        
        if let type = type, type != .all {
            parameters = ["type": type.rawValue]
        }
        
        Alamofire.request(endpoint, method: .get, parameters: parameters).validate().responseJSON { response in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
            
            let diResult: DIResult<DiNotificationsApiResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func updateNotification(notificationId: String, status: DiNotificationStatus, completion: @escaping (DIResult<RequestResult>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(DiNotificationEndpoints.updateStatus.rawValue.replacingOccurrences(of: ":id", with: notificationId))
        let parameters: Parameters = ["status": status.rawValue]
        
        Alamofire.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func readNotifications(by type: DiNotificationType? = nil, completion: @escaping (DIResult<RequestResult>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(DiNotificationEndpoints.readNotifications.rawValue)
        var parameters: Parameters?
        
        if let type = type {
            parameters = ["type": type.rawValue]
        }
        
        Alamofire.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
}
