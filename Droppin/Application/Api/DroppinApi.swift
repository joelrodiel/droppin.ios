//
//  DroppinNetwork.swift
//  Droppin
//
//  Created by Rene Reyes on 10/13/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

enum StatusCode: String, CustomStringConvertible {
    case dOperationSuccess = "dOperationSuccess"
    case dOperationFailure = "dOperationFailure"
    case dNoPinsFound = "dNoPinsFound"
    case dError = "dError"
    
    var description: String {
        switch self{
        case .dOperationSuccess: return "Operation Succes"
        case .dOperationFailure: return "Operation Failure"
        case .dNoPinsFound: return "No Pins Found"
        case .dError: return "Internal Error"
        }
    }
}

// @TODO this should go in some other file!
enum DiError: Error, CustomStringConvertible {
    case GeoCodeError
    case InvalidJsonPin(String)
    case InvalidEndpoint
    
    var description: String {
        switch self{
        case  .GeoCodeError: return "There was a problem getting the address"
        case .InvalidJsonPin(let message): return "Error: \(message)"
        case  .InvalidEndpoint: return "No endpoint available"
        }
    }
}
//case .dNoKeyFoundInApiResponse(let key): return "Key not found in Api Response body: \(key)"

public struct DiCustomError: Error {
    let error: String
}

extension DiCustomError: LocalizedError {
    public var errorDescription: String? {
        return NSLocalizedString(error, comment: "")
    }
}

// case dNoKeyFoundInApiResponse(String)
//case dAlamoFireError(Error)

enum DiApiError: Error, CustomStringConvertible {
    case dMissingUrl
    case dAuthenticationError
    case dSignInError
    case dBadMongo
    case dInvalidData
    case dExpiredData
    case dUserNotFound
    case dValidationError
    case dNotAbleToCompleteAction
    case dMissingParameters
    case dAccountAlreadyExists
    case dNoApiErrorFound
    case dAlamofireError(Error)
    case dError(Error)
    case dNoKeyFoundInApiResponse(String)
    case dErrorConvertingToProperType(String)
    case dEmailAlreadyExists
    case dMobileAlreadyExists

    var description: String {
        switch self{
        case  .dMissingUrl: return "the url is missing"
        case .dAuthenticationError: return "Please make sure you entered the correct credentials."
        case .dSignInError: return "Please make sure you entered the correct name and password"
        case .dBadMongo: return "Internal error. code 1403"
        case .dInvalidData: return "Internal error. code 1404"
        case .dExpiredData: return "Expired data."
        case .dUserNotFound: return "User not found. code 1405"
        case .dValidationError: return "Validation error. code 1406"
        case .dNotAbleToCompleteAction: return "Internal error. code 1407"
        case .dMissingParameters: return "API error. code 1408"
        case .dAccountAlreadyExists: return "Account already exists. code 1409"
        case .dNoApiErrorFound: return "The api server sent an error with no error code"
        case .dAlamofireError(let error): return "Alamofire threw an error: \(error)"
        case .dError(let error): return "Alamofire threw an error: \(error)"
        case .dNoKeyFoundInApiResponse(let key): return "Key was not found in api response body \(key)"
        case .dErrorConvertingToProperType(let description): return description
        case .dMobileAlreadyExists: return "Email already exists."
        case .dEmailAlreadyExists: return "Email already exists."
        }
    }
    
}

// Theses are server errors
extension DiApiError: RawRepresentable {
    typealias RawValue = String
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case "dMissingUrl": self = .dMissingUrl
        case "dMissingParameters": self = .dMissingParameters
        case "dAuthenticationError": self = .dAuthenticationError
        case "dSignInError": self = .dSignInError
        case "dBadMongo": self = .dBadMongo
        case "dInvalidData": self = .dInvalidData
        case "dExpiredData": self = .dExpiredData
        case "dUserNotFound": self = .dUserNotFound
        case "dValidationError": self = .dValidationError
        case "dNotAbleToCompleteAction": self = .dNotAbleToCompleteAction
        case "dAccountAlreadyExists": self = .dAccountAlreadyExists
        case "dEmailAlreadyExists": self = .dEmailAlreadyExists
        case "dMobileAlreadyExists": self = .dMobileAlreadyExists
        default: self = .dNoApiErrorFound
            
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .dMissingUrl: return "dMissingUrl"
        case .dMissingParameters: return "dMissingParameters"
        case .dAuthenticationError: return "dAuthenticationError"
        case .dSignInError: return "dSignInError"
        case .dBadMongo: return "dBadMongo"
        case .dInvalidData: return "dInvalidData"
        case .dExpiredData: return "dExpiredData"
        case .dUserNotFound: return "dUserNotFound"
        case .dValidationError: return "dValidationError"
        case .dNotAbleToCompleteAction: return "dNotAbleToCompleteAction"
        case .dAccountAlreadyExists: return "dAccountAlreadyExists"
        case .dNoApiErrorFound: return "dNoApiErrorFound"
        case .dAlamofireError(let error): return "dAlamofireError"
        case .dError(let error): return "dError"
        case .dNoKeyFoundInApiResponse(let key): return "dNoKeyFoundInApiResponse"
        case .dErrorConvertingToProperType(let description): return "dErrorConvertingToProperType"
        case .dEmailAlreadyExists: return "dEmailAlreadyExists"
        case .dMobileAlreadyExists: return "dMobileAlreadyExists"
        }
    }
}

protocol DroppinApiProtocol {
    func getEndpoint (_ operation: String)  -> String
    func getStatusCode(statusCode:String)->StatusCode?
    func authentication( sendToSignInIfNotAuthenticated error: Error )
//    func lineDescription( of obj: Any, andFunction funcName: String, withDescription desc: String) -> String
}

struct DroppinApi: DroppinApiProtocol  {
    static let shared = DroppinApi()
    
    func authentication( sendToSignInIfNotAuthenticated error: Error ) {
        if let err = error as? AFError {
            switch err {
            case .responseValidationFailed(let reason):
                switch reason {
                case .unacceptableStatusCode(let code):
                    if code == 401 {
                        print("401 error. Changing view to Intial View Controller")
//                        let defaults = UserDefaults.standard
//                        defaults.set(false, forKey: "userLoggedIn")
                        UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
                        UserDefaults.standard.removeObject(forKey: UserPropertyKey.isTester.rawValue)
                        UserDefaults.standard.removeObject(forKey: UserPropertyKey.token.rawValue)
                        let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                        appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                    }
                default:
                    print("reason defautl")
                }
            default:
                print("err default")
            }
        }
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getEndpoint (_ operation: String)  -> String {
        let endpoint = "\(Config.env.baseURL)/\(operation)"
        print("getEndpoint -> \(endpoint)")
        
        return endpoint
    }

//    func createRequestResult(jsonResult:JSON)->RequestResult {
//        return RequestResult(
//            statusCode: self.getStatusCode(statusCode: jsonResult["statusCode"].stringValue),
//            message: jsonResult["message"].stringValue
//        )
//    }
    
    // Why do I need this?! Remove iT!
    func getStatusCode(statusCode:String)->StatusCode? {
        switch statusCode {
        case "dOperationSuccess":
            return StatusCode.dOperationSuccess
        case "dOperationFailure":
            return StatusCode.dOperationFailure
        case "dError":
            return StatusCode.dError
        default:
            return nil
        }
    }
    
}

//struct StandardRequestResult: RequestResult {
//    let statusCode: StatusCode?
//    let errorCode: ErrorCode?
//    let message: String?
//    let metaData:  [String:String]?
//
//    init(statusCode: StatusCode?, message: String?){
//        metaData = nil
//        self.errorCode = nil
//        self.statusCode = statusCode
//        self.message = message
//    }
//
//    init(statusCode: StatusCode?, message: String?, metaData:  [String:String]?){
//        self.metaData = metaData
//        self.errorCode = nil
//        self.statusCode = statusCode
//        self.message = message
//    }
//
//    init(_ json: JSON){
//
//        if let statusCode =  json["statusCode"].string {
//            self.statusCode = StatusCode(rawValue: statusCode)
//        } else {
//            self.statusCode = nil
//        }
//
//        if let errorCode =  json["errorCode"].int {
//            self.errorCode = ErrorCode(rawValue: errorCode)
//        } else {
//            self.errorCode = nil
//        }
//
//        if let message = json["message"].string {
//            self.message = message
//        } else {
//            self.message = nil
//        }
//
//        if json["metaData"].exists() {
//            self.metaData = json["metaData"].reduce([String:String](), { (result: [String:String], item: Any) -> [String:String] in
//                let (key, value) = item as! (String, JSON)
//                var out = result
//                out[key] = value.stringValue
//                return out
//            })
//        } else {
//            self.metaData = nil
//        }
//    }
//}



