//
//  GenericResult.swift
//  Droppin
//
//  Created by Madson on 9/28/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

struct GenericResult: Codable {
    let httpStatusCode: Int?
    let message: String?
    let errorCode: String?
    let statusCode: String?
}
