//
//  ImagesRequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/19/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

class ImagesRequestResult: Codable {
    var statusCode: String?
    var message: String?
    var httpStatusCode: Int?
    var metaData: [[String : String]]?
}
