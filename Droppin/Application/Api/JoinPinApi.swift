import Foundation
import Alamofire
import SwiftyJSON

enum JoinPinEndpoints: String  {
    case requestToJoin = "request-to-join"
}

protocol JoinPinApi {
    var api: DroppinApi {get}
    func requestToJoin(pinId: String?, completion: @escaping (DIResult<JoinPinResponse>)->())
}

extension JoinPinApi {
    func requestToJoin(pinId: String?, completion: @escaping (DIResult<JoinPinResponse>)->()) {
        guard let pinId = pinId else { return }

        print("[JoinPinApi][requestToJoin] ->")
        let parameters: Parameters = [ "pinId": pinId ]
        let endpoint =  api.getEndpoint(JoinPinEndpoints.requestToJoin.rawValue)

        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[JoinPinApi][requestToJoin] -> alamofire response")

            let diResult: DIResult<JoinPinResponse> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }

            completion(diResult)
        }
    }
}
