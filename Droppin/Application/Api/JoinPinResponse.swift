import Foundation
import SwiftyJSON

class JoinPinResponse: RequestResult {
    let isFirstTimeRequest: Bool?

    required  init(_ json: JSON ) throws {
        isFirstTimeRequest = RequestResult.extractBooleanParameterFromApiJsonResponse(returnNilIfNone: json, name: "isFirstTimeRequest")
        try super.init(json)
    }
}
