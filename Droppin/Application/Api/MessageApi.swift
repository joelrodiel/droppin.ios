//
//  MessageApi.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Alamofire
import SwiftyJSON

enum MessageEndpoints: String  {
    case message = "message"
    case getAllByConversationId = "message/by-conversation/:id"
}

protocol MessageApi {
    var api: DroppinApi {get}
    var pagination: ApiPagination {get}
    
    func getMessages(byConversationId conversationId: String, completion: @escaping (DIResult<MessageResponse>)->())
}

extension MessageApi {
    
    func getMessages(byConversationId conversationId: String, completion: @escaping (DIResult<MessageResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let params: Parameters = ["page": pagination.nextPage]
        let endpoint = api.getEndpoint(MessageEndpoints.getAllByConversationId.rawValue.replacingOccurrences(of: ":id", with: conversationId))
        
        Alamofire.request(endpoint, method: .get, parameters: params, encoding: URLEncoding.default).validate().responseJSON { response in
            let result: DIResult<MessageResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func sendMessage(byReceiverId receiverId: String, message: String, completion: @escaping (DIResult<MessageResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let params: Parameters = ["message": message, receiverId: receiverId]
        let endpoint = api.getEndpoint(MessageEndpoints.message.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: params, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<MessageResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func sendMessage(byConversationId conversationId: String, message: String, completion: @escaping (DIResult<MessageResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let params: Parameters = ["message": message, "conversationId": conversationId]
        let endpoint = api.getEndpoint(MessageEndpoints.message.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: params, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<MessageResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
}
