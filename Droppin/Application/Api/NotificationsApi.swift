//
//  NotificationsApi.swift
//  Droppin
//
//  Created by Madson on 11/11/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire

struct NotificationsApi {

    typealias NotificationsCompletion = (Error?, NotificationsResponse?) -> Void
    private static let endpoint = Config.env.baseURL + "/notification"

    static func getNotifications(_ type: NotificationType?, completion: @escaping NotificationsCompletion) {
        var params: Parameters = [:]

        if let type = type, type != .all {
            params["type"] = type.rawValue
        }

        let json = URLEncoding.default

        Alamofire.request(endpoint, method: .get, parameters: params, encoding: json).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }

            do {
                let response = try JSONDecoder.shared.decode(NotificationsResponse.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }
}

struct NotificationsResponse: Codable {
    var notifications: [Notification2]?
}

struct Notification2: Codable {
    var notificationId: String?
    var body: String?
    var title: String?
    var picture: String?
    var type: NotificationType?
    var status: NotificationStatus?
    var action: NotificationAction?
    var createdAt: Date?
    var objectId: String?
    var senderId: String?
}

enum NotificationStatus: String, Codable {
    case new, read
}

enum NotificationType: String, Codable {
    case connection, all, message, pinInvitation, attendeeArrival
}

enum NotificationAction: String, Codable {
    case sender, list, detail, pinDetail
}
