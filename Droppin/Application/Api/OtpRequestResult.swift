//
//  OtpRequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/19/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

class OtpRequestResult: RequestResult {
    let otp: Int
    
    required  init(_ json: JSON ) throws {
        otp = try RequestResult.extractNumberParameterFromApiJsonResponse(json: json, name: "otp")
        try super.init(json)
    }
    
}
