//
//  PasswordResetEndpoint.swift
//  Droppin
//
//  Created by Madson on 9/27/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire

struct PasswordError: Codable {
    var httpStatusCode: Int?
    var message: String?
}

struct PasswordResetEndpoint {
    private static let path = "password-reset"

    static func post(_ email: String, completion: @escaping (PasswordError?, Any?) -> Void) {

        let url = DroppinApi().getEndpoint(path)
        let params = ["email" : email]

        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).validate().responseJSON {
            if $0.error != nil, let data = $0.data {
                do {
                    let error = try JSONDecoder.shared.decode(PasswordError.self, from: data)
                    return completion(error, nil)
                } catch {
                    let error = PasswordError(httpStatusCode: 0, message: "No error defined")
                    return completion(error, nil)
                }
            }
            
            completion(nil, $0.value)
        }
    }
}
