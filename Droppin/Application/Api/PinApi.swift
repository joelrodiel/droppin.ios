import Foundation
import Alamofire
import SwiftyJSON
import MapKit

enum PinEndpoints: String  {
    case createPin = "pin/create"
    case uploadPinImage = "pin/update/image" // uses POST
    case updatePinAddress = "pin/update/address"
    case updatePinStartDate = "pin/update/start-date"
    case updatePinEndDate = "pin/update/end-date"
    case updateExtraDirections = "pin/update/extra-directions"
    case activatePin =  "pin/activate"
    case terminatePin = "pin/:id/terminate"
    case getNear = "pins/full-details"
    case getPinDetail = "pin/details"
    case getPinsDetail = "pins/details"
    case getPinsSummary = "pins/summaries-only"
    case getHostPins = "pins/by-host"
    case getPinStatistic = "pin/:id/statistic"
    case getFirstAttendees = "pin/:id/first-attendees"
}

protocol PinApi {
    var api: DroppinApi {get}
    func createPin(title:String, description:String, completion: @escaping (DIResult<PinRequestResult>)->())
    func uploadPinImage( pinId: String ,image:UIImage, completion: @escaping (Error?, HTTPURLResponse?, String?) -> ())
    func updatePin(pinId: String, street: String, city: String, state: String, zip: String, coordinate: CLLocationCoordinate2D,completion: @escaping (DIResult<RequestResult>)->())
    func updatePin(pinId: String, startDate: Date, completion: @escaping (DIResult<RequestResult>) -> ())
    func updatePin(pinId: String, endDate: Date, completion: @escaping (DIResult<RequestResult>) -> ())
    func updatePin(pinId: String, extraDirections: String, completion: @escaping (DIResult<RequestResult>) -> ())
    func activatePin(for pinId: String, completion: @escaping (DIResult<RequestResult>) -> ())
    func terminatePin(pinId: String, completion: @escaping (DIResult<RequestResult>) -> ())
    func getHostPins(sort: String?, isForNotifications: Bool?, completion: @escaping (DIResult<PinsDetailRequestResult>) -> ())
    func getPinsSummary(nearMe location: CLLocation, withinRange miles: RangeEnumeration, withSearchString searchString: String? ,completion: @escaping (DIResult<PinsSummaryRequestResult>) -> ())
    func getPinsDetail(nearMe location: CLLocation, withinRange miles: RangeEnumeration, withSearchString searchString: String? ,completion: @escaping (DIResult<PinsDetailRequestResult>) -> ())
    func getPinDetail( pinId: String, completion: @escaping (DIResult<PinDetailRequestResult>) -> ())
    func getPinStatistic(_ pinId: String?, completion: @escaping (DIResult<PinStatisticResult>) -> ())
    func getFirstAttendees(pinId: String, completion: @escaping (DIResult<PinRequestResponse>) -> ())
}

extension PinApi {

    func createPin(title:String, description:String, completion: @escaping (DIResult<PinRequestResult>)->()) {
        print("[PinApi][createPin] ->")
        let parameters: Parameters = [ "title": title,"description": description]
        let endpoint =  api.getEndpoint(PinEndpoints.createPin.rawValue)
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[PinApi][createPin] -> alamofire response")
            let diResult: DIResult<PinRequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func uploadPinImage( pinId: String, image: UIImage, completion: @escaping (Error?, HTTPURLResponse?, String?) -> ())  {
        debugPrint("[PinApi][uploadPinImage] -> pinId: \(pinId)")

        guard
            let resizedImage = image.resized(toWidth: 540)
            else {
                debugPrint("[UserApi][uploadProfileImage] -> image is nil")
                
                let error = DiCustomError(error: "Image is null")
                return completion(error, nil, nil)
        }

        let endpoint = api.getEndpoint(PinEndpoints.uploadPinImage.rawValue)

        CDNManager.shared.upload(resizedImage, preset: .pinPhotos) { error, publicId in
            guard error == nil else {
                debugPrint("[PinApi][uploadPinImage] -> CDN upload error \(error!)")
                
                return completion(error, nil, nil)
            }

            guard let publicId = publicId else {
                debugPrint("[PinApi][uploadPinImage] -> Missing image publicId")
                
                let error = DiCustomError(error: "Missing image publicId")
                return completion(error, nil, nil)
            }

            let encoding = JSONEncoding.default
            let params: Parameters = [ "pinId": pinId, "pinPic": publicId ]

            Alamofire.request(endpoint, method: .post, parameters: params, encoding: encoding).validate().response {
                debugPrint("[PinApi][uploadPinImage] -> alamofire response")
                completion($0.error, $0.response, publicId)
            }
        }
    }
    
    func updatePin(pinId: String, street: String, city: String, state: String, zip: String, coordinate: CLLocationCoordinate2D ,completion: @escaping (DIResult<RequestResult>) -> ()  ) {
        print("[PinApi][updatePin] ->")
        
        let parameters: Parameters = [ "pinId": pinId, "street": street,"city": city, "state": state, "zip": zip, "longitude": coordinate.longitude, "latitude": coordinate.latitude]
        let endpoint =  api.getEndpoint(PinEndpoints.updatePinAddress.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[PinApi][updatePin] -> alamofire response")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func updatePin(pinId: String, startDate: Date, endDate: Date, completion: @escaping (DIResult<RequestResult>) -> ()  ) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let formatter = ISO8601DateFormatter()
        let parameters: Parameters = [ "pinId": pinId, "startDate": formatter.string(from: startDate), "endDate": formatter.string(from: endDate)]
        let endpoint =  api.getEndpoint(PinEndpoints.updatePinStartDate.rawValue)
        
        Alamofire.request(endpoint, method: .post,parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Alamofire response")
            
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(diResult)
        }
    }
    
    func updatePin(pinId: String, startDate: Date, completion: @escaping (DIResult<RequestResult>) -> ()  ) {
        print("[PinApi][updatePin] ->startDate")
        let formatter = ISO8601DateFormatter()
        let parameters: Parameters = [ "pinId": pinId, "startDate": formatter.string(from: startDate)]
        let endpoint =  api.getEndpoint(PinEndpoints.updatePinStartDate.rawValue)
        Alamofire.request(endpoint, method: .post,parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[PinApi][updatePin] -> alamofire response")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func updatePin(pinId: String, endDate: Date, completion: @escaping (DIResult<RequestResult>) -> ()  ) {
        print("[PinApi][updatePin] ->endDate")
        let formatter = ISO8601DateFormatter()
        let parameters: Parameters = [ "pinId": pinId, "endDate": formatter.string(from: endDate)]
        let endpoint =  api.getEndpoint(PinEndpoints.updatePinEndDate.rawValue)
        Alamofire.request(endpoint, method: .post,parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[PinApi][updatePin] -> alamofire response")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func updatePin(pinId: String, extraDirections: String, completion: @escaping (DIResult<RequestResult>) -> ()) {
        print("[PinApi][updatePin] -> extraDirections")
        
        let parameters: Parameters = [ "pinId": pinId, "extraDirections": extraDirections]
        let endpoint =  api.getEndpoint(PinEndpoints.updateExtraDirections.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[PinApi][updatePin] -> alamofire response")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func activatePin(for pinId: String, completion: @escaping (DIResult<RequestResult>) -> ()  ) {
        //        print("[PinApi][updatePin] ->isActive")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        let parameters: Parameters = [ "pinId": pinId ]
        let endpoint =  api.getEndpoint(PinEndpoints.activatePin.rawValue)
        Alamofire.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            //            print("[PinApi][activatePin] -> alamofire response")
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: " Alamofire Response"))
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func terminatePin(pinId: String, completion: @escaping (DIResult<RequestResult>) -> ()  ) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "START")
        
        let endpoint =  api.getEndpoint(PinEndpoints.terminatePin.rawValue.replacingOccurrences(of: ":id", with: pinId))
        
        Alamofire.request(endpoint, method: .put, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: " Alamofire Response"))
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func getHostPins(sort: String? = nil, isForNotifications: Bool? = nil, completion: @escaping (DIResult<PinsDetailRequestResult>) -> ()  ) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint =  api.getEndpoint(PinEndpoints.getHostPins.rawValue)
        
        var params: Parameters = [:]
        
        if let sort = sort {
            params["sort"] = sort
        }
        
        if let isForNotifications = isForNotifications {
            params["isForNotifications"] = isForNotifications
        }
        
        Alamofire.request(endpoint, method: .get, parameters: params, encoding: URLEncoding.default).validate().responseJSON { response in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
            let diResult: DIResult<PinsDetailRequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func getPinsSummary( nearMe location: CLLocation, withinRange miles: RangeEnumeration, withSearchString searchString: String? ,completion: @escaping (DIResult<PinsSummaryRequestResult>) -> ()) {
        let endpoint =  api.getEndpoint(PinEndpoints.getPinsSummary.rawValue)
        print("[PinApi][getPinsSummary] ->Start")
        var parameters: Parameters = [
            "longitude": location.coordinate.longitude,
            "latitude": location.coordinate.latitude,
            "range": miles.rawValue
        ]
        if let search = searchString {
            parameters["searchString"] = search
        }

        Alamofire.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            
            let diResult: DIResult<PinsSummaryRequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
            }
    }
    
    func getPinsDetail( nearMe location: CLLocation, withinRange miles: RangeEnumeration, withSearchString searchString: String?  , completion: @escaping (DIResult<PinsDetailRequestResult>) -> ()) {
        let endpoint =  api.getEndpoint(PinEndpoints.getPinsDetail.rawValue)
        print("[PinApi][getPinsDetail] ->Start")
        let parameters: Parameters = [ "longitude": location.coordinate.longitude, "latitude": location.coordinate.latitude, "range": miles.rawValue]
        //        let parameters: Parameters = [ "longitude": location.coordinate.longitude]
        
        Alamofire.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            print("[PinApi][getPinsDetail] ->Response")
            let diResult: DIResult<PinsDetailRequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func getPinDetail(_ pinId: String, completion: @escaping (PinResponse?, Error?) -> ()) {
        let url =  api.getEndpoint(PinEndpoints.getPinDetail.rawValue)
        let parameters: Parameters = ["pinId" : pinId]
        let encoding = URLEncoding.default
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: encoding).validate().response {
            guard let data = $0.data else {
                completion(nil, $0.error)
                return
            }
            
            do {
                let pinResponse = try JSONDecoder.shared.decode(PinResponse.self, from: data)
                completion(pinResponse, nil)
            } catch {
                completion(nil, error)
            }
        }
    }

    func getPinDetail( pinId: String, completion: @escaping (DIResult<PinDetailRequestResult>) -> ()) {
        let endpoint =  api.getEndpoint(PinEndpoints.getPinDetail.rawValue)
//        print("[PinApi][getPinDetail] ->Start")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        let parameters: Parameters = [ "pinId": pinId]
        
        Alamofire.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON { response in
//            print("[PinApi][getPinDetail] -> Response")
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Response"))
            let diResult: DIResult<PinDetailRequestResult> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }

    func getPinStatistic(_ pinId: String?, completion: @escaping (DIResult<PinStatisticResult>) -> ()) {
        print("[PinApi][getPinStatistic] -> Start")

        guard let pinId = pinId else {
            debugPrint("PinApi - getPinStatistic - pinId is nil")
            return
        }

        let endpoint = api.getEndpoint(PinEndpoints.getPinStatistic.rawValue.replacingOccurrences(of: ":id", with: pinId))
        
        Alamofire.request(endpoint).responseJSON { response in
            print("[PinApi][getPinStatistic] -> Api Response received")
            let result: DIResult<PinStatisticResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func getFirstAttendees(pinId: String, completion: @escaping (DIResult<PinRequestResponse>) -> ()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint =  api.getEndpoint(PinEndpoints.getFirstAttendees.rawValue.replacingOccurrences(of: ":id", with: pinId))
        
        Alamofire.request(endpoint, method: .get, parameters: nil, encoding: URLEncoding.default).validate().responseJSON { response in
            let result: DIResult<PinRequestResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
}
