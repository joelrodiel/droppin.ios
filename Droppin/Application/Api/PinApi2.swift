//
//  PinApi2.swift
//  Droppin
//
//  Created by Madson on 10/22/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire

struct PinApi2 {

    private static let endpoint = Config.env.baseURL + "/pin"

    static func update(_ pin: Pin2, completion: @escaping (Error?, HTTPURLResponse?) -> Void) {

        guard let pinId = pin.pinId else {
            debugPrint("PinApi2 - update - pinId is nil")
            return
        }

        guard let params = pin.toJSONDictionary else {
            debugPrint("PinApi2 - update - pin toJSONDictionary error")
            return
        }

        let url = "\(endpoint)/\(pinId)/update"
        let encoding = JSONEncoding.default

        Alamofire.request(url, method: .put, parameters: params, encoding: encoding).validate().response {
            debugPrint("PinApi2 - update - Alamofire response")
            completion($0.error, $0.response)
        }
    }

}
