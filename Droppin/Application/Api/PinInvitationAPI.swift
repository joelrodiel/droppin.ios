//
//  PinInvitationAPI.swift
//  Droppin
//
//  Created by Madson on 10/31/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire

typealias PinInvitationInviteCompletion = (Error?, GenericResult?) -> Void

struct PinInvitationAPI {

    private static let endpoint = Config.env.baseURL + "/pin-invitation"

    static func invite(usersIds: [String], to pinId: String, completion: @escaping PinInvitationInviteCompletion) {
        let encoding = JSONEncoding.default

        let params: [String : Any] = [
            "pinId" : pinId,
            "userIds" : usersIds
        ]

        Alamofire.request(endpoint, method: .post, parameters: params, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }

            do {
                let response = try JSONDecoder.shared.decode(GenericResult.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }
}
