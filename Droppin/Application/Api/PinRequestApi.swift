//
//  PinRequestApi.swift
//  Droppin
//
//  Created by Isaias Garcia on 08/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

//import Foundation
import Alamofire
import SwiftyJSON

enum PinRequestEndpoints: String  {
    case request = "request"
    case leavePin = "request/:id/leave"
    case arrivePin = "request/:id/arrive"
    case acceptRequest = "request/:id/accept"
    case declineRequest = "request/:id/decline"
    case undoRequest = "request/:id/undo"
    case getAttendeePins = "request/by-attendee/:id"
    case deleteRequest = "request/by-pin/:id"
    case getByPinId = "request/by-pin/:id/"
    case getCurrent = "request/current"
}

protocol PinRequestApi {
    var api: DroppinApi {get}
    func getPinRequests(byPinId pinId: String, status: PinRequestStatus?, type: PinRequestType?, limit: Int?, sort: String?, search: String?, completion: @escaping (DIResult<PinRequestResponse>)->())
    func leavePin(requestId: String, completion: @escaping (DIResult<RequestResult>)->())
    func arrivePin(requestId: String, completion: @escaping (DIResult<RequestResult>)->())
    func acceptRequest(requestId: String, force: Bool, completion: @escaping (DIResult<RequestResult>)->())
    func declineRequest(requestId: String, completion: @escaping (DIResult<RequestResult>)->())
    func undoRequest(requestId: String, completion: @escaping (DIResult<RequestResult>)->())
    func getAttendeeRequests(completion: @escaping (DIResult<PinRequestResponse>) -> ())
    func deleteRequest(byPinId pinId: String, completion: @escaping (DIResult<RequestResult>)->())
    func getPinRequest(byPinId pinId: String?, completion: @escaping (DIResult<PinRequestResponse>)->())
    func getCurrentPinRequest(completion: @escaping (DIResult<RequestResult>)->())
}

extension PinRequestApi {
    
    func getPinRequests(byPinId pinId: String, status: PinRequestStatus? = nil, type: PinRequestType? = nil, limit: Int? = nil, sort: String? = nil, search: String? = nil, completion: @escaping (DIResult<PinRequestResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint =  api.getEndpoint(PinRequestEndpoints.request.rawValue)
        var params: Parameters = ["pinId": pinId]
        
        if let status = status {
            params["status"] = status.rawValue
        }

        if let type = type {
            params["type"] = type.rawValue
        }

        if let limit = limit {
            params["limit"] = limit
        }

        if let sort = sort {
            params["sort"] = sort
        }
        
        if let search = search {
            params["search"] = search
        }
        
        Alamofire.request(endpoint, method: .get, parameters: params, encoding: URLEncoding.default).validate().responseJSON { response in
            let result: DIResult<PinRequestResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func leavePin(requestId: String, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(PinRequestEndpoints.leavePin.rawValue.replacingOccurrences(of: ":id", with: requestId))
        
        Alamofire.request(endpoint, method: .put, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func arrivePin(requestId: String, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(PinRequestEndpoints.arrivePin.rawValue.replacingOccurrences(of: ":id", with: requestId))
        
        Alamofire.request(endpoint, method: .put, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func acceptRequest(requestId: String, force: Bool, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        let endpoint = api.getEndpoint(PinRequestEndpoints.acceptRequest.rawValue.replacingOccurrences(of: ":id", with: requestId))
        let parameters: Parameters = ["force": force]

        Alamofire.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)

            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }

            completion(result)
        }
    }

    func declineRequest(requestId: String, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        let endpoint = api.getEndpoint(PinRequestEndpoints.declineRequest.rawValue.replacingOccurrences(of: ":id", with: requestId))

        Alamofire.request(endpoint, method: .put, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)

            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }

            completion(result)
        }
    }
    
    func undoRequest(requestId: String, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        let endpoint = api.getEndpoint(PinRequestEndpoints.undoRequest.rawValue.replacingOccurrences(of: ":id", with: requestId))

        Alamofire.request(endpoint, method: .put, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)

            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }

            completion(result)
        }
    }
    
    func getAttendeeRequests( completion: @escaping (DIResult<PinRequestResponse>) -> ()  ) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let userId = UserDefaults.standard.string(forKey: UserPropertyKey.currentUserId.rawValue) ?? ""
        let endpoint = api.getEndpoint(PinRequestEndpoints.getAttendeePins.rawValue.replacingOccurrences(of: ":id", with: userId))
        
        Alamofire.request(endpoint, method: .get, encoding: JSONEncoding.default).validate().responseJSON { response in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
            let diResult: DIResult<PinRequestResponse> = DiNetwork.createDiResult(response)
            if case .failure( _, let error) = diResult {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            completion(diResult)
        }
    }
    
    func deleteRequest(byPinId pinId: String, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(PinRequestEndpoints.deleteRequest.rawValue.replacingOccurrences(of: ":id", with: pinId))
        
        Alamofire.request(endpoint, method: .delete, parameters: nil, encoding: URLEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func getPinRequest(byPinId pinId: String?, completion: @escaping (DIResult<PinRequestResponse>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let pinId = pinId else {
            debugPrint("PinRequestApi - getPinRequest - pinId is nil")
            return
        }

        let endpoint = api.getEndpoint(PinRequestEndpoints.getByPinId.rawValue.replacingOccurrences(of: ":id", with: pinId))
        
        Alamofire.request(endpoint, method: .get, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<PinRequestResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func getCurrentPinRequest(completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(PinRequestEndpoints.getCurrent.rawValue)

        Alamofire.request(endpoint, method: .get, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
}
