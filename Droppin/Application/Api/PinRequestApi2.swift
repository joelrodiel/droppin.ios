//
//  PinRequestApi2.swift
//  Droppin
//
//  Created by Madson on 10/27/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire

typealias RequestsCompletion = (Error?, PinRequestsResponse2?) -> Void
typealias RequestCompletion = (Error?, PinRequestResponse2?) -> Void
typealias GenericCompletion = (Error?, GenericResult?) -> Void

struct PinRequestApi2 {

    private static let endpoint = Config.env.baseURL + "/request"

    static func getPinRequests(_ pinId: String, status: PinRequestStatus?, completion: @escaping RequestsCompletion) {
        var params: Parameters = ["pinId": pinId]

        if let status = status {
            params["status"] = status.rawValue
        }

        let json = URLEncoding.default

        Alamofire.request(endpoint, method: .get, parameters: params, encoding: json).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }

            do {
                let response = try JSONDecoder.shared.decode(PinRequestsResponse2.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }

    static func getPinRequest(_ pinId: String, type: PinRequestType?, status: PinRequestStatus?, completion: @escaping RequestCompletion) {
        let url = endpoint + "/by-pin/" + pinId
        let encoding = URLEncoding.default
        var params = [String : String]()

        if let type = type {
            params["type"] = type.rawValue
        }

        if let status = status {
            params["status"] = status.rawValue
        }

        Alamofire.request(url, method: .get, parameters: params, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }

            do {
                let response = try JSONDecoder.shared.decode(PinRequestResponse2.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }

    static func getPinRequests(_ userId: String, type: PinRequestType?, status: PinRequestStatus?, completion: @escaping RequestsCompletion) {
        let url = endpoint + "/by-attendee/" + userId
        let encoding = URLEncoding.default
        var params = [String : String]()

        if let type = type {
            params["type"] = type.rawValue
        }

        if let status = status {
            params["status"] = status.rawValue
        }

        Alamofire.request(url, method: .get, parameters: params, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }

            do {
                let response = try JSONDecoder.shared.decode(PinRequestsResponse2.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }

    static func leave(_ pinRequestId: String, completion: @escaping GenericCompletion) {
        let url = endpoint + "/" + pinRequestId + "/leave"
        let encoding = URLEncoding.default

        Alamofire.request(url, method: .put, parameters: nil, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }
            data.printJSON()
            do {
                let response = try JSONDecoder.shared.decode(GenericResult.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }

    static func accept(_ pinRequestId: String, completion: @escaping GenericCompletion) {
        let url = endpoint + "/" + pinRequestId + "/accept"
        let encoding = URLEncoding.default

        Alamofire.request(url, method: .put, parameters: nil, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }
            data.printJSON()
            do {
                let response = try JSONDecoder.shared.decode(GenericResult.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }

    static func delete(_ pinRequestId: String, completion: @escaping GenericCompletion) {
        let url = endpoint + "/" + pinRequestId
        let encoding = URLEncoding.default

        Alamofire.request(url, method: .delete, parameters: nil, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }
            data.printJSON()
            do {
                let response = try JSONDecoder.shared.decode(GenericResult.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }
}

struct User2: Codable {
    var userId: String?
    var firstName: String
    var lastName: String
    var bio: [String]?
    var gender: String?
    //var interests: [String : String]?
    var photo: String?

    var fullName: String {
        return "\(self.firstName) \(self.lastName)"
    }

    var description: String {
        var values = [String]()

        if let bio = bio?.first {
            values.append(bio)
        }

        return values.joined(separator: " - ")
    }

    var interestsDescription: String {
        return "interests here"
    }
}

struct PinRequest2: Codable {
    var pinRequestId: String?
    var attendeeId: User2?
    var inviterId: User2?
    var pinId: Pin2?
    var status: PinRequestStatus?
    var type: PinRequestType?
    var createdAt: Date?
    var updatedAt: Date?
    
    var isInviterHostingIt: Bool {
        guard let inviterId = inviterId?.userId, let hostId = pinId?.hostId?.userId else { return false }
        return inviterId == hostId
    }
}

struct PinRequestsResponse2: Codable {
    var metaData: [PinRequest2]?
}

struct PinRequestResponse2: Codable {
    var metaData: PinRequest2?
}
