//
//  ReportApi.swift
//  Droppin
//
//  Created by Isaias Garcia on 03/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Alamofire
import SwiftyJSON

enum ReportType: String {
    case userAtPin = "userAtPin"
    case interest = "interest"
    case help = "help"
    case abusivePin = "abusivePin"
    case abusiveUser = "abusiveUser"
}

enum ReportEndpoints: String  {
    case report = "report"
}

protocol ReportApi {
    var api: DroppinApi {get}
    
    func send(report: String, description: String?, type: ReportType, completion: @escaping (DIResult<RequestResult>)->())
}

extension ReportApi {
    
    func send(report: String, description: String? = nil, type: ReportType, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(ReportEndpoints.report.rawValue)
        var params: Parameters = ["report": report, "type": type.rawValue]
        
        if let description = description {
            params["description"] = description
        }
        
        Alamofire.request(endpoint, method: .post, parameters: params, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
}
