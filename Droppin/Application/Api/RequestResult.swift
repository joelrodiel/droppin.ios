//
//  RequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/13/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit


typealias MongoId =  String

protocol RequestResultProtocol {
//    var statusCode: StatusCode {get}
    var errorCode: DiApiError? {get}
    var message: String? {get}
    var metaData: [String:Any]? {get}
    
    static func extractCoordinatesFromApiJsonResponse(_: JSON) throws -> CLLocationCoordinate2D
    static func extractStartDateFromApiJsonResponse(_: JSON) throws -> Date
    static func extractPinIdFromApiJsonResponse(_: JSON) throws -> MongoId
    static func extractHostIdFromApiJsonResponse(_: JSON) throws -> MongoId
    static func extractUserIdFromApiJsonResponse(_: JSON) throws -> MongoId
    static func extractPinStatusFromApiJsonResponse(_: JSON) throws -> PinStatus
    static func extractPinImageFromApiJsonResponse(_: JSON) throws -> UIImage
    static func extractUserImageFromApiJsonResponse(_ json: JSON) throws -> UIImage
    static func extractStringParameterFromApiJsonResponse(json: JSON, name: String) throws -> String
    static func extractStringParameterFromApiJsonResponse( returnNilIfNone json: JSON, name: String ) -> String?
    static func extractBooleanParameterFromApiJsonResponse( returnNilIfNone json: JSON, name: String ) -> Bool?
//    static func extractStringParameterFromApiJsonResponse(json: JSON, name: String) -> String?
}

class RequestResult: RequestResultProtocol {
    
    let message: String?
//    let statusCode: StatusCode
    let metaData:  [String:Any]?
    let errorCode: DiApiError?
    
    
//    init(){
//        statusCode = nil
//        errorCode = nil
//        message = nil
//        metaData = nil
//    }
    
//    init(statusCode: StatusCode?, message: String?){
//        metaData = nil
//        self.errorCode = nil
//        self.statusCode = statusCode
//        self.message = message
//    }
    
//    init(statusCode: StatusCode?, message: String?, metaData:  [String:String]?){
//        self.metaData = metaData
//        self.errorCode = nil
//        self.statusCode = statusCode
//        self.message = message
//    }
    
    
    required init(_ json: JSON)  throws {

        if let errorCodeValue =  RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "errorCode") {
            errorCode = DiApiError(rawValue: errorCodeValue)
            message = String(describing: errorCode)
        } else {
            errorCode = nil
            message = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "message")
        }

        if json["metaData"].exists() {
            metaData = json["metaData"].reduce([String:Any](), { (result: [String:Any], item: Any) -> [String:Any] in
                guard let (key,value) = item as? (String,Any) else {
                    return result
                }
                
                var out = result
                out[key] = value
                return out
            })
        } else {
            metaData = nil
        }
    }
    
//    static func extractStringParameterFromApiJsonResponse(json: JSON, name: String) -> String? {
//        return json[name].string
//    }
    

    static func extractBooleanParameterFromApiJsonResponse( returnNilIfNone json: JSON, name: String ) -> Bool? {
        return json[name].bool
    }

    static func extractStringParameterFromApiJsonResponse( returnNilIfNone json: JSON, name: String ) -> String? {
//        print("extractStringParameterFromApiJsonResponse ->  \(name)")
        return json[name].string
    }
    
    
    static func extractStringParameterFromApiJsonResponse(json: JSON, name: String ) throws -> String {
//        print("extractStringParameterFromApiJsonResponse ->  \(name)")
        guard let stringValue = json[name].string else {
            print("extractStringParameterFromApiJsonResponse -> ERROR! Key not found:  \(name)")
            throw DiApiError.dNoKeyFoundInApiResponse("No Key named \(name) found with string value")
        }
        
        return stringValue
    }

    static func extractNumberParameterFromApiJsonResponse(returnNilIfNone json: JSON, name: String) -> Int? {
//        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "\(name)"))
        return json[name].int
    }
    
    static func extractNumberParameterFromApiJsonResponse(json: JSON, name: String) throws -> Int {
//        print("extractNumberParameterFromApiJsonResponse ->  \(name)")
//        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: " \(name)"))
        guard let value = json[name].int else {
            throw DiApiError.dNoKeyFoundInApiResponse("No Key named \(name) found with number value")
        }
        
        return value
    }
    
    static func extractImageParameterFromApiJsonResponse(json: JSON, name: String) throws -> UIImage {
        guard let stringValue = json[name]["data"].rawString() else {
            throw DiApiError.dNoKeyFoundInApiResponse(name)
        }
        
        let decodedData = Data(base64Encoded: stringValue)
        guard let decodedImage = UIImage(data: decodedData!) else {
            throw DiApiError.dErrorConvertingToProperType("image not correct")
        }
        
        
        return decodedImage
    }
    
      static func extractUserImageFromApiJsonResponse(_ json: JSON) throws -> UIImage {
        let propertyName = "profileImage"
        
        guard let stringValue = json[propertyName].rawString() else {
            throw DiApiError.dNoKeyFoundInApiResponse(propertyName)
        }
        
        if let decodedData = Data(base64Encoded: stringValue), let decodedImage = UIImage(data: decodedData) {
            return decodedImage
        }
        
        throw DiApiError.dErrorConvertingToProperType("image not correct")
    }
    
    
      static func extractPinImageFromApiJsonResponse(_ json: JSON) throws -> UIImage {
        let propertyName = "photo"
        return try extractImageParameterFromApiJsonResponse(json: json, name: propertyName)
    }
    
    
//    static func extractPinImageFromApiJsonResponse(_ json: JSON) throws -> UIImage {
//        let propertyName = "photo"
//        guard let stringValue = json[propertyName].rawString() else {
//            throw DiApiError.dNoKeyFoundInApiResponse(propertyName)
//        }
//
//        let decodedData = Data(base64Encoded: stringValue)
//        guard let decodedImage = UIImage(data: decodedData!) else {
//            throw DiApiError.dErrorConvertingToProperType("profileImage not correct")
//        }
//
//
//        return decodedImage
//    }
    
    static func extractPinStatusFromApiJsonResponse(_ json: JSON) throws -> PinStatus {
        let propertyName = "pinStatus"
        let propertyValue =  try extractStringParameterFromApiJsonResponse(json: json, name: propertyName)
        guard let pinStatus = PinStatus(rawValue: propertyValue) else {
            throw DiApiError.dInvalidData
        }
        
        return pinStatus
    }
    
    static func extractStatusCodeFromApiJsonResponse(_ json: JSON) throws -> StatusCode {
        let propertyName = "statusCode"
        let propertyValue =  try extractStringParameterFromApiJsonResponse(json: json, name: propertyName)
        guard let statusCode = StatusCode(rawValue: propertyValue) else {
            throw DiApiError.dInvalidData
        }
        
        return statusCode
    }

    //        if let errorCode =  json["errorCode"].string {
    //            self.errorCode = DiApiError(rawValue: errorCode)
    //        } else {
    //            self.errorCode = nil
    //        }
    
    
    
    static func extractPinIdFromApiJsonResponse(_ json: JSON) throws -> MongoId {
        let propertyName = "pinId"
        return  try extractStringParameterFromApiJsonResponse(json: json, name: propertyName)
        
//        guard let id = json["_id"].string  else {
//            throw DiError.InvalidJsonPin("Invalid PinId")
//        }
//        return id
    }
    
    static func extractObjectIdFromApiJsonResponse(json: JSON, name: String) throws -> MongoId {
        return try extractStringParameterFromApiJsonResponse(json: json, name: name)
    }
    
    static func extractUserIdFromApiJsonResponse(_ json: JSON) throws -> MongoId {
        let propertyName = "userId"
        return  try extractStringParameterFromApiJsonResponse(json: json, name: propertyName)
    }
    
    static func extractHostIdFromApiJsonResponse(_ json: JSON) throws -> MongoId {
        
        let propertyName = "hostId"
        return  try extractStringParameterFromApiJsonResponse(json: json, name: propertyName)
        
//
//        guard let id = json["hostid"].string  else {
//            throw DiError.InvalidJsonPin("Invalid HostId")
//        }
//        return id
    }
    
    static func extractCoordinatesFromApiJsonResponse(_ json: JSON) throws -> CLLocationCoordinate2D {
        guard let longitude = json["loc"]["coordinates"][0].double,  let latitude = json["loc"]["coordinates"][1].double else {
            throw DiError.InvalidJsonPin("No Coordinates found or coordinates are not numbers")
        }
        
        guard longitude != 0 && latitude != 0 else {
            throw DiError.InvalidJsonPin("coordinates are set to 0,0 ")
        }
        
        let coordinates =  CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        return coordinates
    }
    
    static func extractStartDateFromApiJsonResponse(_ json: JSON) throws -> Date {
        guard let dateString = json["startDate"].string else {
            throw DiError.InvalidJsonPin("No startDate Given")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"2017-12-06T01:10:13.000Z"
        
        guard let startDate = dateFormatter.date(from: dateString) else {
            throw DiError.InvalidJsonPin("Invalid Date")
        }
        
        return startDate
    }
    
    static func extractDateFromApiJsonResponse(json: JSON, name: String) throws -> Date {
        guard let dateString = json[name].string else {
            throw DiError.InvalidJsonPin("No \(name) Given")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"2017-12-06T01:10:13.000Z"
        
        guard let startDate = dateFormatter.date(from: dateString) else {
            throw DiError.InvalidJsonPin("Invalid Date")
        }
        
        return startDate
    }
    
    static func extractDateFromApiJsonResponse(returnNilIfNone json: JSON, name: String) -> Date? {
        guard let dateString = json[name].string else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"2017-12-06T01:10:13.000Z"
        
        guard let date = dateFormatter.date(from: dateString) else {
            return nil
        }
        
        return date
    }
    
    static func extractDateFromApiJsonResponse(createdAt json: JSON) throws -> Date {
        guard let dateString = json["createdAt"].string else {
            throw DiError.InvalidJsonPin("No createdAt Given")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"2017-12-06T01:10:13.000Z"
        
        guard let startDate = dateFormatter.date(from: dateString) else {
            throw DiError.InvalidJsonPin("Invalid Date")
        }
        
        return startDate
    }
    
    static func extractDateFromApiJsonResponse(updatedAt json: JSON) throws -> Date {
        guard let dateString = json["updatedAt"].string else {
            throw DiError.InvalidJsonPin("No updatedAt Given")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"2017-12-06T01:10:13.000Z"
        
        guard let startDate = dateFormatter.date(from: dateString) else {
            throw DiError.InvalidJsonPin("Invalid Date")
        }
        
        return startDate
    }
    
    
    static func extractBioParameterFromApiJsonResponse(json: JSON) throws -> String {
        guard let bioJson = json["bio"].array else {
            print("extractStringParameterFromApiJsonResponse -> ERROR! Key not found:  bio")
            throw DiApiError.dNoKeyFoundInApiResponse("No Key named bio found with string value")
        }
        
        return bioJson.count > 0 ? bioJson[0].string ?? "" : ""
    }
}
