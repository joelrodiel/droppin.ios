//
//  RequestToJoinAPI.swift
//  Droppin
//
//  Created by Madson on 11/5/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Alamofire


struct RequestToJoinAPI {

    typealias RequestToJoinCompletion = (Error?, GenericResult?) -> Void

    private static let endpoint = Config.env.baseURL + "/request-to-join"

    static func request(_ pinId: String, completion: @escaping PinInvitationInviteCompletion) {
        let encoding = JSONEncoding.default

        let params: [String : Any] = [
            "pinId" : pinId
        ]

        Alamofire.request(endpoint, method: .post, parameters: params, encoding: encoding).validate().response {
            guard $0.error == nil else {
                completion($0.error, nil)
                return
            }

            guard let data = $0.data else {
                completion($0.error, nil)
                return
            }
            data.printJSON()
            do {
                let response = try JSONDecoder.shared.decode(GenericResult.self, from: data)
                completion(nil, response)
            } catch {
                completion(error, nil)
            }
        }
    }
}
