//
//  SignInRequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 1/1/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

class SignInRequestResult: RequestResult {
    let userId: String?
    
    required  init(_ json: JSON) throws {
        userId =  RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "userId")
        try super.init(json)
        
    }
}
