import Foundation
import Alamofire
import SwiftyJSON

enum Gender: String,  CustomStringConvertible {
    case male = "male"
    case female = "female"
    
    static let allCases = [male, female]
    
    var index: Int {
        switch self {
        case .male: return 0
        case .female: return 1
        }
    }
    
    var description: String {
        switch self{
        case .male: return "male"
        case .female: return "female"
        }
    }
}

enum UserEndpoints: String {
    case createUser = "user/create"
    case updateMobile = "user/update/mobile"
    case verifyMobileAndActivateUser = "user/verify-mobile-and-activate-user"
    case sendOtpSms = "user/send-otp-sms"
    case profileImage = "user/update/profile-image"
    case update = "user/update"
    case updateInterests = "user/update/interests"
    case updateBio = "user/update/bio"
    case updateGender = "user/update/gender"
    case updateBirthdate = "user/update/birth-date"
    case updateToken = "user/update/token"
    case uploadProfileImage = "user/uploadProfileImage"
    case getUserInfo = "user/:id"
    case getUserImage = "user/:id/image"
    case getUserImages = "user/:id/image/all"
    case getUserStatistic = "user/:id/statistic"
    case getCriticalData = "user/critical-info"
    case updatePassword = "user/update/password"
}

protocol UserApi {
    var api: DroppinApi {get}
    func createUser(firstName:String, lastName:String,email:String,password:String,completion: @escaping (DIResult<RequestResult>)->())
    func updateMobile(mobile:String, completion: @escaping (DIResult<RequestResult>)->())
    func verifyMobileAndActivateUser(otp: String, completion: @escaping (DiApiError?, VerifyCodeResult?) -> Void)
    func updateBirthdate(birthDate:Date, completion: @escaping (DIResult<RequestResult>)->())
    func updateGender(gender:Gender?, completion: @escaping (DIResult<RequestResult>)->())
    func updateToken(completion: ((DIResult<RequestResult>) -> Void)?)
    func requestOtp(completion: @escaping (DIResult<OtpRequestResult>)->())
    func getUserImages(_ userId: String, completion: @escaping (Error?, ImagesRequestResult?)->())
    func getUserImage(_ userId: String, completion: @escaping (Error?, ImageRequestResult?)->())
    func update(_ images: [String]?, completion: @escaping (Error?, HTTPURLResponse?)->())
    func getUserInfo(_ userId: String, completion: @escaping (DIResult<UserResponse>) -> ())
    func getUserStatistic(_ userId: String, completion: @escaping (DIResult<UserStatisticResult>) -> ())
    func update(user: User, interests: [Interest], completion: @escaping (DIResult<RequestResult>)->())
    func updatePassword(old: String, new: String, completion: @escaping (DIResult<RequestResult>)->())
}

// @TODO how to handle when the network is not avaialble: NetworkReachabilityManager
extension UserApi {

    func createUser(firstName:String, lastName:String,email:String,password:String,completion: @escaping (DIResult<RequestResult>)->())  {
        print("[DN][createUser] ->start")
        
        var parameters: Parameters = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "password": password
        ]
        
        if let token = UserDefaults.standard.string(forKey: UserPropertyKey.token.rawValue) {
            parameters["token"] = token
        }
        
        print("[DN][createUser] ->api request sent")
        let endpoint =  api.getEndpoint(UserEndpoints.createUser.rawValue)
        Alamofire.request(endpoint, method: .post,parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[DN][createUser][response] -> received")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func updateMobile(mobile:String, completion: @escaping (DIResult<RequestResult>)->())  {
        print("[DN][updateMobile] -> Start")
        let parameters: Parameters = [ "mobile": mobile ]
        let endpoint =  api.getEndpoint(UserEndpoints.updateMobile.rawValue)
        Alamofire.request(endpoint,  method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func updateToken(completion: ((DIResult<RequestResult>) -> Void)? = nil) {
        let mongoIdLength = 24

        guard let userId = UserDefaults.standard.string(forKey: UserPropertyKey.currentUserId.rawValue), userId.count == mongoIdLength else {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No User found. Not sending Device Update"))
            return
        }

        guard let token = UserDefaults.standard.string(forKey: UserPropertyKey.token.rawValue) else {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No token found. Not sending Device Update"))
            return
        }

        print("[UserApi][updateToken] -> Start")
        let parameters: Parameters = [ "token": token ]
        let endpoint =  api.getEndpoint(UserEndpoints.updateToken.rawValue)
        Alamofire.request(endpoint,  method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            guard let completion = completion else { return }
            
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func verifyMobileAndActivateUser(otp: String, completion: @escaping (DiApiError?, VerifyCodeResult?) -> Void)  -> ()  {
        print("[DN][verifyMobileAndActivateUser] -> start")
        let parameters: Parameters = ["otp": otp]
        let endpoint =  api.getEndpoint(UserEndpoints.verifyMobileAndActivateUser.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters:parameters, encoding: JSONEncoding.default).validate().response { response in
            print("[DN][verifyMobileAndActivateUser][response] ->recieved")
            guard let data = response.data else { return }

            if let result = try? JSONDecoder.shared.decode(VerifyCodeResult.self, from: data) {
                completion(nil, result)
            } else if let result = try? JSONDecoder.shared.decode(GenericResult.self, from: data) {
                if let errorCode = result.errorCode, let error = DiApiError(rawValue: errorCode) {
                    completion(error, nil)
                } else {
                    completion(DiApiError.dNoApiErrorFound, nil)
                }
            } else {
                completion(DiApiError.dNoApiErrorFound, nil)
            }
        }
    }
    
    //    func uploadPinImage( pinId: String ,image:UIImage, completion: @escaping (DIResult<RequestResult>)->())  {
    func requestOtp(completion: @escaping (DIResult<OtpRequestResult>)->()){
        print("[DN][sendMobileVerification] ->start")
        let endpoint =  api.getEndpoint(UserEndpoints.sendOtpSms.rawValue)
        Alamofire.request(endpoint, method: .post, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[DN][sendMobileVerification][response] -> callback")
            let diResult: DIResult<OtpRequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
        print("[DN][sendMobileVerification] -> end")
    }
    
    func getUserImages(_ userId: String, completion: @escaping (Error?, ImagesRequestResult?)->())  {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(UserEndpoints.getUserImages.rawValue.replacingOccurrences(of: ":id", with: userId))
        
        Alamofire.request(endpoint).responseJSON(completionHandler: { response in
            guard let data = response.data else {
                completion(response.error, nil)
                return
            }

            do {
                let imageResult = try JSONDecoder.shared.decode(ImagesRequestResult.self, from: data)
                completion(nil, imageResult)
            } catch {
                completion(error, nil)
            }
        })
    }
    
    func getUserImage (_ userId: String, completion: @escaping (Error?, ImageRequestResult?)->())  {
        print("[DN][getUserImage] ->start")
        
        let endpoint = api.getEndpoint(UserEndpoints.getUserImage.rawValue.replacingOccurrences(of: ":id", with: userId))
        
        Alamofire.request(endpoint).responseJSON(completionHandler: { response in
            guard let data = response.data else {
                completion(response.error, nil)
                return
            }

            do {
                let imageResult = try JSONDecoder.shared.decode(ImageRequestResult.self, from: data)
                completion(nil, imageResult)
            } catch {
                completion(error, nil)
            }
        })
    }
    
    func update(_ images: [String]?, completion: @escaping (Error?, HTTPURLResponse?)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(UserEndpoints.profileImage.rawValue)
        var params: Parameters = [:]
        
        if let images = images {
            params["photos"] = images
        }
        
        Alamofire.request(endpoint, method: .post, parameters: params, encoding: JSONEncoding.default).validate().response {
            completion($0.error, $0.response)
        }
    }
    
    func updateInterests(interests: [Interest], completion: @escaping (DIResult<RequestResult>)->())  {
        print("[DN][updateInterests] ->Start")
        
        let parameters: Parameters = ["interests": formatInterestsToJSON(interests: interests)]
        
        let endpoint =  api.getEndpoint(UserEndpoints.updateInterests.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[DN][updateInterests] ->Response")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    private func formatInterestsToJSON(interests: [Interest]) -> [String] {
        let interests = interests.map { $0.label }
        return interests
    }
    
    func updateBio(bio:String?, completion: @escaping (DIResult<RequestResult>)->())  {
        print("[DN][updateBio] ->Start")
        var parameters: Parameters
        parameters = ["bio": bio!]
        let endpoint =  api.getEndpoint(UserEndpoints.updateBio.rawValue)
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func updateGender(gender:Gender?, completion: @escaping (DIResult<RequestResult>)->())  {
        print("[DN][updateGender] ->Start")
        
        var parameters: Parameters
        if let gender = gender {
            parameters = ["gender": gender.description]
        } else {
            parameters = ["gender": "na"]
        }
        
        let endpoint =  api.getEndpoint(UserEndpoints.updateGender.rawValue)
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func updateBirthdate(birthDate:Date, completion: @escaping (DIResult<RequestResult>)->())  {
        print("[DN][updateBirtdate] -> Start")
        let formatter = ISO8601DateFormatter()
        let parameters: Parameters = [
            "birthDate": formatter.string(from: birthDate)
        ]
        let endpoint =  api.getEndpoint(UserEndpoints.updateBirthdate.rawValue)
        Alamofire.request( endpoint , method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            print("[DN][updateBirtdate][Response] -> Api Response recieved")
            let diResult: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            completion(diResult)
        }
    }
    
    func getUserInfo(_ userId: String, completion: @escaping (DIResult<UserResponse>) -> ()) {
        print("[UserApi][getUserInfo] -> Start")
        let endpoint = api.getEndpoint(UserEndpoints.getUserInfo.rawValue.replacingOccurrences(of: ":id", with: userId))
        
        Alamofire.request(endpoint).responseJSON { response in
            print("[UserApi][getUserInfo] -> Api Response received")
            let result: DIResult<UserResponse> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func getUserStatistic(_ userId: String, completion: @escaping (DIResult<UserStatisticResult>) -> ()) {
        print("[UserApi][getUserStatistic] -> Start")
        let endpoint = api.getEndpoint(UserEndpoints.getUserStatistic.rawValue.replacingOccurrences(of: ":id", with: userId))
        
        Alamofire.request(endpoint).responseJSON { response in
            print("[UserApi][getUserStatistic] -> Api Response received")
            let result: DIResult<UserStatisticResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func getUserBadgeCount(_ userId: String, completion: @escaping ([String: Any]?, Error?) -> Void) {
        print("[UserApi][getUserBadgeCount] -> Start")
        let endpoint = api.getEndpoint("user/\(userId)/badgeCount")
        
        Alamofire.request(endpoint).responseJSON { response in
            print("[UserApi][getUserBadgeCount] -> Api Response received")
            
            if let error = response.error {
                completion(nil, error)
                print(error)
            } else if let json = response.result.value as? [String: Any] {
                completion(json, nil)
            }
        }
    }
    
    func resetUserPinNotifications(_ userId: String, completion: @escaping ([String: Any]?, Error?) -> Void) {
        print("[UserApi][resetUserPinNotifications] -> Start")
        let endpoint = api.getEndpoint("user/\(userId)/resetPinNotifications")
        
        var url = URLRequest(url: URL(string: endpoint)!)
        url.httpMethod = HTTPMethod.put.rawValue
        
        Alamofire.request(url).responseJSON { response in
            print("[UserApi]resetUserPinNotifications] -> Api Response received")
            
            if let error = response.error {
                completion(nil, error)
                print(error)
            } else if let json = response.result.value as? [String: Any] {
                completion(json, nil)
            }
        }
    }
    
    func update(user: User, interests: [Interest], completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(UserEndpoints.update.rawValue)
        var parameters = user.asParameters()
        
        parameters["interests"] = formatInterestsToJSON(interests: interests)
        
        Alamofire.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
    
    func loadInterests(userId: String, completion: @escaping ([String]) -> ()) {
        getUserInfo(userId) { result in
            switch result {
            case .success(let data):
                completion(data.user.interests)
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    
    func getCriticalData(completion: @escaping (UserCriticalData?, Error?) -> ()) {
        let url =  api.getEndpoint(UserEndpoints.getCriticalData.rawValue)
        let encoding = URLEncoding.default
        
        Alamofire.request(url, method: .get, encoding: encoding).validate().response {
            guard let data = $0.data else {
                completion(nil, $0.error)
                return
            }
            
            do {
                let response = try JSONDecoder.shared.decode(UserCriticalDataRequest.self, from: data)
                completion(response.metaData, nil)
            } catch let err {
                completion(nil, err)
            }
        }
    }
    
    func updatePassword(old: String, new: String, completion: @escaping (DIResult<RequestResult>)->()) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let endpoint = api.getEndpoint(UserEndpoints.updatePassword.rawValue)
        let parameters: Parameters = ["oldPassword": old, "newPassword": new]
        
        Alamofire.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            let result: DIResult<RequestResult> = DiNetwork.createDiResult(response)
            
            if case .failure( _, let error) = result {
                DiNetwork.authentication(sendToSignInIfNotAuthenticated: error)
            }
            
            completion(result)
        }
    }
}
