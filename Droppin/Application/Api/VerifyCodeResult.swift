//
//  VerifyCodeResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/19/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

struct VerifyCodeResult: Codable {
    let userId: String
}
