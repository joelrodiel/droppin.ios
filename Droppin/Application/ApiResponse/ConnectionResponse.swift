//
//  ConnectionResponse.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class ConnectionResponse: RequestResult {
    var connections: [Connection]?
    var connection: Connection?
    
    required init(_ json: JSON ) throws {
        let data = json["metaData"]
        
        if !data.isEmpty {
            if let requestsCollection = data.array {
                self.connections = requestsCollection.flatMap {
                    do {
                        return try ConnectionResponse.loadConnection(json: $0)
                    } catch let error {
                        print("flatmap exec caused an error!  ERROR!!! \(error)")
                        return nil // flatmap will remove nil pins
                    }
                }
            } else {
                print("[Response][Connection][init] -> No connections")
                self.connections = []
                self.connection = try ConnectionResponse.loadConnection(json: json["metaData"])
            }
        }
        
        try super.init(json)
    }
    
    class func loadConnection(json: JSON) throws -> Connection {
        let connectionId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "connectionId")
        let userOneId: User? = UserResponse.loadUser(json: json["userOneId"])
        let userTwoId: User? = UserResponse.loadUser(json: json["userTwoId"])
        let actionUserId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "actionUserId")
        let status: ConnectionStatus? = ConnectionStatus(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "status") ?? "")
        let createdAt: Date = try RequestResult.extractDateFromApiJsonResponse(createdAt: json)
        
        if let status = status {
            return Connection(connectionId: connectionId, userOneId: userOneId, userTwoId: userTwoId, actionUserId: actionUserId, status: status, createdAt: createdAt)
        } else {
            throw DiApiError.dNoKeyFoundInApiResponse("JSON not valid")
        }
    }
}
