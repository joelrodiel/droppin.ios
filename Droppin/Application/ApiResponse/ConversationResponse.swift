//
//  ConversationResponse.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class ConversationResponse: RequestResult {
    var conversations: [Conversation]?
    var conversation: Conversation?
    
    required init(_ json: JSON ) throws {
        let data = json["metaData"]
        
        if !data.isEmpty {
            if let requestsCollection = data.array {
                self.conversations = requestsCollection.compactMap {
                    do {
                        return try ConversationResponse.loadConversation(json: $0)
                    } catch let error {
                        print("flatmap exec caused an error!  ERROR!!! \(error)")
                        return nil // flatmap will remove nil pins
                    }
                }
            } else {
                print("[Response][Conversation][init] -> No conversations")
                self.conversations = []
                self.conversation = try ConversationResponse.loadConversation(json: json["metaData"])
            }
        }
        
        try super.init(json)
    }
    
    class func loadConversation(json: JSON) throws -> Conversation {
        let conversationId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "conversationId")
        let userOneId: User? = UserResponse.loadUser(json: json["userOneId"])
        let userTwoId: User? = UserResponse.loadUser(json: json["userTwoId"])
        let status: ConversationStatus? = ConversationStatus(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "status") ?? "")
        let subject: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "subject")
        let message: Message? = MessageResponse.loadMessage(returnNilIfNone: json["lastMessage"])
        let pinTitle: String? = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json["pin"], name: "title")
        let hostId: String? = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json["pin"], name: "hostId")
        
        if let status = status {
            var conversation = Conversation(conversationId: conversationId, userOneId: userOneId, userTwoId: userTwoId, subject: subject, status: status, lastMessage: message)
            
            conversation.pinTitle = pinTitle
            conversation.pinHostId = hostId
            
            return conversation
        } else {
            throw DiApiError.dNoKeyFoundInApiResponse("JSON not valid")
        }
    }
}
