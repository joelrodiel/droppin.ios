import Foundation
import SwiftyJSON
import MapKit

class DiNotificationsApiResponse: RequestResult {
    let notifications: [DiNotification]
    
    required  init(_ json: JSON ) throws {
        if let notificationCollection = json["notifications"].array  {
            notifications =  notificationCollection.compactMap {
                do {
                    let notificationId = try RequestResult.extractObjectIdFromApiJsonResponse(json: $0, name: "notificationId")
                    let receiverId = try RequestResult.extractObjectIdFromApiJsonResponse(json: $0, name: "receiverId")
                    let senderId = try RequestResult.extractObjectIdFromApiJsonResponse(json: $0, name: "senderId")
                    let senderName = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "senderName")
                    let title = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "title")
                    let body = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "body")
                    let objectId = $0["objectId"] as AnyObject
                    let type = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "type")
                    let notify = RequestResult.extractBooleanParameterFromApiJsonResponse(returnNilIfNone: $0, name: "notify") ?? true
                    let status = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "status")
                    let createdAt = try RequestResult.extractDateFromApiJsonResponse(createdAt: $0)
                    
                    if let diType = DiNotificationType(rawValue: type), let diStatus = DiNotificationStatus(rawValue: status) {
                        return DiNotification(notificationId: notificationId, receiverId: receiverId, senderId: senderId, senderName: senderName, title: title, body: body, objectId: objectId, type: diType, notify: notify, status: diStatus, createdAt: createdAt)
                    }
                    
                    throw DiApiError.dInvalidData
                } catch let error {
                    print("flatmap exec caused an error!  ERROR!!! \(error)")
                    return nil // flatmap will remove nil pins
                }
            }
        } else {
            print("No DiNotifications")
            notifications = [DiNotification]()
        }
        try super.init(json)
    }
}
