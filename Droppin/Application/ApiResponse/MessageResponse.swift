//
//  MessageResponse.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class MessageResponse: RequestResult {
    var mrmessage: Message?
    var messages: [Message]?
    var pagination: ApiPagination?
    
    required init(_ json: JSON ) throws {
        let data = json["metaData"]
        
        if !data.isEmpty {
            let items = data["items"]
            
            if !items.isEmpty, let requestsCollection = items.array {
                self.messages = requestsCollection.flatMap {
                    do {
                        return try MessageResponse.loadMessage(json: $0)
                    } catch let error {
                        print("flatmap exec caused an error!  ERROR!!! \(error)")
                        return nil // flatmap will remove nil pins
                    }
                }
                
                if !data["pagination"].isEmpty {
                    pagination = ApiPagination()
                    
                    pagination!.page = data["pagination"]["page"].int ?? 0
                    pagination!.total = data["pagination"]["total"].int
                    pagination!.pages = data["pagination"]["pages"].int
                }
            } else {
                print("[Response][Message][init] -> No messages")
                self.messages = []
                self.mrmessage = try MessageResponse.loadMessage(json: json["metaData"])
            }
        }
        
        try super.init(json)
    }
    
    class func loadMessage(json: JSON) throws -> Message {
        let messageId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "messageId")
        let senderId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "senderId")
        let conversationId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "conversationId")
        let message: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "message")
        let status: MessageStatus? = MessageStatus(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "status") ?? "")
        let createdAt: Date = try extractDateFromApiJsonResponse(createdAt: json)
        let updatedAt: Date = try extractDateFromApiJsonResponse(updatedAt: json)
        
        if let status = status {
            return Message(messageId: messageId, senderId: senderId, conversationId: conversationId, message: message, status: status, createdAt: createdAt, updatedAt: updatedAt)
        } else {
            throw DiApiError.dNoKeyFoundInApiResponse("JSON not valid")
        }
    }
    
    class func loadMessage(returnNilIfNone json: JSON) -> Message? {
        do {
            return try loadMessage(json: json)
        } catch let error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure loading message. \(error.localizedDescription)")
            return nil
        }
    }
}
