//
//  PinRequestResponse.swift
//  Droppin
//
//  Created by Isaias Garcia on 09/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class PinRequestResponse: RequestResult {
    let requests: [PinRequest]
    
    required  init(_ json: JSON ) throws {
        if let requestsCollection = json["metaData"].array  {
            requests =  requestsCollection.compactMap {
                do {
                    let pinRequestId = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "pinRequestId")
                    let attendeeId = try RequestResult.extractUserIdFromApiJsonResponse($0["attendeeId"])
                    let pinTitle = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: $0, name: "pinTitle")
                    let attendeeName = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: $0, name: "attendeeName")
                    let type = try PinRequestType(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "type")) ?? PinRequestType.request
                    let status = try PinRequestStatus(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "status")) ?? PinRequestStatus.new
                    let createdAt = try RequestResult.extractDateFromApiJsonResponse(createdAt: $0)
                    let updatedAt = try RequestResult.extractDateFromApiJsonResponse(updatedAt: $0)
                    var pinId = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: $0, name: "pinId")
                    
                    // Extract pin if exists
                    var pin: Pin?
                    if pinId == nil {
                        if let extractedPin = PinsDetailRequestResult.loadPin(json: $0["pinId"]) {
                            pin = extractedPin
                            pinId = extractedPin.pinId
                        } else {
                            return nil
                        }
                    }
                    
                    var request = PinRequest(pinRequestId: pinRequestId, pinId: pinId!, attendeeId: attendeeId, pinTitle: pinTitle, attendeeName: attendeeName, type: type, status: status, createdAt: createdAt, updatedAt: updatedAt)
                    
                    // Set extracted pin
                    request.pin = pin
                    
                    return request
                } catch let error {
                    print("flatmap exec caused an error!  ERROR!!! \(error)")
                    return nil // flatmap will remove nil pins
                }
            }
        } else {
            print("No Pins!")
            requests = [PinRequest]()
        }
        try super.init(json)
    }
}
