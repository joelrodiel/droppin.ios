//
//  PinStatisticResponse.swift
//  Droppin
//
//  Created by Isaias Garcia on 18/05/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class PinStatisticResult: RequestResult {
    let pinStatistic: PinStatistic
    
    required  init(_ json: JSON ) throws {
        let atPin = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "atPin") ?? 0
        let joined = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "joined") ?? 0
        let invited = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "invited") ?? 0
        let maxAge = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "maxAge") ?? 0
        let minAge = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "minAge") ?? 0
        
        self.pinStatistic = PinStatistic(atPin: atPin, joined: joined, invited: invited, maxAge: maxAge, minAge: minAge)
        
        try super.init(json)
    }
}
