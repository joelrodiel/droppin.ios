//
//  UserResponse.swift
//  Droppin
//
//  Created by Isaias Garcia on 09/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class UserResponse: RequestResult {
    
    let user: User
    
    required init(_ json: JSON ) throws {
        guard let user = UserResponse.loadUser(json: json["metaData"]) else {
            throw DiApiError.dNoKeyFoundInApiResponse("JSON not valid")
        }
        
        self.user = user
        
        try super.init(json)
    }
    
    class func loadUser(json: JSON) -> User? {
        do {
            let userId = try RequestResult.extractUserIdFromApiJsonResponse(json)
            let firstName = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "firstName")
            let lastName = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "lastName")
            let bio = try RequestResult.extractBioParameterFromApiJsonResponse(json: json)
            let gender = Gender(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "gender") ?? "male")
            let birthDate = RequestResult.extractDateFromApiJsonResponse(returnNilIfNone: json, name: "birthDate") ?? Date()
            let genderHasChanged = RequestResult.extractBooleanParameterFromApiJsonResponse(returnNilIfNone: json, name: "genderHasChanged") ?? false
            let isBirthdateEditable = RequestResult.extractBooleanParameterFromApiJsonResponse(returnNilIfNone: json, name: "isBirthdateEditable") ?? false
            
            var interests: [String] = []
            
            if let notMaped = json["interests"].array {
                interests = notMaped.map {$0.stringValue}
            }
            
            var user = User(userId: userId, firstName: firstName, lastName: lastName, bio: bio, gender: gender!, birthDate: birthDate, interests: interests)
            user.genderHasChanged = genderHasChanged
            user.isBirthdateEditable = isBirthdateEditable
            
            return user
        } catch let error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "flatmap exec caused an error!  ERROR!!! \(error.localizedDescription)")
            return nil // flatmap will remove nil pins
        }
    }
}
