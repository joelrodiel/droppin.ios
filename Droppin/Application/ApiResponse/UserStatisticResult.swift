//
//  UserStatisticsResult.swift
//  Droppin
//
//  Created by Isaias Garcia on 12/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class UserStatisticResult: RequestResult {
    let userStatistic: UserStatistic
    
    required  init(_ json: JSON ) throws {
        let eventsAttended = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "eventsAttended") ?? 0
        let eventsHosted = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "eventsHosted") ?? 0
        let newRequests = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "newRequests") ?? 0
        let connections = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "connections") ?? 0
        let newMessages = RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json["metaData"], name: "newMessages") ?? 0
        
        self.userStatistic = UserStatistic(eventsAttended: eventsAttended, eventsHosted: eventsHosted,
                                           newRequests: newRequests, connections: connections, newMessages: newMessages)
        
        try super.init(json)
    }
}
