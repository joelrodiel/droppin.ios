//
//  ConnectionCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 20/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import SwiftMoment

class ConnectionCell: UITableViewCell, DiImageLoader {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var connectionStatus: UIView!
    @IBOutlet weak var interest1: UILabel!
    @IBOutlet weak var interest2: UILabel!
    @IBOutlet weak var interest3: UILabel!
    
    var api = DroppinApi()
    private var _connection: Connection?
    var profileAction: ((UIButton) -> Void)?
    var connection: Connection? {
        set {
            self._connection = newValue
            self.refreshView()
        }
        get {
            return self._connection
        }
    }
    
    @IBAction func profilePressed(_ sender: UIButton) {
        self.profileAction?(sender)
    }
    
    override func awakeFromNib() {
        connectionStatus.roundBorders()
        
        interest1.superview?.isHidden = true
        interest2.superview?.isHidden = true
        interest3.superview?.isHidden = true
    }
    
    func refreshView() {
        if let connection = self.connection, let connectee = connection.connectee {
            self.nameLabel.text = connectee.fullName
            
            profileButton.setImage(of: connectee.userId)
            
            loadInterests(userId: connectee.userId) { interests in
                let interestsLabels = [self.interest1, self.interest2, self.interest3]
                
                for (index, interest) in interests.enumerated() {
                    if index > 2 { break }
                    
                    interestsLabels[index]?.text = interest
                    interestsLabels[index]?.superview?.isHidden = false
                }
            }
        }
        
        self.profileButton.layer.cornerRadius = self.profileButton.frame.size.width / 2
        self.profileButton.clipsToBounds = true
    }
}
