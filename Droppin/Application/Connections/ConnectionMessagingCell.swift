//
//  ConnectionMessagingCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 14/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit

class ConnectionMessagingCell: UITableViewCell, DiImageLoader, DroppinView {
    
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    var api = DroppinApi()
    
    private var _connection: Connection?
    
    var connection: Connection? {
        get { return _connection }
        set {
            _connection = newValue
            setupView()
        }
    }
    
    func setupView() {
        self.profileImageButton.roundBorders()

        if let connectee = connection?.connectee {
            nameLabel.text = connectee.fullName
            profileImageButton.setImage(of: connectee.userId)
        }
    }
}
