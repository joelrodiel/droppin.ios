//
//  ConnectionMessaging.swift
//  Droppin
//
//  Created by Isaias Garcia on 13/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class ConnectionsMessagingViewController: UIViewController, DroppinView, ConnectionApi {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var api = DroppinApi()
    var connections: [Connection] = []
    var onSelectedConnection: ((Connection) -> ())?
    
    private enum Segue: String {
        case connectionsToMessages = "ConnectionsToMessagesSegue"
    }
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if isNotAValidUserId {
            self.navigationController?.popViewController(animated: true)
            return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "No userID")
        }
        
        reloadConnections(search: nil)
    }
    
    func reloadConnections(search: String?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        getConnections(byConnectorId: userId!, search: search) { result in
            switch result {
            case .success(let data):
                self.connections = data.connections ?? []
                self.tableView.reloadData()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.localizedDescription)")
            }
        }
    }
}

extension ConnectionsMessagingViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        reloadConnections(search: searchBar.text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        reloadConnections(search: searchBar.text)
    }
}

extension ConnectionsMessagingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let onSelectedConnection = onSelectedConnection {
            onSelectedConnection(connections[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionMessagingCell") as! ConnectionMessagingCell
        
        cell.connection = connections[indexPath.row]
        
        return cell
    }
}
