//
//  ConnectionsViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 18/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import Dollar

class ConnectionsViewController: UIViewController, DroppinView, ConnectionApi {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var api = DroppinApi()
    var userId: String?
    var connections: [Connection] = []
    
    private enum Segues: String {
        case showProfile = "ShowProfileViewSegue"
    }
    
    override func viewDidLoad() {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if userId == nil {
            navigationController?.popViewController(animated: true)
            self.showBasicAlert(title: "No User ID", message: "There's a problem loading the User ID", viewController: self)
            
            return
        }
        
        self.reloadConnections(search: nil)
    }
    
    func reloadConnections(search: String?) {
        getConnections(byConnectorId: userId!, search: search) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                
                self.prepare(connections: data.connections)
            case .failure(let result, let error):
                self.showAlert(title: "Error!", result: result, error: error, viewController: self)
            }
        }
    }
    
    func prepare(connections: [Connection]?) {
        self.connections = connections ?? []
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ProfileViewController, let user = sender as? User {
            viewController.isAbleToSendDirectMessages = true
            viewController.refreshData(userId: user.userId)
            viewController.role = .connector
        }
    }
}

extension ConnectionsViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        reloadConnections(search: searchBar.text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        reloadConnections(search: searchBar.text)
    }
}

extension ConnectionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.connections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: Segues.showProfile.rawValue, sender: self.connections[indexPath.row].connectee)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ConnectionsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCell") as! ConnectionCell
        
        cell.connection = self.connections[indexPath.row]
        
        return cell
    }
}

