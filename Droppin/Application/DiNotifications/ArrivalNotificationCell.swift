//
//  ArrivalNotificationCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/7/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

class ArrivalNotificationCell: NotificationCell {
    
    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var pinDescription: UILabel!
    @IBOutlet weak var pinTitle: UILabel!
    @IBOutlet weak var timeAgo: UILabel!

    private var _notification2: Notification2?
    private var pinId: String?
    
    override class var identifier: String { return "ArrivalNotificationCell" }
    
    var notification2: Notification2? {
        set {
            _notification2 = newValue
            setupData()
        }
        get {
            return _notification2
        }
    }
    
    override func awakeFromNib() {
        pinDescription.text = ""
        pinTitle.text = ""
        timeAgo.text = ""
    }
    
    private func setupData() {
        guard let notification = notification2 else {
            return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: Notification was not loaded")
        }
        
        if let pinId = notification.objectId {
            self.pinId = pinId

            loadPinImage(pinId) {
                self.pinImage.setImage($0)
            }
        }
        
        pinDescription.text = notification.body
        pinTitle.text = notification.title
        timeAgo.text = notification.createdAt?.timeAgoSinceNow
    }
    
//    @IBAction func showPinDetailView(_ sender: Any) {
//        guard let pinId = pinId else { return }
//        delegate?.showPinDetailView(pinId: pinId)
//    }
}
