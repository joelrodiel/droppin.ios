//
//  ConnectionApprovedTableCell.swift
//  Droppin
//
//  Created by Madson on 8/29/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class ConnectionApprovedTableCell: UITableViewCell {
    
    static let identifier = "connectionApprovedRow"
    
    @IBOutlet private(set) weak var title: UILabel!
    @IBOutlet private(set) weak var status: UIImageView!
    @IBOutlet private(set) weak var profile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profile.layer.cornerRadius = profile.frame.size.width / 2
        profile.clipsToBounds = true
    }
}
