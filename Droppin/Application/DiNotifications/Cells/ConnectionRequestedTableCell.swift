//
//  ConnectionRequestedTableCell.swift
//  Droppin
//
//  Created by Madson on 8/29/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class ConnectionRequestedTableCell: UITableViewCell {
    
    static let identifier = "connectionRequestedRow"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var profile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profile.layer.cornerRadius = profile.frame.size.width / 2
        profile.clipsToBounds = true
    }
}
