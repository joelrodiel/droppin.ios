//
//  InvitationReceivedTableCell.swift
//  Droppin
//
//  Created by Madson on 8/29/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class InvitationReceivedTableCell: UITableViewCell {
    
    static let identifier = "invitationReceivedRow"
    
    @IBOutlet private(set) weak var title: UILabel!
    @IBOutlet private(set) weak var profile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.profile.layer.cornerRadius = self.profile.frame.size.width / 2;
        self.profile.clipsToBounds = true;
    }
}
