//
//  NotificationTableCell.swift
//  Droppin
//
//  Created by Madson on 8/29/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CoreLocation
import DateToolsSwift

final class NotificationTableCell: UITableViewCell, Identifiable {
    
    static let identifier = "NotificationTableCell"
    
    @IBOutlet private(set) weak var bodyLabel: UILabel!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var dateLabel: UILabel!
    @IBOutlet private(set) weak var distanceLabel: UILabel!
    @IBOutlet private(set) weak var pictureImageView: UIImageView!
    @IBOutlet private(set) weak var titleStackView: UIStackView!
    @IBOutlet private(set) var distanceStackView: UIStackView!

    private var hasDistance: Bool = false {
        didSet {
            if hasDistance {
                titleStackView.addArrangedSubview(distanceStackView)
            } else {
                titleStackView.removeArrangedSubview(distanceStackView)
                distanceStackView.removeFromSuperview()
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configure(_ request: PinRequest2, _ location: CLLocation?) {
        let inviter = request.inviterId
        let pin = request.pinId

        if let fullName = inviter?.fullName {
            bodyLabel.text = "\(fullName) invited you"
        }

        if let pinTitle = pin?.title {
            titleLabel.text = pinTitle
        }

        pictureImageView.setImage(pin?.photo)

        if let coordinates = pin?.coordinates, let location = location {
            hasDistance = true

            let pinLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
            let distanceInMeters = location.distance(from: pinLocation)
            let distanceInMiles = distanceInMeters * 0.000621371192
            let distance = String(format: "%.1f mi", distanceInMiles)
            self.distanceLabel.text = distance
        }

        dateLabel.text = request.createdAt?.timeAgoSinceNow
    }

    func configure(_ notification: Notification2) {
        hasDistance = false

        bodyLabel.text = notification.body
        titleLabel.text = notification.title
        dateLabel.text = notification.createdAt?.timeAgoSinceNow
        pictureImageView.setImage(notification.picture)
    }

}
