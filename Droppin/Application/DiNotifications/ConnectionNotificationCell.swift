//
//  ConnectionNotificationCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 16/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class ConnectionNotificationCell: NotificationCell, ConnectionApi, UserApi {
    
    override class var identifier: String { return "ConnectionNotificationCell" }

    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var interestsLabel: UILabel!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var stackWidth: NSLayoutConstraint!
    
    var showProfileView: (((UIButton) -> Void))?
    var declineAction: (((UIButton) -> Void))?
    var acceptAction: (((UIButton) -> Void))?
    
    var type: CellType?
    
    enum CellType: String {
        case normal = "normal", request = "request", expired = "expired"
    }
    
    func setupData(_ notification: DiNotification) {
        // Show and hide accept/decline buttons
        if let connection = notification.connection {
            switch connection.status {
            case .pending:
                titleLabel.text = notification.senderName
                bodyLabel.text = "Wants to connect with you"
                type = .request
            case .accepted:
                titleLabel.text = notification.senderName
                bodyLabel.text = "You are now connected"
                type = .expired
            case .declined:
                titleLabel.text = "Connection Declined"
                type = .normal
            case .blocked:
                titleLabel.text = "Connection Blocked"
                type = .normal
            }
            
            // Set accept and decline actions
            showProfileView = { sender in
                self.delegate?.showProfileView(userId: self.notification!.senderId)
            }
            
            acceptAction = { sender in
                self.delegate?.updateConnectionStatusX(connectionId: connection.connectionId, status: .accepted)
            }
            
            declineAction = { sender in
                self.delegate?.updateConnectionStatusX(connectionId: connection.connectionId, status: .declined)
            }
            
            refreshView(for: type!, and: notification)
        }
    }
    
    private func refreshView(for type: CellType, and notification: DiNotification) {
        interestsLabel.text = "..."
        
        switch type {
        case .normal:
            declineButton.isHidden = true
            acceptButton.isHidden = true
            stackWidth.constant = 92
        case .request:
            declineButton.isHidden = false
            acceptButton.isHidden = false
            acceptButton.isUserInteractionEnabled = true
            acceptButton.alpha = 1
            stackWidth.constant = 92
        case .expired:
            declineButton.isHidden = true
            acceptButton.isHidden = false
            acceptButton.isUserInteractionEnabled = false
            acceptButton.alpha = 0.30
            stackWidth.constant = 38
        }
        
        profileButton.setImage(of: notification.senderId)
        loadInterests(userId: notification.senderId)
    }
    
    @IBAction func showProfileView(_ sender: UIButton) {
        self.showProfileView?(sender)
    }
    
    @IBAction func declinePressed(_ sender: UIButton) {
        self.declineAction?(sender)
    }
    
    @IBAction func acceptPressed(_ sender: UIButton) {
        self.acceptAction?(sender)
    }
    
    private func loadInterests(userId: String) {
        loadInterests(userId: userId) { interests in
            var interestsText = ""
            
            for (index, interest) in interests.enumerated() {
                if index > 2 { break }
                if index == 0 {
                    interestsText += interest
                } else {
                    interestsText += ", \(interest)"
                }
            }
            
            self.interestsLabel.text = interestsText
        }
    }
}
