//
//  NotificationsDataSource.swift
//  Droppin
//
//  Created by Madson on 8/30/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftLocation

protocol NotificationsViewControllerDelegate: EventTableViewCellProtocol {
    func errorReceived(_ result: RequestResult?, _ error: DiApiError)
    func showBasicAlert(title: String, message: String)
    func showProfileView(userId: String)
    func showPin(pin: Pin2?)
    func showPinDetailView(pinId: String, for: PinDetailViewController.ActionType?)
}

protocol NotificationsDataSourceDelegate {
    func refresh()
    func updateConnectionStatusX(connectionId: String, status: ConnectionStatus)
    func showBasicAlert(title: String, message: String, viewController: UIViewController)
    func showProfileView(userId: String)
    func showPinDetailView(pinId: String, for actionType: PinDetailViewController.ActionType?)
}

final class NotificationsDataSource: NSObject, DroppinView, PinApi, ConnectionApi, DiNotificationApi {
    
    private(set) var api = DroppinApi()
    private(set) var pins: [Pin] = []
    private(set) var notifications: [DiNotification] = []
    private(set) var notifications2: [Notification2] = []
    private(set) var invitations: [PinRequest2] = []
    private(set) var location: CLLocation? {
        didSet {
            self.tableView?.reloadData()
        }
    }

    var delegate: NotificationsViewControllerDelegate?
    
    internal var segment: Segment = .pins
    private(set) weak var tableView: UITableView?

    override init() {
        super.init()

        Locator.currentPosition(accuracy: .block, onSuccess: { location in
            self.location = location
        }, onFail: { error, last in
            self.location = last
        })
    }
    
    func refresh(_ tableView: UITableView, with segmentIndex: Segment) {
        self.tableView = tableView
        self.segment = segmentIndex
        
        self.tableView?.reloadData()

        switch segmentIndex {
            case .connections: refreshConnnections()
            case .invitations: refreshInvitations()
            case .pins: refreshPins()
            case .all: refreshAll()
        }
    }
        
    func refreshConnnections() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "refreshNotifications")
        
        getNotifications(type: .connection) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                
                self.notifications = data.notifications
                self.tableView?.reloadData()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure:\(error)")
            }
        }
    }
    
    func refreshInvitations() {
        guard let userId = DiHelper.userId else { return }

        PinRequestApi2.getPinRequests(userId, type: .invitation, status: nil) { error, response in
            guard error == nil else {
                debugPrint("NotificationsDataSource - refreshInvitations - error \(error!.localizedDescription)")
                return
            }

            guard let invitations = response?.metaData else {
                debugPrint("NotificationsDataSource - refreshInvitations - metaData is nil")
                return
            }

            self.invitations = invitations
            self.tableView?.reloadData()
        }
    }
    
    func refreshPins() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        DiActivityIndicator.showActivityIndicator()
        
        getHostPins(sort: "-startDate", isForNotifications: true) { diResult in
            switch diResult {
            case .success(let pinsDetailRequestResult):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getHostPinsDetail Success"))
                
                self.pins = pinsDetailRequestResult.pins
                self.tableView?.reloadData()
            case .failure(_, let error):
                print(DiHelper.lineDescription(of: self, andFunction: #function, 
                                               withDescription: "getHostPinsDetail failure: \(error)"))
            }
            
            DiActivityIndicator.hide()
        }
    }

    func refreshAll() {
        NotificationsApi.getNotifications(.all) { (error, response) in
            guard error == nil else {
                debugPrint("NotificationsDataSource - refreshInvitations - error \(error!.localizedDescription)")
                return
            }

            guard let notifications = response?.notifications else {
                debugPrint("NotificationsDataSource - refreshInvitations - metaData is nil")
                return
            }

            self.notifications2 = notifications
            self.tableView?.reloadData()
        }
    }
}

extension NotificationsDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if segment == .invitations {
            let invitation = self.invitations[indexPath.row]
            var actionType: PinDetailViewController.ActionType?
            
            if [.seen, .pending].contains(invitation.status) {
                actionType = invitation.isInviterHostingIt ? .join : .request
            }
            
            if let pinId = invitation.pinId?.pinId {
                delegate?.showPinDetailView(pinId: pinId, for: actionType)
            }
        } else if segment == .all {
            let notification = self.notifications2[indexPath.row]
            guard
                let type = notification.type,
                let senderId = notification.senderId,
                let objectId = notification.objectId
                else {return}
            
            switch type {
            case .connection:
                delegate?.showProfileView(userId: senderId)
            case .all, .pinInvitation, .attendeeArrival:
                delegate?.showPinDetailView(pinId: objectId, for: nil)
            default:
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Action not defined")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rows = CGFloat(0)

        switch segment {
            case .all: rows = UITableViewAutomaticDimension
            case .pins: rows = 300
            case .connections: rows = 67
            case .invitations: rows = UITableViewAutomaticDimension
        }
        
        return rows
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        
        switch segment {
            case .all: rows = notifications2.count
            case .pins: rows = pins.count
            case .connections: rows = notifications.count
            case .invitations: rows = invitations.count
        }

        return rows
    }
}

extension NotificationsDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if segment == .connections, let cell = tableView.dequeue(ConnectionNotificationCell.self) {
            
            let notification = self.notifications[indexPath.row]
            cell.delegate = self
            cell.setupData(notification)
            return cell
            
        } else if segment == .invitations, let cell = tableView.dequeue(NotificationTableCell.self) {

            let invitation = self.invitations[indexPath.row]
            cell.configure(invitation, location)
            return cell
            
        } else if segment == .pins, let cell = tableView.dequeue(EventTableViewCell.self) {
            
            cell.delegate = delegate
            cell.refreshData(pin: self.pins[indexPath.row])
            return cell
            
        } else if segment == .all {

            let notification = self.notifications2[indexPath.row]
            
            if let cell = getCell(for: notification, and: tableView) {
                return cell
            }
        }
        
        let cell = UITableViewCell()
        cell.textLabel?.text = "Not implemented yet"
        return cell
    }
    
    private func getCell(for notification: Notification2, and tableView: UITableView) -> UITableViewCell? {
        guard let type = notification.type else { return nil }
        
        switch type {
        case .attendeeArrival:
            if let cell = tableView.dequeue(ArrivalNotificationCell.self) {
                cell.notification2 = notification
                
                return cell
            }
        default:
            if let cell = tableView.dequeue(NotificationTableCell.self) {
                cell.configure(notification)
                
                return cell
            }
        }
        
        return nil
    }
}

extension NotificationsDataSource: NotificationsDataSourceDelegate {
    
    func showPinDetailView(pinId: String, for actionType: PinDetailViewController.ActionType?) {
        delegate?.showPinDetailView(pinId: pinId, for: actionType)
    }
    
    func showProfileView(userId: String) {
        delegate?.showProfileView(userId: userId)
    }
    
    func refresh() {
        if segment == .pins { refreshPins() }
        else if segment == .connections { refreshConnnections() }
        else if segment == .invitations { refreshInvitations() }
        else if segment == .all { refreshAll() }
    }
    
    func updateConnectionStatusX(connectionId: String, status: ConnectionStatus) {
        DiActivityIndicator.showActivityIndicator()
        
        updateStatus(forConnection: connectionId, status: status) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success: \(data.message ?? "No message")")
                self.refresh()
            case .failure(let result, let error):
                self.delegate?.errorReceived(result, error)
            }
            
            DiActivityIndicator.hide()
        }
    }
}

extension NotificationsDataSource: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var text = "No items"

        switch segment {
        case .all:
            text = "You have no notifications"
        case .pins:
            text = "You have no pins"
        case .connections:
            text = "You have no connection requests"
        case .invitations:
            text = "You have no invitations"
        }

        let font = UIFont.systemFont(ofSize: 18)
        let attributes = [NSAttributedStringKey.font: font]
        let title = NSAttributedString(string: text, attributes: attributes)

        return title
    }
}
