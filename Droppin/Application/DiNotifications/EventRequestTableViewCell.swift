//
//  EventRequestTableViewCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 07/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import SwiftMoment
import SwiftyJSON

protocol EventRequestTableViewCellDelegate {
    func isPinSoon()
}

class EventRequestTableViewCell: UITableViewCell, DiImageLoader, PinRequestApi {
    var api = DroppinApi()
    
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var buttonsStackView: UIStackView!
    
    var delegate: EventRequestTableViewCellDelegate?
    var request: PinRequest!
    var isAwaiting = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.profileImageButton.layer.cornerRadius = self.profileImageButton.imageView!.frame.size.width / 2;
//        self.profileImageButton.clipsToBounds = true;
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.acceptButton.isHidden = true
        self.declineButton.isHidden = true
        self.statusImageView.isHidden = true
        self.undoButton.isHidden = true
    }

    func refreshData(_ request: PinRequest) {
        self.request = request
        
        self.nameLabel.text = self.request.attendeeName
        self.dateLabel.text = moment(request.createdAt).fromNow()
        
        self.profileImageButton.setImage(of: request.attendeeId)
        self.refreshView()
    }
    
    func refreshView() {
        if isAwaiting {
            self.acceptButton.isHidden = true
            self.declineButton.isHidden = true
            self.statusImageView.isHidden = true
            
            self.undoButton.isHidden = false
        } else {
            switch self.request.status {
            case .accepted, .attended, .conflicted, .atPin, .left:
                self.acceptButton.isHidden = true
                self.declineButton.isHidden = true
                self.statusImageView.isHidden = false
                self.statusImageView.image = #imageLiteral(resourceName: "greenCheckmark")
            case .declined:
                self.acceptButton.isHidden = true
                self.declineButton.isHidden = true
                self.statusImageView.isHidden = false
                self.statusImageView.image = #imageLiteral(resourceName: "redCross")
            case .new, .seen, .pending:
                self.acceptButton.isHidden = false
                self.declineButton.isHidden = false
            }
            
            self.undoButton.isHidden = true
        }
    }
    
    func awaitingForUndoing() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.isAwaiting = true
        let when = DispatchTime.now() + 20
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Ready")
            
            self.isAwaiting = false
            self.refreshView()
        }
        
        self.refreshView()
    }
    
    @IBAction func acceptRequest(_ sender: UIButton) {
        acceptRequest(requestId: self.request.pinRequestId, force: false) {result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success: \(data)")
                self.request.status = .accepted
                self.awaitingForUndoing()
                
                if let json = data.metaData?["isPinSoon"] as? JSON, let isPinSoon = json.bool, isPinSoon {
                    self.delegate?.isPinSoon()
                }
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    @IBAction func declineRequest(_ sender: UIButton) {
        declineRequest(requestId: self.request.pinRequestId) {result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success: \(data)")
                self.request.status = .declined
                self.refreshView()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    @IBAction func undoRequest(_ sender: UIButton) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        isAwaiting = false
        
        undoRequest(requestId: self.request.pinRequestId) {result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success: \(data)")
                self.request.status = .seen
                self.refreshView()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    @IBAction func showProfileView(_ sender: UIButton) {
        getUserInfo(self.request.attendeeId) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success: \(data)")
                
                let storyboard = UIStoryboard(name: "ProfileStoryboard", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                
                viewController.refreshData(user: data.user, request: self.request)
                
                self.inputViewController?.show(viewController, sender: nil)
            case .failure(_, let error):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getUserInfo failure: \(error)"))
            }
        }
    }
    
}
