//
//  EventTableViewCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 07/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//
import UIKit
import Dollar
import SwiftDate

protocol EventTableViewCellProtocol: EventRequestTableViewCellDelegate {
    
    func showFirstTimeViewingARequestAlert(alert: UIAlertController)
    func present(_ viewController: UIViewController)
}

class EventTableViewCell: UITableViewCell, DroppinView, PinRequestApi, Identifiable {
    
    static let identifier = "EventRow"
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var attendeesNumberLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var requestsTableView: UITableView!
    
    var delegate: EventTableViewCellProtocol?
    var api = DroppinApi()
    var pin: Pin!
    var requests: [PinRequest] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Round picture borders
        self.eventImageView.layer.cornerRadius = self.eventImageView.frame.size.width / 2;
        self.eventImageView.clipsToBounds = true;
        
        // Round view borders
        self.containerView.layer.cornerRadius = 20;
        self.containerView.clipsToBounds = true;
        
        // Add action to show pin description
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showPinDetail(sender:)))
        eventView.addGestureRecognizer(tapGesture)
        
        // Add table view data source to requestsTableView
        requestsTableView.delegate = self
        requestsTableView.dataSource = self
        
        self.attendeesNumberLabel.text = "..."
        
        self.refreshStatusView()
    }
    
    func refreshData(pin: Pin) {
        self.pin = pin;
        
        self.titleLabel.text = pin.title
        self.dateLabel.text = "\(pin.startDate.colloquial(to: Date())?.capitalized ?? "") \(pin.startDate.string(format: .custom("MMMM dd 'at' hh:mm aa")))"
        self.loadRequestsList()
        
        if let photo = pin.photo {
            self.eventImageView.cldSetImage(publicId: photo, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }
    }
    
    func loadRequestsList() {
        getPinRequests(byPinId: pin.pinId, status: nil, limit: 5) { diResult in
            switch diResult {
            case .success(let data):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getPinRequests Success"))

                self.requests = data.requests
                self.requestsTableView.reloadData()
                self.refreshStatusView()
                self.checkIfFirstTimeViewingARequest()
            case .failure(_, let error):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getPinRequests failure: \(error)"))
            }
        }
    }
    
    func refreshStatusView() {
        self.attendeesNumberLabel.text = String(self.countRequestsBy(type: .accepted))
        
        if self.countRequestsBy(type: .new) > 0 {
            self.statusLabel.text = "You have new requests to join your pin"
            self.statusView.alpha = 1
            self.statusHeightConstraint.constant = 30
        } else {
            self.statusView.alpha = 0
            self.statusHeightConstraint.constant = 0
        }
        
        self.statusView.layoutIfNeeded()
    }
    
    private func checkIfFirstTimeViewingARequest() {
        let newRequestsCount = requests.filter({ $0.status == .new }).count
        let isFirstTimeViewingARequestLoaded = UserDefaults.standard.bool(forKey: UserPropertyKey.isFirstTimeViewingARequestLoaded.rawValue)
        
        if newRequestsCount > 0 && !isFirstTimeViewingARequestLoaded {
            let alert = DiAlert.createAlert(title: "", message: "We recommend only accepting people whose profile picture has a clear view of themselves", dismissTitle: "OK", withDismissAction: nil)
            
            self.delegate?.showFirstTimeViewingARequestAlert(alert: alert)
            UserDefaults.standard.set(true, forKey: UserPropertyKey.isFirstTimeViewingARequestLoaded.rawValue)
        }
    }
    
    func countRequestsBy(type: PinRequestStatus) -> Int {
        let requests = self.requests.filter {
            if type == .accepted {
               return $0.isAccepted
            }
            
            return $0.status == type
        }
        
        return requests.count
    }
    
    @objc func showPinDetail(sender: UITapGestureRecognizer) {
        print("showPinDetail")
        
        //        storyBoa
    }
}

extension EventTableViewCell: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.requests.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestsRow") as! EventRequestTableViewCell
        cell.refreshData(self.requests[indexPath.row])
        cell.delegate = self.delegate
        return cell
    }
}

extension EventTableViewCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "ProfileStoryboard", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        let request = self.requests[indexPath.row]
        let userId = request.attendeeId
        
        viewController.role = .host
        viewController.refreshData(userId: userId, request: request)
        delegate?.present(viewController)
    }
}

