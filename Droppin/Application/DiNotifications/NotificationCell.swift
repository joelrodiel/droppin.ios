//
//  NotificationCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/7/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

class NotificationCell: UITableViewCell, Identifiable {
    class var identifier: String { return "NotificationCell" }
    var notification: DiNotification?
    var delegate: NotificationsDataSourceDelegate?
}

extension NotificationCell: PinApi {
    
    var api: DroppinApi {
        return DroppinApi()
    }
    
    func loadPinImage(_ pinId: String, callback: @escaping (String) -> ()) {
        getPinDetail(pinId) { response, error in
            guard let photo = response?.pin?.photo else { return }
            callback(photo)
        }
    }
}
