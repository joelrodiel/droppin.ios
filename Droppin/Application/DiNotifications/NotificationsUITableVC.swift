import UIKit

class NotificationsUITableVC: UITableViewController, DroppinView, DiNotificationApi {
    var api =  DroppinApi()
    var pinsBySection = [ Section: [Pin] ]()
    let sectionHeaderHeight: CGFloat = 50
    let currentCellHeight: CGFloat = 90
    let previousCellHeight: CGFloat = 80
    var dataAvailable: Bool = false

    enum Section: Int {
        case current = 0, previous
        static let count = 2
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundView?.isHidden = true
        reloadNotifications()
    }

    func reloadNotifications(){
        print("Algo")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        DiActivityIndicator.showActivityIndicator()
        getNotifications(type: nil){ diResult in
            switch diResult {
            case .success(let response):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getNotifications Success"))

                if response.notifications.count > 0 {
                    self.dataAvailable = true
                    self.processNotifications(response.notifications)
                } else {
                    print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No Notifications!"))
                    self.dataAvailable = false
                }
            case .failure(let requestResult, let error):
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
                self.dataAvailable = false
            }
            
            DiActivityIndicator.hide()
            self.tableView.reloadData()
        }
    }

    func processNotifications(_ notifications : [DiNotification]) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
//        notificationsBySection[.current] = notifications.filter {  $0.status == "joinRequest" }
//        notificationsBySection[.previous] = notifications.filter { $0.status == "other" }
    }

    func noItemsDisplay(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let emptyViewController = storyBoard.instantiateViewController(withIdentifier: "emptyView")
        let emptyView = emptyViewController.view
        let label = emptyView?.viewWithTag(3) as! UILabel
        
        label.text = "You have no notifications yet"
        
        self.tableView.backgroundView?.isHidden = false
        self.tableView.backgroundView = emptyView
        self.tableView.separatorStyle = .none
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        return Section.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard dataAvailable else {
            noItemsDisplay()
            return 0
        }

        tableView.backgroundView?.isHidden = true
        self.tableView.separatorStyle = .singleLineEtched

        guard let section = Section(rawValue: section) else {
            return 0
        }

        switch section {
        case .current:
            guard let pins = self.pinsBySection[.current] else {
                return 0
            }
            return pins.count
        case .previous:
            guard let pins = self.pinsBySection[.previous] else {
                return 0
            }
            return pins.count
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        guard let section = Section(rawValue: section) else { return 0 }
        switch section {
        case .current:
            guard let pins = self.pinsBySection[.current], pins.count > 0 else { return 0 }
            return sectionHeaderHeight
        case .previous:
            guard let pins = self.pinsBySection[.previous], pins.count > 0  else {return 0}
            return sectionHeaderHeight
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = Section(rawValue: indexPath.section) else {return 0}
        switch section {
        case .current:
            return currentCellHeight
        case .previous:
            return previousCellHeight
        }
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = Section(rawValue: section), let pins = pinsBySection[section], pins.count > 0  else {return nil}

        switch section {
        case .current:
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "currentPinsHeader")
            let headerCellView = headerCell?.contentView
            headerCellView?.addBorder(side: .bottom, thickness: 1, color: .lightGray)
            return headerCellView
        case .previous:
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "previousPinsHeader")
            let headerCellView = headerCell?.contentView
            headerCellView?.addBorder(side: .bottom, thickness: 1, color: .lightGray)
            return headerCellView
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // @reference https://thatthinginswift.com/switch-unwrap-shortcut/
        let section = Section(rawValue: indexPath.section)
        switch (section) {
        case .some(.current):
            guard let pins = pinsBySection[.current] else {
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "no Pins"))
                let emptyCell = tableView.dequeueReusableCell(withIdentifier: "currentPin", for: indexPath) as! CurrentPinTableViewCell
                return emptyCell
            }
            let pin = pins[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "currentPin", for: indexPath) as! CurrentPinTableViewCell
            cell.refresh(pin)
            return cell
        case .some(.previous):
            guard let pins = pinsBySection[.previous] else {
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "no Pins"))
                let emptyCell = tableView.dequeueReusableCell(withIdentifier: "currentPin", for: indexPath) as! CurrentPinTableViewCell
                return emptyCell
            }
            let pin = pins[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "previousPin", for: indexPath) as! PreviousPinTableViewCell
            cell.refresh(pin)
            return cell
        default:
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "PROBLEM"))
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: "currentPin", for: indexPath) as! CurrentPinTableViewCell
            return emptyCell
        }
    }
}
