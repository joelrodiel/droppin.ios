//
//  NotificationsViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 22/03/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

enum Segment: Int {
    case pins, connections, invitations, all
}

final class NotificationsViewController: UIViewController, DroppinView, UserApi, ConnectionApi {

    @IBOutlet private(set) weak var tableView: UITableView!
    @IBOutlet private(set) weak var segmentedControl: UISegmentedControl!

    private var segmentIndex: Segment = .pins
    
    private(set) var api = DroppinApi()
    private(set) var notificationsDataSource = NotificationsDataSource()
    
    private enum Segue: String {
        case showProfileSegue = "ShowProfileSegue"
        case pinEdit = "pinEdit"
        case showPinDetailViewSegue = "ShowPinDetailViewSegue"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        changeSegment(segmentedControl)
        
        resetPinNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        tableView.delegate = notificationsDataSource
        tableView.dataSource = notificationsDataSource
        tableView.emptyDataSetDelegate = notificationsDataSource
        tableView.emptyDataSetSource = notificationsDataSource
        tableView.tableFooterView = UIView()
        
        notificationsDataSource.delegate = self
    }
    
    @IBAction func changeSegment(_ sender: UISegmentedControl) {
        segmentIndex = Segment(rawValue: sender.selectedSegmentIndex)!
        tableView.removeEmptyView()
        
        switch (segmentIndex) {
        case .pins:
            // TODO: Animate transition
            notificationsDataSource.refresh(tableView, with: .pins)
        case .connections:
            // TODO: Animate transition
            notificationsDataSource.refresh(tableView, with: .connections)
        case .invitations:
            // TODO: Animate transition
            notificationsDataSource.refresh(tableView, with: .invitations)
        case .all:
            // TODO: Animate transition
            notificationsDataSource.refresh(tableView, with: .all)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = segue.destination as? ProfileViewController {
            guard let userId = sender as? String else { return }
            controller.role = .connector
            controller.refreshData(userId: userId)
        } else if let controller = segue.destination as? PinEditController {
            guard let pin = sender as? Pin2 else { return }
            controller.pin = pin
        }
        
        if let controller = segue.destination as? PinDetailViewController {
            guard
                let data = sender as? [String : Any?],
                let pinId = data["pinId"] as? String
                else { return }
            
            let actionType = data["actionType"] as? PinDetailViewController.ActionType
            
            controller.actionType = actionType
            controller.refreshData(pinId: pinId)
            
            return
        }
    }
    
    func resetPinNotifications() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let userId = userId else {return}
        
        resetUserPinNotifications(userId) { result, error in
            if (error != nil) {
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "resetPinNotifications failure: \(error!)"))
            }
        }
    }
}

extension NotificationsViewController: NotificationsViewControllerDelegate {
    
    func showProfileView(userId: String) {
        performSegue(withIdentifier: Segue.showProfileSegue.rawValue, sender: userId)
    }
    
    func showPinDetailView(pinId: String, for actionType: PinDetailViewController.ActionType?) {
        let data: [String : Any?] = ["pinId": pinId, "actionType": actionType]
        performSegue(withIdentifier: Segue.showPinDetailViewSegue.rawValue, sender: data)
    }
    
    func showFirstTimeViewingARequestAlert(alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }
    
    func isPinSoon() {
        showBasicAlert(title: "Pin Soon", message: "This user has already joined another pin happening at the same time. We’ve sent them a notification that they’ve been accepted to this pin.")
    }
    
    func errorReceived(_ result: RequestResult?, _ error: DiApiError) {
        self.showAlert(title: "Error!", result: result, error: error, viewController: self)
    }
    
    func showBasicAlert(title: String, message: String) {
        self.showBasicAlert(title: title, message: message, viewController: self)
    }

    func showPin(pin: Pin2?) {
        performSegue(withIdentifier: Segue.pinEdit.rawValue, sender: pin)
    }
    
    func present(_ viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
