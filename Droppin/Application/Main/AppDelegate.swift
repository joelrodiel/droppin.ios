//
//  AppDelegate.swift
//  Droppin
//
//  Created by Rene Reyes on 10/2/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Branch
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UserApi {
    let api = DroppinApi()
    var window: UIWindow?
    var launched = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        registerForPushNotifications()

        self.launched = true
        
        // Start a Branch Session
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // params will be empty if no data found
                // ... insert custom logic here ...
                guard let params = params as? [String: AnyObject] else { return }
                print("params: %@", params)
                
                let pinId = params["pinId"] as? String
                NotificationCenter.default.post(name: .showQuickPin, object: pinId)
            }
        })
        
        PinLocationManager.sharedManager.startTracking(.foreground)
        checkNetworkStatus()
        
        return true
    }
    
    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        PinLocationManager.sharedManager.startTracking(.background)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        PinLocationManager.sharedManager.startTracking(.foreground)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Droppin")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }

        let token = tokenParts.joined()
//        print("[AppDelegate][application] -> Device Token: \(token)")
        print("----------------------------------------------------------------------")
        print("----------------------------------------------------------------------")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Device Token: \(token)"))
        print("----------------------------------------------------------------------")
        print("----------------------------------------------------------------------")
//        let defaults = UserDefaults.standard
//        defaults.set(token, forKey: "token")
        UserDefaults.standard.set(token  , forKey: UserPropertyKey.token.rawValue)

        self.updateToken() {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "update token completion"))
            
            switch $0 {
            case .success(_):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "token updated!"))
            case .failure(_, let error):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "token update failed! \(error)"))
            }
        }


//
//        let test = defaults.bool(forKey: "userLoggedIn")
//        if ( defaults.bool(forKey: "userLoggedIn")) {
//            self.updateToken(token: token) { (diResult)->() in
//                print("update token completion")
//                switch diResult {
//                case .success(_):
//                    print("[AppDelegate][application][updateToken][result] ->token updated.")
//                    //                self.stopActivityIndicator()
//                    //                DiActivityIndicator.hide()
//                //                self.performSegue(withIdentifier: "mobile", sender: sender)
//                case .failure(_, let error):
//                    print("failure!")
//                }
//            }
//
//        }



    }

    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }

}

extension AppDelegate {
    
    func checkNetworkStatus() {
        DispatchQueue.main.async {
            let connected = Reachability.isConnectedToNetwork()
            
            if !connected, let controller = self.getNavController() {
                DiAlert.alertSorry(message: "App needs internet access to continue.", inViewController: controller)
            }
        }
    }
}
