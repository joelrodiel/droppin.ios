//
//  MainTabsViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 12/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class MainTabsViewController: UITabBarController, UserApi, DroppinView {
    
    var api = DroppinApi()
    private var lastTab: Int?
    
    @IBOutlet weak var tabTitle: UINavigationItem!
    
    enum Tabs: String {
        case profile = "Profile"
        case pins = "My Pins"
        case home = "Home"
        case notifications = "Notifications"
        case settings = "Settings"
        
        var index: Int {
            switch self {
            case .profile: return 0
            case .pins: return 1
            case .home: return 2
            case .notifications: return 3
            case .settings: return 4
            }
        }
    }
    
    private enum SegueIdentifier: String {
        case profileSegue = "ProfileSegue"
        case myPinsSegue = "myPinsSegue"
        case firstTimeSignIn = "FirstTimeSignInSegue"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        super.viewWillAppear(animated)
        
        updateNotificationsBadge()
    }
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        delegate = self
        
        // Set default tab
        selectedIndex = Tabs.home.index
        lastTab = Tabs.home.index

        // Observer to show profile view from AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProfileView(_:)), name: .showProfileView, object: nil)

        // Observer to show profile view from AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(self.showCreatePinView(_:)), name: .showCreatePinView, object: nil)
        
        setupView()
        checkIfFirstTimeSignedIn()
    }
    
    private func setupView() {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.tabBarItem], for: .normal)
        UITabBar.appearance().tintColor = UIColor.aquaMarine
    }
    
    private func checkIfFirstTimeSignedIn() {
        if AccountSettings.isVisibleFirstTimeSignedIn() {
            AccountSettings.setFirstTimeViewAs(as: false)
            performSegue(withIdentifier: SegueIdentifier.firstTimeSignIn.rawValue, sender: nil)
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        // Set tab title
        var title = "Tabs"

        if let index = tabBar.items?.index(of: item) {
            removeNavigationItems(currentTab: index)
            
            switch index {
            case Tabs.profile.index:
                title = Tabs.profile.rawValue
            case Tabs.pins.index:
                title = Tabs.pins.rawValue
            case Tabs.home.index:
                title = Tabs.home.rawValue
            case Tabs.notifications.index:
                title = Tabs.notifications.rawValue
            case Tabs.settings.index:
                title = Tabs.settings.rawValue
            default:
                break
            }
        }

        self.tabTitle.title = title
    }
    
    private func removeNavigationItems(currentTab: Int) {
        // Remove items from navbar if the tab selected is different of the last one
        if lastTab != currentTab {
            navigationItem.leftBarButtonItem = nil
            navigationItem.rightBarButtonItem = nil
            
            lastTab = currentTab
        }
    }
    
    func updateNotificationsBadge() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let userId = userId else {return}
        
        getUserBadgeCount(userId) {result, error in
            if (result != nil) {
                let app = UIApplication.shared // Application object
                
                print("Uhhhh.. \(result!)")
                
                guard let badgeCount: Int = result!["metaData"] as? Int else { // Guard metaData
                    app.applicationIconBadgeNumber = 0 // Reset badge
                    return
                }
                
                if badgeCount > 0 {
                    app.applicationIconBadgeNumber = badgeCount
                } else {
                    app.applicationIconBadgeNumber = 0
                }

                NotificationCenter.default.post(name: .updateBadge, object: badgeCount)
            } else if (error != nil) {
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getUserBadgeCount failure: \(error!)"))
            }
        }
    }
    
    @objc func showProfileView(_ notification: Notification) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let userId = notification.object as? String else {
            return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "showProfileView failure: userId is not defined")
        }
        
        self.performSegue(withIdentifier: SegueIdentifier.profileSegue.rawValue, sender: userId)
    }
    
    @objc func showCreatePinView(_ notification: Notification) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        self.performSegue(withIdentifier: SegueIdentifier.myPinsSegue.rawValue, sender: notification.object)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if segue.identifier == SegueIdentifier.profileSegue.rawValue, let userId = sender as? String, let controller = segue.destination as? ProfileViewController {
            controller.refreshData(userId: userId)
        }
        
        if segue.identifier == SegueIdentifier.myPinsSegue.rawValue {
            guard let controller = segue.destination as? CreatePinForm1VC else { return }
            controller.initialPinTitle = sender as? String
        }
    }
}

extension MainTabsViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = viewController as? ProfileViewController {
            controller.refreshData(userId: userId!)
            controller.role = .owner
        }
        
        updateNotificationsBadge()
    }
}

extension Notification.Name {
    static let showProfileView = Notification.Name("showProfileView")
    static let showCreatePinView = Notification.Name("showCreatePinView")
}
