//
//  ConnectionTableCell.swift
//  Droppin
//
//  Created by Madson on 10/28/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class CheckView: UIView {
    var isSelected: Bool {
        get { return true }
        set {
            backgroundColor = newValue ? .aquaMarine : .border
        }
    }
}

final class ConnectionTableCell: UITableViewCell, Identifiable {

    static var identifier: String = "ConnectionTableCell"

    @IBOutlet private var photoImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var checkView: CheckView!

    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        photoImageView.image = #imageLiteral(resourceName: "profilePicLogo")
        checkView.isSelected = false
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        for button in stackView.subviews {
            if let button = button as? UIButton {
                button.isUserInteractionEnabled = false
                button.contentEdgeInsets = UIEdgeInsetsMake(6, 12, 6, 12)
            }
        }
    }

    func configure(_ user: User2, selected: Bool) {
        if let userId = user.userId {
            photoImageView.setImage(of: userId)
        }
        
        nameLabel.text = user.firstName
        checkView.isSelected = selected
    }
}
