//
//  ConnectionsListController.swift
//  Droppin
//
//  Created by Madson on 10/28/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import KVNProgress

final class ConnectionsListController: UITableViewController {

    @IBOutlet var inviteButton: UIBarButtonItem!

    var pinId: String?

    private var userId = DiHelper.userId
    private var selectedUserIds: [String] = [String]()
    private var connections: [Connection2] = [Connection2]() {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let userId = DiHelper.userId else { return }

        enableInviteButtonIfNeeded()

        ConnectionApi2.getConnections(userId) { (error, response) in
            guard error == nil else { return }
            guard
                let response = response,
                let connections = response.metaData else { return }

            self.connections = connections
        }
    }

    @IBAction private func close(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction private func invite(_ sender: UIBarButtonItem) {
        guard let pinId = pinId else { return }

        KVNProgress.show(withStatus: "Sending invitations")

        PinInvitationAPI.invite(usersIds: selectedUserIds, to: pinId) { (error, result) in
            guard error == nil else {
                KVNProgress.showError(withStatus: "Error when sending invitations")
                return
            }

            KVNProgress.showSuccess(withStatus: "Invitations sent", completion: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
}

// MARK: - TableView delegates

extension ConnectionsListController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connections.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ConnectionTableCell.self, for: indexPath)!

        if let user = userForIndexPath(indexPath), let userId = user.userId {
            let selected = selectedUserIds.contains(userId)
            cell.configure(user, selected: selected)
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let user = userForIndexPath(indexPath) else { return }

        if let userId = user.userId {
            if selectedUserIds.contains(userId), let index = selectedUserIds.firstIndex(of: userId) {
                removeUserId(at: index)
            } else {
                addUserId(userId)
            }
        }

        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}

// MARK: - Helpers

extension ConnectionsListController {
    func addUserId(_ userId: String) {
        selectedUserIds.append(userId)
        enableInviteButtonIfNeeded()
    }

    func removeUserId(at index: Int) {
        selectedUserIds.remove(at: index)
        enableInviteButtonIfNeeded()
    }

    func enableInviteButtonIfNeeded() {
        inviteButton.isEnabled = selectedUserIds.count > 0
    }

    func userForIndexPath(_ indexPath: IndexPath) -> User2? {
        let connection = connections[indexPath.row]

        if let userId = userId {
            if let user = connection.userOneId, let id = user.userId, id != userId {
                return user
            } else if let user = connection.userTwoId, let id = user.userId, id != userId {
                return user
            }
        }

        return nil
    }
}
