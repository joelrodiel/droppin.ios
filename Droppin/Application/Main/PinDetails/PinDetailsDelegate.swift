//
//  PinDetailsViewControllerDelegate.swift
//  Droppin
//
//  Created by Rene Reyes on 11/11/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

protocol PinDetailsDelegate {
    func getPin(completion: @escaping (Pin) -> Void)
}

