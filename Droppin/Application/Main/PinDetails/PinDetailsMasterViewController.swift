//
//  PinDetailsMasterViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/15/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

protocol PinRefresh {
    func refresh(_ pin: Pin2?) -> CGFloat
}

protocol PinDetailsMasterViewControllerDelegate {
    func didTapRequest()
    func pinLoaded(visibleLength: CGFloat)
}

class PinDetailsMasterViewController: UIViewController, DroppinView, PinApi {
    
    var delegate: PinDetailsMasterViewControllerDelegate?
    
//    var activityIndicator = UIActivityIndicatorView()
//    var isDroppinTester = UserDefaults.standard.bool(forKey: UserPropertyKey.isTester.rawValue)
    var api = DroppinApi()
    var pin: Pin?
//    var pinDetailsDotViewController: PinDetailsDotViewController?

    var pinDetailsUIPageViewController: PinDetailsUIPageViewController?
    
    override func viewDidLoad() {
        print("LOAD---------------------------------------------------------------------")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
//        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        super.viewDidLoad()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("----------------------------------------------------------------------")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "prepare segues: " + String(describing: segue.identifier)))
        
        if (segue.identifier ==  "pinDetailsUIPageViewSegue") {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "segue pinDetailsUIPageViewSegue"))
            let pinDetailsUIPageViewController = segue.destination as? PinDetailsUIPageViewController
            
            guard let pdvc = pinDetailsUIPageViewController else {
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "NO pinDetailsUIPageViewController"))
                return
            }
            pdvc.delegate = self
            self.pinDetailsUIPageViewController = pdvc
//            self.pinDetailsUIPageViewController = pdvc as? PinRefresh
        }
        
//        if (segue.identifier ==  "pinDetailsDotViewSegue") {
//            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "segue pinDetailsDotViewSegue"))
//            pinDetailsDotViewController = (segue.destination as? PinDetailsDotViewController)!
//        }
        
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "End"))
    }
    
    
    
    func refresh(pinId: String ) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "PinId: \(pinId)")
        DiActivityIndicator.showActivityIndicator()

        getPinDetail(pinId) { (response, error) in
            DiActivityIndicator.hide()
            
            guard error == nil else {
                self.showAlert(title: "Error!", error: error!, viewController: self)
                return
            }

            guard let response = response else {
                return
            }

            if let pdvc = self.pinDetailsUIPageViewController {
                print("YES REfresh!!!")
                let visibleLength = pdvc.refresh(response.pin)
                self.delegate?.pinLoaded(visibleLength: visibleLength)
            } else {
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "no pinDetailsUIPageViewController to refresh"))
            }
        }
    }

}


//extension PinDetailsMasterViewController: PinDetailsDelegate {
//    func getPin(completion: @escaping (Pin) -> Void)
//        completion
//    }
//}


extension PinDetailsMasterViewController: UIPageViewControllerDelegate {
    
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        print("----------------------------------------------------------------------")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
    }
    
    
//    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
//        super.present(viewControllerToPresent, animated: flag, completion: completion)
//        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
//    }
//
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        print("----------------------------------------------------------------------")
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        
        guard let pinDetailsView = pageViewController.viewControllers?.first  else {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No View Controllers!"))
            return
        }
        
//        guard let pinDetailsDotVC = pinDetailsDotViewController else {
//            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No pinDetailsDotViewController found!"))
//            return
//        }
        
//        print("----------------------------------------------------------------------")
//        print("tag: " + String(describing: pinDetailsView.view.tag))
//        pinDetailsDotVC.pageControl.currentPage = pinDetailsView.view.tag
    }
}
