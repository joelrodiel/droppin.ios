//
//  PinDetailsUIPageViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/15/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class PinDetailsUIPageViewController: UIPageViewController, DroppinView {
    
//    var activityIndicator =  UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        if let firstViewController = viewControllerList.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    lazy var viewControllerList:[UIViewController] = {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //Storyboard Id
        let pinDetailViewIdentifiers = [ "pinDetailsView1"]
//        let pinDetailViewIdentifiers = [ "pinDetailsView1","pinDetailsView2"]
        let pinDetailViews = pinDetailViewIdentifiers.map { storyBoard.instantiateViewController(withIdentifier: $0)}
        
        return  pinDetailViews
    }()
    
}

extension PinDetailsUIPageViewController: PinRefresh {
    func refresh(_ pin: Pin2?) -> CGFloat {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        
        for case let view as PinRefresh in viewControllerList {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "View as PinRefresh"))
            return view.refresh(pin)
        }
        
        return 0
    }
}

extension PinDetailsUIPageViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        guard let vcIndex = viewControllerList.index(of: viewController) else {return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard viewControllerList.count > previousIndex else {return nil}
        
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        guard let vcIndex = viewControllerList.index(of: viewController) else {return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count > nextIndex else {return nil}
        
        return viewControllerList[nextIndex]
    }

}
