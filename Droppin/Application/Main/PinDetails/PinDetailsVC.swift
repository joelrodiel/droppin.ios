import UIKit
import SwiftLocation
import SwiftDate
import CoreLocation
import JGProgressHUD
import DateToolsSwift

class PinDetailsVC: UIViewController, DroppinView, PinRefresh, JoinPinApi, UserApi, PinApi, PinRequestApi {

    var api: DroppinApi = DroppinApi()
    var pin: Pin2?
    
    @IBOutlet weak var hudContainer: UIView!
    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var pinHostPhoto: UIButton!
    @IBOutlet weak var pinTitle: UILabel!
    @IBOutlet weak var pinDescription: UILabel!
    @IBOutlet weak var pinDistance: UILabel!
    @IBOutlet weak var pinStartTime: UILabel!
    @IBOutlet weak var pinEndTime: UILabel!
    @IBOutlet weak var agesLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var join: UIButton!
    @IBOutlet weak var joinCount: UIButton!
    @IBOutlet weak var invited: UIButton!
    @IBOutlet weak var invitedCount: UIButton!
    @IBOutlet weak var atPin: UIButton!
    @IBOutlet weak var atPinCount: UIButton!
    
    var delegate: PinDetailsDelegate?
    private var hud = JGProgressHUD(style: .dark)
    private let VISIBLE_LENGTH: CGFloat = 342
 
    private enum Segues: String {
        case RequestsList = "RequestsList"
        case showPinDetailViewController = "ShowPinDetailViewController"
        case showGuests = "ShowGuestsSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        
        pinTitle.text = ""
        pinDescription.text = ""
        pinDistance.text = "... mi"
        pinStartTime.text = ""
        pinEndTime.text = "-----"
        pinImage.image = nil
        pinHostPhoto.setImage(nil, for: .normal)
        pinHostPhoto.roundBorders()

        mainButton.backgroundColor = DIColors.droppinRed
    }

    @IBAction func requestToJoin(_ sender: UIButton) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        guard let pin = pin else {
            debugPrint("PinDetailsVC - requestToJoin - pin is nil")
            return
        }

        // If the pin owns user
        if isThisMine(toCompare: pin.hostId?.userId) {
            self.performSegue(withIdentifier: Segues.showPinDetailViewController.rawValue, sender: nil)
            return
        }

        let now = Date()
        let chunk = TimeChunk(seconds: 0, minutes: 0, hours: 48, days: 0, weeks: 0, months: 0, years: 0)
        let trueEndDate = pin.endDate.add(chunk)

        if pin.terminated {
            BasicAlert(title: "This pin has ended.").present(from: self)
            return
        } else if let seconds = trueEndDate.component(.second, to: now) {
            if (seconds >= 1 && seconds <= 5) {
                BasicAlert(title: "This pin has just ended.").present(from: self)
                return
            } else if (seconds > 0) {
                BasicAlert(title: "This pin has ended.").present(from: self)
                return
            }
        }
        
        let status = pin.requestToJoinStatus ?? ""
        
        switch status {
        case "has-not-requested":
            self.requestTo(join: sender)
        case "has-requested":
            self.requestTo(unjoin: sender)
        default:
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "No request to join status")
        }
    }

    func requestTo(unjoin sender: UIButton) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "requestTo unjoin"))
        
        guard let pinId = pin?.pinId else {
            debugPrint("PinDetailsVC - requestTo - pinId is nil")
            return
        }

        hud.textLabel.text = "Requesting"
        hud.show(in: hudContainer)
                
        deleteRequest(byPinId: pinId) { (diResult)->() in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "send request to join"))
            switch diResult {
            case .success(_):
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.textLabel.text = "Done"
                self.hud.dismiss(afterDelay: 2)

                print("success")
                sender.setTitle("Request to join", for: .normal)
                self.pin!.requestToJoinStatus = "has-not-requested"
            case .failure(let requestResult, let error):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.dismiss()
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
            }
        }
        
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "End"))
    }
    
    func requestTo(join sender: UIButton) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "requestTo join"))
        
        hud.textLabel.text = "Requesting"
        hud.show(in: hudContainer)
        
        requestToJoin(pinId: pin?.pinId) { (diResult)->() in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "send request to join"))
            switch diResult {
            case .success(_):
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.textLabel.text = "Request sent"
                self.hud.dismiss(afterDelay: 2)
                
                print("success")
                sender.setTitle("Tap to un-request", for: .normal)
                self.pin!.requestToJoinStatus = "has-requested"
            case .failure(_, _):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.dismiss()
                BasicAlert(title: "This pin has ended.").present(from: self)
            }
        }
    }
    
    func refresh(_ pin: Pin2?) -> CGFloat {
        guard let pin = pin else { return 0 }

        pinDistance.text = "... mi"
        
        self.pin = pin

        pinTitle.text = pin.title
        pinDescription.text = pin.description
        pinHostPhoto.setImage(#imageLiteral(resourceName: "Profile"), for: .normal)
        
        if let photo = pin.photo {
            self.pinImage.cldSetImage(publicId: photo, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }
        
        if let hostId = pin.hostId?.userId {
            self.pinHostPhoto.setImage(of: hostId)
        }

        if let pinJoinCount = pin.joinCount {
            joinCount.setTitle(String(pinJoinCount), for: .normal)
        } else {
            joinCount.setTitle("", for: .normal)
        }

        mainButton.setTitle("Request to join", for: .normal)
        mainButton.backgroundColor = DIColors.droppinRed
        mainButton.isEnabled = true
        
        if let requestToJoinStatus = pin.requestToJoinStatus {
            if (requestToJoinStatus == "has-requested") {
                mainButton.setTitle("Un-request", for: .normal)
                mainButton.backgroundColor = DIColors.droppinRed
            }
        }

        if isThisMine(toCompare: pin.hostId?.userId) {
            mainButton.setTitle("View Pin", for: .normal)
            mainButton.backgroundColor = DIColors.droppinGreen
        }
        
        let startDate = DateInRegion(absoluteDate: pin.startDate)
        pinStartTime.text = startDate.string(format: .custom("h:mm a"))
        
        let endDate = DateInRegion(absoluteDate: pin.endDate)
        pinEndTime.text = endDate.string(format: .custom("h:mm a"))
        
        Locator.currentPosition(accuracy: .block, onSuccess: { location in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Hello"))

            guard let coordinates = pin.coordinates else { return }

            let pinLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
            let distanceInMeters = location.distance(from: pinLocation)
            let distanceInMiles = distanceInMeters * 0.000621371192
            self.pinDistance.text = String(format: "%.1f mi", distanceInMiles)
        }, onFail: { err, last in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "hello error"))
            print("Failed to get location: \(err)")
        })
        
        self.checkPinStatistics()
        
        return calculateViewHeight()
    }

    private func calculateViewHeight() -> CGFloat {
        pinDescription.sizeToFit()
        var height = VISIBLE_LENGTH + pinDescription.frame.size.height

//        Removing this because I test on a iOS 10.3 device
//        if let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom {
//            height = height + bottom
//        }
        
        return height
    }
    
    func checkPinStatistics() {
        guard let pin = self.pin else { return }

        getPinStatistic(pin.pinId) { result in
            switch result {
            case .success(let data):
                self.atPinCount.setTitle(String(data.pinStatistic.atPin), for: .normal)
                self.invitedCount.setTitle(String(data.pinStatistic.invited), for: .normal)
                self.joinCount.setTitle(String(data.pinStatistic.joined), for: .normal)
                self.agesLabel.text = "\(data.pinStatistic.minAge) - \(data.pinStatistic.maxAge)"
                
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    @IBAction func showProfileView(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showProfileViewSegue", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        prepareQuickPin()
        
        if let viewController = segue.destination as? ProfileViewController {
            guard let userId = pin?.hostId?.userId else { return }
            
            viewController.isAbleToSendDirectMessages = true
            viewController.refreshData(userId: userId, pin: self.pin!)
            viewController.role = .checker

            return
        } else if let controller = segue.destination as? PinDetailViewController, let pin = pin?.toPin1() {
            controller.pin = pin
        } else if let controller = segue.destination as? RequestsListController, let sender = sender as? UIButton {
            controller.pin = pin

            if [atPin, atPinCount].contains(sender) {
                controller.pinRequestStatus = PinRequestStatus.atPin
            } else if [join, joinCount].contains(sender) {
                controller.pinRequestStatus = PinRequestStatus.accepted
            } else if [invited, invitedCount].contains(sender) {
                controller.pinRequestType = PinRequestType.invitation
            }
        } else if let navigation = segue.destination as? UINavigationController,
            let controller = navigation.topViewController as? ConnectionsListController, let pinId = pin?.pinId {
            controller.pinId = pinId
        } else if let controller = segue.destination as? GuestsViewController {
            guard
                let data = sender as? [String : Any?],
                let type = data["type"] as? GuestsViewController.ListType
                else { return }

            let request = data["request"] as? PinRequest2
            let status = request?.status
            let hostId = pin?.hostId?.userId
            let role = getRole(for: hostId, and: status)

            controller.pinId = pin?.pinId
            controller.pinTitle = pin?.title
            controller.role = role
            controller.type = type
            controller.isCurrent = [.yellow, .green].contains(pin?.pinStatus)

            if let status = pin?.pinStatus, [.green, .red, .yellow].contains(status) {
                controller.hostId = hostId
            }
            
            return
        }
    }

    private func prepareQuickPin() {
        // Launch Quick Pin after segue view has been closed
        if let pinId = pin?.pinId {
            NotificationCenter.default.post(name: .prepareQuickPin, object: pinId)
        }
    }
    
    @IBAction func sharingButtonPressed(_ sender: UIButton) {
        guard
            let pin = pin,
            let pinId = pin.pinId,
            let hostId = pin.hostId?.userId,
            let description = pin.description
            else { return }

        let pinSharing = PinSharing(pinId, hostId, pin.title, description, pin.startDate, pin.endDate, pin.photo)

        sender.isEnabled = false

        pinSharing.share(in: self, and: sender) { type in
            sender.isEnabled = true
            
            if type == .connections {
                self.prepareQuickPin()
            }
        }
    }
}

extension PinDetailsVC: PinDetailRoles {

    @IBAction func showAttendees(_ sender: UIButton) {
        showGuestsView(for: .attendee)
    }

    @IBAction func showAcceptees(_ sender: UIButton) {
        showGuestsView(for: .joined)
    }

    @IBAction func showInvitees(_ sender: UIButton) {
        showGuestsView(for: .invitee)
    }

    private func showGuestsView(for type: GuestsViewController.ListType) {
        guard let pinId = pin?.pinId else { return }
        DiActivityIndicator.showActivityIndicator()

        PinRequestApi2.getPinRequest(pinId, type: nil, status: nil) { error, response in
            DiActivityIndicator.hide()

            let response = response?.metaData
            let data: [String : Any?] = ["request": response, "type": type]

            self.performSegue(withIdentifier: Segues.showGuests.rawValue, sender: data)
        }
    }
}
