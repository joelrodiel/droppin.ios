//
//  PinSharing.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/11/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftDate
import SwiftyJSON
import Firebase

final class PinSharing: DroppinView, PinRequestApi {
    
    private (set) var api = DroppinApi()
    private let pinId: String
    private let hostId: String
    private let pinTitle: String
    private let pinDescription: String
    private let pinStartDate: Date
    private let pinEndDate: Date
    private let pinImage: String?
    
    enum SharingType { case media, connections, presented }
    
    init(_ pinId: String, _ hostId: String, _ pinTitle: String, _ pinDescription: String, _ pinStartDate: Date, _ pinEndDate: Date, _ pinImage: String?) {
        self.pinId = pinId
        self.hostId = hostId
        self.pinTitle = pinTitle
        self.pinDescription = pinDescription
        self.pinStartDate = pinStartDate
        self.pinEndDate = pinEndDate
        self.pinImage = pinImage
    }
    
    func share(in controller: UIViewController, and sourceView: UIView?, completion: ((SharingType) -> ())?) {
        let share = UIAlertAction(title: "Share", style: .default) { action in
            self.getSharingMessage() { message in
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "START")
                
                var url: String?
                
                if let pinImage = self.pinImage {
                    url = CDNManager().cloudinary.createUrl().generate(pinImage, signUrl: true)
                }
                
                Analytics
                    .logEvent("pin_share_sent", parameters: ["pinId" : self.pinId])
                
                let tags = ["pin-shared"]
                let campaign = "pin-shared"
                let link = BranchLink(title: self.pinTitle, description: self.pinDescription, pinId: self.pinId, imageURL: url, campaign: campaign, tags: tags)
                
                link.share(message: message, viewController: controller) { _ in
                    completion?(.media)
                }
            }
        }
        
        let invite = UIAlertAction(title: "Connections", style: .default) { action in
            let storyNav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteConnectionsNavigationController")
            
            if let navigation = storyNav as? UINavigationController, let inviteController = navigation.topViewController as? ConnectionsListController {
                inviteController.pinId = self.pinId
                controller.present(navigation, animated: true, completion: nil)
            }
            
            completion?(.connections)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = sourceView ?? controller.view
        alert.popoverPresentationController?.sourceRect = calculateSourceRect(controller: controller, and: sourceView)
        
        alert.addAction(share)
        alert.addAction(invite)
        alert.addAction(cancel)
        
        controller.present(alert, animated: true) {
            completion?(.presented)
        }
    }
    
    private func calculateSourceRect(controller: UIViewController, and sourceView: UIView?) -> CGRect {
        var x: CGFloat = controller.view.frame.width / 2
        var y: CGFloat = 0
        
        if let frame = sourceView?.frame {
            x = frame.width / 2
            y = frame.height
        }
        
        return CGRect(x: x, y: y, width: 0, height: 0)
    }
    
    private func getSharingMessage(completion: @escaping ((String) -> Void)) {
        if isThisMine(toCompare: hostId) {
            var message = "Join my pin \(pinTitle)."
            let now = Date()
            
            if now.isBetween(date: pinStartDate, and: pinEndDate) {
                message = "Happening right now"
            }
            
            if now.isBetween(date: pinStartDate - 1.hour, and: pinStartDate) {
                message = "Happening soon"
            }
            
            return completion(message)
        } else {
            getPinRequest(byPinId: pinId) { result in
                switch result {
                case .success(let data):
                    if let json = data.metaData?["status"] as? JSON, let status = json.string, PinRequestStatus(rawValue: status) == .accepted {
                        return completion("I just joined \(self.pinTitle), you down to come?")
                    }
                case .failure(_, _):
                    break
                }
                
                completion("You down to come with me to \(self.pinTitle)?")
            }
        }
    }
}
