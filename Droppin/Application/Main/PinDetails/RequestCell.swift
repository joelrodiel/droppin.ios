//
//  RequestCell.swift
//  Droppin
//
//  Created by Madson on 10/27/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class RequestCell: UITableViewCell, Identifiable {
    static let identifier: String = "RequestCell"

    @IBOutlet private var firstLine: UILabel?
    @IBOutlet private var secondLine: UILabel?
    @IBOutlet private var thirdLine: UILabel?
    @IBOutlet private var photoImageView: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        firstLine?.text = nil
        secondLine?.text = nil
        thirdLine?.text = nil
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        firstLine?.text = nil
        secondLine?.text = nil
        thirdLine?.text = nil
    }

    func configure(_ request: PinRequest2, userRole: UserRole?) {
        guard
            let userRole = userRole,
            let attendee = request.attendeeId else {
                return
        }

        if userRole != .outsider {
            let gender = attendee.gender?.capitalized ?? ""

            firstLine?.text       = "\(attendee.fullName) - \(gender)"
            secondLine?.text      = attendee.description
            thirdLine?.text       = attendee.interestsDescription
            photoImageView?.image = #imageLiteral(resourceName: "profilePicLogo")
            photoImageView?.setImage(attendee.photo, placeholder: #imageLiteral(resourceName: "profilePicLogo"))
        } else {
            firstLine?.text  = "Pin user"
            secondLine?.text = attendee.description
            thirdLine?.text  = attendee.interestsDescription
        }
    }
}
