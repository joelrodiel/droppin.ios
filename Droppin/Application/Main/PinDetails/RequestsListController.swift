//
//  RequestsListController.swift
//  Droppin
//
//  Created by Madson on 10/26/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class RequestsListController: UITableViewController, PinRequestApi {

    internal var api = DroppinApi()
    private var isLoading: Bool = true
    private var userId: String?
    private var userRole: UserRole?
    private var requests: [PinRequest2] = [PinRequest2]() {
        didSet {
            tableView.reloadData()
        }
    }

    var pin: Pin2?
    var pinRequestType: PinRequestType?
    var pinRequestStatus: PinRequestStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTitle()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let profileController = segue.destination as? ProfileViewController else { return }
        guard let selectedUserId = userId else { return }

        profileController.userId = selectedUserId
    }
}

extension RequestsListController {
    private func setTitle() {
        if pinRequestStatus == .atPin {
            navigationItem.title = "At Pin"
        } else if pinRequestStatus == .accepted {
            navigationItem.title = "Joined"
        } else {
            navigationItem.title = "Invited"
        }
    }

    private func loadData() {
        isLoading = true
        guard
            let pin = pin,
            let pinId = pin.pinId,
            let userId = DiHelper.userId else {
                isLoading = false
            return
        }

        let status = pinRequestStatus

        pin.userRole(userId, completion: { (role) in

            self.userRole = role

            PinRequestApi2.getPinRequests(pinId, status: status) { (error, response) in
                self.isLoading = false

                guard error == nil else { return }
                guard let requests = response?.metaData else { return }

                self.requests = requests
            }
        })
    }
}

extension RequestsListController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RequestCell.self, for: indexPath)!
        let request = requests[indexPath.row]
        cell.configure(request, userRole: userRole)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = requests[indexPath.row]
        userId = request.attendeeId?.userId
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension RequestsListController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var text = "No items"
        
        if isLoading {
            text = "Loading..."
        } else {
            if pinRequestStatus == .atPin {
                text = "No users at pin"
            } else if pinRequestStatus == .accepted {
                text = "No users joined"
            } else {
                text = "No users invited"
            }
        }

        let font = UIFont.systemFont(ofSize: 18)
        let attributes = [NSAttributedStringKey.font: font]
        let title = NSAttributedString(string: text, attributes: attributes)
        
        return title
    }
}
