//
//  SearchTopMenuViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/6/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit


//enum TableSection: Int {
//    case current = 0, previous
//    static let count = 2
//}
//
//if let tableSection = TableSection(rawValue: section), let eventDetails = events[tableSection]{
//    return eventDetails.count
//}

enum RangeEnumeration: Double, CustomStringConvertible {
    case oneMile = 1
    case fiveMiles = 5
    case tenMiles = 10
    case twentyMiles = 20
    case fiftyMiles = 50
    
    static let allCases: [RangeEnumeration] = [oneMile, fiveMiles, tenMiles, twentyMiles, fiftyMiles]
    
    var description: String {
        switch self {
            case .oneMile:
            return "1 Mile"
            case .fiveMiles:
            return "5 Miles"
            case .tenMiles:
            return "10 Miles"
            case .twentyMiles:
            return "20 Miles"
            case .fiftyMiles:
            return "50 Miles"
        }
    }
}

protocol PinFilterViewControllerDelegate {
    func didTapConnect(range: RangeEnumeration, search: String?)
 
}

class PinFilterViewController: UIViewController, DroppinView  {
    var rangeDataSource = RangeEnumeration.allCases
    var delegate: PinFilterViewControllerDelegate?
    
    @IBOutlet weak var rangePicker: UIPickerView!
    @IBOutlet weak var search: UITextField!
    
    override func viewDidLoad() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        super.viewDidLoad()
        self.rangePicker.delegate = self
        self.rangePicker.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
    }
    
    
    @IBAction func connectAction(_ sender: UIButton) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "start"))
        guard let pinFilterViewControllerDelegate = delegate else {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No Delegate!"))
            view.endEditing(true)
            return
        }
        
        
        pinFilterViewControllerDelegate.didTapConnect(
            range: rangeDataSource[rangePicker.selectedRow(inComponent: 0)],
            search: search.text
            )
        view.endEditing(true)
    }

//    func update(_ observableState: ObservableState, sliderType: SliderType) {
//        switch (observableState) {
//        case .hasExpanded:
//            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
//        default:
//            return
//
//        }
//    }


}

extension PinFilterViewController: Observer {
    func update(_ observableState: ObservableState, sliderType: SliderType) {
        switch (observableState) {
        case .hasExpanded:
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
            search.text = ""
            search.becomeFirstResponder()
        case .hasCollapsed:
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
            view.endEditing(true)
        default:
            return

        }
    }
}

extension PinFilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.rangeDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String( describing: rangeDataSource[row])
    }
    
}



//func update(_ observableState: ObservableState, sliderType: SliderType) {
    //        print("[VC][update] -> " + String(describing: observableState))

    //        switch (observableState) {
    //        case .hasExpanded:
    //            grayView.isHidden = false
    //            UIView.animate(withDuration: 0.3, animations: {
    //                self.grayView.alpha = 0.6 } , completion: nil)
    //        case .hasCollapsed:
    //            UIView.animate(withDuration: 0.3, animations: {
    //                self.grayView.alpha = 0
    //            }, completion: {
    //                (value: Bool) in
    //                self.grayView.isHidden =
    //                true
    //
    //            })
    //
    //        default:
    //            return
    //        }

//}



//extension PinFilterViewController: DISliderDelegate {
//    func willSlideIntoView() {
//        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
//    }
//}

