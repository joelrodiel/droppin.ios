//
//  PinMapViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/17/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SwiftLocation
import JGProgressHUD

protocol MainMapVCDelegate {
    func pinTouched(pinId: String)
}

class MainMapVC: UIViewController, DroppinView, PinApi {
    
    @IBOutlet weak var mapView: MKMapView!
    var api =  DroppinApi()  //use sharedInsyance
    var delegate: MainMapVCDelegate?
    var pins: [PinSummary] = [PinSummary]()
    var annotations: [MKAnnotation]? {
        get {
            return pins.map {$0.annotation!}
        }
    }

    private var hud = JGProgressHUD(style: .dark)
    private var locationLoaded = false
    private var lastRegion: MKCoordinateRegion?
    var loadedByFilter = false
    var locationManager: CLLocationManager? // = CLLocationManager()
    var currentRegionWithPins: MKCoordinateRegion?
    var isRefreshing = false
    var forceRefresh = true
    var range: RangeEnumeration? =  RangeEnumeration.twentyMiles
    var searchString: String?
    
    override func viewWillDisappear(_ animated: Bool) {
        print("[pmVC][viewWillDisappear] -> Start")
        guard locationManager != nil else {
            print("No location manager!")
            return
        }
        
        locationManager?.stopUpdatingLocation()
        //        locationManager?.delegate = nil
        //        locationManager = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: ""))
        super.viewDidDisappear(animated)
        
    }
    
    override func viewDidLoad() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        super.viewDidLoad()
        
        //Doesnt allow the map to show
        let tiles = MKTileOverlay(urlTemplate: nil)
        tiles.canReplaceMapContent = true
        mapView.add(tiles)

        mapView.delegate = self
        mapView.isZoomEnabled = false
        mapView.isScrollEnabled = false
        mapView.isUserInteractionEnabled = true
        
        //Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Creating a location manager and setting self as delegate."))
            
            let lm = CLLocationManager()
            lm.delegate = self
            lm.desiredAccuracy = kCLLocationAccuracyBest
            lm.requestAlwaysAuthorization()
            lm.requestWhenInUseAuthorization()
            lm.startUpdatingLocation()
            locationManager = lm
        }
        
        
        DispatchQueue.main.async {
            print("----------------------------------------------------------------------")
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "startUpdaintLocation()"))
            if (self.locationManager == nil) {
                print("No LocationManager!!!!")
            }
            
            self.locationManager?.startUpdatingLocation()
        }
        
        if #available(iOS 11.0, *) {
            self.addComponentsToMap()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        super.viewDidAppear(animated)
        forceRefresh = true
        locationLoaded = false
        DispatchQueue.main.async {
            print("start updating location")
            if (self.locationManager == nil) {
                print("No LocationManager!!!!")
            }
            self.locationManager?.startUpdatingLocation()
        } 
    }

    @available(iOS 11.0, *)
    func addComponentsToMap() {
        // Add compass
        mapView.showsCompass = false  // Hide built-in compass
        
        let compassButton = MKCompassButton(mapView: mapView)   // Make a new compass
        compassButton.compassVisibility = .visible          // Make it visible
        
        mapView.addSubview(compassButton) // Add it to the view
        
        // Position it as required
        compassButton.translatesAutoresizingMaskIntoConstraints = false
        compassButton.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -12).isActive = true
        compassButton.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 12).isActive = true
        
        let scale = MKScaleView(mapView: mapView)
        scale.scaleVisibility = .visible // always visible
        view.addSubview(scale)
    }
    
    func createMKMapRect(for region:MKCoordinateRegion) -> MKMapRect {
        let topLeft = CLLocationCoordinate2D(latitude: region.center.latitude + (region.span.latitudeDelta/2), longitude: region.center.longitude - (region.span.longitudeDelta/2))
        let bottomRight = CLLocationCoordinate2D(latitude: region.center.latitude - (region.span.latitudeDelta/2), longitude: region.center.longitude + (region.span.longitudeDelta/2))
        let a = MKMapPointForCoordinate(topLeft)
        let b = MKMapPointForCoordinate(bottomRight)
        return MKMapRect(origin: MKMapPoint(x:min(a.x,b.x), y:min(a.y,b.y)), size: MKMapSize(width: abs(a.x-b.x), height: abs(a.y-b.y)))
    }
    
    func refreshPins(range: RangeEnumeration, search: String?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.hud.textLabel.text = "Loading"
            self.hud.show(in: self.view)
        })
        
        self.range = range
        self.searchString = search
        self.forceRefresh = true
        self.loadedByFilter = true
        
        Locator.currentPosition(accuracy: .block,
            onSuccess: { location in
                let region = MKCoordinateRegion(center: location.coordinate, span: self.coordinateSpanFromRange())
                self.updatePinsView(centerLocation: location, region: region)
                
                self.hud.dismiss()
            },
            onFail: { err, last in
                self.hud.dismiss()
                
                if let location = last {
                    let region = MKCoordinateRegion(center: location.coordinate, span: self.coordinateSpanFromRange())
                    self.updatePinsView(centerLocation: location, region: region)
                }
            }
        )
    }
    
    func updatePinsView(centerLocation: CLLocation, region: MKCoordinateRegion){
        
        if isRefreshing {
//            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Currently refreshing. do nothing"))
            return
        }
        
        if forceRefresh {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Force Refresh set"))
            forceRefresh = false
            refreshPins(centerLocation:centerLocation, region:region)
            return
            
        }
        
        guard self.currentRegionWithPins != nil else {
//            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No Region with Pins yet. do nothing."))
//            refreshPins(centerLocation)
            return
        }
        
        if (isLocationInsideRange(location: centerLocation, range: self.currentRegionWithPins!)) {
//            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Location still inside so no refreshing. no nothing."))
//            print("still inside")
            return
        }
//        let rect = createMKMapRect(for: currentRegionWithPins)
//        let point = MKMapPointForCoordinate(centerLocation.coordinate)
//
//        if ( MKMapRectContainsPoint(rect, point)) {
//            print("still inside region")
//            return
//        }
        
        refreshPins(centerLocation: centerLocation, region: region)
    }
    
    
    func isLocationInsideRange (location: CLLocation, range: MKCoordinateRegion) -> Bool {
        let rect = createMKMapRect(for: range)
        let point = MKMapPointForCoordinate(location.coordinate)
        
//        if ( MKMapRectContainsPoint(rect, point)) {
//            print("still inside region")
//            return true
//        }
//        return false
        
        return MKMapRectContainsPoint(rect, point)
    }
    
    func refreshPins( centerLocation: CLLocation, region: MKCoordinateRegion){
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        isRefreshing = true
        
        guard let range = self.range else {
            print("there's a problem")
            return
        }
        
        getPinsSummary(nearMe: centerLocation, withinRange: range, withSearchString: searchString ){ diResult in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getPinsSummary->Result..."))
            switch diResult {
            case .success(let requestResult):
                print("success...")

                self.mapView.removeAnnotations(self.mapView.annotations)

                guard let pins = requestResult.pinsSummary else {
                    print("empty pins...")
//                    self.mapView.removeAnnotations(self.mapView.annotations)
                    self.isRefreshing = false
                    return
                }
                
                self.pins = pins

                guard let annotations = self.annotations else {
                    print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No annotations found"))
                    self.isRefreshing = false
                    return
                }

//                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "adding annotations. count: \(annotations.count)"))
//                self.mapView.removeAnnotations(self.mapView.annotations)
                self.mapView.addAnnotations(annotations)
                
                self.currentRegionWithPins = region
                self.isRefreshing = false

                self.zoomMapFor(annotations)

                if pins.count == 0 {
                    self.displayNoPinsPopup()
                }
            case .failure(let requestResult, let error):
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
                self.isRefreshing = false
            }
        }
    }
    
    func displayNoPinsPopup() {
        let closeAction = UIAlertAction(title: "Close", style: .cancel)
        let createPinAction = UIAlertAction(title: "Create Pin", style: .default) { _ in
            NotificationCenter.default.post(name: .showCreatePinView, object: self.searchString)
            self.searchString = nil
        }

        var message = "Currently there are no pins, create a pin that everyone can join!"
        
        if let search = searchString, search.count == 0, loadedByFilter {
            message = "Currently there are no pins. Be the first to drop a pin in this area!"
        }
        
        let alert = UIAlertController(title: "No Pin", message: message, preferredStyle: .alert)

        alert.addAction(closeAction)
        alert.addAction(createPinAction)

        self.present(alert, animated: true)
        self.loadedByFilter = false
    }

    func grayOutBackground() {
        if (mapView.subviews.count > 0 ) {
            mapView.subviews[0].alpha = 0.9
        }
    }
    
    // One degree of latitude is always  111 kilometers (69 miles).
    func coordinateSpanFromRange() -> MKCoordinateSpan {

        guard let range = self.range else {
            print("something wrong with range")
            return MKCoordinateSpan(latitudeDelta: 20/69, longitudeDelta: 20/69)
        }
        
        let delta: Double =  range.rawValue / 69
        return MKCoordinateSpan(latitudeDelta: delta, longitudeDelta: delta)

    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
            print("didExitRegion!")
    }
    

}

extension MainMapVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locationLoaded, !loadedByFilter {
            return
        }
        
        guard let location = locations.last else { return }
        let center =  CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: coordinateSpanFromRange())
        updatePinsView(centerLocation: location, region: region)
        lastRegion = region
        
        locationLoaded = true
    }
}

extension MainMapVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: circleOverlay)
            circleRenderer.strokeColor = UIColor.gray
            circleRenderer.lineWidth = 3
            return circleRenderer
        }

        if let pinOverlay = overlay as? PinOverlay {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "PinOverlay"))
            let pinRenderer = PinOverlayRenderer(overlay: pinOverlay)
            return pinRenderer
        }

        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        if annotation is MKUserLocation { return nil }
        let pinAnnotationView = PinAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
        return pinAnnotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: " Selected a pin?"))
        if let pinAnnotationView = view as? PinAnnotationView, let pinAnnotation = pinAnnotationView.annotation as? PinAnnotation{
//            pinAnnotationView.image = #imageLiteral(resourceName: "Pin Redness")

//            UIView.transition(with: pinAnnotationView, duration: 0.3, options: [.curveEaseIn], animations: {
//                pinAnnotationView.alpha = 1
//            }, completion: { _ in
//                pinAnnotationView.alpha = 0
//            })
            
            UIView.animateKeyframes(withDuration: 0.75, delay: 0, options: [], animations: {
                
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5, animations: {
                    pinAnnotationView.alpha = 0
                    pinAnnotationView.tintColor = UIColor.yellow
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                    pinAnnotationView.alpha = 1
                    pinAnnotationView.tintColor = nil
                })
                
            }, completion: {_ in
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "I'm done")
            })
            
//            pinAnnotationView.alpha = 0.1
            
            
//            pinAnnotationView.configureWith(animationDuration: 2, annotationImage: UIImage(named: "Your-Annotation-Image")
            
            
            
            guard let mainMapDelegate = delegate else {
                print("No delegate assiged!")
                return
            }

            mainMapDelegate.pinTouched(pinId: pinAnnotation.pinId)
            mapView.deselectAnnotation(view.annotation, animated: true)
        }
    }
}

//    func createPinAnnotation(id: Int, title: String, subtitle: String, type: String,  _ long: Double, _ lat: Double) -> PinAnnotation{
//
////            let coordinate = CLLocationCoordinate2DMake(long, lat)
////            let title = title
////            let subtitle = subtitle
//
////            let typeRawValue = 1
//
//        let annotation = PinAnnotation(id: id , coordinate: CLLocationCoordinate2DMake(long, lat), title: title, subtitle: subtitle, type: PinType.bluePin)
//
//        return annotation
//
//
////        let location = CLLocationCoordinate2DMake(long, lat)
////        let myAnnotation = MKPointAnnotation()
////        myAnnotation.coordinate = location
////        myAnnotation.title = title
////
////        return myAnnotation
//        //        myAnnotation.subtitle = "Come visit me"
//        //                mapView.addAnnotation(myAnnotation)
//    }
    
//    func getPins( pinsData: [Dictionary<String, Any>] ){
//        for pinData in pinsData {
//            let geo = pinData["geo"] as!  Dictionary<String, Double>
//
//            let pin = Pin(
//                id: pinData["id"] as! Int,
//                title:  pinData["title"] as! String,
//                //                overlay: createPinOverlay( geo["long"]!,  geo["lat"]!),
//                overlay: PinOverlay( geo["long"]!,  geo["lat"]!),
//                //                annotation: createPinAnnotation(id: pinData["id"],title: pinData["title"] as! String,  subtitle: pinData["subTitle"] as! String, type: "redPin"  ,geo["long"]!,  geo["lat"]!)
//                annotation: PinAnnotation(id: pinData["id"] as! Int,
//                                          coordinate: CLLocationCoordinate2DMake(geo["long"]!, geo["lat"]!),
//                                          title: pinData["title"] as! String,
//                                          subtitle: pinData["subTitle"] as! String,
//                                          type: PinType.bluePin
//                )
//            )
//
//            pins.append(pin)
//        }
//
//    }
    
    
extension MainMapVC {
    private func zoomMapFor(_ annotations: [MKAnnotation]) {
        let coordinates = annotations.map { $0.coordinate }

        if let region = self.zoomableRegionFor(coordinates) {
            self.mapView.setRegion(region, animated: true)
        } else if let region = lastRegion {
            self.mapView.setRegion(region, animated: true)
        }
    }

    private func zoomableRegionFor(_ coordinates: [CLLocationCoordinate2D]) -> MKCoordinateRegion? {
        guard coordinates.count > 0 else { return nil }

        var maxLat: CLLocationDegrees = -90.0
        var maxLon: CLLocationDegrees = -180.0
        var minLat: CLLocationDegrees = 90.0
        var minLon: CLLocationDegrees = 180.0

        for item in coordinates {
            maxLat = (item.latitude > maxLat) ? item.latitude : maxLat
            minLat = (item.latitude < minLat) ? item.latitude : minLat
            maxLon = (item.longitude > maxLon) ? item.longitude : maxLon
            minLon = (item.longitude < minLon) ? item.longitude : minLon
        }

        let latitude = (maxLat + minLat) / 2
        let longitude = (maxLon + minLon) / 2
        let latitudeDelta = (maxLat - minLat) + 0.0255
        let longitudeDelta = (maxLon - minLon) + 0.0255

        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
        let region = MKCoordinateRegion(center: center, span: span)

        return region
    }
}
 


