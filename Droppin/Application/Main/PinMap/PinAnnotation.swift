//
//  PinAnnotation.swift
//  Droppin
//
//  Created by Rene Reyes on 11/18/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import MapKit

class PinAnnotation: NSObject, MKAnnotation {
    
    
    private var pin: PinCore
    
    var pinId: String {return pin.pinId}
    var title: String? { return pin.title }
    var subtitle: String? { return nil }
    var coordinate: CLLocationCoordinate2D {return pin.coordinates}
    var status: PinStatus {return pin.status}
    
    
//    var pinId: String
//    var title: String?
//    var subtitle: String?
//    var coordinate: CLLocationCoordinate2D
//    var status: PinStatus?
    
    init(_ pin: PinCore) {
        self.pin = pin
    }
    

    
//    init(pinId: String,  coordinates: CLLocationCoordinate2D) {
//        self.pinId = pinId
//        self.coordinate = coordinates
//        self.title = nil
//        self.subtitle = nil
//        self.status = nil
//    }
//
//    convenience init(_ pin: Pin) {
//        self.init(pinId: pin.pinId, coordinates: pin.coordinates)
//        title = pin.title
//        status = pin.status
//    }
}
