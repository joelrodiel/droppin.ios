//
//  PinAnnotationView.swift
//  Droppin
//
//  Created by Rene Reyes on 11/18/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import MapKit

class PinAnnotationView: MKAnnotationView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        print("[PinAnnotationView][init] -> Start")
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        guard let pinAnnotation = self.annotation as? PinAnnotation else { return }
//        guard let pinStatus = pinAnnotation.status else { return }
        image = pinAnnotation.status.image()
        canShowCallout = false
        rightCalloutAccessoryView = UIButton(type: .infoDark)
    }
    
}
