//
//  PinOverlay.swift
//  Droppin
//
//  Created by Rene Reyes on 11/18/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import MapKit

class PinOverlay: NSObject, MKOverlay {
    let coordinate: CLLocationCoordinate2D
    let boundingMapRect: MKMapRect
    
    init(_ long: Double, _ lat: Double) {
        coordinate = CLLocationCoordinate2DMake(long, lat)
        boundingMapRect = MKMapRectMake(0,0,10,10)
    }
}
