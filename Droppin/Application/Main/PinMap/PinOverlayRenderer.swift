//
//  PinOverlayRenderer.swift
//  Droppin
//
//  Created by Rene Reyes on 11/18/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import MapKit

class PinOverlayRenderer: MKOverlayRenderer {
    var pinImage: UIImage
    
    override init(overlay: MKOverlay) {
        pinImage = UIImage(named: "Pin Blue")!   //60x86
        super.init(overlay: overlay)
    }
    
    //        guard let image = UIImage(named: "Pin Blue") else {
    //            return
    //        }
    //        let pinButton = UIButton(type: .custom)
    //        pinButton.setImage(image, for: .normal)
    //        pinButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
    //        pinButton.frame = CGRect(x: 100, y: 100, width: 20, height: 20)
    ////        btn.addTarget(self.navigationController, action: #selector(CustomGalleryViewController.showAlbums(_:)), for:  UIControlEvents.touchUpInside)
    //        let title = "my first dynamic pin!"
    //        pins.append(Pin(id: 1,  title:title, button:pinButton))
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        guard let pinImageReference = pinImage.cgImage else {
            return
        }
        let drawRect = self.rect(for: overlay.boundingMapRect)
        context.scaleBy(x: 1.5, y: -2)
        context.translateBy(x: 0.0, y: -drawRect.size.height)
        context.draw(pinImageReference, in: drawRect)
    }
}
