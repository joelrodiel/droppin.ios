import UIKit

enum SideMenuSection: Int, CustomStringConvertible{
    case profile = 0, myPins, settings, devSettings
    var description: String {
        switch self {
        case .profile: return "Profile"
        case .myPins: return "My Pins"
//        case .notifications: return "Notifications"
        case .settings: return "Settings"
        case .devSettings: return "Dev Settings"
        }
    }
}


protocol SideMenuViewControllerDelegate {
    func didTapSection(_ sideMenuSection:SideMenuSection)
}

class SideMenuVC: UIViewController {
    var delegate: SideMenuViewControllerDelegate?
    @IBOutlet weak var myPins: UIButton!
    @IBOutlet weak var notificationsBadgeButton: SSBadgeButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        notificationsBadgeButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 125)
    }

    @IBAction func sectionAction(_ sender: UIButton) {
        guard let sideMenuVCDelegate = delegate else {
            print("[smVC][sectionAction] -> No delegate was assigned!")
            return
        }
        sideMenuVCDelegate.didTapSection(SideMenuSection( rawValue: sender.tag)!)
    }
}
