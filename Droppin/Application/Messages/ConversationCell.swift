//
//  MessageCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 30/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit

class ConversationCell: UITableViewCell, DiImageLoader, DroppinView {
    var api = DroppinApi()
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var pinLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var status: UIView!
    
    private var _conversation: Conversation?
    var conversation: Conversation? {
        get {return _conversation}
        set {
            _conversation = newValue
            refreshView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupStyle()
    }
    
    func setupStyle() {
        profileButton.roundBorders()
        
        status.roundBorders()
        status.isHidden = true
        
        timeLabel.text = ""
        pinLabel.text = ""
    }
    
    func refreshView() {
        if let conversation = self.conversation, let receiver = conversation.receiver {
            profileButton.setImage(of: receiver.userId)
            
            titleLabel.text = receiver.fullName
            descriptionLabel.text = conversation.lastMessage?.message
            
            if let lastMessage = conversation.lastMessage {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm aa"
                
                timeLabel.text = formatter.string(from: lastMessage.createdAt)
                status.isHidden = isThisMine(toCompare: lastMessage.senderId) || lastMessage.status != .new
            }
            
            if let title = conversation.pinTitle, let hostId = conversation.pinHostId {
                let isHost = DiHelper.userId == hostId
                pinLabel.text = isHost ? "Joined to your pin \"\(title)\"" : "Host of \"\(title)\""
            }
        }
    }
}
