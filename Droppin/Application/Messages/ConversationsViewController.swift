//
//  ConversationsViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 30/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit

class ConversationsViewController: UIViewController, DroppinView, ConversationApi, DiNotificationApi {
    var api = DroppinApi()
    var conversations: [Conversation] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    private enum Segues: String {
        case SendMessageSegue = "SendMessageSegue"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.reloadConnections()
    }
    
    func reloadConnections() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        getConversations() { result in
            switch result {
            case .success(let data):
                self.prepare(conversations: data.conversations)
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    func prepare(conversations: [Conversation]?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.conversations = conversations ?? []
        self.tableView.reloadData()
        
        if self.conversations.count == 0 {
            return self.tableView.displayEmptyView(title: "You have no messages")
        }
        
        self.tableView.removeEmptyView()
        self.markMessagesAsRead()
    }
    
    private func markMessagesAsRead() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        readNotifications(by: .message) { result in
            switch result {
            case .success(_):
                NotificationCenter.default.post(name: .updateBadge, object: 0)
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.description)")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = segue.destination as? MessagesViewController, let conversation = sender as? Conversation {
            controller.conversation = conversation
        }
    }
}

extension ConversationsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: Segues.SendMessageSegue.rawValue, sender: conversations[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension ConversationsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell") as! ConversationCell
        
        cell.conversation = conversations[indexPath.row]
        
        return cell
    }
}
