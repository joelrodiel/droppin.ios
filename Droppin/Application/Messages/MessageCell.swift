//
//  MessageCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftMoment

class MessageCell: UITableViewCell {
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var status2Image: UIImageView!
    @IBOutlet weak var leadingContraint: NSLayoutConstraint!
    @IBOutlet weak var trailingContraint: NSLayoutConstraint!
    
    private let HORIZONTAL_SPACE: CGFloat = 16
    private var _message: Message?
    
    var message: Message? {
        get {return _message}
        set {
            _message = newValue
            refreshView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.messageView.layer.cornerRadius = 10;
        self.messageView.clipsToBounds = true;
    }
    
    func refreshView() {
        if let message = self.message {
            messageLabel.text = message.message
            dateLabel.text = moment(message.createdAt).format("hh:mm aaa")
            
            self.changeStatus(status: message.status)
            
            if message.isThisForMe() {
                trailingContraint.constant = HORIZONTAL_SPACE * 3
                leadingContraint.constant = HORIZONTAL_SPACE
            } else {
                leadingContraint.constant = HORIZONTAL_SPACE * 3
                trailingContraint.constant = HORIZONTAL_SPACE
            }
        }
    }
    
    func changeStatus(status: MessageStatus) {
        switch status {
        case .new:
            self.statusImage.isHidden = true
            self.status2Image.isHidden = true
        case .sent:
            self.statusImage.isHidden = false
            self.status2Image.isHidden = true
        case .seen:
            self.statusImage.isHidden = false
            self.status2Image.isHidden = false
        }
    }
}
