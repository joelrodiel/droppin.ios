//
//  MessageSocketClient.swift
//  Droppin
//
//  Created by Isaias Garcia on 10/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import SwiftyJSON

class MessageSocketClient: DiSocketClient {
    
    private let KEY = "CONVERSATION"
    private let KEY_FOR_STATUS = "STATUS"
    private var _conversationId: String
    
    var conversationId: String {
        get { return _conversationId }
    }
    
    init(conversationId: String) {
        self._conversationId = conversationId
        super.init()
    }
    
    func listen(forMessage callback: @escaping (Message) -> ()) {
        super.listen(event: "\(KEY)-\(conversationId)") { data in
            if data.count == 0 {
                return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Data not valid")
            }
            
            do {
                let message = try MessageResponse.loadMessage(json: JSON(data[0]))
                
                if message.isThisForMe() {
                    return callback(message)
                }
            } catch let error {
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "JSON not valid. Error: \(error)")
            }
        }
    }
    
    func listen(forStatus callback: @escaping (ResponseForStatus) -> ()) {
        super.listen(event: "\(KEY)-\(conversationId)-\(KEY_FOR_STATUS)") { data in
            if data.count == 0 {
                return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Data not valid")
            }
            
            let json = JSON(data[0])
            
            do {
                let messageId: String = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "messageId")
                let status: MessageStatus? = MessageStatus(rawValue: RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "status") ?? "")
                
                if let status = status {
                    return callback(ResponseForStatus(messageId: messageId, status: status))
                }
            } catch let error {
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "JSON not valid. Error: \(error)")
            }
        }
    }
    
    struct ResponseForStatus {
        let messageId: String
        let status: MessageStatus
        
        init(messageId: String, status: MessageStatus) {
            self.messageId = messageId
            self.status = status
        }
    }
}
