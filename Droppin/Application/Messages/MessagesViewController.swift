//
//  MessagesViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit

class MessagesViewController: UIViewController, DroppinView, MessageApi, ConversationApi {
    
    @IBOutlet weak var connectionsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var receiverLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    let api = DroppinApi()
    var socket: MessageSocketClient?
    var pagination = ApiPagination()
    
    var conversation: Conversation?
    var messages: [Message] = []
    
    private var awaitingForConnection: Bool = false
    
    override func viewDidDisappear(_ animated: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.socket?.disconnect()
        super.viewDidDisappear(animated)
    }
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let conversation = conversation {
            setupView(conversation: conversation)
        }
        
        if awaitingForConnection {
            self.connectionsView.isHidden = true
        }
        
        super.viewDidLoad()
    }
    
    func setupView(forDirect receiverId: String) {
        DiActivityIndicator.showActivityIndicator()
        awaitingForConnection = true
        
        findOrCreateConversation(byReceiverId: receiverId) { result in
            DiActivityIndicator.hide()
            
            switch result {
            case .success(let data):
                guard let conversation = data.conversation else { return self.goBack(message: "Conversation not available") }
                self.setupView(conversation: conversation)
            case .failure(_, let error):
                self.goBack(message: "Conversation not available", error: error)
            }
        }
    }
    
    func setupView(conversation: Conversation) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.conversation = conversation
        self.connectionsView.isHidden = true
        
        self.tableView.estimatedRowHeight = 77
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        self.tableView.addInfiniteScroll { (tableView) -> Void in
            self.reloadMessages()
        }
        
        self.sendButton.isEnabled = false
        self.messageField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.reloadMessages()
        
        self.socket = MessageSocketClient(conversationId: conversation.conversationId)
        self.socket!.listen(forMessage: insertNewMessage)
        self.socket!.listen(forStatus: updateMessageStatus)
        
        receiverLabel.text = conversation.receiver?.fullName
        descriptionLabel.text = ""
        
        if let title = conversation.pinTitle, let hostId = conversation.pinHostId {
            let isHost = DiHelper.userId == hostId
            descriptionLabel.text = isHost ? "Joined to your pin \"\(title)\"" : "Host of \"\(title)\""
        }
    }
    
    func findConversation(connection: Connection) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let receiverId = connection.connecteeId else {
            return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Receiver ID not found")
        }
        
        findOrCreateConversation(byReceiverId: receiverId) { result in
            switch result {
            case .success(let data):
                guard let conversation = data.conversation else {
                    return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Conversation not found")
                }
                
                self.setupView(conversation: conversation)
                self.view.endEditing(true)
            case .failure(let result, let error):
                self.showAlert(title: "Error", result: result, error: error, viewController: self)
            }
        }
    }
    
    func insertNewMessage(message: Message) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.messages.insert(message, at: 0)
        self.tableView.reloadData()
    }
    
    func updateMessageStatus(response: MessageSocketClient.ResponseForStatus) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let index = self.messages.index(where: {$0.messageId == response.messageId}) {
            messages[index].status = response.status
            self.tableView.reloadData()
        }
    }
    
    func reloadMessages() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if !self.pagination.hasNextPage {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "No more messages")
            return self.tableView.finishInfiniteScroll()
        }
        
        getMessages(byConversationId: conversation!.conversationId) { result in
            switch result {
            case .success(let data):
                if let pagination = data.pagination {
                    self.pagination = pagination
                }
                
                self.messages = self.messages + (data.messages ?? [])
                
                self.tableView.finishInfiniteScroll()
                self.tableView.reloadData()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        sendButton.isEnabled = false

        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        sendMessage(byConversationId: conversation!.conversationId, message: self.messageField.text ?? "") { result in
            switch result {
            case .success(let data):
                if let message = data.mrmessage {
                    self.messages.insert(message, at: 0)
                }
                
                self.messageField.text = nil
                self.tableView.reloadData()
            case .failure(let result, let error):
                self.sendButton.isEnabled = true
                self.showAlert(title: "Error!", result: result, error: error, viewController: self)
            }
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.sendButton.isEnabled = textField.text != nil && textField.text!.count > 0
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = segue.destination as? ConnectionsMessagingViewController {
            controller.onSelectedConnection = findConversation
        }
    }
}

extension MessagesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
        
        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        cell.message = messages[indexPath.row]
        
        return cell
    }
}

