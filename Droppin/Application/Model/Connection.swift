//
//  Connection.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

enum ConnectionStatus: String, Codable {
    case pending = "pending"
    case accepted = "accepted"
    case declined = "declined"
    case blocked = "blocked"
}

struct Connection {
    let connectionId: String
    let userOneId: User?
    let userTwoId: User?
    let actionUserId: String
    let status: ConnectionStatus
    let createdAt: Date

    init(connectionId: String, userOneId: User?, userTwoId: User?, actionUserId: String, status: ConnectionStatus, createdAt: Date) {
        self.connectionId = connectionId
        self.userOneId = userOneId
        self.userTwoId = userTwoId
        self.actionUserId = actionUserId
        self.status = status
        self.createdAt = createdAt
    }
    
    var connecteeId: String? {
        get {
            if let connectee = self.connectee {
                return connectee.userId
            }
            
            return nil
        }
    }
    
    var connectee: User? {
        get {
            if let userOne = self.userOneId, userOne.userId == DiHelper.userId {
                return self.userTwoId
            }
            
            if let userTwo = self.userTwoId, userTwo.userId == DiHelper.userId {
                return self.userOneId
            }
            
            return nil
        }
    }
}
