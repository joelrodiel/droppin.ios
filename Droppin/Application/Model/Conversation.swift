//
//  Conversation.swift
//  Droppin
//
//  Created by Isaias Garcia on 30/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

enum ConversationStatus: String {
    case created = "created"
}

struct Conversation {
    let conversationId: String
    let userOneId: User?
    let userTwoId: User?
    let subject: String
    let status: ConversationStatus
    var lastMessage: Message?
    var pinTitle: String?
    var pinHostId: String?
    
    init(conversationId: String, userOneId: User?, userTwoId: User?, subject: String, status: ConversationStatus, lastMessage: Message?) {
        self.conversationId = conversationId
        self.userOneId = userOneId
        self.userTwoId = userTwoId
        self.subject = subject
        self.status = status
        self.lastMessage = lastMessage
    }
    
    var receiverId: String? {
        get {
            if let receiver = self.receiver {
                return receiver.userId
            }
            
            return nil
        }
    }
    
    var receiver: User? {
        get {
            if let userOne = self.userOneId, userOne.userId == DiHelper.userId {
                return self.userTwoId
            }
            
            if let userTwo = self.userTwoId, userTwo.userId == DiHelper.userId {
                return self.userOneId
            }
            
            return nil
        }
    }
}
