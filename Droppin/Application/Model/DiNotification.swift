import Foundation
import SwiftyJSON

enum DiNotificationType: String {
    case connection = "connection", message = "message", pinInvitation = "pinInvitation", attendeeArrival = "attendeeArrival", all = "all"
}

enum DiNotificationStatus: String {
    case new, read
}

struct DiNotification  {
    let notificationId: String
    let receiverId: String
    let senderId: String
    let senderName: String
    let title: String
    let body: String
    let objectId: AnyObject
    let type: DiNotificationType
    let notify: Bool
    let status: DiNotificationStatus
    let createdAt: Date

    init(notificationId: String, receiverId: String, senderId: String, senderName: String, title: String,
         body: String, objectId: AnyObject, type: DiNotificationType, notify: Bool, status: DiNotificationStatus,
         createdAt: Date) {
        self.notificationId = notificationId
        self.receiverId = receiverId
        self.senderId = senderId
        self.senderName = senderName
        self.title = title
        self.body = body
        self.objectId = objectId
        self.type = type
        self.notify = notify
        self.status = status
        self.createdAt = createdAt
    }
    
    var connection: Connection? {
        get {
            var connection: Connection?
            
            if let json = self.objectId as? JSON {
                do {
                    connection = try ConnectionResponse.loadConnection(json: json)
                } catch let error {
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                }
            }
            
            return connection
        }
    }
}
