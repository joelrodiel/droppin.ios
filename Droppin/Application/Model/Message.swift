//
//  Message.swift
//  Droppin
//
//  Created by Isaias Garcia on 30/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

enum MessageStatus: String {
    case new = "new"
    case sent = "sent"
    case seen = "seen"
}

struct Message {
    let messageId: String
    let senderId: String
    let conversationId: String
    let message: String
    var status: MessageStatus
    let createdAt: Date
    let updatedAt: Date
    
    init(messageId: String, senderId: String, conversationId: String, message: String, status: MessageStatus, createdAt: Date, updatedAt: Date) {
        self.messageId = messageId
        self.senderId = senderId
        self.conversationId = conversationId
        self.message = message
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
    
    func isThisForMe() -> Bool {
        return senderId != DiHelper.userId
    }
}
