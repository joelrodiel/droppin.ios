//
//  PinAddress.swift
//  Droppin
//
//  Created by Isaias Garcia on 22/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import CoreLocation

struct PinAddress {
    var street: String?
    var city: String?
    var state: String?
    var zip: String?
    var location: CLLocationCoordinate2D?

    var fullAddress: String? {
        if let street = street, let city = city, let state = state {
            return "\(street), \(city), \(state)"
        }

        return nil
    }
    
    func isComplete() -> Bool {
        return street != nil && city != nil && state != nil && zip != nil && location != nil
    }
}
