//
//  PinRequest.swift
//  Droppin
//
//  Created by Isaias Garcia on 09/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

enum PinRequestStatus: String, Codable {
    case new = "new"
    case seen = "seen"
    case declined = "declined"
    case accepted = "accepted"
    case left = "left"
    case atPin = "at-pin"
    case attended = "attended"
    case conflicted = "conflicted"
    case pending = "pending"
}

enum PinRequestType: String, Codable {
    case request = "request"
    case invitation = "invitation"
}

struct PinRequest  {
    let pinRequestId: String
    let pinId: String
    let attendeeId: String
    let pinTitle: String?
    let attendeeName: String?
    let type: PinRequestType
    var status: PinRequestStatus
    let createdAt: Date
    let updatedAt: Date
    var pin: Pin?
    
    init(pinRequestId: String, pinId: String, attendeeId: String, pinTitle: String?, attendeeName: String?, type: PinRequestType, status: PinRequestStatus, createdAt: Date, updatedAt: Date) {
        self.pinRequestId = pinRequestId
        self.pinId = pinId
        self.attendeeId = attendeeId
        self.pinTitle = pinTitle
        self.attendeeName = attendeeName
        self.type = type
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - PinRequest helpers

extension PinRequest {
    var isCurrent: Bool {
        let currentDate = Date()
        let condition1 = self.status == .accepted || self.status == .atPin || self.status == .left
        let condition2 = self.pin!.startDate <= currentDate
        let condition4 = self.pin!.status != .black

        return condition1 && condition2 && condition4
    }

    var isAccepted: Bool {
        return [.accepted, .left, .atPin, .attended, .conflicted].contains(self.status)
    }

    var isJoined: Bool {
        let currentDate = Date()
        let condition1 = [.accepted, .left, .atPin].contains(self.status)
        let condition2 = self.pin!.startDate >= currentDate
        let condition3 = self.pin!.endDate <= currentDate

        return condition1 && (condition2 || condition3)
    }

    var isPrevious: Bool {
        let condition1 = self.pin!.status == .black || self.pin!.status == .gray
        let condition2 = self.status == .attended

        return condition1 && condition2
    }

    var isConflicted: Bool {
        let condition1 = self.status == .conflicted
        return condition1
    }

    var isRequested: Bool {
        let allowedStatuses = [PinRequestStatus.new, PinRequestStatus.seen]
        let condition1 = allowedStatuses.contains(self.status)
        return condition1
    }
    
    var isRequest: Bool {
        let condition1 = type == .request
        let condition2 = self.pin!.status != .black
        return condition1 && condition2
    }
}
