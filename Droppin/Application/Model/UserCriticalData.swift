//
//  UserCriticalData.swift
//  Droppin
//
//  Created by Isaias Garcia on 10/30/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

struct UserCriticalDataRequest: Codable {
    
    var httpStatusCode: Int?
    var message: String?
    var metaData: UserCriticalData?
}

struct UserCriticalData: Codable {
    var mobile: String?
    var email: String?
}
