//
//  AddEventViewBController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/10/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

protocol PinDropCreationDelegate {
    func pinId(_ pinId: String)
}

class CreatePinForm1VC: UIViewController, DroppinView, PinApi {
    
    let api: DroppinApi = DroppinApi()
    var pinId: String?
    
    @IBOutlet weak var pinTitle: DefaultTextField!
    @IBOutlet weak var pinDescription: DefaultTextView!
    @IBOutlet weak var error: UILabel!
    @IBOutlet weak var lengthIndicator: UILabel!
    
    final let DESCRIPTION_MIN_LENGTH: Int = 3
    final let DESCRIPTION_MAX_LENGTH: Int = 300
    
    var initialPinTitle: String?
    private let titleMaxLength = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    private func setupView() {
        pinTitle.delegate = self
        pinTitle.addBottomBorder(.steel)
        pinTitle.text = initialPinTitle
        
        pinDescription.text = ""
        pinDescription.placeholder = "Short description"
        pinDescription.defaultDelegate = self
        pinDescription.minLength = DESCRIPTION_MIN_LENGTH
        pinDescription.maxLength = DESCRIPTION_MAX_LENGTH
        pinDescription.isScrollEnabled = false
        
        lengthIndicator.text = "0/\(DESCRIPTION_MAX_LENGTH)"
        
        updateNextButtonState()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        // If not valid the error message will be displayed
        if !isFormValid() {
            return error.isHidden = false
        }
        
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()
        
        self.createPin(title: pinTitle.text!, description: pinDescription.text!) { diResult in
            switch diResult {
            case .success(let requestResult):
                self.pinId = requestResult.pinId
                self.performSegue(withIdentifier: "addPhotoCreatePinSegue", sender: self)
            case .failure(let requestResult, let error):
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
            }
            
            DiActivityIndicator.hide()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PinDropCreationDelegate {
            print("[PinTitleCreatePinVC][prepare] -> segue PinDropCreationDelegate  pinId: \(String(describing: pinId))")
            vc.pinId(pinId!)
        }
    }
    
    @IBAction func cancelCreation(_ sender: Any) {
        navigationController?.dismiss(animated: true)
    }
}

extension CreatePinForm1VC: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return validateField(textField, range: range, replacementText: string, maxLength: titleMaxLength)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateNextButtonState()
    }
    
    func updateNextButtonState() {
        navigationItem.rightBarButtonItem?.isEnabled = isFormValid()
    }
    
    func isFormValid() -> Bool {
        let titleValidation = !pinTitle.text!.isEmpty && pinTitle.text!.count >= 1 && pinTitle.text!.count <= titleMaxLength
        let descriptionValidation = pinDescription.isValid()
        
        return titleValidation && descriptionValidation
    }
}

extension CreatePinForm1VC: DefaultTextViewDelegate {
    
    func defaultTextView(isValid valid: Bool) {
        updateNextButtonState()
    }
    
    func defaultTextView(allCharacters count: Int) {
        lengthIndicator.text = "\(count)/\(DESCRIPTION_MAX_LENGTH)"
    }
}
