//
//  AddEventViewAController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/10/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

// Upload Photo
class CreatePinForm2VC: PinPhotoController, DroppinView, PinApi, PinDropCreationDelegate {

    var pinId: String?
    let api =  DroppinApi()
    var isPickingPhoto: Bool = false
    
    @IBOutlet weak var imageButton: UIButton!
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        delegate = self
        
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("[AddPhotoCreatePinVC][prepare] -> segue PinDropCreationDelegate  pinId: \(String(describing: pinId))")
        guard let pinId = self.pinId else {
            // @todo throw error
            print("No PinID!")
            return
        }
        if let vc = segue.destination as? PinDropCreationDelegate {
            vc.pinId(pinId)
        }
    }
    @IBAction func nextAction(_ sender: Any) {
        
        guard let pinId = self.pinId else {
            // @todo throw error
            print("No PinID!")
            return
        }
        
        let pinImage = UIImageJPEGRepresentation(imageButton.image(for: .normal)!, 0.2)!
        UserDefaults.standard.set(pinImage, forKey: "pinImage\(pinId)")
        
        DiActivityIndicator.showActivityIndicator()
        navigationItem.rightBarButtonItem?.isEnabled = false

        self.uploadPinImage(pinId: pinId, image: imageButton.image(for: .normal)!) { error, result, _ in
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            
            guard error == nil else {
                BasicAlert(title: "Error!", message: error!.localizedDescription)
                    .present(from: self)
                return
            }

            self.performSegue(withIdentifier: "locationCreatePinSegue", sender: self)

            DiActivityIndicator.hide()
        }
    }
    
    @IBAction func addPhotoAction(_ sender: UIButton) {
        takePhoto(sender)
    }
    
    func pinId(_ pinId: String) {
        self.pinId = pinId
    }
}

extension CreatePinForm2VC: PinPhotoControllerDelegate {
    
    func pinPhoto(didSelected image: UIImage) {
        imageButton.setImage(image, for: .normal)
        navigationItem.rightBarButtonItem?.title = "Next"
    }
}
