//
//  ExtraDirectionsViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 21/05/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import SwiftLocation

class ExtraDirectionsViewController: UIViewController, DroppinView, PinApi {
    
    var api = DroppinApi()
    var pinId: String?
    var address: PinAddress?
    
    @IBOutlet weak var extraDirectionsTextView: DefaultTextView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchBar: DefaultTextField!
    @IBOutlet weak var errorMessage: UILabel!
    
    final let DIRECTIONS_MAX_LENGTH: Int = 100
    
    // Create a completer
    lazy var searchCompleter: MKLocalSearchCompleter = {
        let searchCompleter = MKLocalSearchCompleter()
        searchCompleter.delegate = self
        return searchCompleter
    }()
    
    private var selectedResult: MKLocalSearchCompletion?
    private var searchSource: [MKLocalSearchCompletion]?
    
    enum Segue: String {
        case extraDirectionsToLocation = "ExtraDirectionsToLocationSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.validateForm()
    }
    
    private func setupView() {
        self.extraDirectionsTextView.defaultDelegate = self
        self.extraDirectionsTextView.text = ""
        self.extraDirectionsTextView.placeholder = "Extra directions? Ex. Next to the..."
        self.extraDirectionsTextView.maxLength = DIRECTIONS_MAX_LENGTH
        self.extraDirectionsTextView.isScrollEnabled = false
        
        self.defaultTextView(didChange: extraDirectionsTextView.text)
        
        self.searchBar.delegate = self
        self.searchBar.addBottomBorder(.steel)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        guard let pinId = self.pinId else {
            // @todo throw error
            print("No PinID!")
            return
        }
        
        // If not valid the error message will be displayed
        if !isFormValid() {
            return errorMessage.isHidden = false
        }
        
        guard let street = address?.street,
            let city = address?.city,
            let state = address?.state,
            let location = address?.location
            else {
                return DiAlert.alertSorry(message: "This address is not complete, try a more specific address", inViewController: self)
            }
        
        let zip = address?.zip ?? "-----"

        DiActivityIndicator.showActivityIndicator()
        
        updatePin(pinId: pinId, street: street, city: city, state: state, zip: zip, coordinate: location) { diResult in
            switch diResult {
            case .success(_):

                // No extra directions
                if let text = self.extraDirectionsTextView.text, text.count == 0 {
                    DiActivityIndicator.hide()
                    return self.performSegue(withIdentifier: Segue.extraDirectionsToLocation.rawValue, sender: nil)
                } else {
                    self.updatePin(pinId: pinId, extraDirections: self.extraDirectionsTextView.text) { result in
                        switch result {
                        case .success(_):
                            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                            self.performSegue(withIdentifier: Segue.extraDirectionsToLocation.rawValue, sender: nil)
                        case .failure(let requestResult, let error):
                            self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
                        }

                        DiActivityIndicator.hide()
                    }
                }

            case .failure(let requestResult, let error):
                self.showAlert(title: "Sorry", result: requestResult, error: error, viewController: self)
            }
            
            DiActivityIndicator.hide()
        }
    }
    
    private func getCurrentLocation() {
        DiActivityIndicator.showActivityIndicator()
        
        Locator.currentPosition(accuracy: .city, onSuccess: { location in
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                DiActivityIndicator.hide()
                
                if let error = error {
                    return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.localizedDescription)")
                }
                
                if let placemarks = placemarks, placemarks.count > 0 {
                    self.address = self.extractAddress(placemark: placemarks[0])
                    self.searchBar.text = self.address?.fullAddress
                } else {
                    self.address = nil
                }
                
                self.validateForm()
            }
        }, onFail: { err, last in
            DiActivityIndicator.hide()
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failed to get location: \(err)")
            self.showBasicAlert(title: "Failed to get location", message: err.description, viewController: self)
        })
    }
    
    func isFormValid() -> Bool {
        let extraDirectionsValidation = extraDirectionsTextView.text!.count <= 325
        let locationValidation = address != nil
        
        return extraDirectionsValidation && locationValidation
    }
    
    func validateForm() {
        navigationItem.rightBarButtonItem?.isEnabled = isFormValid()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "PinDropCreationDelegate  pinId: \(String(describing: pinId))")
        
        guard let pinId = self.pinId else {
            // @todo throw error
            print("No PinID!")
            return
        }
        
        if let vc = segue.destination as? PinDropCreationDelegate {
            vc.pinId(pinId)
        }
    }
}

extension ExtraDirectionsViewController: DefaultTextViewDelegate {

    func defaultTextView(didChange text: String) {
        self.validateForm()
    }
}

extension ExtraDirectionsViewController: PinDropCreationDelegate {
    
    func pinId(_ pinId: String) {
        self.pinId = pinId
    }
}

extension ExtraDirectionsViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchTableView.isHidden = false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        findForResults(searchText: nil)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        findForResults(searchText: textField.text ?? "" + string)
        
        return true
    }
    
    private func findForResults(searchText: String?) {
        // Remove selected result
        self.selectedResult = nil
        self.address = nil
        self.validateForm()
        
        // Search for results
        if let searchText = searchText, searchText.count > 0 {
            searchCompleter.queryFragment = searchText
        } else {
            // If the user don't write text
            self.searchSource = nil
            self.searchTableView.reloadData()
        }
    }
}

extension ExtraDirectionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.item == 0 ? 60 : 53
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        searchTableView.isHidden = true
        
        if indexPath.item == 0 {
            return getCurrentLocation()
        }
        
        let selectedResult = self.searchSource![indexPath.row - 1]
        self.searchBar.text = selectedResult.title
        self.searchAddress(by: selectedResult)
        
        searchBar.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = searchSource?.count ?? 0
        return count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 {
            return self.searchTableView.dequeueReusableCell(withIdentifier: "LocationCell")!
        }
        
        let index = indexPath.item - 1
        let cell = self.searchTableView.dequeueReusableCell(withIdentifier: SearchCell.identifier) as! SearchCell
        
        cell.titleLabel.text = self.searchSource![index].title
        cell.subtitleLabel.text = self.searchSource![index].subtitle
        
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
}

extension ExtraDirectionsViewController: MKLocalSearchCompleterDelegate {
    
    func extractAddress(placemark: CLPlacemark) -> PinAddress {
        var address = PinAddress()
        
        if let dictionary = placemark.addressDictionary {
            address.street = dictionary["Street"] as? String
            address.city = dictionary["City"] as? String
            address.state = dictionary["State"] as? String
            address.zip = dictionary["ZIP"] as? String
            
            if address.street == nil, let name = dictionary["Name"] as? String {
                address.street = name
            }
            
            if address.city == nil, let administrative = dictionary["SubAdministrativeArea"] as? String {
                address.city = administrative
            }
        }
        
        address.location = placemark.location?.coordinate
        
        return address
    }
    
    func searchAddress(by localSearch: MKLocalSearchCompletion) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let request = MKLocalSearchRequest(completion: localSearch)
        let search = MKLocalSearch(request: request)
        
        search.start { (response, error) -> Void in
            self.address = nil
            
            if let response = response, response.mapItems.count > 0 {
                self.address = self.extractAddress(placemark: response.mapItems[0].placemark)
            }
            
            self.validateForm()
        }
    }
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        // Filter and remove "Search Nearby" options
        self.searchSource = completer.results.filter {$0.subtitle != "Buscar Cerca" && $0.subtitle != "Search Nearby"}
        
        // Remove selected result if results count is equal to zero
        if completer.results.count == 0 {
            self.selectedResult = nil
        }
        
        DispatchQueue.main.async {
            self.searchTableView.reloadData()
        }
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: error.localizedDescription)
    }
}
