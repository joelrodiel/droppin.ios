//
//  PinDropNavigationController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/30/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class PinDropNavigationController: UINavigationController {
    var pinId: Int?
    
    override func viewDidLoad() {
        print("[PinDropNavigationController][viewDidLoad]")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("[PinDropNavigationController][prepare]")
        
    }
}
