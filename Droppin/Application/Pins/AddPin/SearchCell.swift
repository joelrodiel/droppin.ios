//
//  SearchCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 10/17/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

class SearchCell: UITableViewCell, Identifiable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    static var identifier: String = "SearchCell"
}
