//
//  StartDateCreatePinVC.swift
//  Droppin
//
//  Created by Rene Reyes on 11/29/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import SwiftMoment
import SwiftyJSON
import JGProgressHUD

class StartDateCreatePinVC: UIViewController, DroppinView, PinApi {
    
    var pinId: String?
    let api =  DroppinApi()
    
    @IBOutlet weak var warnLabel: UILabel!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var dropPin: UIBarButtonItem!
    
    private let dateFormatter = DateFormatter()
    private let startDatePicker = UIDatePicker()
    private let endDatePicker = UIDatePicker()
    private var hud = JGProgressHUD(style: .dark)
    
    private enum Segue: String {
        case unwindToMainViewControllerForZooming = "unwindToMainViewControllerForZooming"
        case unwindToMyEventsControllerForAddPin = "unwindToMyEventsControllerForAddPin"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        
        let today = Date()
        
        startDate.addBottomBorder(.steel)
        startDate.delegate = self
        startDatePicker.datePickerMode = .dateAndTime
        startDatePicker.minimumDate = today
        startDatePicker.maximumDate = moment(today).add(1, .Months).date
        startDatePicker.addTarget(self, action: #selector(startDateChanged), for: .valueChanged)
        
        startDateChanged()
        
        endDate.addBottomBorder(.steel)
        endDate.delegate = self
        endDatePicker.date = moment(today).add(2, .Hours).date
        endDatePicker.addTarget(self, action: #selector(endDateChanged), for: .valueChanged)
        
        endDateChanged()
        dropPin.isEnabled = true
    }
    
    @IBAction func startDateEditing(_ sender: UITextField) {
        sender.inputView = startDatePicker
    }
    
    @IBAction func endDateEditing(_ sender: UITextField) {
        sender.inputView = endDatePicker
    }
    
    @objc func startDateChanged() {
        // Display a Warning Label when the user selects a start time more than 12 hours from current time
        warnLabel.isHidden = startDatePicker.date <= moment().add(12, .Hours).date
        
        // Add text to field
        startDate.text = dateFormatter.string(from: startDatePicker.date)
        
        // Change date if the user selects a start date greater than end date
        if startDatePicker.date > endDatePicker.date {
            endDatePicker.date = moment(startDatePicker.date).add(2, .Hours).date
            endDateChanged()
        }
        
        // Change date if the user selects a start date lower than the minimum end date
        if moment(startDatePicker.date).add(48, .Hours).date < endDatePicker.date {
            endDatePicker.date = moment(startDatePicker.date).add(2, .Hours).date
            endDateChanged()
        }
        
        // Set restrictions to endDatePicker
        endDatePicker.minimumDate = startDatePicker.date
        endDatePicker.maximumDate = moment(startDatePicker.date).add(48, .Hours).date
    }
    
    @objc func endDateChanged() {
        // Add text to field
        endDate.text = dateFormatter.string(from: endDatePicker.date)
    }
    
    @IBAction func dropPinAction(_ sender: Any) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Creating"
        hud.show(in: self.view)
        dropPin.isEnabled = false
        
        guard let pinId = self.pinId else {
            print("No PinID!")
            return
        }
        
        self.updatePin(pinId: pinId, startDate: startDatePicker.date, endDate: endDatePicker.date) { diResult in
            switch diResult {
            case .success(let data):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Success! Show end date view controller."))
                
                if let json = data.metaData?["isPinSoon"] as? JSON, let isPinSoon = json.bool, isPinSoon {
                    self.hud.dismiss()
                    self.dropPin.isEnabled = true
                    
                    let closeAction = UIAlertAction(title: "Close", style: .cancel)
                    let confirmAction = UIAlertAction(title: "Proceed", style: .default) { action in
                        self.finishPinCreation(pinId)
                    }
                    
                    let alert = UIAlertController(title: "Pin Soon", message: "This pin cannot be created during this time. You are already Hosting a pin or Joined to another Pin that occurs within an hour of this time.", preferredStyle: .alert)
                    
                    alert.addAction(closeAction)
                    alert.addAction(confirmAction)
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.finishPinCreation(pinId)
                }
            case .failure(let requestResult, let error):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.dismiss()
                self.dropPin.isEnabled = true
                
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
            }
        }
    }
    
    func finishPinCreation(_ pinId: String) {
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Activating"
        hud.show(in: self.view)
        dropPin.isEnabled = false
        
        self.activatePin(for: pinId) { diActiveResult  in
            switch diActiveResult {
            case .success(_):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success! Unwind to mypins list.")
                self.hud.textLabel.text = "Finishing"
                
                self.getPinDetail(pinId) { (response, error) in
                    if let pin = response?.pin {
                        pin.calculateDistanceInMiles { err, distance in
                            var distance = distance
                            
                            if let error = err {
                                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                                distance = 55
                            }
                            
                            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                            self.hud.textLabel.text = "Done"
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                self.hud.dismiss(animated: true)
                                self.dropPin.isEnabled = true
                                
                                let sender: [String : Any?] = ["pin": pin, "distance": distance]
                                self.performSegue(withIdentifier: Segue.unwindToMainViewControllerForZooming.rawValue, sender: sender)
                            })
                        }
                    }
                }
            case.failure(let result, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure!")
                
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.dismiss()
                self.dropPin.isEnabled = true
                
                let errorMessage = result?.errorCode?.description ?? "Error: \(error)"
                self.showBasicAlert(title: "Error!", message: errorMessage, viewController: self)
            }
        }
    }
    
    override func prepare(for unwind: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        if unwind.identifier == Segue.unwindToMainViewControllerForZooming.rawValue {
            guard
                let controller = unwind.destination as? ViewController,
                let data = sender as? [String : Any?],
                let pin = data["pin"] as? Pin2,
                let distance = data["distance"] as? Double
                else { return }

            let range = RangeEnumeration.allCases.first { return distance < $0.rawValue }
            controller.newPinData = ["pin": pin, "zoom": range]
        }
    }
}

extension StartDateCreatePinVC: PinDropCreationDelegate {
    func pinId(_ pinId: String) {
        self.pinId = pinId
    }
}

extension StartDateCreatePinVC: UITextFieldDelegate {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        DispatchQueue.main.async(execute: {
            (sender as? UIMenuController)?.setMenuVisible(false, animated: false)
        })
        return false
    }
}
