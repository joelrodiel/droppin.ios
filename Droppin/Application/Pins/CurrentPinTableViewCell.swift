//
//  CurrentEventsTableViewCell.swift
//  Droppin
//
//  Created by Rene Reyes on 11/9/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import SwiftDate

class CurrentPinTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // @reference http://malcommac.github.io/SwiftDate/formatters.html
    // @reference https://www.w3.org/TR/NOTE-datetime
    func refresh(_ pin: Pin) {
        title.text = pin.title

        let pinStartDate = DateInRegion(absoluteDate: pin.startDate)
        startTime.text = pinStartDate.string(format: .custom("h:mm a"))
        startDate.text = pinStartDate.string(format: .custom("MM-dd-yyyy"))
        
        if let photo = pin.photo {
            self.photo.cldSetImage(publicId: photo, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }
    }
    
}

class PinTableViewCell: UITableViewCell, Identifiable {
    
    static var identifier: String = "pinCell"
    static var height: CGFloat = 107
    
    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    func load(pin: Pin) {
        titleLabel.text = pin.title
        dateLabel.text = pin.startDate.string(custom: "dd/MM/YY 'at' hh:mm aa")
        self.distanceLabel.text = "... mi"
        
        pin.calculateDistance { distance in
            self.distanceLabel.text = distance
        }
        
        if let photo = pin.photo {
            pinImageView.cldSetImage(publicId: photo, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }
    }
}
