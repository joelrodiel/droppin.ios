//
//  CurrentPinsTableViewCell.swift
//  Droppin
//
//  Created by Rene Reyes on 11/8/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class CurrentPinsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var pinTitle: UILabel!
    @IBOutlet weak var pinImage: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
