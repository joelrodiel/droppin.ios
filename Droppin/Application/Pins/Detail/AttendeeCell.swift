//
//  AttendeeCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/12/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class AttendeeCell: UICollectionViewCell, Identifiable {
    
    static var identifier: String = "AttendeeCell"
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        name.text = ""
    }
    
    func load(request: PinRequest) {
        image.setImage(of: request.attendeeId)
        name.text = request.attendeeName
    }
}
