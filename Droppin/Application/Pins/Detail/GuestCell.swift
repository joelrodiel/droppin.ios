//
//  GuestCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 20/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import SwiftMoment

@objc protocol GuestCellDelegate {
    
    @objc optional func userCell(userDidPressed: String)
}

class GuestCell: UITableViewCell, DiImageLoader {
    
    enum Identifier: String {
        case atPin = "GuestAtPinCell", joined = "GuestJoinedCell", outsider = "GuestOutsiderCell"
    }
    
    static var height: CGFloat = 72
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var profile: UIImageView?
    @IBOutlet weak var interest1: UILabel!
    @IBOutlet weak var interest2: UILabel!
    @IBOutlet weak var interest3: UILabel!
    
    var api = DroppinApi()
    var userId: String?
    var userName: String?
    var delegate: GuestCellDelegate?
    
    func refreshView() {
        guard let userId = userId else { return }
        let interestsLabels = [self.interest1, self.interest2, self.interest3]
        
        if let nameLabel = nameLabel {
            nameLabel.text = userName
        }
        
        if let profile = profile {
            profile.setImage(of: userId)
        }
        
        interestsLabels.forEach { $0?.superview?.isHidden = true }
        
        loadInterests(userId: userId) { interests in
            for (index, interest) in interests.enumerated() {
                if index > 2 { break }
                
                interestsLabels[index]?.text = interest
                interestsLabels[index]?.superview?.isHidden = false
            }
        }
    }
    
    @IBAction func profilePressed(_ sender: UIButton) {
        if let userId = userId {
            delegate?.userCell?(userDidPressed: userId)
        }
    }
}
