//
//  AttendeesViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 03/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

class GuestsViewController: UIViewController, DroppinView, PinRequestApi, UserApi, PinApi {
    
    @IBOutlet weak var titleNavItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    
    enum ListType { case attendee, joined, invitee }
    
    private var host: Host?
    private var items: [Any] = []
    
    var api = DroppinApi()
    var hostId: String?
    var pinId: String?
    var pinTitle: String?
    var requests: [PinRequest] = []
    var type: ListType?
    var role: UserRole = .outsider
    var isCurrent = false
    var isHostAnAttendee = false

    private enum Segue: String {
        case showProfileView = "ShowProfileViewSegue"
    }
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        super.viewDidLoad()
        
        loadHost()
        loadRequests()
        setupView()
    }

    private func setupView() {
        guard let type = type else { return }
        
        switch type {
        case .attendee:
            titleNavItem.title = "At Pin"
        case .joined:
            titleNavItem.title = "Joined"
            searchBarHeight.constant = 0
        case .invitee:
            titleNavItem.title = "Invited"
            searchBarHeight.constant = 0
        }
        
        searchBar.placeholder = "Search attendees at \"\(pinTitle ?? "Pin")\""
    }
    
    private func goBack(error: String?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let error = error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Error: \(error)")
            DiAlert.alertSorry(message: "Data not available", inViewController: self)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    private func loadHost() {
        guard let pinId = pinId else { return }
        
        getPinDetail(pinId) { response, error in
            guard
                let pin = response?.pin,
                let host = pin.hostId,
                let type = self.type,
                let isHostAnAttendee = pin.isHostAnAttendee
                else { return }
            
            self.isHostAnAttendee = isHostAnAttendee
            
            let attendeeValidation = type == .attendee && isHostAnAttendee
            let joinedValidation = type == .joined && !isHostAnAttendee
            let isHostDisplayed = attendeeValidation || joinedValidation
            
            if isHostDisplayed {
                self.host = host
                self.items = self.createArray()
                self.tableView.reloadData()
            }
        }
    }
    
    private func createArray() -> [Any] {
        var items = requests as [Any]
        
        if let host = host {
            items.insert(host, at: 0)
        }
        
        return items
    }
    
    private func loadRequests(search: String? = nil) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let pinId = pinId else { return goBack(error: "No PinId") }
        guard let type = type else { return goBack(error: "No Type") }
        let status: PinRequestStatus?
        var sort: String?
        
        switch type {
        case .attendee:
            status = .atPin
            sort = "arrivalDate"
        case .joined:
            status = .accepted
        case .invitee:
            role = .outsider
            return getPinRequests(byPinId: pinId, type: .invitation, completion: displayRequestsInList)
        }
        
        getPinRequests(byPinId: pinId, status: status, sort: sort, search: search, completion: displayRequestsInList)
    }
    
    private func displayRequestsInList(result: DIResult<PinRequestResponse>) {
        switch result {
        case .success(let data):
            self.requests = data.requests
            self.items = self.createArray()
            self.tableView.reloadData()
        case .failure(_, let error):
            DiAlert.alertSorry(message: error.localizedDescription, inViewController: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = segue.destination as? ProfileViewController {
            guard let row = sender as? Int else { return }
            let isAtPin = [.attendee].contains(role) || ([.host].contains(role) && isHostAnAttendee)
            
            controller.isAbleToConnect = isAtPin
            
            if let host = items[row] as? Host, let userId = host.userId {
                controller.refreshData(userId: userId)
                controller.isAbleToSendDirectMessages = true
                controller.role = .attendee
            } else if let request = items[row] as? PinRequest {
                controller.refreshData(userId: request.attendeeId)
                controller.isAbleToSendDirectMessages = isAtPin
                
                if let type = type {
                    controller.isAcceptee = type == .joined
                    
                    if (type == .joined) {
                        controller.isAbleToConnect = false
                    }
                }
                
                controller.role = role
            }
        }
    }
}

extension GuestsViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        loadRequests(search: searchBar.text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        loadRequests(search: searchBar.text)
    }
}

extension GuestsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Segue.showProfileView.rawValue, sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GuestCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = getIdentifier(for: role).rawValue
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! GuestCell
        let row = indexPath.row
        
        if let host = items[row] as? Host {
            cell.userId = host.userId
            cell.userName = host.fullName
        } else if let request = items[row] as? PinRequest {
            cell.userId = request.attendeeId
            cell.userName = request.attendeeName
        }
        
        cell.refreshView()
        
        return cell
    }
    
    private func getIdentifier(for role: UserRole) -> GuestCell.Identifier {
        switch role {
        case .host, .attendee, .owner:
            return GuestCell.Identifier.atPin
        case .joined, .conflicted:
            return GuestCell.Identifier.joined
        case .outsider, .connector, .checker:
            return GuestCell.Identifier.outsider
        }
    }
}
