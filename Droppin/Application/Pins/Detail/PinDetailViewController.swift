//
//  PinDetailViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 02/04/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CoreLocation
import JGProgressHUD

protocol PinDetailRoles: DroppinView {}

protocol PinDetailViewControllerDelegate {
    func afterTerminate(pin: Pin)
}

extension PinDetailRoles {
    
    func getRole(for hostId: String?, and status: PinRequestStatus?) -> UserRole {
        if isThisMine(toCompare: hostId) { return .host }
        guard let status = status else { return .outsider }
        
        switch status {
        case .new, .seen, .declined, .pending:
            return .outsider
        case .accepted, .left, .conflicted:
            return .joined
        case .atPin, .attended:
            return .attendee
        }
    }
}

class PinDetailViewController: UIViewController, PinDetailRoles, PinApi, PinRequestApi {
    
    private enum Segue: String {
        case showGuestsView = "ShowGuestsViewSegue"
        case showPinEditViewSegue = "ShowPinEditViewSegue"
    }
    
    var delegate: PinDetailViewControllerDelegate?
    var api = DroppinApi()
    var pin: Pin?
    var locationManager: PinLocationManager?
    var role: UserRole = .outsider
    var isRefreshing = false
    var request: PinRequest?
    var firstAttendees: [PinRequest] = []
    var actionType: ActionType?

    enum ActionType {
        case end, request, unJoin, conflicted, join
    }

    private var currentStatus: PinRequestStatus?
    private var hud = JGProgressHUD(style: .dark)

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var extraDirectionsLabel: UILabel!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endView: UIView!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var atPinView: UIView!
    @IBOutlet weak var atPinLabel: UILabel!
    @IBOutlet weak var joinedView: UIView!
    @IBOutlet weak var joinedLabel: UILabel!
    @IBOutlet weak var invitedView: UIView!
    @IBOutlet weak var invitedLabel: UILabel!
    @IBOutlet weak var actionButton: PrimaryButton!
    @IBOutlet weak var directMessageButton: UIButton!
    @IBOutlet weak var attendeesCollection: UICollectionView!
    @IBOutlet weak var attendeesIndicator: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if view.subviews.count > 1 {
            view.subviews[1].removeFromSuperview()
        }

        if let request = request {
            self.pin = request.pin
        }
        
        if !isRefreshing {
            self.refreshView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupStyle()
        self.showOverlay()
        
        self.locationManager = PinLocationManager.sharedManager
    }
    
    func goBack(error: String?) {
        if let error = error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Error: \(error)")
            DiAlert.alertSorry(message: "Pin not available", inViewController: self)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func refreshData(pinId: String) {
        DiActivityIndicator.showActivityIndicator()
        self.isRefreshing = true
        
        getPinDetail(pinId: pinId) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Pin Loaded")
                self.pin = data.pin
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
            
            DiActivityIndicator.hide()
            self.isRefreshing = false
            self.refreshView()
        }
    }
    
    func setupStyle() {
        // Add borders
        startView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        startView.layer.borderWidth = 1
        
        endView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        endView.layer.borderWidth = 1
        
        reportButton.isHidden = true
    }
    
    func refreshView() {
        guard let pin = pin else {
            if !isRefreshing {
                goBack(error: "Pin not available")
            }
            
            return
        }
        
        self.removeOverlay()
        loadFirstAttendees(for: pin.pinId)
        
        if isThisMine(toCompare: pin.hostId) {
            role = .host
            let editButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(showPinEditView))
            navigationItem.rightBarButtonItem = editButton
            reportButton.isHidden = true
        } else {
            navigationItem.rightBarButtonItem = nil
            reportButton.isHidden = false
        }
        
        nameLabel.text = pin.title
        descriptionLabel.text = pin.description
        startLabel.text = pin.startDate.string(format: .custom("hh:mm aa"))
        endLabel.text = pin.endDate.string(format: .custom("hh:mm aa"))
        addressLabel.text = pin.address
        extraDirectionsLabel.text = pin.extraDirections
        
        if let photo = pin.photo {
            self.imageView.cldSetImage(publicId: photo, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
            // Scale image here
            self.imageView.contentMode = .scaleAspectFill
            self.imageView.clipsToBounds = true
        }

        if User.checkAvailability(of: .pinAddress, for: role) {
            addressLabel.isHidden = false
            extraDirectionsLabel.isHidden = false
        }
        
        // Start tracking
        if role != .host && (request?.status == .accepted || request?.status == .atPin || request?.status == .left) {
            currentStatus = request?.status
            
            locationManager?.currentRequest = request
            locationManager?.delegate = self
            locationManager?.left = request?.status == .left
            
            directMessageButton.isHidden = false
        } else {
            directMessageButton.isHidden = true
        }
        
        if [.attendee, .joined, .conflicted].contains(role) {
            if isConflicted() {
                setupButton(for: .conflicted)
            } else {
                setupButton(for: .unJoin)
            }
        } else if let request = request, request.isRequested {
            setupButton(for: .unJoin)
        }
        
        // Add action
        if role == .host, pin.status != .black, pin.startDate <= Date() {
            setupButton(for: .end)
        }
        
        if let actionType = actionType {
            setupButton(for: actionType)
        }
        
        getPinStatistic(pin.pinId) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                
                self.atPinLabel.text = String(data.pinStatistic.atPin)
                self.joinedLabel.text = String(data.pinStatistic.joined)
                self.invitedLabel.text = String(data.pinStatistic.invited)
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }

    @objc
    private func showPinEditView() {
        let navigationController = PinDetailStoryboard.navigation

        if let topViewController = navigationController.topViewController as? PinEditController, let pin = pin?.toPin2() {
            topViewController.pin = pin
            topViewController.editDelegate = self
        }

        navigationController.present(from: self)
    }

    private func setupButton(for action: ActionType) {
        var title: String!
        var target: Selector!

        self.actionType = action

        actionButton.removeTarget(self, action: nil, for: .allEvents)

        switch action {
        case .end:
            title = "End"
            target = #selector(terminatePin)
            actionButton.backgroundColor = DIColors.droppinRed
        case .request:
            title = "Request to join"
            target = #selector(requestToJoin)
            actionButton.backgroundColor = DIColors.droppinRed
        case .unJoin:
            title = "Un-Request"
            target = #selector(requestToUnjoin)
            actionButton.backgroundColor = DIColors.droppinRed
        case .conflicted:
            title = "Join"
            target = #selector(joinConflicted)
            actionButton.backgroundColor = DIColors.droppinGreen
        case .join:
            title = "Join"
            target = #selector(requestToJoin)
            actionButton.backgroundColor = DIColors.droppinGreen
        }

        actionButton.isHidden = false
        actionButton.setTitle(title, for: .normal)
        actionButton.addTarget(self, action: target, for: .touchUpInside)
    }

    @objc
    func requestToJoin() {
        guard let pinId = pin?.pinId, let action = actionType else {
            return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "No pinID")
        }
        
        self.hud.textLabel.text = action == .request ? "Requesting" : "Joining"
        self.hud.show(in: self.view)
        
        RequestToJoinAPI.request(pinId) { error, _ in
            if error != nil {
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.dismiss()
                DiAlert.alertSorry(message: "There is a problem requesting this pin", inViewController: self)
                return
            }
            
            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            self.hud.textLabel.text = "Done"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hud.dismiss(animated: true)
                self.goBack(error: nil)
            }
        }
    }
    
    @objc
    func requestToUnjoin() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "requestTo unjoin")

        guard let pinId = pin?.pinId else {
            debugPrint("PinDetailsVC - requestTo - pinId is nil")
            return
        }

        DiAlert.confirmOrCancel(title: "Un-Request", message: "Are you you sure you want to Un-Request?", confirmTitle: "Yes", cancelTitle: "Cancel", inViewController: self, withConfirmAction: {
            self.hud.textLabel.text = "Un-Requesting"
            self.hud.show(in: self.view)

            self.deleteRequest(byPinId: pinId) { (diResult)->() in
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "send request to join"))
                switch diResult {
                case .success(_):
                    self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    self.hud.textLabel.text = "Done"

                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.hud.dismiss(animated: true)
                        self.showPinOverlay(text: "The pin has been removed.")
                    }
                case .failure(let requestResult, let error):
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.dismiss()
                    self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
                }
            }
        })
    }

    private func loadFirstAttendees(for pinId: String) {
        getFirstAttendees(pinId: pinId) { result in
            switch result {
            case .success(let data):
                self.firstAttendees = data.requests
                self.attendeesIndicator.isHidden = self.firstAttendees.count == 0
                self.attendeesCollection.reloadData()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    func isConflicted() -> Bool {
        return request != nil && request!.status == .conflicted
    }
    
    @objc func joinConflicted() {
        let alert = UIAlertController(title: "Time Conflicts", message: "You are already joined to a pin that's starting time conflicts with this pin. If you join this second pin, you will automatically un-join the pin your currently joined to", preferredStyle: .alert)
        
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel)
        
        let confirm = UIAlertAction(title: "OK", style: .default) { alert in
            guard let requestId = self.request?.pinRequestId else {
                return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "No pinRequestID")
            }
            
            self.hud.textLabel.text = "Joining"
            self.hud.show(in: self.view)
            
            self.acceptRequest(requestId: requestId, force: true) { result in
                switch result {
                case .success(_):
                    self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    self.hud.textLabel.text = "Done"
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.hud.dismiss(animated: true)
                        self.goBack(error: nil)
                    }
                case .failure(let result, let error):
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.dismiss()
                    self.showAlert(title: "Sorry!", result: result, error: error, viewController: self)
                }
            }
        }
        
        alert.addAction(dismiss)
        alert.addAction(confirm)
        
        present(alert, animated: true, completion: nil)
    }
    
    @objc func terminatePin() {
        DiAlert.confirmOrCancel(title: "End Pin", message: "Are you sure you want to end this Pin?", inViewController: self) {
            DiActivityIndicator.showActivityIndicator()
            
            self.terminatePin(pinId: self.pin!.pinId) { result in
                switch result {
                case .success(let data):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success message: \(data.message ?? "No message")")
                    self.goBack(error: nil)
                    self.showPinOverlay(text: "The pin has been ended.")
                    self.delegate?.afterTerminate(pin: self.pin!)
                case .failure(_, let error):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                }
                
                DiActivityIndicator.hide()
            }
        }
    }

    private func updateRequestStatus(isUserInRadius: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let newStatus: PinRequestStatus = isUserInRadius ? .atPin : .left
        
        guard let requestId: String = request?.pinRequestId, newStatus != currentStatus else {
            return DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Not updated")
        }
        
        let callback: (DIResult<RequestResult>)->() = { result in
            switch result {
            case .success(_):
                self.currentStatus = newStatus
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.localizedDescription)")
            }
        }
        
        if isUserInRadius {
            arrivePin(requestId: requestId, completion: callback)
        } else {
            leavePin(requestId: requestId, completion: callback)
        }
    }
    
    @IBAction func showAttendeesView(_ sender: Any? = nil) {
        performSegue(withIdentifier: Segue.showGuestsView.rawValue, sender: GuestsViewController.ListType.attendee)
    }
    
    @IBAction func showAccepteesView(_ sender: Any) {
        performSegue(withIdentifier: Segue.showGuestsView.rawValue, sender: GuestsViewController.ListType.joined)
    }
    
    @IBAction func showInviteesView(_ sender: Any) {
        performSegue(withIdentifier: Segue.showGuestsView.rawValue, sender: GuestsViewController.ListType.invitee)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = segue.destination as? GuestsViewController {
            guard let type = sender as? GuestsViewController.ListType else { return }
            let status = request?.status
            let hostId = pin?.hostId
            
            controller.pinId = pin?.pinId
            controller.pinTitle = pin?.title
            controller.role = getRole(for: hostId, and: status)
            controller.type = type
            controller.isCurrent = [.yellow, .green].contains(pin?.status)

            if let status = pin?.status, [.green, .red, .yellow].contains(status) {
                controller.hostId = hostId
            }
            
            return
        }
        
        if let controller = segue.destination as? MessagesViewController {
            guard let receiverId = pin?.hostId else { return }
            controller.setupView(forDirect: receiverId)
            
            return
        }
    }
}

extension PinDetailViewController: PinLocationManagerDelegate {
    
    func locationManager(isUserInRadius: Bool, awaiting: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "User is in radius: \(isUserInRadius ? "YES" : "NO")")
        
        if isUserInRadius {
            self.locationManager?.left = false
            return updateRequestStatus(isUserInRadius: true)
        } else if awaiting {
            self.locationManager?.left = true
            return updateRequestStatus(isUserInRadius: false)
        }
        
        self.locationManager?.left = true
        
        DiAlert.confirmOrCancel(title: "Looks like you left \"\(pin!.title)\" hope you enjoyed it", message: "if you are still at the pin, please tap \"report\"", confirmTitle: "OK", cancelTitle: "Report", inViewController: self, withConfirmAction: {
            self.updateRequestStatus(isUserInRadius: false)
        }, cancelAction: {
            self.send(report: "User is still at the pin", type: .userAtPin) { result in
                switch result {
                case .success(_):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                case .failure(_, _):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure")
                }
            }
        })
    }
    
    func locationManager(warnCount: Int) {
        DiAlert.confirmOrCancel(title: "Are you still at the pin?", message: nil, confirmTitle: "Yes", cancelTitle: "No", inViewController: self, withConfirmAction: {
            self.locationManager?.awaiting = false
        }, cancelAction: {
            self.locationManager?.left = true
            self.updateRequestStatus(isUserInRadius: false)
        })
    }
}

extension PinDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return firstAttendees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AttendeeCell.identifier, for: indexPath) as! AttendeeCell
        
        cell.load(request: firstAttendees[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showAttendeesView()
    }
}

extension PinDetailViewController {
    func showPinOverlay(text: String) {
        let overlay = UIView()
        overlay.backgroundColor = .white
        overlay.frame = self.view.frame

        let label = UILabel()
        label.text = text
        label.translatesAutoresizingMaskIntoConstraints = false
        overlay.addSubview(label)

        label.centerXAnchor.constraint(equalTo: overlay.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: overlay.centerYAnchor).isActive = true
        self.view.addSubview(overlay)
    }
}

extension PinDetailViewController: ReportApi {
    
    @IBAction func reportPin(_ sender: UIButton) {
        let confirmAction = {
            guard let pinId = self.pin?.pinId else { return }
            let report = "Pin: \(pinId)"
            let hud = JGProgressHUD(style: .dark)
            
            hud.textLabel.text = "Reporting"
            hud.show(in: self.view)
            
            self.send(report: report, type: .abusivePin) { result in
                switch result {
                case .success(_):
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.dismiss()
                    
                    DiAlert.alert(title: "Report sent", message: "This pin has been reported. The Upin team will review it.", dismissTitle: "OK", inViewController: self, withDismissAction: nil)
                case .failure(_, let error):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    hud.dismiss(afterDelay: 2)
                }
            }
        }
        
        DiAlert.confirmOrCancel(title: "Reporting a Pin", message: "Are you sure you want to report this Pin?", confirmTitle: "Report", cancelTitle: "Cancel", inViewController: self, withConfirmAction: confirmAction)
    }
}

extension PinDetailViewController: PinEditControllerDelegate {
    
    func pinEdit(_ controller: PinEditController, on pinUpdated: Pin2) {
        BasicAlert(title: "Pin successfuly updated!").present(from: self)
        
        self.pin = pinUpdated.toPin1()
    }
}
