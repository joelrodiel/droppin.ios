//
//  PinLocationManager.swift
//  Droppin
//
//  Created by Isaias Garcia on 02/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import CoreLocation
import SwiftLocation
import SwiftyJSON

@objc protocol PinLocationManagerDelegate {
    
    @objc optional func locationManager(updated: CLLocation, distance: CLLocationDistance)
    @objc optional func locationManager(isUserInRadius: Bool, awaiting: Bool)
    @objc optional func locationManager(warnCount: Int)
}

private struct PinRequestManager {
    let metaData: JSON
    
    init(metaData: JSON) {
        self.metaData = metaData
    }
    
    var id: String? {
        guard let pinRequestId = metaData["pinRequestId"].string else {return nil}
        return pinRequestId
    }
    
    var pinLocation: CLLocation? {
        let pin = metaData["pinId"]
        
        do {
            let location2d = try RequestResult.extractCoordinatesFromApiJsonResponse(pin)
            return CLLocation(latitude: location2d.latitude, longitude: location2d.longitude)
        } catch _ {
            return nil
        }
    }
}

private struct PinManager {
    let metaData: JSON
    
    init(metaData: JSON) {
        self.metaData = metaData
    }
    
    var id: String? {
        guard let id = metaData["pinId"].string else {return nil}
        return id
    }
    
    var location: CLLocation? {
        do {
            let location2d = try RequestResult.extractCoordinatesFromApiJsonResponse(metaData)
            return CLLocation(latitude: location2d.latitude, longitude: location2d.longitude)
        } catch _ {
            return nil
        }
    }
}

class PinLocationManager: PinRequestApi {
    var api = DroppinApi()
    
    static let sharedManager: PinLocationManager = {
        let manager = PinLocationManager()
        return manager
    }()
    
    var currentRequest: PinRequest?
    private var socket: PinRequestSocketClient?
    private var lastLocation: CLLocation?
    private var trackingRequest: LocationRequest?
    private var limit: Double { get { return DiConfig.atPinRadiusInFeet * Double(limitCount) } }
    private var limitCount: Int
    private var arrivedAtPin: Bool
    var left: Bool
    var delegate: PinLocationManagerDelegate?
    var awaiting: Bool
    
    enum TrackingType {case background, foreground}
    
    private init() {
        self.arrivedAtPin = false
        self.left = false
        self.limitCount = 1
        self.awaiting = false
    }

    func startTracking(_ type: TrackingType) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        stopTracking()
        
        // Check if the user has logged in
        guard let attendeeId = DiHelper.userId else { return }
        
        // Start socket
        socket = PinRequestSocketClient(attendeeId: attendeeId)

        // Listen refreshes on current request
        socket?.listen() { data in
            if data["request"].exists() {
                self.updateCurrent(request: PinRequestManager(metaData: data["request"]))
            } else if data["pin"].exists() {
                self.updateCurrent(pin: PinManager(metaData: data["pin"]))
            }
        }
        
        let onUpdate: LocationRequest.Success = { loc in
            self.lastLocation = loc
            self.socket?.onLocationUpdated()
        }
        
        let onFail: LocationRequest.Failure = { err, _ in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(err.localizedDescription)")
        }
        
        switch type {
        case .background:
            Locator.subscribeSignificantLocations(onUpdate: onUpdate, onFail: onFail)
        case .foreground:
            Locator.subscribePosition(accuracy: .house, onUpdate: onUpdate, onFail: onFail)
        }
    }
    
    func stopTracking() {
        if let request = trackingRequest {
            let stopped = Locator.stopRequest(request)
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Stopped \(stopped)")
        }
        
        socket?.disconnect()
    }
    
    private func updateCurrent(request: PinRequestManager) {
        guard
            let location = lastLocation,
            let distance = request.pinLocation?.distance(from: location),
            let requestId = request.id
            else { return }
        
        let status: PinRequestStatus = self.isDistanceInRadius(distance) ? .atPin : .left
        
        socket?.onStatusUpdated(requestId: requestId, for: status)
    }
    
    private func updateCurrent(pin: PinManager) {
        guard
            let location = lastLocation,
            let distance = pin.location?.distance(from: location),
            let pinId = pin.id
            else { return }

        let status = self.isDistanceInRadius(distance)
        
        socket?.onStatusUpdated(pinId: pinId, for: status)
    }
    
    private func checkLocationLimit(isDistanceInRadius: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if isDistanceInRadius {
            self.delegate?.locationManager?(isUserInRadius: isDistanceInRadius, awaiting: self.awaiting)
            return
        }
        
        if limit < DiConfig.atPinRadiusLimitInFeet && !left {
            limitCount = limitCount + 1
            
            self.delegate?.locationManager?(warnCount: limitCount)
            self.awaitThenLeft()
        } else if !left {
            self.delegate?.locationManager?(isUserInRadius: false, awaiting: self.awaiting)
            left = true
        }
    }
    
    private func awaitThenLeft() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        awaiting = true
        
        let when = DispatchTime.now() + 10
        DispatchQueue.main.asyncAfter(deadline: when) {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "End")
            
            if self.awaiting {
                self.delegate?.locationManager?(isUserInRadius: false, awaiting: self.awaiting)
                self.left = true
            }
        }
    }
    
    private func isDistanceInRadius(_ distance: CLLocationDistance) -> Bool {
        return Measurement(value: distance, unit: UnitLength.meters) < Measurement(value: limit, unit: UnitLength.feet)
    }
}
