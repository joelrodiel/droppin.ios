//
//  PickAddressController.swift
//  Droppin
//
//  Created by Madson on 10/19/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import MapKit

protocol PickAddressDelegate {
    func didSelectAddress(_ pickAddressController: PickAddressController, address: PinAddress)
}

final class PickAddressController: UITableViewController {

    @IBOutlet private weak var searchBar: UISearchBar!

    lazy var searchCompleter: MKLocalSearchCompleter = {
        let searchCompleter = MKLocalSearchCompleter()
        searchCompleter.delegate = self
        return searchCompleter
    }()

    private var selectedResult: MKLocalSearchCompletion?
    private var searchSource: [MKLocalSearchCompletion]?

    var textAddress: String?
    var delegate: PickAddressDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = searchBar
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
        searchBar.textContentType = UITextContentType.fullStreetAddress
        searchBar.text = textAddress
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.resignFirstResponder()
    }

    @IBAction private func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension PickAddressController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Remove selected result
        self.selectedResult = nil

        // Search for results
        if searchText.count > 0 {
            return searchCompleter.queryFragment = searchText
        }

        // If the user don't write text
        self.searchSource = nil
        tableView.reloadData()
    }
}

extension PickAddressController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let selectedResult = self.searchSource![indexPath.row]
        self.searchBar.text = selectedResult.title
        self.searchAddress(by: selectedResult)

        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = searchSource?.count ?? 0

        return count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)

        cell.textLabel?.text = self.searchSource![indexPath.row].title
        cell.detailTextLabel?.text = self.searchSource![indexPath.row].subtitle

        return cell
    }

    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
}

extension PickAddressController: MKLocalSearchCompleterDelegate {

    func extractAddress(placemark: CLPlacemark) -> PinAddress {
        var address = PinAddress()

        if let dictionary = placemark.addressDictionary {
            address.street = dictionary["Street"] as? String
            address.city = dictionary["City"] as? String
            address.state = dictionary["State"] as? String
            address.zip = dictionary["ZIP"] as? String

            if address.street == nil, let name = dictionary["Name"] as? String {
                address.street = name
            }

            if address.city == nil, let administrative = dictionary["SubAdministrativeArea"] as? String {
                address.city = administrative
            }
        }

        address.location = placemark.location?.coordinate

        return address
    }

    func searchAddress(by localSearch: MKLocalSearchCompletion) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        let request = MKLocalSearchRequest(completion: localSearch)
        let search = MKLocalSearch(request: request)

        search.start { (response, error) -> Void in

            if let response = response, response.mapItems.count > 0 {
                let address = self.extractAddress(placemark: response.mapItems[0].placemark)
                self.delegate?.didSelectAddress(self, address: address)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        // Filter and remove "Search Nearby" options
        self.searchSource = completer.results.filter {$0.subtitle != "Buscar Cerca" && $0.subtitle != "Search Nearby"}

        // Remove selected result if results count is equal to zero
        if completer.results.count == 0 {
            self.selectedResult = nil
        }

        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: error.localizedDescription)
    }
}
