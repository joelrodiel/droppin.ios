//
//  PinEditController.swift
//  Droppin
//
//  Created by Madson on 10/17/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CoreLocation
import DZNEmptyDataSet
import SwiftMoment
import JGProgressHUD

typealias EmptyState = DZNEmptyDataSetSource & DZNEmptyDataSetDelegate

protocol PinEditControllerDelegate {
    
    func pinEdit(_ controller: PinEditController, on pinUpdated: Pin2)
}

final class PinEditController: PinPhotoTableController, DroppinView, PinApi {
    
    @IBOutlet weak var image: UIButton?
    @IBOutlet weak var titleTextField: UITextField?
    @IBOutlet weak var descriptionTextView: UITextView?
    @IBOutlet weak var descriptionCountLabel: UILabel?
    @IBOutlet weak var addressTextField: UITextField?
    @IBOutlet weak var extraDirectionsTextField: UITextField?
    @IBOutlet weak var startTimeTextField: TextFieldDatePicker?
    @IBOutlet weak var endTimeTextField: TextFieldDatePicker?
    @IBOutlet weak var actionButton: UIButton?
    @IBOutlet weak var tableHeaderView: UIView?
    @IBOutlet weak var tableFooterView: UIView?

    private (set) var api = DroppinApi()
    var editDelegate: PinEditControllerDelegate?
    var isPickingPhoto: Bool = false
    var pin: Pin2?
    private var shouldShowEmptyState: Bool = true {
        didSet {
            image?.isHidden = shouldShowEmptyState
            actionButton?.isHidden = shouldShowEmptyState
            tableView.reloadData()
            hydrateFields()
            toogleBarButtons()
        }
    }
    private var pinRequest: PinRequest2? {
        didSet {
            tableView.reloadData()
        }
    }

    private enum ActionType {
        case none, join, leave, unRequest, requestToJoin
    }

    private var hud: JGProgressHUD = JGProgressHUD(style: .dark)
    private var editButton: UIBarButtonItem?
    private var closeButton: UIBarButtonItem?
    private var cancelButton: UIBarButtonItem?
    private var saveButton: UIBarButtonItem?
    private var actionType: ActionType = .none {
        didSet {
            actionButton?.isHidden = false

            switch actionType {
            case .none:
                actionButton?.isHidden = true
            case .join:
                actionButton?.setTitle("Join", for: .normal)
            case .leave:
                actionButton?.setTitle("Joined", for: .normal)
            case .unRequest:
                actionButton?.setTitle("Un-request", for: .normal)
            case .requestToJoin:
                actionButton?.setTitle("Request to join", for: .normal)
            }
        }
    }

    internal override var isEditing: Bool {
        didSet {
            toogleFields()
            toogleBarButtons()
        }
    }

    private var address: PinAddress? {
        didSet {
            pin?.street = address?.street
            pin?.city = address?.city
            pin?.state = address?.state
            pin?.zip = address?.state

            if let longitude = address?.location?.longitude, let latitude = address?.location?.latitude {
                var location = Location()
                location.coordinates = [longitude, latitude]
                location.type = "Point"
                pin?.loc = location
            }

            addressTextField?.text = self.address?.fullAddress
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        setupUI()
        setupEmptyState()
        tableView.tableHeaderView = tableHeaderView
        tableView.tableFooterView = tableFooterView
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isPickingPhoto {
            setupDefaults()
            hydrateFields()
        }
    }

    @objc private func toogleEdit(_ sender: UIBarButtonItem) {
        isEditing = !isEditing
        
        hydrateFields()
    }

    @objc private func save(_ sender: UIBarButtonItem) {
        hydrateObject()
        
        if !isValid() {
            return DiAlert.alertSorry(message: "Please add title and description", inViewController: self)
        }

        guard var pin = pin, let pinId = pin.pinId, let imageButton = image else {
            debugPrint("PinEditController - save - pin is nil")
            return
        }
        
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Saving"
        hud.show(in: self.view)
        
        self.uploadPinImage(pinId: pinId, image: imageButton.image(for: .normal)!) { error, result, publicId in
            pin.photo = publicId
            self.update(pin)
        }
    }

    private func update(_ pinUpdated: Pin2) {
        PinApi2.update(pinUpdated) { (error, response) in
            self.hud.dismiss(animated: true)
            
            guard error == nil else {
                debugPrint("PinEditController - update - error is not nil")
                debugPrint(error!.localizedDescription)
                return
            }
            
            self.close(nil)
            self.editDelegate?.pinEdit(self, on: pinUpdated)
        }
    }
    
    @objc private func close(_ sender: UIBarButtonItem?) {
        if navigationController?.viewControllers.count == 1 {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction private func request(_sender: UIButton) {

        if actionType == .leave {
            guard let pinRequestId = pinRequest?.pinRequestId else { return }

            PinRequestApi2.delete(pinRequestId) { _, _ in
                self.getPinRequestsIfNeeded()
            }
        } else if actionType == .unRequest {
            guard let pinRequestId = pinRequest?.pinRequestId else { return }

            PinRequestApi2.delete(pinRequestId) { _, _ in
                self.getPinRequestsIfNeeded()
            }
        } else if actionType == .requestToJoin || actionType == .join {
            guard let pinId = pin?.pinId else { return }

            RequestToJoinAPI.request(pinId) { _, _ in
                self.getPinRequestsIfNeeded()
            }
        }
    }

    @IBAction private func editPhoto(_ sender: UIButton) {
        if isEditing {
            takePhoto(sender)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if shouldShowEmptyState == false {
            rows = super.tableView(tableView, numberOfRowsInSection: section)
        }
        return rows
    }
}

extension PinEditController: PinPhotoControllerDelegate {
    
    func pinPhoto(didSelected image: UIImage) {
        self.image?.setImage(image, for: .normal)
    }
}

// UITableView delegates

extension PinEditController {
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

// Helpers

extension PinEditController {

    private func isValid() -> Bool {
        guard
            let title = titleTextField?.text?.trimmed, title.isEmpty == false,
            let description = descriptionTextView?.text?.trimmed, description.isEmpty == false
            else { return false }
        
        return true
    }
    
    private func setupDefaults() {
        isEditing = true
        actionType = .none
        shouldShowEmptyState = false
        actionButton?.isHidden = true
    }

    private func setupUI() {
        setupBarButtons()
        setupDatePickers()
    }

    private func setupEmptyState() {
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        tableView.tableFooterView = UIView()
    }

    private func setupBarButtons() {
        let edit   = #selector(toogleEdit(_:))
        let save   = #selector(save(_:))
        let close  = #selector(close(_:))
        let cancel = #selector(close(_:))

        editButton   = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: edit)
        saveButton   = UIBarButtonItem(title: "Save", style: .done, target: self, action: save)
        closeButton  = UIBarButtonItem(title: "Close", style: .plain, target: self, action: close)
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: cancel)
    }

    private func setupDatePickers() {
        let today = Date()
        
        startTimeTextField?.minimumDate = today
        startTimeTextField?.maximumDate = moment(today).add(1, .Months).date
        startTimeTextField?.datePickerDelegate = self
        
        startTimeTextField?.type = .datetime
        endTimeTextField?.type = .datetime
    }
    
    private func hydrateFields() {
        guard shouldShowEmptyState == false else { return }
        guard let pin = pin else { return }

        image?.setImage(pin.photo, for: .normal)

        titleTextField?.text = pin.title
        descriptionTextView?.text = pin.description
        addressTextField?.text = pin.fullAddress
        extraDirectionsTextField?.text = pin.extraDirections
        startTimeTextField?.date = pin.startDate
        endTimeTextField?.date = pin.endDate
    }

    private func hydrateObject() {
        guard let title = titleTextField?.text?.trimmed, title.isEmpty == false else {
            debugPrint("PinEditController - hydrateObject - title is nil")
            return
        }

        pin?.title = title
        pin?.description = descriptionTextView?.text
        pin?.extraDirections = extraDirectionsTextField?.text

        if let date = startTimeTextField?.date {
            pin?.startDate = date
        }

        if let date = endTimeTextField?.date {
            pin?.endDate = date
        }
    }

    private func toogleBarButtons() {
        if isEditing {
            navigationItem.title = "Editing"
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton
        } else {
            navigationItem.title = "Pin Details"

            if navigationController?.viewControllers.count == 1 {
                navigationItem.leftBarButtonItem = closeButton
            } else {
                navigationItem.leftBarButtonItem = nil
            }

            if isThisMine(toCompare: pin?.hostId?.userId) && shouldShowEmptyState == false {
                navigationItem.rightBarButtonItem = editButton
            }
        }
    }

    private func toogleFields() {
        let fields = [titleTextField, descriptionTextView, addressTextField,
                     extraDirectionsTextField, startTimeTextField, endTimeTextField]

        for field in fields {
            field?.isUserInteractionEnabled = isEditing
        }
    }

    private func getPinRequestsIfNeeded() {
        guard let pinId = pin?.pinId else { return }

        if isThisMine(toCompare: pin?.hostId?.userId) {
            self.actionType = .none
            return
        }

        PinRequestApi2.getPinRequest(pinId, type: .request, status: nil) { (error, response) in
            self.getPinRequestsInvitations()

            guard
                let request = response?.metaData,
                let status = request.status else {
                    self.actionType = .requestToJoin
                    return
            }

            self.pinRequest = request
            let pendingStatuses: [PinRequestStatus] = [.new, .seen]

            if status == .accepted {
                self.actionType = .leave
            } else if pendingStatuses.contains(status) {
                self.actionType = .unRequest
            } else {
                self.actionType = .none
            }
        }
    }

    private func getPinRequestsInvitations() {
        guard let pinId = pin?.pinId else { return }

        PinRequestApi2.getPinRequest(pinId, type: .invitation, status: .pending, completion: { (error, response) in
            guard let request = response?.metaData else { return }

            if request.inviterId?.userId == request.pinId?.hostId?.userId {
                self.pinRequest = request
                self.actionType = .join
            }
        })
    }
}

// TextFieldDatePickerDelegate

extension PinEditController: TextFieldDatePickerDelegate {
    
    func textFieldDatePicker(onChanged date: Date) {
        guard
            let startTimeTextField = startTimeTextField,
            let endTimeTextField = endTimeTextField
            else { return }
        
        // Change date if the user selects a start date greater than end date
        if startTimeTextField.date > endTimeTextField.date {
            endTimeTextField.date = moment(startTimeTextField.date).add(2, .Hours).date
        }
        
        // Change date if the user selects a start date lower than the minimum end date
        if moment(startTimeTextField.date).add(48, .Hours).date < endTimeTextField.date {
            endTimeTextField.date = moment(startTimeTextField.date).add(2, .Hours).date
        }
        
        // Set restrictions to endDatePicker
        endTimeTextField.minimumDate = startTimeTextField.date
        endTimeTextField.maximumDate = moment(startTimeTextField.date).add(48, .Hours).date
    }
}

// UITextFieldDelegate

extension PinEditController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == addressTextField {
            let controller = PinDetailStoryboard.pickAddress

            if let controller = controller.topViewController as? PickAddressController {
                controller.delegate = self
                controller.textAddress = textField.text
            }

            controller.present(from: self)
        }
    }
}

// PickAddressDelegate

extension PinEditController: PickAddressDelegate {
    func didSelectAddress(_ pickAddressController: PickAddressController, address: PinAddress) {
        self.address = address
    }
}

extension PinEditController: EmptyState {
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -58
    }

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = pinRequest == nil ? "Loading" : "This pin has ended"

        let font = UIFont.systemFont(ofSize: 18)
        let attributes = [NSAttributedStringKey.font: font]
        let title = NSAttributedString(string: text, attributes: attributes)

        return title
    }
}
