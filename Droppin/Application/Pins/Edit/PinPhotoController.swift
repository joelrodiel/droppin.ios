//
//  PinPhotoController.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/14/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

protocol PinPhotoControllerDelegate {
    
    var isPickingPhoto: Bool {get set}
    
    func pinPhoto(didSelected image: UIImage)
}

extension PinPhotoControllerDelegate {
    
    mutating func takePhoto(in controller: UIViewController, with imagePickerController: UIImagePickerController, and sourceView: UIView) {
        isPickingPhoto = true
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sourceView
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: sourceView.frame.size.width / 2, y: sourceView.frame.size.height, width: 0, height: 0)
        
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {(action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                controller.present(imagePickerController, animated: true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            controller.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        controller.present(actionSheet, animated: true, completion: nil)
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "End"))
    }
    
    func selected(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            pinPhoto(didSelected: image)
        }
        
        picker.dismiss(animated: true)
    }
    
    func dismiss(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

class PinPhotoController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var imagePickerController: UIImagePickerController = UIImagePickerController()
    var delegate: PinPhotoControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
    }
    
    func takePhoto(_ sourceView: UIView) {
        delegate?.takePhoto(in: self, with: imagePickerController, and: sourceView)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        delegate?.selected(picker, didFinishPickingMediaWithInfo: info)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        delegate?.dismiss(picker)
    }
}

class PinPhotoTableController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var imagePickerController: UIImagePickerController = UIImagePickerController()
    var delegate: PinPhotoControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
    }
    
    func takePhoto(_ sourceView: UIView) {
        delegate?.takePhoto(in: self, with: imagePickerController, and: sourceView)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        delegate?.selected(picker, didFinishPickingMediaWithInfo: info)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        delegate?.dismiss(picker)
    }
}
