import UIKit

class EmptyMyPinsVC: UIViewController{

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var titleText: String?
    var descriptionText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let description = descriptionText {
            descriptionLabel.text = description
        }
        
        if let title = titleText {
            titleLabel.text = title
        }
    }
}
