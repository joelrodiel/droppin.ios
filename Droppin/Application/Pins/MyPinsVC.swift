import UIKit
import SwiftDate
import Dollar
import ScrollableSegmentedControl

class MyPinsVC: UIViewController, DroppinView, PinApi, PinRequestApi {
    
    @IBOutlet weak var segmentedView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newPinItem: UIBarButtonItem!
    @IBOutlet weak var currentView: UIView!
    
    private var segmentedBar = ScrollableSegmentedControl()
    
    var api =  DroppinApi()
    var pinsBySection = [Section: [Pin]]()
    var requestsBySection = [Section: [PinRequest]]()
    var tabIndex: Section?
    var initialAction: InitialAction?
    var embeddedViewController: PinDetailViewController?
    var hasCurrentPins: Bool = false
    
    enum InitialAction: String {
        case createPin = "CreatePinSegue"
    }

    private enum Segue: String {
        case myPinsToPinDetail = "MyPinsToPinDetailSegue"
        case embedToPinDetail = "EmbedToPinDetailSegue"
    }
    
    @IBAction func unwindToMyEventsController(forCancel unwindSegue:UIStoryboardSegue){
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: " Cancel"))
    }

    @IBAction func unwindToMyEventsController(forAddPin unwindSegue:UIStoryboardSegue){
        reloadPins()
    }

    enum Section: Int {
        case current = 0, hosting, joined, requested, previous
        static let count = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.navigationItem.rightBarButtonItem = newPinItem
        
        self.reloadPins()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        self.tableView.backgroundView?.isHidden = true
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tabIndex = Section.hosting
        
        if let action = self.initialAction {
            self.performSegue(withIdentifier: action.rawValue, sender: nil)
        }
    }
    
    func reloadPins() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        DiActivityIndicator.showActivityIndicator()
        
        getHostPins() { diResult in
            switch diResult {
            case .success(let data):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getHostPinsDetail Success"))
                self.process(pins: data.pins)
            case .failure(let requestResult, let error):
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
            }
            
            DiActivityIndicator.hide()
            self.tableView.reloadData()
        }
        
        getAttendeeRequests() { diResult in
            switch diResult {
            case .success(let data):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "getAttendeeRequests Success"))
                self.process(requests: data.requests)
            case .failure(let requestResult, let error):
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
            }

            DiActivityIndicator.hide()
            self.tableView.reloadData()
        }
    }
    
    func process(pins: [Pin]) {
        self.pinsBySection[.hosting] = pins.filter { $0.isHosting }
        self.pinsBySection[.previous] = pins.filter { $0.isPrevious }
        self.pinsBySection[.current] = pins.filter { $0.isCurrent }
        
        self.enableCurrentTab()
    }
    
    func process(requests: [PinRequest]) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        
        let requests = requests.filter { $0.isRequest }
        
        requestsBySection[.joined] = requests.filter { $0.isJoined }
        requestsBySection[.current] = requests.filter { $0.isCurrent }
        requestsBySection[.previous] = requests.filter { $0.isPrevious }
        
        // Add conflicted requests
        let conflicted = requests.filter { $0.isConflicted }
        let requested = requests.filter { $0.isRequested }
        
        requestsBySection[.requested] = conflicted + requested
        
        self.enableCurrentTab()
    }
    
    func enableCurrentTab() {
        let pins = self.pinsBySection[.current] ?? []
        let requests = self.requestsBySection[.current] ?? []
        hasCurrentPins = pins.count > 0 || requests.count > 0
        
        changePinsList(self.segmentedBar)
        
        guard let controller = embeddedViewController else { return }
        
        if hasCurrentPins {
            if pins.count > 0 {
                controller.pin = pins[0]
            } else if requests.count > 0 {
                controller.request = requests[0]
                controller.pin = requests[0].pin
            }
            
            controller.removeOverlay()
            controller.role = .joined
            controller.refreshView()
        } else {
            controller.showOverlay(text: "You are not in a pin right now.", withLoading: false)
        }
    }
    
    func chengeEmptyViewVisibility(visible: Bool) {
        if visible {
            let emptyMyPinsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "emptyMyPinsView") as! EmptyMyPinsVC
            emptyMyPinsVC.titleText = ""
            
            switch tabIndex! {
            case .hosting:
                emptyMyPinsVC.titleText = "You are currently not hosting any pins."
                emptyMyPinsVC.descriptionText = "Create a pin by tapping on the plus sign on the top right."
            case .joined:
                emptyMyPinsVC.descriptionText = "You are currently not joined to any pins."
            case .requested:
                emptyMyPinsVC.descriptionText = "You have not requested to join any pins."
            case .previous:
                emptyMyPinsVC.descriptionText = "You have not joined or hosted any pins in the last month."
            case .current:
                emptyMyPinsVC.descriptionText = "You are currently not hosting or attending any pin."
            }
            
            self.tableView.backgroundView = emptyMyPinsVC.view
        } else {
            self.tableView.backgroundView = nil
        }
    }

    @IBAction func changePinsList(_ sender: ScrollableSegmentedControl) {
        let index = sender.selectedSegmentIndex
        self.tabIndex = Section.init(rawValue: index)
        self.tableView.reloadData()
        
        if self.tabIndex == .current {
            self.currentView.isHidden = false
            self.tableView.isHidden = true
        } else {
            self.currentView.isHidden = true
            self.tableView.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if segue.identifier == Segue.myPinsToPinDetail.rawValue, let controller = segue.destination as? PinDetailViewController {
            if let pin = sender as? Pin {
                controller.pin = pin
                controller.role = .attendee
                controller.delegate = self
            } else if let request = sender as? PinRequest {
                let index = self.tabIndex!
                
                controller.request = request
                controller.role = index == .joined ? .joined : .outsider
            }
        }
        
        if segue.identifier == Segue.embedToPinDetail.rawValue, let controller = segue.destination as? PinDetailViewController {
            self.embeddedViewController = controller
            self.embeddedViewController!.delegate = self
            self.embeddedViewController!.isRefreshing = true
        }
    }
}

extension MyPinsVC: PinDetailViewControllerDelegate {
    
    func afterTerminate(pin: Pin) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        self.reloadPins()
    }
}

extension MyPinsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = self.tabIndex!
        let pin: Pin!
        
        switch index {
        case .hosting:
            pin = pinsBySection[.hosting]![indexPath.row]
        case .previous:
            pin = indexPath.section == 0 ? pinsBySection[.previous]![indexPath.row] : requestsBySection[.previous]![indexPath.row].pin
        case .current:
            if let currentPins = self.pinsBySection[.current], currentPins.count > 0 {
                pin = self.pinsBySection[.current]![indexPath.row]
            } else {
                pin = self.requestsBySection[.current]![indexPath.row].pin
            }
        case .joined, .requested:
            self.performSegue(withIdentifier: Segue.myPinsToPinDetail.rawValue, sender: requestsBySection[index]![indexPath.row])
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        self.performSegue(withIdentifier: Segue.myPinsToPinDetail.rawValue, sender: pin)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MyPinsVC: UITableViewDataSource {
    
    func countItemsInCurrentSection(_ section: Int) -> Int {
        let index = self.tabIndex ?? .hosting
        
        switch index {
        case .hosting:
            if let pins = pinsBySection[.hosting] {
                return pins.count
            }
        case .joined:
            if let requests = requestsBySection[.joined] {
                return requests.count
            }
        case .requested:
            if let requests = requestsBySection[.requested] {
                return requests.count
            }
        case .previous:
            if let pins = pinsBySection[.previous], section == 0 {
                return pins.count
            }
            if let requests = requestsBySection[.previous], section == 1 {
                return requests.count
            }
        case .current:
            if let currentPins = self.pinsBySection[.current], currentPins.count > 0 {
                return currentPins.count
            } else if let currentRequests = self.requestsBySection[.current], currentRequests.count > 0 {
                return currentRequests.count
            }
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.tabIndex == .previous, section == 0, countItemsInCurrentSection(0) > 0 {
            return "Hosted Pins"
        }
        
        if self.tabIndex == .previous, section == 1, countItemsInCurrentSection(1) > 0 {
            return "Attended Pins"
        }
        
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tabIndex == .previous ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PinTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = countItemsInCurrentSection(section)
        var visible = count == 0
        
        if self.tabIndex == .previous {
            visible = countItemsInCurrentSection(0) + countItemsInCurrentSection(1) == 0
        }
        
        chengeEmptyViewVisibility(visible: visible)
        
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = self.tabIndex ?? .hosting
        let cell = tableView.dequeueReusableCell(withIdentifier: PinTableViewCell.identifier, for: indexPath) as! PinTableViewCell
        let pin: Pin!

        switch index {
        case .hosting:
            pin = self.pinsBySection[.hosting]![indexPath.row]
        case .previous:
            pin = indexPath.section == 0 ? self.pinsBySection[.previous]![indexPath.row] : self.requestsBySection[.previous]![indexPath.row].pin
        case .current:
            if let currentPins = self.pinsBySection[.current], currentPins.count > 0 {
                pin = self.pinsBySection[.current]![indexPath.row]
            } else {
                pin = self.requestsBySection[.current]![indexPath.row].pin
            }
        default:
            pin = self.requestsBySection[tabIndex!]![indexPath.row].pin
        }

        cell.load(pin: pin)

        return cell
    }
}

// MARK: UI Helpers

extension MyPinsVC {
    
    private func setupUI() {
        setupSegmentedBar()
    }
    
    private func setupSegmentedBar() {
        segmentedView.addSubview(segmentedBar)
        segmentedView.addBottomBorder(.border)
        
        segmentedBar.translatesAutoresizingMaskIntoConstraints = false
        segmentedBar.topAnchor.constraint(equalTo: segmentedView.topAnchor).isActive = true
        segmentedBar.rightAnchor.constraint(equalTo: segmentedView.rightAnchor).isActive = true
        segmentedBar.bottomAnchor.constraint(equalTo: segmentedView.bottomAnchor).isActive = true
        segmentedBar.leftAnchor.constraint(equalTo: segmentedView.leftAnchor).isActive = true
        
        segmentedBar.segmentStyle = .textOnly
        segmentedBar.insertSegment(withTitle: "Current", at: 0)
        segmentedBar.insertSegment(withTitle: "Hosting", at: 1)
        segmentedBar.insertSegment(withTitle: "Joined", at: 2)
        segmentedBar.insertSegment(withTitle: "Requested", at: 3)
        segmentedBar.insertSegment(withTitle: "Previous", at: 4)
        
        segmentedBar.selectedSegmentIndex = 1
        segmentedBar.underlineSelected = true
        
        let action = #selector(self.changePinsList(_:))
        segmentedBar.addTarget(self, action: action, for: .valueChanged)
        
        segmentedBar.selectedSegmentContentColor = UIColor.aquaMarine
        segmentedBar.tintColor = UIColor.aquaMarine
    }
}
