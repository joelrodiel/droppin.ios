//
//  Pin.swift
//  Droppin
//
//  Created by Rene Reyes on 12/2/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import MapKit
import SwiftLocation

enum PinStatus: String, Codable {
    case red = "redpin" //scheduled
    case green = "greenpin" //active
    case yellow = "yellowpin" // aboutToStart
    case gray = "graypin" // past
    case black = "blackpin" // black
    
    func image() -> UIImage {
        switch self {
        case .red:
            return #imageLiteral(resourceName: "Pin Red")
        case .green:
            return #imageLiteral(resourceName: "Pin Green")
        case .yellow:
            return #imageLiteral(resourceName: "Pin Yellow")
        case .gray:
            return #imageLiteral(resourceName: "Pin Gray")
        case .black:
            return #imageLiteral(resourceName: "Pin Gray")
        }
    }
}

struct Pin: PinCore  {
    let pinId: String
    let title: String
    let coordinates: CLLocationCoordinate2D
    let status: PinStatus
    let startDate: Date
    let endDate: Date
    let hostId: String
    let description: String
    let street: String
    let city: String
    let state: String
    let zip: String
    let extraDirections: String?
    
    let photo: String?
    var annotation: PinAnnotation? = nil
    
    let joinCount: Int?
    var requestToJoinStatus: String?
    var statistic: PinStatistic?
    
    init(pinId: String, hostId: String, title: String, startDate:Date, endDate: Date, description: String, street: String, city: String, state: String, zip: String, extraDirections: String?, coordinates: CLLocationCoordinate2D, status: PinStatus, joinCount: Int?, requestToJoinStatus: String?, photo: String?) {
        self.pinId = pinId
        self.hostId = hostId
        self.title = title
        self.description = description
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
        self.extraDirections = extraDirections
        self.coordinates = coordinates
        self.startDate = startDate
        self.endDate = endDate
        self.status = status
        self.photo = photo
        self.joinCount = joinCount
        self.requestToJoinStatus = requestToJoinStatus
        self.annotation = PinAnnotation(self)
    }
    
    init(pinId: String, hostId: String, title: String, startDate:Date, endDate: Date, description: String, street: String, city: String, state: String, zip: String, extraDirections: String?, coordinates: CLLocationCoordinate2D, status: PinStatus, photo: String?) {
        self.init(pinId:pinId, hostId:hostId, title:title, startDate:startDate, endDate: endDate, description:description, street:street, city:city, state:state, zip:zip, extraDirections: extraDirections, coordinates:coordinates, status:status, joinCount:nil, requestToJoinStatus:nil, photo:photo)
    }
    
    var address: String {
        get { return "\(self.street), \(self.city)" }
    }
    
    var cllocation: CLLocation {
        get { return CLLocation(latitude: self.coordinates.latitude, longitude: self.coordinates.longitude) }
    }
    
    func calculateDistance(callback: @escaping (String) -> ()) {
        Locator.currentPosition(accuracy: .block, onSuccess: { location in
            let pinLocation = CLLocation(latitude: self.coordinates.latitude, longitude: self.coordinates.longitude)
            let distanceInMeters = location.distance(from: pinLocation)
            let distanceInMiles = distanceInMeters * 0.000621371192
            let distance = String(format: "%.1f mi", distanceInMiles)
            
            callback(distance)
        }, onFail: { err, last in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "hello error"))
            print("Failed to get location: \(err)")
        })
    }
    
    func toPin2() -> Pin2 {
        return Pin2(from: self)
    }
}

struct PinStatistic {
    let atPin: Int
    let joined: Int
    let invited: Int
    let maxAge: Int
    let minAge: Int
}

// MARK: Pin helpers

extension Pin {
    var isCurrent: Bool {
        let currentDate = Date()
        let condition1 = self.status != .black
        let condition2 = self.startDate <= currentDate
        let condition3 = self.endDate >= currentDate

        return condition1 && condition2 && condition3
    }

    var isPrevious: Bool {
        let condition1 = self.status == .gray
        let condition2 = self.status == .black

        return condition1 || condition2
    }

    var isHosting: Bool {
        let condition1 = self.status != .black

        return condition1
    }
}
