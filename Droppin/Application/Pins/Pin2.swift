//
//  Pin2.swift
//  Droppin
//
//  Created by Madson on 9/7/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftLocation

struct PinResponse: Codable {
    var httpStatusCode: Int?
    var message: String?
    var pin: Pin2?
}

struct Pin2: Codable {

    // MARK: - Properties

    var pinId: String?
    var title: String
    var pinStatus: PinStatus
    var requestToJoinStatus: String?
    var street: String?
    var city: String?
    var state: String?
    var zip: String?
    var description: String?
    var startDate: Date
    var endDate: Date
    var hostId: Host?
    var isActive: Bool
    var joinCount: Int?
    var loc: Location?
    var photo: String?
    var terminated: Bool
    var extraDirections: String?
    var isHostAnAttendee: Bool?

    // MARK: - Computed properties

    init(from pin: Pin) {
        self.pinId = pin.pinId
        self.title = pin.title
        self.pinStatus = pin.status
        self.requestToJoinStatus = pin.requestToJoinStatus
        self.street = pin.street
        self.city = pin.city
        self.state = pin.state
        self.zip = pin.zip
        self.description = pin.description
        self.startDate = pin.startDate
        self.endDate = pin.endDate
        self.hostId = Host(userId: pin.hostId)
        self.isActive = true
        self.loc = Location(from: pin.coordinates)
        self.photo = pin.photo
        self.terminated = true
        self.extraDirections = pin.extraDirections
    }
    
    var coordinates: CLLocationCoordinate2D? {
        guard
            let loc = self.loc,
            let coordinates = loc.coordinates,
            coordinates.count == 2,
            let lon = coordinates.first,
            let lat = coordinates.last else { return nil }

        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }

    var fullAddress: String? {
        get {
            if let street = street, let city = city, let state = state {
                return "\(street), \(city), \(state)"
            }

            return nil
        }
    }
    
    func calculateDistanceInMiles(callback: @escaping (LocationError?, Double?) -> ()) {
        let calculateThenAnswer: (CLLocation) -> () = { location in
            guard
                let longitude = self.coordinates?.longitude,
                let latitude = self.coordinates?.latitude
                else {
                    let error = LocationError.other("Latitude and longitude not available")
                    return callback(error, nil)
            }
            
            let pinLocation = CLLocation(latitude: latitude, longitude: longitude)
            let distanceInMeters = location.distance(from: pinLocation)
            let distanceInMiles = distanceInMeters * 0.000621371192
            
            callback(nil, distanceInMiles)
        }
        
        Locator.currentPosition(accuracy: .block, onSuccess: { location in
            calculateThenAnswer(location)
        }, onFail: { err, last in
            guard let last = last else { return callback(err, nil) }
            calculateThenAnswer(last)
        })
    }

    func userRole(_ userId: String, completion: @escaping (UserRole?) -> Void) {
        if self.hostId?.userId == userId {
            completion(UserRole.host)
        } else {
            guard let pinId = pinId else {
                completion(nil)
                return
            }

            PinRequestApi2.getPinRequest(pinId, type: .request, status: nil) { (error, response) in
                guard
                    error == nil,
                    let request = response?.metaData,
                    let status = request.status else {
                        completion(nil)
                        return
                }

                let role = self.getRole(for: status)
                completion(role)
            }
        }
    }

    func toPin1() -> Pin? {
        guard
            let pinId = pinId,
            let street = street,
            let city = city,
            let state = state,
            let zip = zip,
            let description = description,
            let hostId = hostId?.userId,
            let photo = photo,
            let coordinates = coordinates
            else { return nil }

        return Pin(pinId: pinId, hostId: hostId, title: title, startDate: startDate, endDate: endDate, description: description, street: street, city: city, state: state, zip: zip, extraDirections: extraDirections, coordinates: coordinates, status: pinStatus, photo: photo)
    }
    
    private func getRole(for status: PinRequestStatus) -> UserRole {
        switch status {
        case .atPin, .attended:
            return .attendee
        case .left, .conflicted, .accepted:
            return .joined
        default:
            return .outsider
        }
    }
}

extension Pin2 {
    var toJSONDictionary: [String: Any]? {
        do {
            let json = try JSONEncoder.shared.encode(self)
            let another = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String: Any]
            return another
        } catch {
            debugPrint("Pin2 - toJSON - JSONEncoder error")
            debugPrint(error.localizedDescription)
            return nil
        }
    }
}

struct Location: Codable {
    var coordinates: [Double]?
    var type: String?
    
    init() {
    }
    
    init(from coordinates: CLLocationCoordinate2D) {
        self.coordinates = [coordinates.longitude, coordinates.latitude]
        self.type = "Point"
    }
}

struct Host: Codable {
    var userId: String?
    var firstName: String?
    var lastName: String?
    var photo: String?
    
    init(userId: String) {
        self.userId = userId
    }
    
    var fullName: String? {
        guard
            let firstName = self.firstName,
            let lastName = self.lastName
            else { return nil }
        return "\(firstName) \(lastName)"
    }
}
