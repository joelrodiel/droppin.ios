import Foundation
import SwiftyJSON
import MapKit

class PinDetailRequestResult: RequestResult {

    let pin: Pin?
    required  init(_ json: JSON ) throws {
        pin = PinsDetailRequestResult.loadPin(json: json["pin"])

        try super.init(json)
    }
}

