//
//  PinRequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/4/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

// NOT USED!
class PinRequestResult: RequestResult {
//    let statusCode: StatusCode?
//    let errorCode: ErrorCode?
//    let message: String?
//    let metaData:  [String:String]?
    let pinId: String
    
//    override init(){
////        statusCode = nil
////        errorCode = nil
////        message = nil
////        metaData = nil
//        pinId = nil
//        super.init()
//    }
    
    
    
    required init(_ json: JSON) throws {
//        pinId = json["pinId"].string
        pinId = try RequestResult.extractPinIdFromApiJsonResponse(json)
        try super.init(json)
        
        
//        if let statusCode =  json["statusCode"].string {
//            self.statusCode = StatusCode(rawValue: statusCode)
//        } else {
//            self.statusCode = nil
//        }
//
//        if let errorCode =  json["errorCode"].int {
//            self.errorCode = ErrorCode(rawValue: errorCode)
//        } else {
//            self.errorCode = nil
//        }
//
//        message = json["message"].string
//
//
//        if json["metaData"].exists() {
//            self.metaData = json["metaData"].reduce([String:String](), { (result: [String:String], item: Any) -> [String:String] in
//                let (key, value) = item as! (String, JSON)
//                var out = result
//                out[key] = value.stringValue
//                return out
//            })
//        } else {
//            self.metaData = nil
//        }
    }
    
}
