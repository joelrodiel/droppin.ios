//
//  PinRequestSocketClient.swift
//  Droppin
//
//  Created by Isaias Garcia on 1/16/19.
//  Copyright © 2019 Mintz Holdings, LLC. All rights reserved.
//

import SwiftyJSON

class PinRequestSocketClient: DiSocketClient {
    
    private let LOCATION_UPDATED_KEY = "LOCATION-UPDATED"
    private let REQUEST_STATUS_UPDATED_KEY = "REQUEST-STATUS-UPDATED"
    private let PIN_STATUS_UPDATED_KEY = "PIN-STATUS-UPDATED"
    private let CURRENT_REQUEST_KEY = "CURRENT-REQUEST-REFRESHED-"
    
    private var _attendeeId: String

    var attendeeId: String {
        get { return _attendeeId }
    }
    
    init(attendeeId: String) {
        self._attendeeId = attendeeId
        super.init()
    }

    func onLocationUpdated() {
        super.emit(event: LOCATION_UPDATED_KEY, data: [attendeeId])
    }
    
    func onStatusUpdated(requestId: String, for status: PinRequestStatus) {
        super.emit(event: REQUEST_STATUS_UPDATED_KEY, data: [requestId, status.rawValue])
    }
    
    func onStatusUpdated(pinId: String, for status: Bool) {
        super.emit(event: PIN_STATUS_UPDATED_KEY, data: [pinId, status])
    }
    
    func listen(_ completion: @escaping (JSON) -> ()) {
        let event = CURRENT_REQUEST_KEY + attendeeId
        
        super.listen(event: event) { data in
            if data.count > 0 {
                completion(JSON(data[0]))
            }
        }
    }
}
