//
//  Pin.swift
//  Droppin
//
//  Created by Rene Reyes on 12/2/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import MapKit

protocol PinCore {
    var pinId: String {get}
    var title: String {get}
    var coordinates: CLLocationCoordinate2D {get}
    var status: PinStatus {get}
    var startDate: Date {get}
    var annotation: PinAnnotation? {get}
}


struct PinSummary: PinCore  {
    let pinId: String
    let title: String
    let coordinates: CLLocationCoordinate2D
    let status: PinStatus
    let startDate: Date
//    lazy var annotation = PinAnnotation(self)
    var annotation: PinAnnotation? = nil

    
    init(pinId: String,  title: String, startDate: Date ,coordinates: CLLocationCoordinate2D, status: PinStatus) {
        self.pinId = pinId
        self.title = title
        self.startDate  = startDate
        self.coordinates = coordinates
        self.status = status
        self.annotation = PinAnnotation(self)
//        self.annotation = PinAnnotation(pin)
    }
    
}
//
//public protocol MKAnnotation : NSObjectProtocol {
//
//
//    // Center latitude and longitude of the annotation view.
//    // The implementation of this property must be KVO compliant.
//    public var coordinate: CLLocationCoordinate2D { get }
//
//
//    // Title and subtitle for use by selection UI.
//    optional public var title: String? { get }
//
//    optional public var subtitle: String? { get }
//}


