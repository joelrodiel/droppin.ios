//
//  PinRequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/4/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class PinSummaryRequestResult: RequestResult {
    let pinSummary: PinSummary?
    
    required  init(_ json: JSON ) throws {
        let coordinates = try RequestResult.extractCoordinatesFromApiJsonResponse(json)
        let startDate = try RequestResult.extractStartDateFromApiJsonResponse(json)
        let status = try RequestResult.extractPinStatusFromApiJsonResponse(json)
        let pinId = try RequestResult.extractPinIdFromApiJsonResponse(json)
        let title = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "title")
        
        pinSummary =  PinSummary(
            pinId: pinId,
            title: title,
            startDate: startDate,
            coordinates: coordinates,
            status: status
        )
        
        try super.init(json)
    }
}
