import Foundation
import SwiftyJSON
import MapKit

class PinsDetailRequestResult: RequestResult {
    let pins: [Pin]
    required  init(_ json: JSON ) throws {
        if let pinsCollection = json["pins"].array  {
            pins =  pinsCollection.compactMap {
                return PinsDetailRequestResult.loadPin(json: $0)
            }
        } else {
            print("No Pins!")
            pins = [Pin]()
        }
        try super.init(json)
    }

    class func loadPin(json: JSON) -> Pin? {
        do {
            let photo = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "photo")
            let coordinates = try RequestResult.extractCoordinatesFromApiJsonResponse(json)
            let startDate = try RequestResult.extractStartDateFromApiJsonResponse(json)
            let endDate = try RequestResult.extractDateFromApiJsonResponse(json: json, name: "endDate")
            let status = try RequestResult.extractPinStatusFromApiJsonResponse(json)
            let pinId = try RequestResult.extractPinIdFromApiJsonResponse(json)
            let hostId = try RequestResult.extractUserIdFromApiJsonResponse(json["hostId"])
            let title = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "title")
            let description = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "description")
            let street = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "street")
            let city = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "city")
            let state = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "state")
            let zip = try RequestResult.extractStringParameterFromApiJsonResponse(json: json, name: "zip")
            let extraDirections = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "extraDirections")
            let joinCount =  RequestResult.extractNumberParameterFromApiJsonResponse(returnNilIfNone: json, name: "joinCount")
            let requestToJoinStatus = RequestResult.extractStringParameterFromApiJsonResponse(returnNilIfNone: json, name: "requestToJoinStatus")
            
            return  Pin( pinId: pinId,
                         hostId: hostId,
                         title: title,
                         startDate: startDate,
                         endDate: endDate,
                         description: description,
                         street: street,
                         city: city,
                         state: state,
                         zip: zip,
                         extraDirections: extraDirections,
                         coordinates: coordinates,
                         status: status,
                         joinCount: joinCount,
                         requestToJoinStatus: requestToJoinStatus,
                         photo: photo
            )
        } catch let error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "flatmap exec caused an error!  ERROR!!! \(error.localizedDescription)")
            return nil // flatmap will remove nil pins
        }
    }
}
