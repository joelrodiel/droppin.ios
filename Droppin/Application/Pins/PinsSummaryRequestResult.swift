//
//  PinsRequestResult.swift
//  Droppin
//
//  Created by Rene Reyes on 12/4/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class PinsSummaryRequestResult : RequestResult {
    
    let pinsSummary: [PinSummary]?
    
     required  init(_ json: JSON ) throws {
    
        guard let statusCodeValue = json["statusCode"].string,  let statusCode = StatusCode(rawValue: statusCodeValue) ,statusCode != StatusCode.dNoPinsFound else {
            print("No Pins near you!")
            pinsSummary = nil
            try super.init(json)
            return
        }
        
        
        guard let pinsCollection = json["pins"].array else {
            print("No Pins near you!")
            pinsSummary = nil
            try super.init(json)
            return
        }
        
        print("[PinsSummaryRequestResult] ->create pinsCollection")
        pinsSummary = pinsCollection.flatMap {
            do {
//                guard let longitude = $0["loc"]["coordinates"][0].double,  let latitude = $0["loc"]["coordinates"][1].double else {
//                    throw DiError.InvalidJsonPin("No Coordinates found or coordinates are not numbers")
//                }
//
//                guard longitude != 0 && latitude != 0 else {
//                    throw DiError.InvalidJsonPin("coordinates are set to 0,0 ")
//                }

                //@TODO this should not happen! The api needs to send accurate info!
//                guard let startDateString = $0["startDate"].string else {
//                    throw DiError.InvalidJsonPin("No startDate Given")
//                }

//                guard let pinId = $0["_id"].string else {
//                    throw DiError.InvalidJsonPin("Invalid PinId")
//                }

                let pinId = try RequestResult.extractPinIdFromApiJsonResponse($0)
                //            let longitude = CLLocationDegrees($0["loc"]["coordinates"][0].double!)
                //            let latitude = CLLocationDegrees($0["loc"]["coordinates"][1].double!)

//                let coordinates = try CLLocationCoordinate2D(latitude: latitude, longitude: longitude)


                let coordinates = try RequestResult.extractCoordinatesFromApiJsonResponse($0)
//                let coordinates = try extractCoordinatesFromApiJsonResponse($0)
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"2017-12-06T01:10:13.000Z"

//                guard let startDate = dateFormatter.date(from: startDateString) else {
//                    throw DiError.InvalidJsonPin("Invalid Date")
//                }

                let startDate = try RequestResult.extractStartDateFromApiJsonResponse($0)
//                let type = PinType(rawValue: $0["type"].stringValue) ?? PinType.misc
                
                let status = try RequestResult.extractPinStatusFromApiJsonResponse($0)
                
                let title = try RequestResult.extractStringParameterFromApiJsonResponse(json: $0, name: "title")

                return PinSummary(
                    pinId: pinId,
                    title: title,
                    startDate: startDate,
                    coordinates: coordinates,
                    status: status
                )
            } catch {
                return nil // if theres a problem with the pin then with nil it will be removed uwing flatmap
            }
        }
        
        print("pins count: \(String(describing: pinsSummary?.count))")
        
        try super.init(json)
    }
}


//extend RequestResult {
//
//    extractCoordinatesFromApiJsonResponse(_ json JSON) throws -> CLLocationCoordinate2D {
//        guard let longitude = json["loc"]["coordinates"][0].double,  let latitude = json["loc"]["coordinates"][1].double else {
//            throw DiError.InvalidJsonPin("No Coordinates found or coordinates are not numbers")
//        }
//
//        guard longitude != 0 && latitude != 0 else {
//            throw DiError.InvalidJsonPin("coordinates are set to 0,0 ")
//        }
//
//          let coordinates = try CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//
//        return coordinates
//    }
//}


