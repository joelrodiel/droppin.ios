//
//  CurrentEventsTableViewCell.swift
//  Droppin
//
//  Created by Rene Reyes on 11/9/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import SwiftDate

class PreviousPinTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var startDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func refresh(_ pin: Pin) {
        title.text = pin.title
        
        let pinStartDate = DateInRegion(absoluteDate: pin.startDate)
        startTime.text = pinStartDate.string(format: .custom("h:mm a"))
        startDate.text = pinStartDate.string(format: .custom("MM-dd-yyyy"))
        
        if let photo = pin.photo {
            self.photo.cldSetImage(publicId: photo, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }
    }

}
