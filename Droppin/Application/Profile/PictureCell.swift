//
//  PictureCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 20/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

class PictureCell: UICollectionViewCell {
    
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var indicator: UILabel!
    
    var imageButtonCallback: (() -> ())?
    var removeButtonCallback: (() -> ())?
    
    private var _image: String?
    
    var image: String? {
        get { return _image }
        set {
            _image = newValue
            prepareData()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.image = nil
    }

    override func awakeFromNib() {
        imageButton.roundBorders()
    }
    
    func prepareData() {
        if let image = image {
            self.imageButton.cldSetImage(publicId: image, cloudinary: CDNManager.shared.cloudinary, forState: .normal, signUrl: true)
            removeButton.isHidden = false
            indicator.isHidden = true
        } else {
            imageButton.setImage(#imageLiteral(resourceName: "profilePicLogo"), for: .normal)
            removeButton.isHidden = true
            indicator.isHidden = false
        }
    }
    
    @IBAction func imageButtonPressed(_ sender: UIButton) {
        imageButtonCallback?()
    }
    
    @IBAction func removeButtonPressed(_ sender: UIButton) {
        image = nil
        removeButtonCallback?()
    }
}
