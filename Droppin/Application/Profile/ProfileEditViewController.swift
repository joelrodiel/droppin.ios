//
//  ProfileEditViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 19/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import TagListView
import CropViewController

class ProfileEditViewController: UIViewController, DroppinView, DiImageLoader {
    
    @IBOutlet weak var firstName: DefaultTextField!
    @IBOutlet weak var lastName: DefaultTextField!
    @IBOutlet weak var gender: DefaultTextField!
    @IBOutlet weak var birthDate: DefaultTextField!
    @IBOutlet weak var birthdateMessageLabel: UILabel!
    @IBOutlet weak var bio: DefaultTextView!
    @IBOutlet weak var interestsTags: TagListView!
    
    var api = DroppinApi()
    var user: User!
    var interests = getInterests()
    
    private var imagesController: ImagesPickerViewController?
    private let dateFormatter = DateFormatter()
    private let birthDatePicker = UIDatePicker()
    private let genderPicker = UIPickerView()
    
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if DiHelper.isNotAValidUserId {
            return goBack(error: "User ID not valid")
        }
        
        if let imagesController = self.childViewControllers.first as? ImagesPickerViewController {
            self.imagesController = imagesController
            self.imagesController?.isEditingImages = true
        }
        
        setupView()
        loadUserInfo()
    }
    
    func goBack(error: String?) {
        navigationController?.popViewController(animated: true)
        
        if let error = error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: error)
            DiAlert.alertSorry(message: error, inViewController: self)
        }
    }
    
    func setupView() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        gender.clearButtonMode = .never
        genderPicker.delegate = self
        gender.inputView = genderPicker
        gender.delegate = self
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        birthDate.clearButtonMode = .never
        birthDate.delegate = self
        birthDate.inputView = birthDatePicker
        birthdateMessageLabel.text = ""
        birthDatePicker.datePickerMode = .date
        birthDatePicker.maximumDate = Date()
        birthDatePicker.addTarget(self, action: #selector(birthDateChanged), for: .valueChanged)
        
        interestsTags.paddingX = 10
        interestsTags.paddingY = 10
        (interestsTags as UIView).cornerRadius = 20
        interestsTags.textFont = UIFont.systemFont(ofSize: 17)
        interestsTags.alignment = .center
        interestsTags.delegate = self
        interestsTags.removeAllTags()
        
        for i in 1...interests.count {
            interests[i - 1].tagView = interestsTags.addTag(interests[i - 1].label)
        }
    }
    
    func loadUserInfo() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()
        
        getUserInfo(DiHelper.userId!) { result in
            switch result {
            case .success(let data):
                self.user = data.user
                
                self.loadImages(userId: self.user.userId) { images in
                    self.imagesController?.setupData(data: images)
                    DiActivityIndicator.hide()
                    self.prepareData()
                }
            case .failure(_, let error):
                DiActivityIndicator.hide()
                self.goBack(error: "Error: \(error.localizedDescription)")
            }
        }
    }
    
    func prepareData() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        firstName.text = user.firstName
        lastName.text = user.lastName
        gender.isEnabled = !user.genderHasChanged
        gender.text = user.gender.rawValue.capitalized
        genderPicker.selectRow(user.gender.index, inComponent: 0, animated: false)
        birthDate.text = dateFormatter.string(from: user.birthDate)
        birthDatePicker.date = user.birthDate
        bio.text = user.bio

        user.interests.forEach { interest in
            if let index = interests.index(where: {$0.label == interest}), let tagView = interests[index].tagView {
                interests[index].isSelected = true
                tagView.changeStatus(selected: true)
            }
        }
    }
    
    @objc func birthDateChanged() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        birthDate.text = dateFormatter.string(from: birthDatePicker.date)
    }
    
    @IBAction func saveUser(_ sender: Any) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        self.user = extractUserFromForm()
        
        if !isUserValid(user: user) {
            return DiAlert.alertSorry(message: "Data is not valid", inViewController: self)
        }
        
        guard let imagesController = imagesController else { return updateUser() }
        
        let filtered = imagesController.getImages().compactMap { $0 }
        if filtered.count > 0 {
            DiActivityIndicator.showActivityIndicator()
            
            uploadImages(images: filtered) { error in
                if let error = error {
                    DiActivityIndicator.hide()
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                    DiAlert.alertSorry(message: "Pictures not updated", inViewController: self)
                    return
                }
                
                self.updateUser()
            }
        } else {
            DiAlert.alertSorry(message: "Picture not selected", inViewController: self)
        }
    }
    
    func updateUser() {
        let selectedInterests = self.interests.filter{ $0.isSelected }
        
        self.update(user: user, interests: selectedInterests) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success \(data.message ?? "No message")")
                self.goBack(error: nil)
            case .failure(let result, let error):
                self.showAlert(title: "Sorry", result: result, error: error, viewController: self)
            }
            
            DiActivityIndicator.hide()
        }
    }
    
    func extractUserFromForm() -> User {
        let userId = user.userId
        let firstName = self.firstName.text ?? ""
        let lastName = self.lastName.text ?? ""
        let bio = self.bio.text ?? ""
        let gender = Gender.allCases[genderPicker.selectedRow(inComponent: 0)]
        let birthDate = birthDatePicker.date
        let interests: [String] = []
        
        return User(userId: userId, firstName: firstName, lastName: lastName, bio: bio, gender: gender, birthDate: birthDate, interests: interests)
    }
    
    func isUserValid(user: User) -> Bool {
        let firstNameValid = user.firstName.count > 0 && user.firstName.count < 100
        let lastNameValid = user.lastName.count > 0 && user.lastName.count < 100
        let bioValid = user.bio.count > 0 && user.bio.count < 320
        
        return firstNameValid && lastNameValid && bioValid
    }
    
}

extension ProfileEditViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Gender.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Gender.allCases[row].rawValue.capitalized
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        gender.text = Gender.allCases[row].rawValue.capitalized
    }
}

extension ProfileEditViewController: UITextFieldDelegate {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        DispatchQueue.main.async(execute: {
            (sender as? UIMenuController)?.setMenuVisible(false, animated: false)
        })
        return false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == birthDate {
            showBirthdateAlert()
        }
    }
}

extension ProfileEditViewController: TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let index = interests.index(where: {$0.tagView == tagView}) {
            let selectedCount = interests.filter({$0.isSelected}).count
            let isSelected = !interests[index].isSelected
            
            if !isSelected || (isSelected && selectedCount < 9) {
                interests[index].isSelected = !interests[index].isSelected
                tagView.changeStatus(selected: interests[index].isSelected)
            }
        }
    }
}

private extension TagView {
    
    func changeStatus(selected: Bool) {
        if selected {
            tagBackgroundColor = #colorLiteral(red: 0.527591357, green: 0.8006302442, blue: 0.716739279, alpha: 1)
            textColor = UIColor.white
            borderColor = UIColor.white
        } else {
            tagBackgroundColor = UIColor.white
            textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
            borderColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        }
    }
}

extension ProfileEditViewController {
    func showBirthdateAlert() {
        if user.isBirthdateEditable {
            birthdateMessageLabel.text = "You can only change your birthdate once."
        } else {
            birthdateMessageLabel.text = "You can no longer change your birthdate."
            birthDate.resignFirstResponder()
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.birthdateMessageLabel.text = ""
        }
    }
}
