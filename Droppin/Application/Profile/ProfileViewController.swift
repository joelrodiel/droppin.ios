//
//  ProfileViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/22/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import TagListView
import JGProgressHUD

class ProfileViewController: UIViewController, DiImageLoader, PinRequestApi, ConnectionApi, DroppinView {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var hostView: UIView!
    @IBOutlet var pinLabel: UILabel!
    @IBOutlet weak var hostViewHeight: NSLayoutConstraint!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var bioBackgroundView: UIView!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet var plusButton: UIButton!
    @IBOutlet var plusButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var connectionsButton: UIButton!
    @IBOutlet weak var connectionsNumberLabel: UILabel!
    @IBOutlet var editButton: UIBarButtonItem!
    @IBOutlet weak var directMessageButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!

    var api = DroppinApi()
    var user: User!
    var userId: String?
    var pin: Pin2?
    var request: PinRequest?
    var connection: Connection?
    var isAbleToSendDirectMessages = false
    var isAbleToConnect = false
    var isAcceptee = false
    var role: UserRole = .outsider
    
    private var hud = JGProgressHUD(style: .dark)
    private let AGE_TEXT = "Age {{age}}"

    enum Segue: String {
        case profileToConnectionsSegue = "ProfileToConnectionsSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupStyle()
        self.showOverlay()
        self.refreshView()
        
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if user != nil {
            refreshData(userId: user.userId)
        }
    }
    
    func setupStyle() {
        // Round borders
        profileImageView.roundBorders()

        genderLabel.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        genderLabel.layer.borderWidth = 1
        
        ageLabel.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        ageLabel.layer.borderWidth = 1
        
        bioBackgroundView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        bioBackgroundView.layer.borderWidth = 1
        
        tagListView.paddingX = 10
        tagListView.paddingY = 10
        (tagListView as UIView).cornerRadius = 20
        tagListView.textFont = UIFont.systemFont(ofSize: 17)
        tagListView.alignment = .center

        hostView.isHidden = true
        hostViewHeight.constant = 0
        
        reportButton.isHidden = true
        directMessageButton.isHidden = true
    }
    
    func refreshData(userId: String) {
        getUserInfo(userId) { result in
            switch result {
            case .success(let data):
                self.user = data.user
                self.refreshView()
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "refreshData failure: \(error)")
                self.showOverlay(text: "Profile not available")
            }
        }
    }
    
    func refreshData(userId: String, pin: Pin2) {
        self.pin = pin
        refreshData(userId: userId)
    }
    
    func refreshData(userId: String, request: PinRequest) {
        self.request = request
        refreshData(userId: userId)
    }
    
    func refreshData(user: User, request: PinRequest) {
        self.user = user
        self.request = request
        self.refreshView()
    }
    
    func refreshView() {
        guard let user = self.user, self.isViewLoaded else { return }
        
        self.removeOverlay()

        if isThisMine(toCompare: user.userId) {
            role = .host
            self.directMessageButton.isHidden = true
            self.plusButton.isHidden = true
            tabBarController?.navigationItem.rightBarButtonItem = editButton
            reportButton.isHidden = true
        } else {
            self.connectionsButton.isEnabled = false
            self.connectionsNumberLabel.isEnabled = false
            self.checkConnection()
            reportButton.isHidden = false
        }

        displayInformation(for: role, and: user)

        if let pin = self.pin {
            self.pinLabel.text = pin.title

            hostView.isHidden = false
            hostViewHeight.constant = 42
        }

        self.contentView.layoutIfNeeded()

        // Update view height
        bioLabel.sizeToFit()

        contentViewHeight.constant += bioLabel.frame.height - 20.5
        contentViewHeight.constant += 16

        // Check connection
        self.checkStatistics()
    }

    private func displayInformation(for role: UserRole, and user: User) {
        loadProfilePicture(for: profileImageView, userId: user.userId)

        self.directMessageButton.isHidden = isThisMine(toCompare: user.userId) || !isAbleToSendDirectMessages

        nameLabel.text = User.checkAvailability(of: .fullName, for: role) ? user.fullName : user.abbreviatedName
        bioLabel.text = user.bio

        // Set gender and age
        genderLabel.text = user.gender.rawValue.capitalized
        ageLabel.text = AGE_TEXT.replacingOccurrences(of: "{{age}}", with: String(self.user.age))

        // Set interests tags
        tagListView.removeAllTags()
        tagListView.addTags(self.user.interests)

        nameLabel.isHidden = !User.checkAvailability(of: [.fullName, .shortName], for: role)
        connectionsButton.isHidden = !User.checkAvailability(of: .connectionsCount, for: role)
        connectionsNumberLabel.isHidden = !User.checkAvailability(of: .connectionsCount, for: role)
        profileImageView.isHidden = !User.checkAvailability(of: [.mainPicture, .allPictures], for: role)
        bioLabel.isHidden = !User.checkAvailability(of: .bio, for: role)
        genderLabel.isHidden = !User.checkAvailability(of: .gender, for: role)
        ageLabel.isHidden = !User.checkAvailability(of: .age, for: role)
        tagListView.isHidden = !User.checkAvailability(of: .interests, for: role)
        
        if isAcceptee {
            nameLabel.isHidden = true
        }
    }
    
    func checkStatistics() {
        getUserStatistic(self.user!.userId) { result in
            switch result {
            case .success(let data):
                self.connectionsNumberLabel.text = String(data.userStatistic.connections)
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
            }
        }
    }
    
    func refreshView(forConnection connection: Connection?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if connection == nil {
            if isAbleToConnect {
                return self.plusButton.setImage(#imageLiteral(resourceName: "Add Connection"), for: .normal)
            }
            
            self.plusButton.isHidden = true
            return
        }
        
        switch connection!.status {
        case .pending:
            self.plusButton.setImage(#imageLiteral(resourceName: "Message"), for: .normal)
        case .accepted:
            self.nameLabel.text = user.abbreviatedName
            self.plusButton.setImage(#imageLiteral(resourceName: "Remove Connection"), for: .normal)
        default:
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Functionality not defined for this connection status")
        }
    }
    
    func checkConnection() {
        if let connection = self.connection {
            return self.refreshView(forConnection: connection)
        }
        
        getConnection(byConnecteeId: user.userId) { result in
            switch result {
            case .success(let data):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                
                self.connection = data.connection
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.localizedDescription)")
            }
            
            self.refreshView(forConnection: self.connection)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        if let viewController = segue.destination as? ConnectionsViewController {
            viewController.userId = self.user.userId
            return
        }

        if let controller = segue.destination as? MessagesViewController {
            controller.setupView(forDirect: self.user.userId)
        }
    }
    
    @IBAction func connectionButtonPressed(_ sender: UIButton) {
        if let connection = self.connection {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Already connected")
            
            switch connection.status {
            case .accepted:
                DiAlert.confirmOrCancel(title: "Confirm action", message: "Are you sure you want to permanently “un-connect” from your connections?", inViewController: self) {
                    DiActivityIndicator.showActivityIndicator()
                    
                    self.delete(connection: connection.connectionId) {result in
                        switch result {
                        case .success(_):
                            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                            self.connection = nil
                            self.refreshView(forConnection: nil)
                        case .failure(let result, let error):
                            self.showAlert(title: "Error!", result: result, error: error, viewController: self)
                        }
                        
                        DiActivityIndicator.hide()
                    }
                }
            default:
                break
            }
        } else {
            DiActivityIndicator.showActivityIndicator()
            
            createConnection(connecteeId: user.userId) { result in
                switch result {
                case .success(let data):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success")
                    
                    if let connection = data.connection {
                        self.connection = connection
                        self.refreshView(forConnection: connection)
                        
                        let alert = UIAlertController(title: "Request sent", message: "Request to connect with \(self.user.abbreviatedName) has been sent!", preferredStyle: .alert)
                        self.present(alert, animated: true)
                        
                        let when = DispatchTime.now() + 2.5
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            alert.dismiss(animated: true, completion: nil)
                        }
                    }
                case .failure(let result, let error):
                    self.showAlert(title: "Not connected", result: result, error: error, viewController: self)
                }
                
                DiActivityIndicator.hide()
            }
        }
    }
}

extension ProfileViewController: ReportApi {
    
    @IBAction func reportUser(_ sender: UIButton) {
        let confirmAction = {
            let userId = self.user.userId
            let report = "User: \(userId)"
            let hud = JGProgressHUD(style: .dark)
            
            hud.textLabel.text = "Reporting"
            hud.show(in: self.view)
            
            self.send(report: report, type: .abusivePin) { result in
                switch result {
                case .success(_):
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.dismiss()
                    
                    DiAlert.alert(title: "Report sent", message: "This user has been reported. The Upin team will review it.", dismissTitle: "OK", inViewController: self, withDismissAction: nil)
                case .failure(_, let error):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    hud.dismiss(afterDelay: 2)
                }
            }
        }
        
        DiAlert.confirmOrCancel(title: "Reporting a user", message: "Are you sure you want to report this user?", confirmTitle: "Report", cancelTitle: "Cancel", inViewController: self, withConfirmAction: confirmAction)
    }
}
