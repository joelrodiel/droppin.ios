//
//  AccountEditTableViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 10/30/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class AccountEditTableViewController: UITableViewController, UserApi {
    
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: PasswordTextField!
    @IBOutlet weak var confirmNewPassword: UITextField!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var matchError: UILabel!
    
    private (set) var api = DroppinApi()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        validateFields()
    }
    
    private func setupView() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let fields: [UITextField] = [oldPassword, newPassword, confirmNewPassword]
        
        fields.forEach {
            $0.text = ""
            $0.addTarget(self, action: #selector(validateFields), for: .editingChanged)
        }
        
        passwordError.isHidden = true
        matchError.isHidden = true
    }
    
    
    @objc private func validateFields() {
        navigationItem.rightBarButtonItem?.isEnabled = isValid()
    }
    
    private func isValid() -> Bool {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard
            let oldPassword = oldPassword.text,
            let newPassword = newPassword.text,
            let confirmNewPassword = confirmNewPassword.text
            else { return false }
        
        let oldValidation = oldPassword.count > 0
        let newValidation = newPassword.count > 0
        let confirmValidation = confirmNewPassword.count > 0
        
        return oldValidation && newValidation && confirmValidation
    }
    
    private func checkIfNewPasswordsValid() -> Bool {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard
            let newPassword = newPassword.text,
            let confirmNewPassword = confirmNewPassword.text
            else { return false }
        
        let passwordValidation = self.newPassword.isValid()
        passwordError.isHidden = passwordValidation
        
        let matchValidation = newPassword == confirmNewPassword
        matchError.isHidden = matchValidation
        
        return passwordValidation && matchValidation
    }
    
    @IBAction func dismiss(_ sender: Any? = nil) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func savePassword(_ sender: Any) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if !checkIfNewPasswordsValid() { return }
        
        DiActivityIndicator.showActivityIndicator()
        
        guard let oldPassword = oldPassword.text, let newPassword = newPassword.text else { return }
        
        updatePassword(old: oldPassword, new: newPassword) { result in
            DiActivityIndicator.hide()
            
            switch result {
            case .success(_):
                self.dismiss()
            case .failure(_, let error):
                DiAlert.alertSorry(message: "We cannot update your password", inViewController: self)
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure \(error.description)")
            }
        }
    }
}
