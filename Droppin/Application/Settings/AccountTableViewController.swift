//
//  AccountViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 10/30/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class AccountTableViewController: UITableViewController, UserApi {
    
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var email: UITextField!
    
    private (set) var api = DroppinApi()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        loadUserInfo()
    }
    
    private func setupView() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        mobile.text = ""
        email.text = ""
    }
    
    private func loadUserInfo() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()
        
        getCriticalData { (criticalData, error) in
            DiActivityIndicator.hide()
            
            guard
                let mobile = criticalData?.mobile,
                let email = criticalData?.email,
                error == nil
                else {
                    return self.goBack(message: "There is a problem loading user info", error: error)
            }
            
            self.mobile.text = mobile
            self.email.text = email
        }
    }
}
