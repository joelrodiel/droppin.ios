//
//  HelpViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 11/1/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import JGProgressHUD

final class HelpViewController: UIViewController, ReportApi {
    
    private (set) var api = DroppinApi()
    
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var message: DefaultTextView!
    
    private final let MIN_LENGTH: Int = 3
    private final let MAX_LENGTH: Int = 300
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        updateNextButtonState()
    }
    
    private func setupView() {
        subject.addBottomBorder(.steel)
        subject.addTarget(self, action: #selector(updateNextButtonState), for: .editingChanged)
        
        message.addBottomBorder(.steel)
        message.defaultDelegate = self
        message.minLength = MIN_LENGTH
        message.maxLength = MAX_LENGTH
        message.text = ""
        message.placeholder = "Message"
        message.isScrollEnabled = false
    }
    
    private func isValid() -> Bool {
        guard let subjectText = subject.text else { return false }
        
        let subjectValidation = subjectText.count > 0
        let messageValidation = message.isValid()
        
        return subjectValidation && messageValidation
    }
    
    @objc private func updateNextButtonState() {
        navigationItem.rightBarButtonItem?.isEnabled = isValid()
    }
    
    @IBAction func sendReport(_ sender: Any) {
        let hud = JGProgressHUD(style: .dark)
        
        hud.textLabel.text = "Sending message"
        hud.show(in: self.view)
        
        send(report: subject.text!, description: message.text!, type: .help) { result in
            switch result {
            case .success(_):
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                hud.textLabel.text = "Message has been sent"
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    hud.dismiss()
                    self.navigationController?.popViewController(animated: true)
                }
            case .failure(_, let error):
                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                hud.textLabel.text = "Message not sent"
                hud.dismiss(afterDelay: 3.0)
                
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.description)")
            }
        }
    }
}

extension HelpViewController: DefaultTextViewDelegate, UITextFieldDelegate {

    func defaultTextView(didChange text: String) {
        updateNextButtonState()
    }
}
