//
//  SettingsViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/20/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, DroppinView, CoreApi {
    
    @IBOutlet weak var version: UILabel!
    
    var api = DroppinApi()
    
    private enum Section: Int {
        case account = 0, info = 1, logout = 2
    }
    
    private enum InfoSubsection: Int {
        case policy = 0, help = 1
    }
    
    private enum Segue: String {
        case showAccount = "ShowAccountSegue"
        case mainStoryboard = "mainStoryboardSegue"
        case showPrivacy = "ShowPrivacySegue"
        case showHelp = "ShowHelpSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        self.version.text = Config.version(for: Config.env)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section: Section = Section(rawValue: indexPath.section)!
        
        switch section {
        case .account:
            performSegue(withIdentifier: Segue.showAccount.rawValue, sender: nil)
        case .info:
            let subsection: InfoSubsection = InfoSubsection(rawValue: indexPath.row)!
            
            switch subsection {
            case .policy:
                performSegue(withIdentifier: Segue.showPrivacy.rawValue, sender: nil)
            case .help:
                performSegue(withIdentifier: Segue.showHelp.rawValue, sender: nil)
            }
        case .logout:
            logout()
        }
    }
    
    private func logout() {
        DiAlert.confirmOrCancel(title: "Logout", message: "You will be returned to the sign in screen", confirmTitle: "Logout", cancelTitle: "Cancel", inViewController: self, withConfirmAction: {
            UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
            UserDefaults.standard.removeObject(forKey: UserPropertyKey.isTester.rawValue)
            AccountSettings.removeFirstTimeSignedIn()
            PinLocationManager.sharedManager.stopTracking()
            
            self.logout() {result in
                switch result {
                case .success(_):
                    print("Token removed from server")
                    // Clear notificiations
                    let app = UIApplication.shared
                    app.applicationIconBadgeNumber = 0
                case .failure(let requestResult, let error):
                    self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
                }
            }
            
            self.performSegue(withIdentifier: Segue.mainStoryboard.rawValue, sender: self)
        }, cancelAction: {})
    }
}
