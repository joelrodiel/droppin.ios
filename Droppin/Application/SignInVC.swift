//
//  LoginViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/2/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SignInVC: UIViewController, DroppinView, CoreApi, UserApi {
    
    let api: DroppinApi = DroppinApi()
    
    @IBOutlet weak var email: EmailTextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signInButton: DRegistrationButton!

    @IBAction func forgotPassword(_ sender: Any) {
        let email = self.email.text

        ForgotPasswordAlert(placeholder: email).present(from: self) {
            self.sendResetPassword(email: $0)
        }
    }

    private func sendResetPassword(email: String) {

        PasswordResetEndpoint.post(email) { (error, response) in
            if let error = error {
                BasicAlert(title: "Password reset", message: error.message ?? "No error defined").present(from: self)
                return
            }

            BasicAlert(title: "Check your inbox", message: "Your new password was sent.").present(from: self)
        }
    }
    
    @IBAction func emailOnChange(_ sender: Any) {
        updateSignInState()
    }
    
    @IBAction func joinNow(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.isAccountCreated.rawValue)
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.token.rawValue)
        
        self.performSegue(withIdentifier: "navigationSegue", sender: self)
    }
    
    @IBAction func passwordOnChange(_ sender: Any) {
        updateSignInState()
    }
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        super.viewDidLoad()
        updateSignInState()
        setupUI()
    }
    
    @IBAction func signInAction(_ sender: Any) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()

        signIn(userId: email.text!, password: password.text!, completion: {
            diResult in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Completion")
            switch diResult {
            case .success(let requestResult):
                UserDefaults.standard.set(requestResult.userId  , forKey: UserPropertyKey.currentUserId.rawValue)
                UserDefaults.standard.set(true  , forKey: UserPropertyKey.isAccountCreated.rawValue)
                AccountSettings.setFirstTimeViewAs(as: false)
                PinLocationManager.sharedManager.startTracking(.foreground)

                // updates token after user sign in
                self.updateToken()

                self.performSegue(withIdentifier: "navigationSegue", sender: self)
            case .failure(let requestResult, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(String(describing: error))")

                if case .dAuthenticationError = error {
                    DiAlert.alertAttention(message: "\(String(describing: error))", inViewController: self )
                    
                } else {
                    self.showAlert(title: "Sorry", result: requestResult, error: error, viewController: self)
                }
            }
        })
        DiActivityIndicator.hide()
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: "Sign In Failed", message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction) in
            
        }))
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func updateSignInState(){
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        guard let emailText = email.text, !emailText.isEmpty, email.isValid() else {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "emailText  is empty")
            signInButton.isEnabled = false
            return
        }

        guard let passwordText = password.text, !passwordText.isEmpty else {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "passwordText  empty")
            signInButton.isEnabled = false
            return
        }

        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "enable signin button")
        signInButton.isEnabled = true
    }
    
    func validateFields(){
        
    }
    
    func validateEmail(){
        
    }
    
}

extension SignInVC {
    
    private func setupUI() {
        setupTextFields()
        setupDefaultBackgroundGradient()
    }
    
    private func setupTextFields() {
        email.addBottomBorder(.white)
        password.addBottomBorder(.white)

        let attributes = [NSAttributedStringKey.foregroundColor:UIColor.white]

        email.attributedPlaceholder = NSAttributedString(string:"E-mail", attributes: attributes)
        password.attributedPlaceholder = NSAttributedString(string:"Password", attributes: attributes)
        
        email.addTarget(self, action: #selector(emailViewChanged), for: .editingChanged)
    }
    
    @objc
    private func emailViewChanged(){
        email.text = email.text?.lowercased()
    }
}
