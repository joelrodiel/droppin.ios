//
//  JoinNavigationController.swift
//  Droppin
//
//  Created by Isaias Garcia on 16/05/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit

//class JoinNavigationController: UINavigationController {
//
//    @IBAction func dismissJoinNavigationController(segue: UIStoryboardSegue) {
////        if let nav = self.navigationController {
////            nav.dismiss(animated: true)
////        }
//        self.dismiss(animated: true) {
//            self.viewControllers = []
//        }
//    }
//
//}

class DismissNavControllerSegue: UIStoryboardSegue {
    
    override func perform() {
        if let nav = self.source.navigationController {
            nav.dismiss(animated: true)
        }
    }
}
