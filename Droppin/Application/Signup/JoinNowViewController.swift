//
//  JoinNowViewController.swift
//  Alamofire
//
//  Created by Rene Reyes on 11/20/17.
//

import UIKit

class JoinNowViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    @IBAction func joinNowButton(_ sender: UIButton) {
        print("----------------------------------------------------------------------")
        UserDefaults.standard.set(false, forKey: UserPropertyKey.isTester.rawValue)
        self.performSegue(withIdentifier: "joinNowSegue", sender: sender)
    }

    
    @IBAction func joinNowDevAction(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: UserPropertyKey.isTester.rawValue)
        self.performSegue(withIdentifier: "joinNowSegue", sender: sender)
    }
    
    
    @IBAction func signInAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "signInSegue", sender: sender)
    }
    
}

extension JoinNowViewController {
    private func setupUI() {
        setupDefaultBackgroundGradient()
    }
}
