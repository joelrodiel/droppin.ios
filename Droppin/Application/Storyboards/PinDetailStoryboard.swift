//
//  PinDetailStoryboard.swift
//  Droppin
//
//  Created by Madson on 10/17/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

struct PinDetailStoryboard: StoryboardType {
    
    static var storyboard = UIStoryboard(name: "PinDetail", bundle: nil)
    
    static var navigation: UINavigationController {
        return self.storyboard.instantiateViewController(withIdentifier: "navigation") as! UINavigationController
    }

    static var pickAddress: UINavigationController {
        return self.storyboard.instantiateViewController(withIdentifier: "pickAddress") as! UINavigationController
    }
}
