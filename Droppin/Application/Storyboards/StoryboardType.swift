//
//  StoryboardType.swift
//  Droppin
//
//  Created by Madson on 10/17/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

protocol StoryboardType {
    static var storyboard: UIStoryboard { get }
}
