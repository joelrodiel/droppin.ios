//
//  FirstTimeSignUpViewController.swift
//  Droppin
//
//  Created by Isaias Garcia on 27/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class FirstTimeSignUpViewController: UIViewController {
    
    @IBAction func dismissViewController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
