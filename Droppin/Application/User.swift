import Foundation


protocol UserProtocol {
    var userId: String {get}
    var firstName: String {get}
    var lastName: String {get}
}

enum UserPermission {
    case mainPicture, allPictures, fullName, shortName, bio, gender, interests, score, age, pinAddress, connectionsCount
}

enum UserRole {
    case host, attendee, joined, outsider, connector, owner, conflicted, checker
    
    var description: String {
        switch self {
        case .host:
            return "Host"
        case .attendee:
            return "Attendee"
        case .joined:
            return "Joined"
        case .outsider:
            return "Outsider"
        case .connector:
            return "Connector"
        case .owner:
            return "Owner"
        case .conflicted:
            return "Conflicted"
        case .checker:
            return "Checker"
        }
    }
}

struct User: UserProtocol  {
    let userId: String
    let firstName: String
    let lastName: String
    let bio: String
    let gender: Gender
    let birthDate: Date
    let interests: [String]
    var genderHasChanged: Bool
    var isBirthdateEditable: Bool

    init(userId: String, firstName: String, lastName: String, bio: String, gender: Gender, birthDate: Date, interests: [String]) {
        self.userId = userId
        self.firstName = firstName
        self.lastName = lastName
        self.bio = bio
        self.gender = gender
        self.birthDate = birthDate
        self.interests = interests
        self.genderHasChanged = false
        self.isBirthdateEditable = false
    }
    
    var age: Int {
        return Calendar.current.dateComponents([.year], from: birthDate, to: Date()).year ?? 0
    }
    
    var abbreviatedName: String {
        let lastname = self.lastName.prefix(1)
        return "\(self.firstName) \(lastname)."
    }
    
    var fullName: String {
        return "\(self.firstName) \(self.lastName)"
    }
    
    func asParameters() -> [String : Any] {
        var parameters: [String : Any] = [:]
        
        parameters["firstName"] = firstName
        parameters["lastName"] = lastName
        parameters["bio"] = bio
        parameters["gender"] = gender.rawValue
        parameters["birthDate"] = birthDate.iso8601()
        
        return parameters
    }
    
    static func checkAvailability(of permissions: [UserPermission], for role: UserRole) -> Bool {
        var count = 0
        
        permissions.forEach {
            if checkAvailability(of: $0, for: role) {
                count = count + 1
            }
        }
        
        return count != 0
    }
    
    static func checkAvailability(of permission: UserPermission, for role: UserRole) -> Bool {
        return getPermissions(for: role).contains(permission)
    }
    
    private static func getPermissions(for role: UserRole) -> [UserPermission] {
        switch role {
        case .host:
            return [.allPictures, .bio, .interests, .gender, .age, .fullName, .score, .connectionsCount, .pinAddress]
        case .attendee:
            return [.allPictures, .bio, .interests, .gender, .age, .fullName, .score, .connectionsCount, .pinAddress]
        case .joined, .conflicted:
            return [.shortName, .mainPicture, .bio, .interests, .score, .pinAddress]
        case .outsider:
            return [.bio, .interests]
        case .checker:
            return [.shortName, .mainPicture, .gender, .bio, .interests]
        case .connector:
            return [.allPictures, .bio, .interests, .gender, .age, .fullName, .score]
        case .owner:
            return [.allPictures, .bio, .interests, .gender, .age, .fullName, .score, .connectionsCount]
        }
    }
}

struct UserStatistic {
    let eventsAttended: Int
    let eventsHosted: Int
    let newRequests: Int
    let connections: Int
    let newMessages: Int
    
    init(eventsAttended: Int, eventsHosted: Int, newRequests: Int, connections: Int, newMessages: Int) {
        self.eventsAttended = eventsAttended
        self.eventsHosted = eventsHosted
        self.newRequests = newRequests
        self.connections = connections
        self.newMessages = newMessages
    }
}
