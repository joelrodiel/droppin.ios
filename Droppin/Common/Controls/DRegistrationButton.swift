//
//  DRegistrationButton.swift
//  Droppin
//
//  Created by Rene Reyes on 10/28/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class DRegistrationButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        self.cornerRadius = 8
        setTitleColor(UIColor.white, for: .normal)
        backgroundColor = UIColor.aquaMarine
    }
    
    
    
    
//    override open var isEnabled: Bool {
//        willSet{
//            if newValue == false {
//                self.setTitleColor(UIColor.gray, for: UIControlState.disabled)
//                self.alpha = 0.5
//            } else {
//                self.setTitleColor(UIColor.gray, for: UIControlState.disabled)
//                self.alpha = 0.5
//            }
//        }
//    }
    
}

