//
//  DateTimePickerTextField.swift
//  Droppin
//
//  Created by Madson on 6/20/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class DateTimePickerTextField: UITextField {

    //-------------------------------------------------------------
    //MARK: Constructors
    //-------------------------------------------------------------
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    //-------------------------------------------------------------
    //MARK: Properties
    //-------------------------------------------------------------
    public var date: Date? {
        didSet {
            guard let date = date else { return }
            self.picker.date = date
            self.text = picker.date.short
        }
    }

    public lazy var picker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .dateAndTime
        return picker
    }()

    //-------------------------------------------------------------
    //MARK: Private Methods
    //-------------------------------------------------------------
    private func setup() {
        self.inputView = self.picker
        self.picker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
    }

    @objc
    private func dateChanged(_ picker: UIDatePicker) {
        self.date = picker.date
        self.text = picker.date.short
    }
}
