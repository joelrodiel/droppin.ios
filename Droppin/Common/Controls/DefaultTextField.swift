//
//  DefaultTextField.swift
//  Droppin
//
//  Created by Rene Reyes on 12/12/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class DefaultTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        styleSetup()
        
        
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        styleSetup()
    }
    
//    override init(frame: CGRect, textContainer: NSTextContainer?) {
//        super.init(frame: frame, textContainer: textContainer)
//
//    }
    
    func styleSetup(){
//        tintColor = UIColor.white
        self.clearButtonMode = .whileEditing
    }
    
    
//    func createBorder(){
////        let border = CALayer()
////        let width = CGFloat(0.25)
////        border.borderColor = UIColor.gray.cgColor
////        border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: self.frame.size.height)
////        border.borderWidth = width
////        self.layer.addSublayer(border)
////        self.layer.masksToBounds = true
//        //print("border created")
//    }
    
   
}
