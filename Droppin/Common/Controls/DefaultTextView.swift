//
//  DefaultTextView.swift
//  Droppin
//
//  Created by Rene Reyes on 12/6/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

@objc protocol DefaultTextViewDelegate {
    
    @objc optional func defaultTextView(didChange text: String)
    @objc optional func defaultTextView(isValid valid: Bool)
    @objc optional func defaultTextView(allCharacters count: Int)
    @objc optional func defaultTextView(leftCharacters count: Int)
}

class DefaultTextView: UITextView {
    
    var defaultDelegate: DefaultTextViewDelegate?
    var maxLength: Int?
    var minLength: Int?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        delegate = self
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        delegate = self
    }
    
    func isValid() -> Bool {
        var minLengthValidation = false
        
        if let min = minLength {
            minLengthValidation = text.count >= min
        } else {
            minLengthValidation = true
        }
        
        var maxLengthValidation = false
        
        if let max = maxLength {
            maxLengthValidation = text.count <= max
        } else {
            maxLengthValidation = true
        }
        
        return minLengthValidation && maxLengthValidation
    }

    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.steel.withAlphaComponent(0.55)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}

extension DefaultTextView: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        defaultDelegate?.defaultTextView?(didChange: textView.text)
        defaultDelegate?.defaultTextView?(isValid: isValid())
        defaultDelegate?.defaultTextView?(allCharacters: textView.text.count)
        
        if let max = maxLength {
            defaultDelegate?.defaultTextView?(leftCharacters: max - textView.text.count)
        }
        
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return validateField(textView, range: range, replacementText: text)
    }
    
    private func validateField(_ field: UITextInput, range: NSRange, replacementText string: String) -> Bool {
        guard let max = maxLength else { return true }
        var text = ""
        
        if let field = field as? UITextField, let ftext = field.text {
            text = ftext
        }
            
        else if let field = field as? UITextView, let ftext = field.text {
            text = ftext
        }
        
        let newLength = text.count + string.count - range.length
        return newLength <= max && string != "\n"
    }
}
