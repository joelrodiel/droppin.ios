//
//  DiLabel.swift
//  Droppin
//
//  Created by Isaias Garcia on 20/09/18.
//  Copyright © 2018 Reyes Labs. All rights reserved.
//

import Foundation

class DiLabel: UILabel {
    
    private final let BREAK_LINE_KEY = "{{break_line}}"
    
    override func awakeFromNib() {
        setupView()
    }
    
    private func setupView() {
        breakTextIfNeeded()
    }
    
    private func breakTextIfNeeded() {
        if let text = text, text.contains(BREAK_LINE_KEY) {
            self.text = text.replacingOccurrences(of: BREAK_LINE_KEY, with: "\n")
            self.numberOfLines = 0
        }
    }
}
