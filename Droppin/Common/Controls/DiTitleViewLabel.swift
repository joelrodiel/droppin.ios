//
//  DiTitleViewLabel.swift
//  Droppin
//
//  Created by Isaias Garcia on 20/09/18.
//  Copyright © 2018 Reyes Labs. All rights reserved.
//

import Foundation

final class DiTitleViewLabel: DiLabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        font = UIFont.systemFont(ofSize: 27, weight: .light)
        textAlignment = .center
    }
}
