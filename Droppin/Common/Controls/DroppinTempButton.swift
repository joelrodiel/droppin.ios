//
//  DRegistrationButton.swift
//  Droppin
//
//  Created by Rene Reyes on 10/28/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class DroppinTempButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        layer.borderWidth = 1.0
        layer.borderColor = DIColors.twitterBlue.cgColor
        layer.cornerRadius = frame.size.height/2
        
        let color = DIColors.twitterBlue
        let titleColor = UIColor.white
      
        setTitleColor(titleColor, for: .normal)
        backgroundColor = color
    }
    
}
