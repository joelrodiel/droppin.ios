//
//  EmailTextField.swift
//  Droppin
//
//  Created by Isaias Garcia on 13/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class EmailTextField: DefaultTextField {
    
    func isValid() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
}
