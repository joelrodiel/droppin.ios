//
//  InterestsView.swift
//  Droppin
//
//  Created by Isaias Garcia on 24/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import CropViewController

protocol ImagesPickerViewControllerDelegate {
    
    func imagesPicker(_ picturesCollection: UICollectionView, pickedAtLeastOne: Bool)
}

class ImagesPickerViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var picturesCollection: UICollectionView!
    
    private let MAIN_PICTURE = 0
    private let MAX_IMAGES = 3
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    private var selectedImageIndex: Int?
    private var images: [String?] = [nil, nil, nil]
    var isEditingImages: Bool = false
    
    var delegate: ImagesPickerViewControllerDelegate?
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        setupView()
    }
    
    func setupView() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        profileImage.roundBorders()
        picturesCollection.delegate = self
        picturesCollection.dataSource = self
        picturesCollection.reloadData()
    }
    
    func setupData(data: [String?]?) {
        var images = data ?? []
        
        if images.count < 3 {
            for _ in 0...self.MAX_IMAGES - images.count - 1 {
                images.append(nil)
            }
        }
        
        if let image = images[0] {
            profileImage.setImage(image)
        }
        
        self.images = images
        picturesCollection.reloadData()
    }
    
    func getImages() -> [String?] {
        return images
    }
    
    func getMainImage() -> String? {
        if let first = images[MAIN_PICTURE] {
            return first
        }
        
        if let other = images.first(where: { $0 != nil }) {
            return other
        }
        
        return nil
    }
    
    @objc func pickImage(index: Int, and sourceView: UIView) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        selectedImageIndex = index
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sourceView
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: sourceView.frame.size.width / 2, y: sourceView.frame.size.height, width: 0, height: 0)
        
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {(action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion:nil)
            }else{
                print("camera not available")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion:nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true)
    }
    
    func checkIfPickedAtLeastOne() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let pickedOne = images.filter({ $0 != nil }).count > 0
        delegate?.imagesPicker(picturesCollection, pickedAtLeastOne: pickedOne)
    }
}

extension ImagesPickerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PictureCell", for: indexPath) as! PictureCell
        
        cell.image = images[indexPath.item]
        cell.indicator.text = String(indexPath.item + 1)
        cell.imageButtonCallback = {self.pickImage(index: indexPath.item, and: cell)}
        cell.removeButtonCallback = {
            self.images[indexPath.item] = nil
            
            if indexPath.item == 0 {
                self.profileImage.image = #imageLiteral(resourceName: "profilePicLogo")
            }
            
            self.checkIfPickedAtLeastOne()
        }
        
        if isEditingImages {
            cell.removeButton.setTitleColor(DIColors.droppinRed, for: .normal)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        images.insert(images.remove(at: sourceIndexPath.item), at: destinationIndexPath.item)
        let publicId = self.images[self.MAIN_PICTURE] ?? ""
        self.profileImage.cldSetImage(publicId: publicId, cloudinary: CDNManager.shared.cloudinary, placeholder: #imageLiteral(resourceName: "profilePicLogo"))
        collectionView.reloadData()
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
        case .began:
            guard let selectedIndexPath = picturesCollection.indexPathForItem(at: gesture.location(in: picturesCollection)) else {
                break
            }
            
            picturesCollection.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            picturesCollection.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            picturesCollection.endInteractiveMovement()
        default:
            picturesCollection.cancelInteractiveMovement()
        }
    }
}

extension ImagesPickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let imageUncropped = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cropViewController = CropViewController(croppingStyle: .circular, image: imageUncropped)
            
            cropViewController.delegate = self
            
            self.present(cropViewController, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        guard let index = selectedImageIndex else { return }
        guard let data = UIImageJPEGRepresentation(image, 1) else { return }
        guard let lowImage = UIImage(data: data, scale: 1) else { return }
        guard let resizedImage = lowImage.resized(toWidth: 540) else { return }

        DiActivityIndicator.showActivityIndicator()
        
        CDNManager.shared.upload(resizedImage, preset: .profilePhotos, completion: { error, publicId in
            DiActivityIndicator.hide()
            
            guard error == nil else {
                BasicAlert(title: "Upload error", message: error!.localizedDescription).present(from: self)
                return
            }

            guard let publicId = publicId else {
                debugPrint("ImagesPickerViewController.cropViewController - publicId is nil")
                return
            }

            if index == self.MAIN_PICTURE {
                self.profileImage.image = resizedImage
            }

            self.images[index] = publicId
            self.picturesCollection.reloadData()
            self.checkIfPickedAtLeastOne()
        })

        cropViewController.dismiss(animated: true) {}
    }
}
