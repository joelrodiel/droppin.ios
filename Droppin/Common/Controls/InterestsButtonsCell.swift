//
//  InterestsButtonsCell.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/10/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

protocol InterestButtonDelegate {

    func interestButton(findIndexById id: Int) -> Int?
    func interestButton(selected: Bool, page: Int, row: Int, col: Int)
    func suggestAnInterest()
}

final class InterestButton: UIButton {
    
    var numberLabel: UILabel!
    
    private var _interest: Interest?
    private var _isSuggestion: Bool = false
    
    var page: Int!
    var row: Int!
    var col: Int!
    var delegate: InterestButtonDelegate?
    
    var interest: Interest? { set { _interest = newValue; setupData() } get { return _interest } }
    var isSuggestion: Bool { set { _isSuggestion = newValue; setupData() } get { return _isSuggestion} }
    
    private func setupData() {
        if isSuggestion {
            setTitle("Suggest an interest", for: .normal)
            addTarget(self, action: #selector(suggestInterest), for: .touchUpInside)
        } else {
            setTitle(interest?.label, for: .normal)
            addTarget(self, action: #selector(selectInterest), for: .touchUpInside)
        }
        
        setupView()
    }
    
    private func setupView() {
        if let interest = interest {
            numberLabel.isHidden = !interest.isSelected
            numberLabel.textColor = UIColor.aquaMarine
            numberLabel.backgroundColor = UIColor.white
            numberLabel.roundBorders()
            
            backgroundColor = interest.isSelected ? UIColor.aquaMarine : UIColor.white
            borderColor = UIColor.aquaMarine
            
            setTitleColor(interest.isSelected ? UIColor.white : UIColor.aquaMarine, for: .normal)
            
            if var index = delegate?.interestButton(findIndexById: interest.id) {
                index = index + 1
                numberLabel.text = String(index)
                
                if index <= 3 {
                    numberLabel.textColor = UIColor.gold
                    backgroundColor = UIColor.gold
                    borderColor = UIColor.gold
                }
            }
            
            return
        }
        
        if isSuggestion {
            numberLabel.isHidden = true
            addTarget(self, action: #selector(suggestInterest), for: .touchUpInside)
            borderColor = UIColor.white
            addDashedBorder()
        }
    }
    
    @objc private func selectInterest() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let selected = !interest!.isSelected
        delegate?.interestButton(selected: selected, page: page, row: row, col: col)
    }
    
    @objc private func suggestInterest() {
        delegate?.suggestAnInterest()
    }
    
    private func addDashedBorder() {
        // Removing the default solid border
        borderWidth = 0
        borderColor = .white
        
        let color = UIColor.aquaMarine.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 1, height: frameSize.height - 1)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1.2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 14.5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

final class InterestsButtonsCell: UITableViewCell {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var button1: InterestButton!
    
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var button2: InterestButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button1.numberLabel = label1
        button2.numberLabel = label2
    }
}
