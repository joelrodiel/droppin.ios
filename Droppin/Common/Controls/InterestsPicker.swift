//
//  InterestsPicker.swift
//  Droppin
//
//  Created by Isaias Garcia on 24/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Pageboy

protocol InterestsPickerViewControllerDelegate {
    
    func interestsPicker(valid: Bool)
}

class InterestsPickerViewController: PageboyViewController, ReportApi {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private static let NUMBER_OF_INTERESTS_IN_TABLE = 12
    
    private var interests: [[[Interest]]] = []
    private var controllers: [UITableViewController] = []
    
    var api = DroppinApi()
    var selectedInterests: [Interest] = []
    var pickerDelegate: InterestsPickerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        dataSource = self
        delegate = self
        
        let ointerests = getInterests().chunked(into: InterestsPickerViewController.NUMBER_OF_INTERESTS_IN_TABLE)
        
        ointerests.forEach { cinterests in
            self.interests.append(cinterests.chunked(into: 2))
            
            let controller = UIStoryboard(name: "InterestsPicker", bundle: nil).instantiateViewController(withIdentifier: "InterestsTableView") as! UITableViewController
            
            controller.tableView.delegate = self
            controller.tableView.dataSource = self
            controller.tableView.isScrollEnabled = false
            
            controllers.append(controller)
        }

        pageControl.numberOfPages = controllers.count
        pageControl.currentPage = 0
        
        updateDescriptionLabel()
        
        reloadPages()
    }
    
    private func updateDescriptionLabel() {
        let interestsCount = interests.flatMap { $0.flatMap { $0 } }.count
        descriptionLabel.text = "\(selectedInterests.count) of \(interestsCount) interests selected"
    }
    
    func isValid() -> Bool {
        return selectedInterests.count >= DiConfig.minInterests && selectedInterests.count <= DiConfig.maxInterests
    }
    
    private func suggestInterest(interest: String) {
        DiActivityIndicator.showActivityIndicator()
        
        send(report: interest, type: .interest) { result in
            DiActivityIndicator.hide()
            
            switch result {
            case .success(_):
                DiAlert.alert(title: "Suggestion sent", message: nil, dismissTitle: "Dismiss", inViewController: self, withDismissAction: nil)
            case .failure(_, let error):
                DiAlert.alertSorry(message: "There is a problem", inViewController: self)
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Error \(error.description)")
            }
        }
    }
}

extension InterestsPickerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interests[pageControl.currentPage].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InterestsButtonsCell") as! InterestsButtonsCell
        
        cell.button1.interest = interests[pageControl.currentPage][indexPath.item][0]
        cell.button1.page = pageControl.currentPage
        cell.button1.row = indexPath.item
        cell.button1.col = 0
        cell.button1.delegate = self
        
        if isLastItem(index: indexPath.item) {
            cell.button2.isSuggestion = true
            cell.button2.delegate = self
        } else {
            cell.button2.interest = interests[pageControl.currentPage][indexPath.item][1]
            cell.button2.page = pageControl.currentPage
            cell.button2.row = indexPath.item
            cell.button2.col = 1
            cell.button2.delegate = self
        }
        
        return cell
    }
    
    private func isLastItem(index: Int) -> Bool {
        return pageControl.currentPage == controllers.count - 1 && index == interests[pageControl.currentPage].count - 1
    }
}

extension InterestsPickerViewController: InterestButtonDelegate {
    
    func suggestAnInterest() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        DiAlert.textInput(title: "Suggest an Interest", message: "Type in the field below", placeholder: "Interest", oldText: nil, confirmTitle: "Send", cancelTitle: "Cancel", inViewController: self, withConfirmAction: {input in
            self.suggestInterest(interest: input)
        }, cancelAction: nil)
    }
    
    func interestButton(findIndexById id: Int) -> Int? {
        return selectedInterests.index(where: {$0.id == id})
    }
    
    func interestButton(selected: Bool, page: Int, row: Int, col: Int) {
        let selectable = !selected || selectedInterests.count < DiConfig.maxInterests

        if selected && selectable {
            interests[page][row][col].isSelected = selected
            selectedInterests.append(interests[page][row][col])
        } else if !selected, let index = selectedInterests.index(where: {$0.id == interests[page][row][col].id}) {
            selectedInterests.remove(at: index)
            interests[page][row][col].isSelected = false
        }

        updateDescriptionLabel()
        controllers[page].tableView.reloadData()
        pickerDelegate?.interestsPicker(valid: isValid())
    }
}

extension InterestsPickerViewController: PageboyViewControllerDataSource, PageboyViewControllerDelegate {
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, willScrollToPageAt index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        pageControl.currentPage = index
        controllers[index].tableView.reloadData()
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return controllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return controllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
}
