//
//  InterestsChooser.swift
//  Droppin
//
//  Created by Isaias Garcia on 13/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import TagListView

@objc protocol InterestsViewDelegate {
    
    @objc optional func interestsView(selected: Int)
    @objc optional func interestsView(valid: Bool)
}

class InterestsView: TagListView {
    
    private var interests: [Interest]!
    var interestsDelegate: InterestsViewDelegate?
    var selectedInterests: [Interest]? {
        set {
            setupData(selectedInterests: newValue)
        }
        get { return interests }
    }
    
    override func awakeFromNib() {
        setupView()
        loadInterests()
    }
    
    private func setupView() {
        paddingX = 40
        paddingY = 20
        marginY = 20
        marginX = 20
        (self as UIView).cornerRadius = 25
        alignment = .center
        delegate = self
        
        if let font = UIFont(name: "Arial", size: 16) {
            textFont = font
        }
        
        removeAllTags()
    }
    
    private func loadInterests() {
        interests = getInterests()
        
        for i in 1...interests.count {
            interests[i - 1].tagView = addTag(interests[i - 1].label)
            interests[i - 1].tagView?.changeStatus(selected: false)
        }
    }
    
    private func setupData(selectedInterests: [Interest]?) {
        selectedInterests?.forEach { interest in
            if let index = interests.index(where: {$0.label == interest.label}), let tagView = interests[index].tagView {
                interests[index].isSelected = true
                tagView.changeStatus(selected: true)
            }
        }
    }
    
    func getSelectedInterests() -> [Interest] {
        return interests.filter({$0.isSelected})
    }
    
    func countSelectedInterests() -> Int {
        return getSelectedInterests().count
    }
    
    func isValid() -> Bool {
        return countSelectedInterests() >= DiConfig.minInterests
    }
}

extension InterestsView: TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let index = interests.index(where: {$0.tagView == tagView}) {
            let isSelected = !interests[index].isSelected
            
            if !isSelected || (isSelected && countSelectedInterests() < DiConfig.maxInterests) {
                interests[index].isSelected = !interests[index].isSelected
                tagView.changeStatus(selected: interests[index].isSelected)
            }
            
            interestsDelegate?.interestsView?(selected: countSelectedInterests())
            interestsDelegate?.interestsView?(valid: isValid())
        }
    }
}

private extension TagView {
    
    func changeStatus(selected: Bool) {
        (self as UIButton).borderWidth = 1
        
        if selected {
            tagBackgroundColor = #colorLiteral(red: 0.2549019608, green: 0.8666666667, blue: 0.6941176471, alpha: 1)
            textColor = UIColor.white
            borderColor = UIColor.white
        } else {
            tagBackgroundColor = UIColor.white
            textColor = #colorLiteral(red: 0.2549019608, green: 0.8666666667, blue: 0.6941176471, alpha: 1)
            borderColor = #colorLiteral(red: 0.2549019608, green: 0.8666666667, blue: 0.6941176471, alpha: 1)
        }
    }
}
