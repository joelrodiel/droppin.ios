//
//  PasswordTextField.swift
//  Droppin
//
//  Created by Isaias Garcia on 1/11/19.
//  Copyright © 2019 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

final class PasswordTextField: DefaultTextField {
    
    // At least one number, upper case, lower case. Lenght min 8
    private static let REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d\\W]{8,}"
    
    // At least one number, upper case, lower case and special character. Lenght min 8
    private static let REGEX_WITH_SPECIAL_CHAR = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*\\W)[A-Za-z\\d\\W]{8,}"
    
    func isValid() -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", PasswordTextField.REGEX)
        return predicate.evaluate(with: text)
    }
}
