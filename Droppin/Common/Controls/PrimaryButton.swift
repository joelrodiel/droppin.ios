//
//  PrimaryButton.swift
//  Droppin
//
//  Created by Rene Reyes on 11/22/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class PrimaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        cornerRadius = 8
        backgroundColor = UIColor.aquaMarine
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    }
    
    func enable(enable: Bool) {
        isEnabled = enable
        alpha = enable ? 1 : 0.3
    }
}

