//
//  PrimaryButton.swift
//  Droppin
//
//  Created by Rene Reyes on 11/22/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SecondaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        layer.borderWidth = 0
        layer.borderColor = DIColors.white.cgColor
        layer.cornerRadius = 5
        
        setTitleColor(DIColors.white, for: .normal)
        backgroundColor = DIColors.primary
    }
}


