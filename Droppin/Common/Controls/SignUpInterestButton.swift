//
//  DRegistrationButton.swift
//  Droppin
//
//  Created by Rene Reyes on 10/28/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SignUpInterestButton: UIButton {
    var isOn = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        
        backgroundColor = UIColor.white
        layer.borderColor = DIColors.droppinRed.cgColor
        setTitleColor(DIColors.droppinRed, for: .normal)
        
        layer.borderWidth = 2.0
        layer.cornerRadius = frame.size.height/2
        
        addTarget(self, action: #selector(SignUpInterestButton.buttonPressed), for: .touchUpInside)
        
    }
    
    @objc func buttonPressed(){
        activateButton(bool: !isOn)
    }
    
    func activateButton(bool: Bool){
        isOn = bool
        let color = bool ? DIColors.droppinRed : .clear
        let titleColor = bool ? . white : DIColors.droppinRed
        setTitleColor(titleColor, for: .normal)
        backgroundColor = color
    }
}

