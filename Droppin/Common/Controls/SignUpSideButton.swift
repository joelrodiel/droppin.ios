//
//  SignUpSideButton.swift
//  Droppin
//
//  Created by Rene Reyes on 10/30/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SignUpSideButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        layer.borderWidth = 0
        layer.borderColor = UIColor.white.cgColor
        layer.cornerRadius = 5
        
        setTitleColor(DIColors.droppinDarkerGreen, for: .normal)
        backgroundColor = UIColor.white
    }
}
