//
//  SignupMainButton.swift
//  Droppin
//
//  Created by Rene Reyes on 10/30/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SignupMiniMainButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initButton()
    }
    
    func initButton(){
        layer.borderWidth = 1.0
        layer.borderColor = DIColors.droppinDarkerGreen.cgColor
        layer.cornerRadius = 5
        
        setTitleColor(UIColor.white, for: .normal)
        backgroundColor = DIColors.droppinDarkerGreen
    }

}
