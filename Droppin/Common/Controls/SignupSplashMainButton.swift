//
//  DRegistrationAlternateButton.swift
//  Droppin
//
//  Created by Rene Reyes on 10/28/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SignupSplashMainButton: DRegistrationButton {

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        initButton()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        initButton()
//    }
    
    override func initButton(){
        super.initButton()
//        layer.borderWidth = 2.0
        layer.borderColor = DIColors.droppinRed.cgColor
//        layer.cornerRadius = frame.size.height/2
//        setTitleColor(UIColor.white, for: .normal)
        backgroundColor = DIColors.droppinRed
    }

}
