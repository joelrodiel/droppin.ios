//
//  TextFieldDatePicker.swift
//  Droppin
//
//  Created by Isaias Garcia on 23/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import SwiftMoment

@objc protocol TextFieldDatePickerDelegate {
    
    @objc optional func textFieldDatePicker(onChanged date: Date)
}

final class TextFieldDatePicker: DefaultTextField {
    
    var datePickerDelegate: TextFieldDatePickerDelegate?
    
    private let dateFormatter = DateFormatter()
    private let datePicker = UIDatePicker()
    
    var minimumDate: Date? { set { datePicker.minimumDate = newValue } get { return datePicker.minimumDate} }
    var maximumDate: Date? { set { datePicker.maximumDate = newValue } get { return datePicker.maximumDate} }
    var date: Date { set { datePicker.date = newValue; dateChanged() } get { return datePicker.date } }
    var _type: DateType = .date
    
    var type: DateType {
        get { return _type }
        set {
            _type = newValue
            datePicker.datePickerMode = typeToPickerMode()
        }
    }
    
    enum DateType {
        case date, datetime
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        delegate = self
        
        clearButtonMode = .never
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        datePicker.datePickerMode = typeToPickerMode()
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        
        inputView = datePicker
    }
    
    func isDateSelected() -> Bool {
        guard let text = text else { return false }
        return text != ""
    }
    
    @objc private func dateChanged() {
        text = dateToString()
        datePickerDelegate?.textFieldDatePicker?(onChanged: datePicker.date)
    }
    
    private func typeToPickerMode() -> UIDatePickerMode {
        switch type {
        case .date:
            return .date
        case .datetime:
            return .dateAndTime
        }
    }
    
    private func dateToString() -> String {
        switch type {
        case .date:
            return datePicker.date.format(with: .short)
        case .datetime:
            return datePicker.date.short
        }
    }
}

extension TextFieldDatePicker: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        dateChanged()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        DispatchQueue.main.async(execute: {
            (sender as? UIMenuController)?.setMenuVisible(false, animated: false)
        })
        return false
    }
}
