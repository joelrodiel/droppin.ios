//
//  DiNotificationsManager.swift
//  Droppin
//
//  Created by Isaias Garcia on 30/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UserNotifications

private struct DiPushNotification {
    var notificationId: String?
    let alert: String
    let category: String
    let action: String
    let data: [AnyHashable : Any]
    
    init(data: [AnyHashable : Any]) {
        self.init(data: data, subAction: nil)
    }
    
    init(data: [AnyHashable : Any], subAction: String?) {
        let notificationId = data["notificationId"] as? String
        let aps = data["aps"] as? [AnyHashable : Any] ?? [:]
        let alert = aps["alert"] as? String ?? "No alert message defined"
        let category = aps["category"] as? String ?? "Category not defined"
        var action = "Action not defined"
        
        if subAction == "upin.action.profile" || category == "upin.category.profile" {
            action = "user.show"
        } else if category == "upin.category.pin" {
            action = "pin.\(data["action"] ?? "default")"
        } else if category == "upin.category.pinInvitation" {
            action = "pin.show"
        }
        
        self.notificationId = notificationId
        self.alert = alert
        self.category = category
        self.action = action
        self.data = data
    }
}

private protocol DiNotificationsManager: UNUserNotificationCenterDelegate {
    var launched: Bool { get set }
    var window: UIWindow? { get set }
}

extension AppDelegate: DiNotificationsManager, DiNotificationApi {
    
    func registerForPushNotifications() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        UNUserNotificationCenter.current().delegate = self
        setPushCategories()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    private func getNotificationSettings() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
    
    func getNavController() -> UINavigationController? {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        return self.window?.rootViewController as? UINavigationController
    }
    
    private func setPushCategories() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        let defaultAction = UNNotificationAction(identifier: "upin.action.default", title: "Open", options: [.foreground])
        let profileAction = UNNotificationAction(identifier: "upin.action.profile", title: "Show profile", options: [.foreground])
        
        let profileCategory = UNNotificationCategory(identifier: "upin.category.profile", actions: [defaultAction, profileAction], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([profileCategory])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        waitForAppLaunching {
            let notification = DiPushNotification(data: response.notification.request.content.userInfo, subAction: response.actionIdentifier)
            self.launchAction(notification: notification)
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        let notification = DiPushNotification(data: notification.request.content.userInfo)

        DiAlert.confirmOrCancel(title: "Notification", message: notification.alert, confirmTitle: "View", cancelTitle: "Dismiss", inViewController: getNavController(), withConfirmAction: {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Confirm Action")
            self.launchAction(notification: notification)
            self.checkNotificationAsRead(notification)
        }, cancelAction: {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Dismiss Action")
            self.checkNotificationAsRead(notification)
        })
    }
    
    private func checkNotificationAsRead(_ notification: DiPushNotification) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let notificationId = notification.notificationId {
            updateNotification(notificationId: notificationId, status: DiNotificationStatus.read) { result in
                switch result {
                case .success(let data):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Success: \(data)")
                case .failure(_, let error):
                    DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.localizedDescription)")
                }
            }
        } else {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "No notificationId")
        }
    }
    
    private func waitForAppLaunching(_ callback: @escaping () -> Void) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if !self.launched {
                return self.waitForAppLaunching(callback)
            }
            
            DiActivityIndicator.hide()
            
            callback()
        }
    }
    
    private func launchAction(notification: DiPushNotification) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let nav = self.getNavController() {
            switch notification.action {
            case "user.show":
                return NotificationCenter.default.post(name: .showProfileView, object: notification.data["userId"])
            case "pin.create":
                return NotificationCenter.default.post(name: .showCreatePinView, object: nil)
            case "pin.show":
                if let pinId = notification.data["pinId"] as? String {
                    let pinDetailViewController = UIStoryboard(name: "Pins", bundle: nil).instantiateViewController(withIdentifier: "PinDetailViewController") as! PinDetailViewController
                    
                    pinDetailViewController.refreshData(pinId: pinId)
                    pinDetailViewController.role = .joined
                    
                    return nav.pushViewController(pinDetailViewController, animated: true)
                }
            default:
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Action not defined")
            }
        }
    }
}
