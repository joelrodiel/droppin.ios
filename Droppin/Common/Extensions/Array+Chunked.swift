//
//  Array+Chunked.swift
//  Droppin
//
//  Created by Isaias Garcia on 01/10/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
