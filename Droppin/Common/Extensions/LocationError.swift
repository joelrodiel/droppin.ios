//
//  LocationError.swift
//  Droppin
//
//  Created by Isaias Garcia on 28/09/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import SwiftLocation

extension LocationError {
    
    var description: String {
        get {
            switch self {
            case .timedout:
                return "GPS Location request exceeded the limit of time."
            case .notDetermined:
                return "GPS Location is not determined."
            case .denied:
                return "Please turn on the GPS for a better experience!"
            case .restricted:
                return "GPS service is restricted."
            case .disabled:
                return "GPS service is disabled."
            default:
                return "GPS service is not available."
            }
        }
    }
}
