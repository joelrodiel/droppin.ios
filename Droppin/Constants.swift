//
//  Constants.swift
//  Droppin
//
//  Created by Rene Reyes on 10/28/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import UIKit

struct DIColors {
    static let twitterBlue = UIColor(red: 29.0/255.0, green: 161.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    static let droppinRed = UIColor(red: 248.0/255.0, green: 111.0/255.0, blue: 99.0/255.0, alpha: 1.0)
    static let droppinGreen = UIColor.aquaMarine
    static let droppinDarkerGreen = UIColor(red: 88/255.0, green: 149/255.0, blue: 131/255.0, alpha: 1.0)
    
    static let white = UIColor.white
    static let black = UIColor.black
    static let primary = UIColor(red: 88/255.0, green: 149/255.0, blue: 131/255.0, alpha: 1.0) // darker green
    static let success = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let secondary = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let danger = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let warning = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let info = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let light = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let dark = UIColor(red: 159.0/255.0, green: 242.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let dev = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    
    
    
}
