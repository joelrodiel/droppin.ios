//
//  DroppinView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/13/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

protocol DroppinView {
//    var activityIndicator:  UIActivityIndicatorView {get set}
    var isTester:Bool { get }
//    func startActivityIndicator(_ view: UIView)
//    func stopActivityIndicator()
    func showAlert(title: String, result: RequestResult?, error: DiApiError, viewController: UIViewController)
    func showBasicAlert(title: String, message: String, viewController: UIViewController)
    func className( of obj: Any) -> String
    func lineDescription( of obj: Any, andFunction funcName: String, withDescription desc: String) -> String
}


extension DroppinView {
    
    var userId: String? {
        get {
            let mongoIdLength = 24
            
            if let userId = UserDefaults.standard.string(forKey: UserPropertyKey.currentUserId.rawValue), userId.count == mongoIdLength {
                return userId
            }
            
            return nil
        }
    }
    
    var isTester: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserPropertyKey.isTester.rawValue)
        }
    }
    
    var isNotAValidUserId: Bool {
        get { return self.userId == nil }
    }
    
    func setTesterRole(state: Bool) {
        switch state {
        case true:
            UserDefaults.standard.set(true, forKey: UserPropertyKey.isTester.rawValue)
        case false:
            UserDefaults.standard.removeObject(forKey: UserPropertyKey.isTester.rawValue)
        }
    }
    
    func randomName(_ prefix: String) -> String {
        let uniqueString = "\(arc4random_uniform(1000))\(arc4random_uniform(1000))"
        return  "\(prefix)-\(uniqueString)"
    }
    
    func isThisMine(toCompare paramUserId: String?) -> Bool {
        guard let paramUserId = paramUserId else { return false }
        guard let localUserId = self.userId else { return false }

        return paramUserId == localUserId
    }
    
//    UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
    
    
    
    
    
//    func startActivityIndicator(_ view: UIView) {
//        activityIndicator.center = view.center
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
//        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
//        activityIndicator.backgroundColor = UIColor.darkGray
//        view.addSubview(activityIndicator)
//
//        let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
//        view.addConstraint(horizontalConstraint)
//
//        let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
//
//        view.addConstraint(verticalConstraint)
//
//        activityIndicator.startAnimating()
//        UIApplication.shared.beginIgnoringInteractionEvents()
//    }
    
//    func stopActivityIndicator(){
//        activityIndicator.stopAnimating()
//        UIApplication.shared.endIgnoringInteractionEvents()
//    }
//
  
    func showAlert(title: String, error: Error, viewController: UIViewController) {
        let message = error.localizedDescription
        self.showBasicAlert(title: title, message: message, viewController: viewController)
    }

    func showAlert(title: String, result: RequestResult?, error: DiApiError, viewController: UIViewController) {
        var message = error.description
        
        switch error {
        case .dAlamofireError(let error):
            message = error.localizedDescription
        default:
            if let errorCode = result?.errorCode {
                message = errorCode.description
            }
        }
        
        DiHelper.printLineDescription(of: viewController, andFunction: #function, withDescription: "Failure: \(error.description)")
        
        self.showBasicAlert(title: title, message: message, viewController: viewController)
    }
    
    func showBasicAlert(title: String, message: String, viewController: UIViewController)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction) in
            
        }))
        
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func className( of obj: Any) -> String {
        //prints more readable results for dictionaries, arrays, Int, etc
        return String(describing: type(of: obj))
    }

    func lineDescription( of obj: Any, andFunction funcName: String, withDescription desc: String) -> String{
        return "[\(className(of: self))][\(funcName)] ->\(desc)"
    }
    
    func changeEmptyViewVisibility(tableView: UITableView, visible: Bool, title: String?, description: String?) {
        if visible {
            let emptyMyPinsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "emptyMyPinsView") as! EmptyMyPinsVC
            
            emptyMyPinsVC.titleText = title ?? ""
            emptyMyPinsVC.descriptionText = description ?? "You have no items"
            
            tableView.backgroundView = emptyMyPinsVC.view
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func validateField(_ field: UITextInput, range: NSRange, replacementText string: String, maxLength: Int) -> Bool {
        var text = ""
        
        if let field = field as? UITextField, let ftext = field.text {
            text = ftext
        }
        
        else if let field = field as? UITextView, let ftext = field.text {
            text = ftext
        }
        
        let newLength = text.count + string.count - range.length
        return newLength <= maxLength
    }
}
    //
//    enum InterestType : CustomStringConvertible {
//        case music
//        case outdoors
//        case mastermind
//        case gaming
//        case politics
//        case health
//        case fitness
//        case spirituality
//        case communication
//        case science
//        case lifecoaching
//        case stayingathome
//        case psychology
//        case math
//        case art
//        case business
//        case partying
//        case computerscience
//
//        var description: String {
//            switch self{
//            case .music: return "Music"
//            case .outdoors: return "Outdoors"
//            case .mastermind: return "mastermind"
//            case .gaming: return "gaming"
//            case .politics: return "politics"
//            case .health: return "health"
//            case .fitness: return "fitness"
//            case .spirituality: return "spirituality"
//            case .communication: return "communication"
//            case .science: return "science"
//            case .lifecoaching: return "lifecoaching"
//            case .stayingathome: return "stayingathome"
//            case .psychology: return "psychology"
//            case .math: return "math"
//            case .art: return "art"
//            case .business: return "business"
//            case .partying: return "partying"
//            case .computerscience: return "computerscience"
//            }
//        }
//    }
//
//
//    struct Interest {
//        var isSelected: Bool?
//        let name: InterestType
//        let id: Int
//        let label: String
//    }
//
//    func getInterests()->[Interest]{
//
//        return [
//            Interest(isSelected: nil, name: InterestType.music, id: 0, label: InterestType.music.description),
//            Interest(isSelected: nil, name: InterestType.outdoors, id: 1, label: InterestType.outdoors.description),
//            Interest(isSelected: nil, name: InterestType.mastermind, id: 2, label: InterestType.mastermind.description),
//            Interest(isSelected: nil, name: InterestType.gaming, id: 3, label: InterestType.gaming.description),
//            Interest(isSelected: nil, name: InterestType.politics, id: 4, label: InterestType.politics.description),
//            Interest(isSelected: nil, name: InterestType.health, id: 5, label: InterestType.health.description),
//            Interest(isSelected: nil, name: InterestType.fitness, id: 6, label: InterestType.fitness.description),
//            Interest(isSelected: nil, name: InterestType.spirituality, id: 7, label: InterestType.spirituality.description),
//            Interest(isSelected: nil, name: InterestType.communication, id: 8, label: InterestType.communication.description),
//            Interest(isSelected: nil, name: InterestType.science, id: 9, label: InterestType.science.description),
//            Interest(isSelected: nil, name: InterestType.lifecoaching, id: 10, label: InterestType.lifecoaching.description),
//            Interest(isSelected: nil, name: InterestType.stayingathome, id: 11, label: InterestType.stayingathome.description),
//            Interest(isSelected: nil, name: InterestType.psychology, id: 12, label: InterestType.psychology.description),
//            Interest(isSelected: nil, name: InterestType.math, id: 13, label: InterestType.math.description),
//            Interest(isSelected: nil, name: InterestType.art, id: 14, label: InterestType.art.description),
//            Interest(isSelected: nil, name: InterestType.business, id: 15, label: InterestType.business.description),
//            Interest(isSelected: nil, name: InterestType.partying, id: 16, label: InterestType.partying.description),
//            Interest(isSelected: nil, name: InterestType.computerscience, id: 17, label: InterestType.computerscience.description),
//            ]
//
//    }
//
    
    
//    struct Interests{
//        let statusCode: StatusCode?
//        let message: String?
//        let metaData: [String:String]?
//
//        init(statusCode: StatusCode?, message: String?){
//            metaData = nil
//            self.statusCode = statusCode
//            self.message = message
//        }
//
//        init(statusCode: StatusCode?, message: String?, metaData: [String:String]?){
//            self.metaData = metaData
//            self.statusCode = statusCode
//            self.message = message
//        }
//    }


//    extension DroppinView {
//
//        private var activityIndicator = UIActivityIndicatorView()
//
//        let isDroppinTester = UserDefaults.standard.bool(forKey: UserPropertyKey.isTester.rawValue)
//
//        func startActivityIndicator() {
//            activityIndicator.center = self.view.center
//            activityIndicator.hidesWhenStopped = true
//            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
//            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
//            view.addSubview(activityIndicator)
//
//            let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
//            view.addConstraint(horizontalConstraint)
//
//            let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
//
//            view.addConstraint(verticalConstraint)
//
//
//
//            self.activityIndicator.startAnimating()
//            UIApplication.shared.beginIgnoringInteractionEvents()
//        }
//
//        func stopActivityIndicator(){
//            self.activityIndicator.stopAnimating()
//            UIApplication.shared.endIgnoringInteractionEvents()
//        }
//
//
//        func showBasicAlert(title: String, message: String){
//            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction) in
//
//            }))
//
//            present(alert, animated: true, completion: nil)
//        }
//
//        func className( of obj: Any) -> String {
//            //prints more readable results for dictionaries, arrays, Int, etc
//            return String(describing: type(of: obj))
//        }
//
//        func lineDescription( of obj: Any, andFunction funcName: String, withDescription desc: String) -> String{
//            return "[\(className(of: self))][\(funcName)] ->\(desc)"
//}

