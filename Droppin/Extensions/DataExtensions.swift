//
//  DataExtensions.swift
//  Droppin
//
//  Created by Madson on 10/31/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

extension Data {
    func printJSON() {
        do {
            let json = try JSONSerialization.jsonObject(with: self, options: .allowFragments)
            debugPrint(json)
        } catch {
            debugPrint(error)
        }
    }
}
