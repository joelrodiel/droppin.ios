//
//  DateExtensions.swift
//  Droppin
//
//  Created by Madson on 10/18/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

extension Date {

    var short: String {
        let formatter = DateFormatter.short
        return formatter.string(from: self)
    }
}
