//
//  ImageViewExtensions.swift
//  Droppin
//
//  Created by Madson on 10/18/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

extension UIImageView: UserApi {
    var api: DroppinApi {
        return DroppinApi()
    }

    func setImage(_ imageName: String?, placeholder: UIImage? = nil) {
        guard let id = imageName else { return }
        guard let cdn = CDNManager.shared.cloudinary else { return }
        
        cldSetImage(publicId: id, cloudinary: cdn, signUrl: true, resourceType: .image,
                    transformation: nil, placeholder: placeholder)
    }

    func setImage(of userId: String, placeholder: UIImage = #imageLiteral(resourceName: "Profile")) {
        image = placeholder
        roundBorders()
        
        getUserImage(userId) { (error, request) in
            guard let image = request?.metaData?["profileImage"] else { return }
            self.setImage(image)
        }
    }
}
