//
//  JSONDecoderExtensions.swift
//  Droppin
//
//  Created by Madson on 9/7/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

extension JSONDecoder {
    static let shared: JSONDecoder = {
        $0.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
        return $0
    }( JSONDecoder() )
}

extension JSONEncoder {
    static let shared: JSONEncoder = {
        $0.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
        return $0
    }( JSONEncoder() )
}
