//
//  StringExtensions.swift
//  Droppin
//
//  Created by Madson on 9/23/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

extension String {
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
