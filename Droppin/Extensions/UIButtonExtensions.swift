//
//  UIButtonExtensions.swift
//  Droppin
//
//  Created by Isaias Garcia on 10/26/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

extension UIButton: UserApi {
    var api: DroppinApi {
        return DroppinApi()
    }
    
    func setImage(_ imageName: String?, placeholder: UIImage? = nil, for state: UIControlState = .normal) {
        guard let id = imageName else { return }
        guard let cdn = CDNManager.shared.cloudinary else { return }
        
        cldSetImage(publicId: id, cloudinary: cdn, forState: state, signUrl: true, transformation: nil, placeholder: placeholder)
    }
    
    func setImage(of userId: String, placeholder: UIImage = #imageLiteral(resourceName: "Profile"), for state: UIControlState = .normal) {
        setImage(placeholder, for: state)
        roundBorders()
        
        getUserImage(userId) { (error, request) in
            guard let image = request?.metaData?["profileImage"] else { return }
            self.setImage(image, placeholder: placeholder, for: state)
        }
    }
}
