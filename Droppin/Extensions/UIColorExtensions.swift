//
//  UIColorExtensions.swift
//  Droppin
//
//  Created by Madson on 9/20/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

extension UIColor {
    @nonobjc class var aquaMarine: UIColor {
        return UIColor(red: 65.0 / 255.0, green: 221.0 / 255.0, blue: 177.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var salmon: UIColor {
        return UIColor(red: 1.0, green: 112.0 / 255.0, blue: 98.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var steel: UIColor {
        return UIColor(red: 138.0 / 255.0, green: 138.0 / 255.0, blue: 143.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var tabBarItem: UIColor {
        return UIColor(red: 140.0 / 255.0, green: 140.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var gold: UIColor {
        return UIColor(red: 238.0 / 255.0, green: 194.0 / 255.0, blue: 41.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var border: UIColor {
        return UIColor(red: 200.0 / 255.0, green: 199.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
    }
}
