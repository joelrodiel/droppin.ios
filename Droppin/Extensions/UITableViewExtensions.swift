//
//  UITableViewExtensions.swift
//  Droppin
//
//  Created by Madson on 8/30/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

extension UITableView {
    
    func dequeue<T: Identifiable>(_ cell: T.Type) -> T? {
        return self.dequeueReusableCell(withIdentifier: cell.identifier) as? T
    }
    
    func dequeue<T: Identifiable>(_ cell: T.Type, for indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withIdentifier: cell.identifier, for: indexPath) as? T
    }
    
    func removeEmptyView() {
        backgroundView = nil
        separatorStyle = .singleLine
    }
    
    func displayEmptyView(title: String, description: String? = nil) {
        let emptyVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "emptyMyPinsView") as! EmptyMyPinsVC
        
        emptyVC.titleText = description == nil ? "" : title
        emptyVC.descriptionText = description == nil ? title : description
        separatorStyle = .none
        
        backgroundView = emptyVC.view
    }
}
