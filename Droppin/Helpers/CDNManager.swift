//
//  CDNManager.swift
//  Droppin
//
//  Created by Madson on 26/05/18.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import Cloudinary

/*
 Every new preset has to be configured on:
 https://cloudinary.com/console/settings/upload#upload_presets
 */

enum Preset: String {
    case profilePhotos = "profile-photos"
    case pinPhotos = "pin-photos"
}

struct CDNManager {
    
    static let shared = CDNManager()
    let cloudinary: CLDCloudinary!
    
    private let config: CLDConfiguration = {
        let apiKey    = Config.env.cloudinary.apiKey
        let apiSecret = Config.env.cloudinary.apiSecret
        let cloudName = Config.env.cloudinary.cloudName
        return CLDConfiguration(cloudName: cloudName, apiKey: apiKey, apiSecret: apiSecret, privateCdn: false, secure: true)
    }()
    
    init() {
        self.cloudinary = CLDCloudinary(configuration: self.config)
    }
    
    func upload(_ image: UIImage, preset: Preset, completion: @escaping (NSError?, String?) -> Void) {
        guard let data = UIImageJPEGRepresentation(image, 1.0) else {
            return
        }
        
        let uploader = self.cloudinary.createUploader()
        
        let params = ["folder" : preset.rawValue]
        
        let uploadParams = CLDUploadRequestParams(params: params as [String: AnyObject])
        uploadParams.setUploadPreset(preset.rawValue)
        
        uploader.signedUpload(data: data, params: uploadParams, progress: nil) { (result, error) in
            completion(error, result?.publicId)
        }
    }

    func downloadImage(from publicId: String?, completion: @escaping (UIImage?, NSError?) -> Void) {
        guard let publicId = publicId else {
            return
        }

        guard let signedURL = self.signedURL(publicId: publicId) else {
            return
        }

        self.download(url: signedURL) { (image, error) in
            completion(image, error)
        }
    }
}

extension CDNManager {

    private func download(url: String, completion: CLDCompletionHandler?) {
        self.cloudinary.createDownloader().fetchImage(url, nil, completionHandler: completion)
    }

    private func signedURL(publicId: String?) -> String? {
        guard let publicId = publicId else { return nil }
        return self.cloudinary.createUrl().generate(publicId, signUrl: true)
    }
}
