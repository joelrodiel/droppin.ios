//
//  Config.swift
//  Droppin
//
//  Created by Madson on 20/06/18.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

struct Config {
    static var env: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if configuration.range(of: "Dev") != nil || configuration.range(of: "Debug") != nil {
                return Environment.development
            } else if configuration.range(of: "Staging") != nil {
                return Environment.staging
            }
        }
        return Environment.production
    }()
    
    static var version: String? = {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }()
    
    static var build: String? = {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
    }()
    
    static func version(for environment: Environment) -> String? {
        guard
            let appVersion = Config.version,
            let build = Config.build
            else { return nil }
        
        var version = "Version \(appVersion)"
        
        if environment != .production {
            version += " (\(build))"
        }
        
        return version
    }
}
