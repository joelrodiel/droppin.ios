//
//  ViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 10/2/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//
import UIKit

enum SwipingDirection {
    case up
    case down
    case left
    case right
    
    var description : String {
        switch self {
        case .down: return "Down"
        case .up: return "Up"
        case .left: return "Left"
        case .right: return "Right"
        }
    }
    
}

enum SliderType {
    case LeftSlider
    case TopSlider
    case BottomSlider
}

enum SlidingOrientation {
    case Horizontal
    case Vertical
}

enum ObservableState {
    case isExpanding
    case hasExpanded
    case isCollapsing
    case hasCollapsed
    case mightExpand
    
    var description : String {
        switch self {
        case .isExpanding: return "Is Expanding"
        case .hasExpanded: return "has expanded"
        case .isCollapsing: return "is collapsing"
        case .hasCollapsed: return "has Collapsed"
        case .mightExpand: return "might expand"
        }
    }
}

protocol Observable {
    var observers: [Observer] {get set}
    mutating func attach(_ observe: Observer)
    func notifyObservers(state: ObservableState, sliderType: SliderType)
}


// @todo guard against observing self!
extension Observable {
     mutating func attach(_ observer: Observer) {
        
//        guard (observer === observer) else {
//            return
//        }
        
        observers.append(observer)
    }
    
    
    mutating func attach(_ observerList: [Observer]) {
        for observer in observerList {
            attach(observer)
//            observers.append(observer)
        }
    }
    
    func notifyObservers(state: ObservableState, sliderType: SliderType){
        for observer in observers {
            observer.update(state, sliderType: sliderType)
        }
    }
}

protocol Observer {
    func update(_ observableState: ObservableState, sliderType: SliderType)
}

protocol DISliderDelegate {
    func willSlideIntoView()
    
}

class DISlider: Observable, Observer {
    let shadowOpacity:Float = 0.8
    let shadowRadius: CGFloat = 6
    var isExpanded: Bool, isExpanding: Bool, isSliding: Bool, isCollapsing: Bool
    var didSlid: Bool
    var visibleLength: CGFloat, bufferLength: CGFloat
    let animationTime: Double
    var totalLength: CGFloat
    let type: SliderType
    let slidingOrientation: SlidingOrientation
    let view: UIView
    var expandedLocation: CGPoint, collapsedLocation: CGPoint
    var enableDragExpand: Bool, enableDragCollapse: Bool
    let parentViewSize: CGSize
    var observers = [Observer]()
    var isCollapsed: Bool {
        return !isExpanded
    }
    
//    var delegate: DISliderDelegate?
    
    init(type: SliderType, sliderView view: UIView, visibleLength: CGFloat, isExpanded: Bool = false ,animationTime: Double = 0.3, enableDragExpand: Bool = true, enableDragCollapse: Bool = true, parentViewSize: CGSize){
        self.type = type
        self.isExpanded = isExpanded
        self.isExpanding = false
        self.isCollapsing = false
        self.visibleLength = visibleLength
        self.bufferLength = 222
        self.animationTime = animationTime
        self.totalLength = self.visibleLength + self.bufferLength
        self.enableDragExpand = enableDragExpand
        self.enableDragCollapse = enableDragCollapse
        self.parentViewSize = parentViewSize
        
        self.view = view
//        self.delegate = self.view
        switch type {
        case .BottomSlider:
            self.slidingOrientation = .Vertical
            expandedLocation = CGPoint(x: 0, y: parentViewSize.height - visibleLength)
            collapsedLocation = CGPoint(x: 0, y: parentViewSize.height)
        case .TopSlider:

            self.slidingOrientation = .Vertical
            expandedLocation = CGPoint(x:0, y:-bufferLength)
            collapsedLocation = CGPoint(x:0, y:-totalLength)
        case .LeftSlider:

            self.slidingOrientation = .Horizontal
            expandedLocation = CGPoint(x: -bufferLength, y: 0)
            collapsedLocation = CGPoint(x: -totalLength, y: 0)
        }
        
        isSliding = false
        didSlid = false
        (isExpanded) ? expand() : collapse()
        updateViewProperties(parentViewSize: parentViewSize)
    }
    
    func refreshView(for visibleLength: CGFloat) {
        self.visibleLength = visibleLength
        self.bufferLength = 222
        self.totalLength = self.visibleLength + self.bufferLength
        updateViewProperties(parentViewSize: parentViewSize)
        expandedLocation = CGPoint(x: 0, y: parentViewSize.height - visibleLength)
    }
    
    func updateViewProperties(parentViewSize: CGSize){
        switch type {
        case .BottomSlider:
            view.frame.origin.x = 0
            view.frame.origin.y = parentViewSize.height
//            view.frame.size.height = totalLength
            expandedLocation = CGPoint(x: 0, y: parentViewSize.height - visibleLength)
            collapsedLocation = CGPoint(x: 0, y: parentViewSize.height)
        case .LeftSlider:
            view.frame.origin.x = -totalLength
            view.frame.origin.y = 0
            view.frame.size.width = totalLength
            expandedLocation = CGPoint(x: -bufferLength, y: 0)
            collapsedLocation = CGPoint(x: -totalLength, y: 0)
        case .TopSlider:
            view.frame.origin.x = 0
            view.frame.origin.y = -totalLength
            view.frame.size.height = totalLength
            expandedLocation = CGPoint(x:0, y:-bufferLength)
            collapsedLocation = CGPoint(x:0, y:-totalLength)
        }
    }

    // Not sure if this is the right place here. The VC controller should decide what to do.
    func update(_ observableState: ObservableState, sliderType: SliderType)  {
        if (observableState == .mightExpand) {
            guard isExpanded else {
                return
            }
            print("[Observer][update] ->A Slider mightExpand so collapse me. Who sent the update?: " + String(describing: sliderType))
            self.collapse()
        }
    }
    
    func completeTransition(){
//            print("[Slider][completeTransition] ->")
        
        guard didSlid else {
            return
        }
        
            if (isExpanded) { // going from expanded to collapsed
                if (isPastGravityHold()) {
//                    print("[handlePanGesture] ->is Expanded, PastGravityHold so collapsing")
                    collapse()
                } else {
//                    print("[handlePanGesture] ->is Expanded, NOT PastGravityHold so expanding")
                    expand()
                }
                
            } else {  //if collapsed
                if (isPastGravityHold()) {
//                    print("[handlePanGesture] ->is NOT Expanded, PastGravityHold so expanding")
                    expand()
                } else {
//                    print("[handlePanGesture] ->is NOT Expanded, NOT PastGravityHold so collapsing")
                    collapse()
                }
            }
        
        didSlid = false
    }
    
    func toggle(){
        print("toggle")
        (isExpanded) ? collapse() : expand()
    }
    
    func expand(){
        guard (view.frame.origin != expandedLocation) else {
            isExpanded = true
            return
        }
        isExpanding = true
        notifyObservers(state: .isExpanding, sliderType: type)
//        delegate?.willSlideIntoView()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layer.shadowOpacity = self.shadowOpacity
            self.view.layer.shadowRadius = self.shadowRadius
        }, completion: nil)
        animate(expandedLocation)
        isExpanding = false
        isExpanded = true
        notifyObservers(state: .hasExpanded, sliderType: type)
    }
    
    func collapse(){
        
        guard (view.frame.origin != collapsedLocation) else {
            isExpanded = false
            return
        }
        
        isCollapsing = true
        notifyObservers(state: .isCollapsing, sliderType: type)
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layer.shadowOpacity = 0
            self.view.layer.shadowRadius = 0
        }, completion: nil)
        
        
        animate(collapsedLocation)
        
        isExpanded = false
        
        notifyObservers(state: .hasCollapsed, sliderType: type)
        isCollapsing = false
    }
    
    private func currentLocation() -> CGFloat{
        switch (type) {
        case .BottomSlider, .TopSlider:
            return view.frame.origin.y
        case .LeftSlider:
            return view.frame.origin.x
        }
    }
    
    private func expandedSlideLocation() -> CGFloat {
        switch (type) {
        case .BottomSlider, .TopSlider:
            return expandedLocation.y
        case .LeftSlider:
            return expandedLocation.x
        }
    }
    
    private func collapsedSlideLocation() -> CGFloat {
        switch (type) {
        case .BottomSlider, .TopSlider:
            return collapsedLocation.y
        case .LeftSlider:
            return collapsedLocation.x
        }
    }
    
    
    private func withinAllowedLocation() -> Bool {
        switch (type ) {
        case .LeftSlider:
            if (currentLocation() <= expandedSlideLocation() && currentLocation() >= collapsedSlideLocation()){
                return true
            }
        case .BottomSlider:
            if (currentLocation() >= expandedSlideLocation() &&  currentLocation() <= collapsedSlideLocation()  ){
                return true
            }
        case .TopSlider:
            if (currentLocation() >= collapsedSlideLocation() && currentLocation() <= expandedSlideLocation() ){
                return true
            }
        }
        return false
    }
    
    
    
    
    func isPastGravityHold() -> Bool {
        
        let quarterOfSliderVisibleLength = visibleLength * 0.25
        
        switch (type) {
        case .LeftSlider:
            if (isExpanded) {
                if (view.frame.origin.x <= expandedLocation.x - quarterOfSliderVisibleLength ) {
                    return true
                }
            } else {
                if (view.frame.origin.x >= (collapsedLocation.x + quarterOfSliderVisibleLength) ) {
                    return true
                }
            }
        case .BottomSlider:
            if (isExpanded) {
                if (view.frame.origin.y >= (expandedLocation.y + quarterOfSliderVisibleLength) ) {
                    return true
                }
            } else {
                if (view.frame.origin.y <= ( collapsedLocation.y  - quarterOfSliderVisibleLength) ) {
                    
                    return true
                }
                
            }
        case .TopSlider:
            if (isExpanded) {
                if (view.frame.origin.y <= (expandedLocation.y + quarterOfSliderVisibleLength) ) {
                    return true
                }
            } else {
                if (view.frame.origin.y >= ( collapsedLocation.y  - quarterOfSliderVisibleLength) ) {
                    //                    print("[isPastGravityHold] -> for collapsed bottomSlider? YES")
                    return true
                }
            }
        }
        
        return false
    }
    
    private func  allowedToSlide() -> Bool {
        
        if ( withinAllowedLocation()){
            
            if (enableDragExpand && isCollapsed) {
                return true
            }
            
            if (enableDragCollapse && isExpanded) {
                return true
            }
            
            if (isExpanding || isCollapsing) {
                return true
            }
            
        }
        return false
    }
    
    
    func canSlide(to swipingDirection: SwipingDirection) -> Bool {
        
        switch swipingDirection {
        case .down:
            if (slidingOrientation == .Vertical) {
//                print("CanSlide? YES its vertical")
                return true
            }
            
        case .left:
            if (slidingOrientation == .Horizontal) {
//                print("CanSlide? YES its horizontal")
                return true
            }
        case .right:
            if (slidingOrientation == .Horizontal) {
//                print("CanSlide? YES its horizontal")
                return true
            }
        case .up:
            if (slidingOrientation == .Vertical) {
//                print("CanSlide? YES its vertical")
                return true
            }
        }
//        print("CanSlide? NO its " + String(describing: slidingOrientation))
        return false
    }
    

    func slide(translation: CGPoint){
        if (allowedToSlide()){
            if (isCollapsed && isPastGravityHold()) {
                notifyObservers(state: .mightExpand, sliderType: type)
            }
            isSliding = true
            self.view.layer.shadowOpacity = shadowOpacity
            self.view.layer.shadowRadius = shadowRadius
            switch type {
            case .LeftSlider:
//                print("SLIDING" + String(describing: view.frame.origin.x))
                view.frame.origin.x +=  translation.x
            case .TopSlider:
//                print("SLIDING" + String(describing: view.frame.origin.y))
                view.frame.origin.y +=  translation.y
            case .BottomSlider:
//                print("SLIDING" + String(describing: view.frame.origin.y))
                view.frame.origin.y +=  translation.y
            }
            isSliding = false
            didSlid = true
        }
//        print("-slide-allowedToSlide ---- NO!")
    }
    
    internal func animate(_ newPosition: CGPoint){

        guard ( self.view.frame.origin != newPosition) else {
            return
        }
        
//        if (self.view.frame.origin == newPosition) {
//            return
//        }
        
        UIView.animate(withDuration: animationTime,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut,
                       animations: {self.view.frame.origin = newPosition },
                       completion: nil)
    }
    
}


