//
//  DiActivityIndicator.swift
//  Droppin
//
//  Created by Rene Reyes on 12/25/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

//import Foundation
import UIKit

final class DiActivityIndicator: NSObject {
    
    static let sharedInstance = DiActivityIndicator()
    
    var isShowing = false
    var dismissTimer: Timer?
    
    
    lazy var containerView: UIView = {
        let view = UIView()
        //        view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.5)
        return view
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        return view
    }()
    
    class func showActivityIndicator() {
        showActivityIndicatorWhileBlockingUI(true)
    }
    
    class func showActivityIndicatorWhileBlockingUI(_ blockingUI: Bool) {
        
        if sharedInstance.isShowing {
            return // TODO:
        }
        
        DispatchQueue.main.async {
            if
                let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                //                let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate,
                let window = appDelegate.window {
                
                sharedInstance.isShowing = true
                sharedInstance.containerView.isUserInteractionEnabled = blockingUI
                sharedInstance.containerView.alpha = 0
                window.addSubview(sharedInstance.containerView)
                sharedInstance.containerView.frame = window.bounds
                
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
                    sharedInstance.containerView.alpha = 1
                    
                }, completion: { _ in
                    
                    sharedInstance.containerView.addSubview(sharedInstance.activityIndicator)
                    sharedInstance.activityIndicator.center = sharedInstance.containerView.center
                    sharedInstance.activityIndicator.startAnimating()
                    
                    sharedInstance.activityIndicator.alpha = 0
                    sharedInstance.activityIndicator.transform = CGAffineTransform(scaleX: 0.0001, y: 0.0001)
                    UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
                        sharedInstance.activityIndicator.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        sharedInstance.activityIndicator.alpha = 1
                        
                    }, completion: { _ in
                        sharedInstance.activityIndicator.transform = CGAffineTransform.identity
                        
                        if let dismissTimer = sharedInstance.dismissTimer {
                            dismissTimer.invalidate()
                        }
                        
                        
                        sharedInstance.dismissTimer = Timer.scheduledTimer(timeInterval: DiConfig.forcedHideActivityIndicatorTimeInterval, target: self, selector: #selector(DiActivityIndicator.forcedHideActivityIndicator), userInfo: nil, repeats: false)
                    })
                })
            }
        }
    }
    
    @objc class func forcedHideActivityIndicator() {
        hideActivityIndicator() {
            if
                let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let viewController = appDelegate.window?.rootViewController {
                DiAlert.alertSorry(message: NSLocalizedString("Wait is too long, the operation may not be completed.", comment: ""), inViewController: viewController)
            }
        }
    }
    
    class func hide() {
        hideActivityIndicator() {
        }
    }
    
    class func hideActivityIndicator(completion: @escaping  () -> Void) {
        
        DispatchQueue.main.async {
            
            if sharedInstance.isShowing {
                
                sharedInstance.activityIndicator.transform = CGAffineTransform.identity
                
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
                    sharedInstance.activityIndicator.transform = CGAffineTransform(scaleX: 0.0001, y: 0.0001)
                    sharedInstance.activityIndicator.alpha = 0
                    
                }, completion: { _ in
                    sharedInstance.activityIndicator.removeFromSuperview()
                    
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
                        sharedInstance.containerView.alpha = 0
                        
                    }, completion: { _ in
                        sharedInstance.containerView.removeFromSuperview()
                        
                        completion()
                    })
                })
            }
            
            sharedInstance.isShowing = false
        }
    }
}


