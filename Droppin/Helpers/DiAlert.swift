
import UIKit
//import YepKit
//import Proposer

final class DiAlert {
    
    class func createAlert(title: String, message: String?, dismissTitle: String, withDismissAction dismissAction: (() -> Void)?) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action: UIAlertAction = UIAlertAction(title: dismissTitle, style: .default) { action in
            if let dismissAction = dismissAction {
                dismissAction()
            }
        }
        
        alertController.addAction(action)
        
        return alertController
    }
    
    class func alert( title: String, message: String?, dismissTitle: String, inViewController viewController: UIViewController?, withDismissAction dismissAction: (() -> Void)?) {
        
        DispatchQueue.main.async {
            let alertController = createAlert(title: title, message: message, dismissTitle: dismissTitle, withDismissAction: dismissAction)
            
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    class func alertSorry( message: String?, inViewController viewController: UIViewController?, withDismissAction dismissAction: @escaping () -> Void) {
        alert(title: NSLocalizedString("Sorry", comment: ""), message: message, dismissTitle: NSLocalizedString("OK", comment: ""), inViewController: viewController, withDismissAction: dismissAction)
    }
    
    class func alertAttention( message: String?, inViewController viewController: UIViewController?) {
        alert(title: NSLocalizedString("Oops!", comment: ""), message: message, dismissTitle: NSLocalizedString("OK", comment: ""), inViewController: viewController, withDismissAction: nil)
    }
    
    class func alertSorry( message: String?, inViewController viewController: UIViewController?) {
        
        alert(title: NSLocalizedString("Sorry", comment: ""), message: message, dismissTitle: NSLocalizedString("OK", comment: ""), inViewController: viewController, withDismissAction: nil)
    }
    
    class func textInput( title: String, placeholder: String?, oldText: String?, dismissTitle: String, inViewController viewController: UIViewController?, withFinishedAction finishedAction: ((_ text: String) -> Void)?) {
        
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            
            alertController.addTextField { textField in
                textField.placeholder = placeholder
                textField.text = oldText
            }
            
            let action: UIAlertAction = UIAlertAction(title: dismissTitle, style: .default) { action in
                if let finishedAction = finishedAction {
                    if let textField = alertController.textFields?.first, let text = textField.text {
                        finishedAction(text)
                    }
                }
            }
            alertController.addAction(action)
            
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    static weak var confirmAlertAction: UIAlertAction?
    
    class func textInput(title: String, message: String?, placeholder: String?, oldText: String?, confirmTitle: String, cancelTitle: String, inViewController viewController: UIViewController?, withConfirmAction confirmAction: ((String) -> Void)?, cancelAction: (() -> Void)?) {
        
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alertController.addTextField { textField in
                textField.placeholder = placeholder
                textField.text = oldText
                textField.addTarget(self, action: #selector(DiAlert.handleTextFieldTextDidChangeNotification(_:)), for: .editingChanged)
            }
            
            let _cancelAction: UIAlertAction = UIAlertAction(title: cancelTitle, style: .cancel) { action in
                cancelAction?()
            }
            
            alertController.addAction(_cancelAction)
            
            let _confirmAction: UIAlertAction = UIAlertAction(title: confirmTitle, style: .default) { action in
                if let textField = alertController.textFields?.first, let text = textField.text {
                    
                    confirmAction?(text)
                }
            }
            _confirmAction.isEnabled = false
            self.confirmAlertAction = _confirmAction
            
            alertController.addAction(_confirmAction)
            
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc class func handleTextFieldTextDidChangeNotification(_ sender: UITextField) {
        guard  let fieldValue = sender.text else {
            DiAlert.confirmAlertAction?.isEnabled  = false
            return
        }
        
        if (fieldValue.count >= 1) {
            DiAlert.confirmAlertAction?.isEnabled =  true
        } else {
            DiAlert.confirmAlertAction?.isEnabled = false
        }
        
        
//        DiAlert.confirmAlertAction?.enabled =   (sender.text?.characters.count >= 1) ?? false
//        DiAlert.confirmAlertAction?.enabled = sender.count >= 1
    }
    
    class func confirmOrCancel( title: String, message: String?, confirmTitle: String, cancelTitle: String, inViewController viewController: UIViewController?, withConfirmAction confirmAction: @escaping () -> Void, cancelAction: (() -> Void)? = nil) {
        
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let cancelAction: UIAlertAction = UIAlertAction(title: cancelTitle, style: .cancel) { action in
                cancelAction?()
            }
            alertController.addAction(cancelAction)
            
            let confirmAction: UIAlertAction = UIAlertAction(title: confirmTitle, style: .default) { action in
                confirmAction()
            }
            alertController.addAction(confirmAction)
            
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    class func confirmOrCancel(title: String?, message: String?, inViewController viewController: UIViewController, confirmAction: @escaping () -> Void) {
        let cancelAction = UIAlertAction(title: "No", style: .cancel)
        let confirmAction = UIAlertAction(title: "Yes", style: .default, handler: {_ in confirmAction()})
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        viewController.present(alertController, animated: true)
    }
}

extension UIViewController {
    
//    func alertCanNotAccessCameraRoll() {
//
//        DispatchQueue.main.async {
//            DiAlert.confirmOrCancel(title: NSLocalizedString("Sorry", comment: ""), message: NSLocalizedString("Yep can not access your Camera Roll!\nBut you can change it in iOS Settings.", comment: ""), confirmTitle: String, cancelTitle: NSLocalizedString("Dismiss", comment: ""), inViewController: self, withConfirmAction: {
//
//                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
//
//            }, cancelAction: {
//            })
//        }
//    }
//
//    func alertCanNotOpenCamera() {
//
//        DispatchQueue.main.async {
//            DiAlert.confirmOrCancel(title: NSLocalizedString("Sorry", comment: ""), message: NSLocalizedString("Yep can not open your Camera!\nBut you can change it in iOS Settings.", comment: ""), confirmTitle: String, cancelTitle: NSLocalizedString("Dismiss", comment: ""), inViewController: self, withConfirmAction: {
//
//                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
//
//            }, cancelAction: {
//            })
//        }
//    }
//
//    func alertCanNotAccessMicrophone() {
//
//        DispatchQueue.main.async {
//            DiAlert.confirmOrCancel(title: NSLocalizedString("Sorry", comment: ""), message: NSLocalizedString("Yep can not access your Microphone!\nBut you can change it in iOS Settings.", comment: ""), confirmTitle: String, cancelTitle: NSLocalizedString("Dismiss", comment: ""), inViewController: self, withConfirmAction: {
//
//                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
//
//            }, cancelAction: {
//            })
//        }
//    }
//
//    func alertCanNotAccessContacts() {
//
//        DispatchQueue.main.async {
//            DiAlert.confirmOrCancel(title: NSLocalizedString("Sorry", comment: ""), message: NSLocalizedString("Yep can not read your Contacts!\nBut you can change it in iOS Settings.", comment: ""), confirmTitle: String, cancelTitle: NSLocalizedString("Dismiss", comment: ""), inViewController: self, withConfirmAction: {
//
//                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
//
//            }, cancelAction: {
//            })
//        }
//    }
//
//    func alertCanNotAccessLocation() {
//
//        DispatchQueue.main.async {
//            DiAlert.confirmOrCancel(title: NSLocalizedString("Sorry", comment: ""), message: NSLocalizedString("Yep can not get your Location!\nBut you can change it in iOS Settings.", comment: ""), confirmTitle: String, cancelTitle: NSLocalizedString("Dismiss", comment: ""), inViewController: self, withConfirmAction: {
//
//                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
//
//            }, cancelAction: {
//            })
//        }
//    }
    
//    func showProposeMessageIfNeedForContactsAndTryPropose(propose: Propose) {
//
//        if PrivateResource.Contacts.isNotDeterminedAuthorization {
//
//            DispatchQueue.main.async {
//
//                DiAlert.confirmOrCancel(title: NSLocalizedString("Notice", comment: ""), message: NSLocalizedString("Yep need to read your Contacts to continue this operation.\nIs that OK?", comment: ""), confirmTitle: NSLocalizedString("OK", comment: ""), cancelTitle: NSLocalizedString("Not now", comment: ""), inViewController: self, withConfirmAction: {
//
//                    propose()
//
//                }, cancelAction: {
//                })
//            }
//
//        } else {
//            propose()
//        }
//    }
}


