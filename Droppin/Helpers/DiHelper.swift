
import UIKit

final class DiHelper: NSObject {
    
    class var userId: String? {
        get {
            if let userId = UserDefaults.standard.string(forKey: UserPropertyKey.currentUserId.rawValue), userId.count == 24 {
                return userId
            }
            
            return nil
        }
    }
    
    class var isNotAValidUserId: Bool {
        get { return self.userId == nil }
    }
    
    class func lineDescription( of obj: Any, andFunction funcName: String, withDescription desc: String) -> String{
        return "[\(String(describing: type(of: obj)))][\(funcName)] -> \(desc)"
    }
    
    class func printLineDescription(of obj: Any, andFunction funcName: String, withDescription desc: String) {
        print(lineDescription(of: obj, andFunction: funcName, withDescription: desc))
    }
    
    class func printAndFocusLine(toPrint obj: Any) {
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(obj)
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
        print(".........................")
    }
}
