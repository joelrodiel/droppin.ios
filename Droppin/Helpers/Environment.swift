//
//  Environment.swift
//  Droppin
//
//  Created by Madson on 10/3/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

enum Environment: String {
    case development
    case staging
    case production

    var baseURL: String {
        switch self {
        case .development: return "http://192.168.5.133:3007"
        case .staging: return "https://upin-api-staging.herokuapp.com"
        case .production: return "https://upin-api-prod.herokuapp.com"
        }
    }

    var cloudinary: (cloudName: String, apiKey: String, apiSecret: String) {
        switch self {
        case .development: return ("upin-dev", "292458566622473", "WxvnWodpDPBRzmG1tt489lTAhQU")
        case .staging: return ("upin-dev", "292458566622473", "WxvnWodpDPBRzmG1tt489lTAhQU")
        case .production: return ("hmny2igvn", "383731863295494", "NAmBxi_fA7a13gGlyXcYakvlY6s")
        }
    }

}
