//
//  userView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/5/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class JoinForm1VC: UIViewController, DroppinView, UserApi {
    let api = DroppinApi()
    
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: EmailTextField!
    @IBOutlet weak var password: PasswordTextField!
    @IBOutlet weak var policyBlurp: UITextView!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    @IBOutlet weak var legalButton: UIButton!
    @IBOutlet var info: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        nextButton.isEnabled = true
        
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        password.delegate = self
        firstName.tag = 0
        lastName.tag = 1
        email.tag = 2
        password.tag = 3
        
        emailError.isHidden = true
        email.addTarget(self, action: #selector(showEmailError), for: .editingChanged)
        
        passwordError.isHidden = true
        password.addTarget(self, action: #selector(showPasswordError), for: .editingChanged)
        
        if isTester {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Is Tester"))
            firstName.text = randomName("test")
            lastName.text = Lorem.lastName
            email.text = Lorem.emailAddress
            password.text = firstName.text

            for textField in info {
                textField.addTarget(self, action: #selector(editingChanged(sender:)), for: .editingChanged)
            }
        }

        updateNextButtonState()
    }
    
    @objc func editingChanged(sender: UITextField){
        //@todo what if the text is cut&pasted with a space in the begining?
        if sender.text?.count == 1 && sender.text?.first == " " {
            sender.text = ""
            return
        }
        
        let fieldEmpty = self.info.map{$0.text?.isEmpty}
        if fieldEmpty.contains(where: {$0 == true}) {
            self.nextButton.isEnabled = false
            return
        }
        self.nextButton.isEnabled = true
    }

   
    @IBAction func next(_ sender: UIBarButtonItem) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        DiActivityIndicator.showActivityIndicator()

        self.createUser(firstName: self.firstName.text!, lastName: self.lastName.text!, email: self.email.text!, password: self.password.text!) { (diResult)->() in
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "createUser completion"))
            switch diResult {
            case .success(_):
                print("[next][createUser][result] ->user added.")
                DiActivityIndicator.hide()
                self.performSegue(withIdentifier: "mobile", sender: sender)
            case .failure(_, let error):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "failure: \(error)"))
                switch error {
                case .dEmailAlreadyExists:
                    self.showBasicAlert(title: "Account already exists!", message: "The email you entered is already registered.", viewController: self)
                case .dAccountAlreadyExists:
                    self.showBasicAlert(title: "Account already exists!", message: "The email you entered is already registered.", viewController: self)
                default:
                    self.showBasicAlert(title: "Error!", message: "Please try again.", viewController: self)
                }
            }
            
            DiActivityIndicator.hide()
        }
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "End"))
    }
    
    // Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Touches Began"))
        self.view.endEditing(true)
    }
    
    func updateNextButtonState(){
        guard !(firstName.text?.isEmpty)!, !(lastName.text?.isEmpty)!, email.isValid(), password.isValid() else {
            nextButton.isEnabled = false
            return
        }
        nextButton.isEnabled = true
    }
}

extension JoinForm1VC {
    
    @objc private func showEmailError() {
        email.text = email.text?.lowercased()
        emailError.isHidden = email.isValid()
    }
    
    @objc private func showPasswordError() {
        passwordError.isHidden = password.isValid()
    }
}

extension JoinForm1VC: UITextFieldDelegate, UITextViewDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1 ) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        updateNextButtonState()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        updateNextButtonState()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        nextButton.isEnabled = false
        return true
    }
}

extension JoinForm1VC {
    private func setupUI() {
        setupNavigationBar()
        setupTextFields()
        setupLegalButton()
    }
    private func setupNavigationBar() {
        removeNavigationBarTraits()
    }
    private func setupTextFields() {
        _ = [firstName, lastName, email, password].map {
            $0?.keyboardAppearance = .dark
            $0?.addBottomBorder(.steel)
        }

        _ = [firstName, lastName].map {
            $0?.autocapitalizationType = .words
        }

        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.steel]

        firstName.attributedPlaceholder = NSAttributedString(string:"First name", attributes: attributes)
        lastName.attributedPlaceholder = NSAttributedString(string:"Last name", attributes: attributes)
        email.attributedPlaceholder = NSAttributedString(string:"E-mail", attributes: attributes)
        password.attributedPlaceholder = NSAttributedString(string:"Password", attributes: attributes)
    }
    private func setupLegalButton() {
        let text = "By clicking Next, You agree to UPIN's\nUser Agreement, Privacy Policy and Cookie Policy"

        let color = UIColor.aquaMarine

        let attributedString = NSMutableAttributedString(string: text, attributes: [
                .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                .foregroundColor: UIColor.steel,
                .kern: -0.08
            ])

        attributedString.addAttribute(.foregroundColor, value: color, range: NSRange(location: 38, length: 14))
        attributedString.addAttribute(.foregroundColor, value: color, range: NSRange(location: 54, length: 14))
        attributedString.addAttribute(.foregroundColor, value: color, range: NSRange(location: 73, length: 13))

        legalButton.titleLabel?.numberOfLines = 0
        legalButton.titleLabel?.textAlignment = .center
        legalButton.setAttributedTitle(attributedString, for: .normal)
    }
}
