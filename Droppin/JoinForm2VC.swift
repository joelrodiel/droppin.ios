//
//  MobileView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/11/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
//import Alamofire

class JoinForm2VC: UIViewController,DroppinView, UITextFieldDelegate, UserApi {
//    var activityIndicator = UIActivityIndicatorView()
//    var isDroppinTester = UserDefaults.standard.bool(forKey: UserPropertyKey.isTester.rawValue)
    let api: DroppinApi = DroppinApi()
    
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var nextButton: UIBarButtonItem!

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationController = segue.destination as? JoinForm3VC {
            destinationController.phoneNumber = phoneNumber()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotificationCenter()
        mobile.delegate = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitMobile(_ sender: UIBarButtonItem) {
        print("[MobileView][submitMobile] -> button pressed")

        guard let phoneNumber = self.phoneNumber() else {
            print("[MobileView][submitMobile] -> invalid phone number")
            return
        }

//        self.startActivityIndicator(self.view)
        DiActivityIndicator.showActivityIndicator()

        self.updateMobile(mobile: phoneNumber) { diResult in
            
            switch diResult {
            case .success(_):
                self.performSegue(withIdentifier: "verify", sender: sender)
            case .failure(_, let error):
                print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "failure!"))
                switch error {
                case .dMobileAlreadyExists:
                    self.showBasicAlert(title: "Phone number already exists!", message: "The phone number you entered is already registered.", viewController: self)
                default:
                    self.showBasicAlert(title: "Error!", message: "Please try again.", viewController: self)
            }
            }
            
//        self.stopActivityIndicator()
            DiActivityIndicator.hide()
        }
 
    }
    
}

// MARK: - Helpers

extension JoinForm2VC {
    private func phoneNumber() -> String? {
        guard let number = self.mobile.text,
            let country = self.country.text else {
                return nil
        }
        
        if country.trimmed.isEmpty {
            return "+1" + number
        } else {
            return country + number
        }
    }

    private func setupNotificationCenter() {
        NotificationCenter.default.addObserver(forName: .UITextFieldTextDidChange, object: nil, queue: nil) {
            guard let textField = $0.object as? UITextField else { return }
            guard textField == self.mobile else { return }
            guard let text = textField.text?.trimmed else { return }

            self.nextButton.isEnabled = !text.isEmpty && text.count > 9
        }
    }
}

// MARK: - UI

extension JoinForm2VC {
    private func setupUI() {
        setupNextButton()
        setupNavigationBar()
        setupTextFields()
    }

    private func setupNextButton() {
        self.nextButton.isEnabled = false
    }

    private func setupNavigationBar() {
        removeNavigationBarTraits()
    }

    private func setupTextFields() {
        _ = [country, mobile].map {
            $0?.keyboardAppearance = .dark
            $0?.addBottomBorder(.steel)
        }
        
        country.keyboardType = .phonePad
        mobile.keyboardType = .phonePad
        
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.steel,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17.0, weight: .regular)
        ]
        
        country.attributedPlaceholder = NSAttributedString(string:"+1", attributes: attributes)
        mobile.attributedPlaceholder = NSAttributedString(string:"Phone number", attributes: attributes)
    }
}
