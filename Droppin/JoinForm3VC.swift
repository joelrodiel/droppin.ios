//
//  VerifyMobileView
//  Droppin
//
//  Created by Rene Reyes on 10/11/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

protocol DeleteTextFieldDelegate {
    func deletePressed(_ textField: UITextField)
}

final class DeleteTextField: UITextField {
    var deleteDelegate: DeleteTextFieldDelegate?

    var isValid: Bool {
        guard let text = self.text else { return false }
        return text.trimmed.count > 0
    }

    override func deleteBackward() {
        deleteDelegate?.deletePressed(self)
    }
}

// Enter Code
final class JoinForm3VC:  UIViewController, DroppinView, UserApi {
    
    let api: DroppinApi = DroppinApi()
    var phoneNumber: String?
    
    @IBOutlet weak var code0: DeleteTextField!
    @IBOutlet weak var code1: DeleteTextField!
    @IBOutlet weak var code2: DeleteTextField!
    @IBOutlet weak var code3: DeleteTextField!
    @IBOutlet weak var code4: DeleteTextField!
    @IBOutlet weak var code5: DeleteTextField!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!

    override func viewDidLoad() {
        print("[VerifyMobileView][viewDidLoad] -> start")
        super.viewDidLoad()

        setupUI()
        resend(nil)

        print("[VerifyMobileView][viewDidLoad] -> end")
    }

    @IBAction func verifyCode(_ sender: UIBarButtonItem) {
        startVerification()
    }

    @IBAction func resend(_ sender: UIButton?) {
        self.requestOtp() { diResult in
            switch diResult {
            case .success(let requestResult):
                print("OTP: \(requestResult.otp)")
            case .failure(_, _):
                self.showBasicAlert(title: "Error", message: "We are experiencing an issue with the app. Please try again later.", viewController: self)
            }
        }
    }

    func startVerification() {
        print("[VMView][startVerification] -> pressed")

        DiActivityIndicator.showActivityIndicator()

        let code = self.verificationCode()

        self.verifyMobileAndActivateUser(otp: code) { error, result in
            if let error = error {
                if error == .dInvalidData {
                    self.showAlert(message: "That code is wrong.")
                } else if error == .dExpiredData {
                    self.showAlert(message: "That code has expired. You can request a new one.")
                } else {
                    self.showAlert(message: "There was an issue verifying your phone. Please try again.")
                }

                return
            }

            guard let userId = result?.userId else {
                self.showAlert(message: String("There was an issue verifying your phone. Please try again."))
                return
            }

            UserDefaults.standard.set(userId, forKey: UserPropertyKey.currentUserId.rawValue)
            UserDefaults.standard.set(true, forKey: UserPropertyKey.isAccountCreated.rawValue)

            // updates token after user creation
            self.updateToken()

            self.performSegue(withIdentifier: "BDayViewSegue", sender: nil)
        }

        DiActivityIndicator.hide()
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction) in
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Helpers

extension JoinForm3VC {
    private func verificationCode() -> String {
        var code = ""

        for field in [code0, code1, code2, code3, code4, code5] {
            if let text = field?.text {
                code += text
            }
        }

        return code
    }

    private func toggleNextButton() {
        self.nextButton.isEnabled = self.code0.isValid && self.code1.isValid && self.code2.isValid
            && self.code3.isValid && self.code4.isValid && self.code5.isValid
    }
}

// MARK: - UI

extension JoinForm3VC {

    private func setupUI() {
        setupNavigationBar()
        setupTextFields()
        setupNextButton()
        hydrateLabels()
    }

    private func hydrateLabels() {
        if let phoneNumber = phoneNumber {
            messageLabel.text = "Code sent to \(phoneNumber)"
        } else {
            messageLabel.text = "Code was sent"
        }
    }
    
    private func setupNextButton() {
        self.nextButton.isEnabled = false
    }

    private func setupNavigationBar() {
        removeNavigationBarTraits()
    }

    private func setupTextFields() {
        _ = [code0, code1, code2, code3, code4, code5].map {
            $0?.delegate = self
            $0?.deleteDelegate = self
            $0?.keyboardType = .numberPad
            $0?.keyboardAppearance = .dark
            $0?.borderStyle = .none
            $0?.borderColor = .steel
            $0?.borderWidth = 1
            $0?.cornerRadius = 4
            $0?.addTarget(self, action: #selector(setupTarget(_:)), for: .editingChanged)
        }
    }
    
    @objc private func setupTarget(_ sender: UITextField) {
        self.toggleNextButton()
        
        let isEmpty = sender.text?.isEmpty ?? true

        if isEmpty == false {
            if let next = self.view.viewWithTag(sender.tag + 1) {
                next.becomeFirstResponder()
            } else {
                self.startVerification()
            }
        }
    }
}

// MARK: - UITextFieldDelegate

extension JoinForm3VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.text = ""
        self.toggleNextButton()
        return true
    }
}

// MARK: - DeleteTextFieldDelegate

extension JoinForm3VC: DeleteTextFieldDelegate {
    func deletePressed(_ textField: UITextField) {
        let isEmpty = textField.text?.isEmpty ?? true
        let previousTag = textField.tag - 1

        if isEmpty == true, let next = self.view.viewWithTag(previousTag) as? UITextField {
            next.becomeFirstResponder()
        } else {
            textField.text = ""
        }
    }
}
