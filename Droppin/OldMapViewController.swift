//
//  OldMapViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/6/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

    import UIKit
    import MapKit
    import CoreLocation
    
    class OldMapViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
        
        @IBOutlet weak var distanceRangePicker: UIPickerView!
        
        var distanceRangePickerDataSource = ["1 mile", "2 miles", "10 miles", "20 miles", "30 miles", "50 miles", "100 miles"]
        
        @IBOutlet weak var searchView: UIView!
        @IBOutlet weak var mapView: MKMapView!
        var locationManager: CLLocationManager?
        
        
        var isMenuShowing = false
        var isSearchViewShowing = false
        @IBOutlet weak var sideMenuView: UIView!
        //    @IBOutlet weak var sideMenuTableView: UITableView!
        @IBOutlet weak var searchAction: UIButton!
        
        @IBOutlet weak var searchViewTopContraint: NSLayoutConstraint!
        @IBOutlet weak var leadingContraint: NSLayoutConstraint!
        @IBOutlet weak var searchViewHeightContraint: NSLayoutConstraint!
        
        var originalSearchViewHeight: CGFloat = 60
        let animationTime: Double = 0.2
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.distanceRangePicker.delegate = self
            self.distanceRangePicker.dataSource = self
            
            self.initSideMenu()
            self.initMap()
            self.initSearchView()
            
        }
        
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return self.distanceRangePickerDataSource.count;
            
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return self.distanceRangePickerDataSource[row]
        }
        
        
        
        
        override func viewDidAppear(_ animated: Bool) {
            print("[ViewController][viewDidAppear] ->start")
            super.viewDidAppear(true)
            let defaults = UserDefaults.standard
            if ( !defaults.bool(forKey: "userLoggedIn")) {
                print("[ViewController][viewDidAppear] ->has userLoggedIn=false")
                print("[ViewController][viewDidAppear] ->lastUserId = " + String(defaults.integer(forKey: "lastUserId") ) )
                if ( defaults.integer(forKey: "lastUserId") > 0      ){
                    return self.performSegue(withIdentifier: "loginSegue", sender: self)
                }
                return self.performSegue(withIdentifier: "joinNow", sender: self)
            }
            
            self.originalSearchViewHeight = self.searchViewHeightContraint.constant
            self.searchViewTopContraint.constant = -self.originalSearchViewHeight
            
        }
        
        
        @IBAction func logoutAction(_ sender: Any) {
            let defaults = UserDefaults.standard
            defaults.set(false, forKey: "userLoggedIn")
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
        
        
        //    @IBAction func logoutAction(_ sender: Any) {
        //        let defaults = UserDefaults.standard
        //        defaults.set(false, forKey: "userLoggedIn")
        //        self.performSegue(withIdentifier: "loginSegue", sender: self)
        //    }
        
        func initSearchView(){
            self.searchViewTopContraint.constant = -self.originalSearchViewHeight
            self.searchView.layer.shadowOpacity = 1
            self.searchView.layer.shadowRadius = 6
        }
        
        func initSideMenu(){
            self.leadingContraint.constant = -150
            self.sideMenuView.layer.shadowOpacity = 1
            self.sideMenuView.layer.shadowRadius = 6
        }
        
        func initMap(){
            print("[VC][initMap] ->Start")
            self.locationManager = CLLocationManager()
            self.locationManager!.delegate = self
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                print("[VC][initMap] ->authorizationStatus == authorizedWhenInUse")
                self.locationManager!.startUpdatingLocation()
            } else {
                self.locationManager!.requestWhenInUseAuthorization()
            }
            
            
            self.mapView.isScrollEnabled = false
            
        }
        
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            switch status {
            case .notDetermined:
                print("NotDetermined")
            case .restricted:
                print("Restricted")
            case .denied:
                print("Denied")
            case .authorizedAlways:
                print("AuthorizedAlways")
            case .authorizedWhenInUse:
                print("AuthorizedWhenInUse")
                locationManager!.startUpdatingLocation()
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            let location = locations.first!
            let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500)
            mapView.setRegion(coordinateRegion, animated: true)
            locationManager?.stopUpdatingLocation()
            locationManager = nil
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Failed to initialize GPS: ", error.localizedDescription)
        }
        
        @IBAction func searchButtonAction(_ sender: Any) {
            self.searchAction(sender)
        }
        
        
        @IBAction func searchAction(_ sender: Any) {
            if(self.isSearchViewShowing){
                self.searchViewTopContraint.constant = -self.originalSearchViewHeight
            }else{
                self.searchViewTopContraint.constant = 0
            }
            UIView.animate(withDuration: self.animationTime, animations: {
                self.view.layoutIfNeeded()
            })
            self.isSearchViewShowing = !self.isSearchViewShowing
        }
        
        @IBAction func openSideMenu(_ sender: Any) {
            if(self.isMenuShowing){
                self.leadingContraint.constant = -150
            }else{
                self.leadingContraint.constant = 0
            }
            UIView.animate(withDuration: self.animationTime, animations: {
                self.view.layoutIfNeeded()
            })
            self.isMenuShowing = !self.isMenuShowing
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        
        
}

