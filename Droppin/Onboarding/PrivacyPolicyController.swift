//
//  PrivacyPolicyController.swift
//  Droppin
//
//  Created by Madson on 9/25/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

final class PrivacyPolicyController: UIViewController {
    @IBOutlet weak var termsLabel: UILabel!

    override func viewDidLoad() {
        setupUI()
    }

    @IBAction func done(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PrivacyPolicyController {
    func setupUI() {
        setupTerms()
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        removeNavigationBarTraits()
    }

    func setupTerms() {
        guard let fileURL = Bundle.main.url(forResource: "terms", withExtension: ".md") else { return }
        var text = ""

        do {
            text = try String(contentsOf: fileURL, encoding: .utf8)
        } catch { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = 18
        paragraphStyle.maximumLineHeight = 18
        paragraphStyle.paragraphSpacing = 0

        let attrs: [NSAttributedString.Key : Any] = [
            .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
            .foregroundColor: UIColor(white: 0.0, alpha: 1.0),
            .kern: 0.05,
            .paragraphStyle: paragraphStyle
        ]

        let font = UIFont.systemFont(ofSize: 13.0, weight: .semibold)
        let attributedText = NSMutableAttributedString(string: text, attributes: attrs)

        attributedText.addAttribute(.font, value: font, range: NSRange(location: 61, length: 7))
        attributedText.addAttribute(.font, value: font, range: NSRange(location: 72, length: 2))
        attributedText.addAttribute(.font, value: font, range: NSRange(location: 78, length: 2))
        attributedText.addAttribute(.font, value: font, range: NSRange(location: 86, length: 3))
        attributedText.addAttribute(.foregroundColor, value: UIColor.aquaMarine, range: NSRange(location: 313, length: 16))
        attributedText.addAttribute(.font, value: font, range: NSRange(location: 972, length: 4))
        attributedText.addAttribute(.font, value: font, range: NSRange(location: 1098, length: 5))
        attributedText.addAttribute(.font, value: font, range: NSRange(location: 1111, length: 134))

        termsLabel.attributedText = attributedText
    }
}
