//
//  Identifiable.swift
//  Droppin
//
//  Created by Madson on 8/30/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

protocol Identifiable {
    static var identifier: String { get }
}
