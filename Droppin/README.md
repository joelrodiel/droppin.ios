

## Common Problems

### No such module 'SomeModule'

### Red Files, Missing files
Select the file or files in the left hand Project Navigator (the folder icon)
In the right sidebar click on "File Inspector" which is the leftmost icon resembling a page
In the "Identity and Type" section, there is a grey box labeled "Location".
Click on the small icon to the lower left that resembles a file browser - this will come up with a file browser to locate the file.


###  Clean the Build Folder
This will delete all of the products and intermediate files in the build folder. To clean the build folder you can use the shortcut Command+Option+Shift+K or Menu Bar → Product → Hold Option Key → Clean build Folder.

### Delete the Module Cache
Deleting the module cache will help to fixed code completion errors etc. To delete the module cache close the Xcode and delete the ~/Library/Developer/Xcode/DerivedData/ModuleCache directory.


##  Reference
  
### pod update
  When you run pod update PODNAME, CocoaPods will try to find an updated version of the pod PODNAME, without taking into account the version listed in Podfile.lock. It will update the pod to the latest version possible (as long as it matches the version restrictions in your Podfile).
  
  If you run pod update with no pod name, CocoaPods will update every pod listed in your Podfile to the latest version possible.

### How to Update Pods

Every time the pod install command is run — and downloads and install new pods — it writes the version it has installed, for each pods, in the Podfile.lock file. This file keeps track of the installed version of each pod and locks those versions.
When you run pod install, it only resolves dependencies for pods that are not already listed in the Podfile.lock.

pod outdated
When you run pod outdated, CocoaPods will list all pods which have newer versions than the ones listed in the Podfile.lock (the versions currently installed for each pod). This means that if you run pod update PODNAME on those pods, they will be updated — as long as the new version still matches the restrictions like pod 'MyPod', '~>x.y' set in your Podfile.
