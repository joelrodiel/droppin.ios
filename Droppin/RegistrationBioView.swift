//
//  RegistrationBioView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/26/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class RegistrationBioView: UIViewController, DroppinView, UserApi {
    
    @IBOutlet weak var bio: DefaultTextView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lengthIndicator: UILabel!
    
    final let MIN_LENGTH: Int = 3
    final let MAX_LENGTH: Int = 300
    
    let api: DroppinApi = DroppinApi()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")

        if let image = UserDefaults.standard.object(forKey: AccountSettings.profileImageData) as? String {
            profileImage.cldSetImage(publicId: image, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }

        profileImage.roundBorders()
        
        bio.defaultDelegate = self
        bio.minLength = MIN_LENGTH
        bio.maxLength = MAX_LENGTH
        bio.text = ""
        bio.placeholder = "Who are you? What do you do? Why?"
        bio.isScrollEnabled = false
        
        lengthIndicator.text = "0/\(MAX_LENGTH)"
        
        updateNextButtonState(valid: bio.isValid())
    }
    
    private func updateNextButtonState(valid: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = valid
    }
    
    @IBAction func NextAction(_ sender: UIButton) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()
        
        self.updateBio(bio: bio.text) { diResult in
            switch diResult {
            case .success(_):
                self.performSegue(withIdentifier: "interestsSegue", sender: sender)
            case .failure(let requestResult, let error):
                self.showAlert(title: "Error!", result: requestResult, error: error, viewController: self)
            }
            
            DiActivityIndicator.hide()
        }
    }
}

extension RegistrationBioView: DefaultTextViewDelegate {

    func defaultTextView(isValid valid: Bool) {
        updateNextButtonState(valid: valid)
    }
    
    func defaultTextView(allCharacters count: Int) {
        lengthIndicator.text = "\(count)/\(MAX_LENGTH)"
    }
}

