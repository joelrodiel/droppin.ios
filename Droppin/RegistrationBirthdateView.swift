//
//  BirthdateView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/17/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import SwiftMoment

class RegistrationBirthdateView: UIViewController, DroppinView, UserApi {
    
    @IBOutlet var bdayField: TextFieldDatePicker!
    @IBOutlet var gender: UISegmentedControl!

    let api: DroppinApi = DroppinApi()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        checkIfFormValid()
    }
    
    private func setupView() {
        let today = Date()
        let calendar = NSCalendar.current
        if let date = calendar.date(byAdding: .year, value: -18, to: today) {
            bdayField.date = date
        }
        bdayField.maximumDate = Date()
        bdayField.datePickerDelegate = self
        bdayField.addBottomBorder(.steel)
        
        // Disabled back button to avoid verify code again
        navigationItem.hidesBackButton = true
    }
    
    private func checkIfFormValid() {
        let dateValidation = bdayField.isDateSelected()
        let genderValidation = gender.selectedSegmentIndex != -1
        
        navigationItem.rightBarButtonItem?.isEnabled = dateValidation && genderValidation
    }
    
    @IBAction func changeGender(_ sender: UISegmentedControl) {
        checkIfFormValid()
    }
    
    @IBAction func next(_ sender: UIButton) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        DiActivityIndicator.showActivityIndicator()
        
        updateBirthdate() { bdayError in
            self.updateGender() { genderError in
                DiActivityIndicator.hide()
                
                if bdayError == nil && genderError == nil {
                    return self.performSegue(withIdentifier: "photoProfile", sender: sender)
                }
                
                let error = bdayError?.description ?? genderError?.description ?? "No Error"
                
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                DiAlert.alertSorry(message: "The operation couldn’t be completed.", inViewController: self)
            }
        }
    }
    
    private func updateBirthdate(callback: @escaping ((DiApiError?) -> ())) {
        self.updateBirthdate(birthDate: bdayField.date) { diResult in
            switch diResult {
            case .success(_):
                callback(nil)
            case .failure(_, let error):
                callback(error)
            }
        }
    }
    
    private func updateGender(callback: @escaping ((DiApiError?) -> ())) {
        let gender = Gender.allCases[self.gender.selectedSegmentIndex]
        
        self.updateGender(gender: gender) { diResult in
            switch diResult {
            case .success(_):
                callback(nil)
            case .failure(_, let error):
                callback(error)
            }
        }
    }
}

extension RegistrationBirthdateView: TextFieldDatePickerDelegate {
    
    func textFieldDatePicker(onChanged date: Date) {
        checkIfFormValid()
    }
}
