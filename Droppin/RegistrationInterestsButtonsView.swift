//
//  RegistrationInterestsButtonsView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/29/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class RegistrationInterestsButtonsView: UIViewController,DroppinView {
//    var activityIndicator = UIActivityIndicatorView()
//    var isDroppinTester = UserDefaults.standard.bool(forKey: UserPropertyKey.isTester.rawValue)
    @IBOutlet var interestsCollection: [SignUpInterestButton]!
    
    var interests:[Interest]?

    func getSelectedInterests()->[String]?{
        var interests:[String] = []
        interests.append("music")

        if interests.isEmpty {
            return nil
        }

        return interests
    }
    
    
//    buttons.map { $0.setTitle(String(number), forState: .Normal) }
    
    
    
    @IBAction func interestButtonTouch(_ sender: SignUpInterestButton) {
        interests![sender.tag].isSelected = sender.isOn
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interestsCollection.map {$0.isHidden = true}  // Prob not correct use of map
        
        self.interests = getInterests()
        for interest in self.interests! {
            interestsCollection[interest.id].isHidden = false
            interestsCollection[interest.id].setTitle(interest.label, for: .normal)
            interestsCollection[interest.id].tag = interest.id
            self.interests![interest.id].isSelected = interestsCollection[interest.id].isOn
//            interest.isSelected = interestsCollection[interest.id].isOn
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
