//
//  RegistrationInterestsView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/17/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class RegistrationInterestsView: UIViewController, DroppinView, UserApi {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    private static let titleKey = "Select up to {{number}} interests in order of preference"
    
    let api: DroppinApi = DroppinApi()
    var interestPicker: InterestsPickerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    private func setupView() {
        titleLabel.text = RegistrationInterestsView.titleKey.replacingOccurrences(of: "{{number}}", with: String(DiConfig.maxInterests))
    }
    
    @IBAction func next(_ sender: UIButton) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        DiActivityIndicator.showActivityIndicator()
        
        let selectedInterests = interestPicker.selectedInterests
        
        updateInterests(interests: selectedInterests, completion:{ diResult in
            switch diResult {
            case .success(_):
                AccountSettings.setFirstTimeViewAs(as: true)
                PinLocationManager.sharedManager.startTracking(.foreground)
                
                self.performSegue(withIdentifier: "navigationSegue", sender: self)
            case .failure(_, let error):
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error.localizedDescription)")
                DiAlert.alertSorry(message: "There's a problem here!", inViewController: self)
            }
        })

        DiActivityIndicator.hide()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        if let controller = segue.destination as? InterestsPickerViewController {
            interestPicker = controller
            interestPicker.pickerDelegate = self
            navigationItem.rightBarButtonItem?.isEnabled = interestPicker.isValid()
        }
    }
}

extension RegistrationInterestsView: InterestsPickerViewControllerDelegate {
    
    func interestsPicker(valid: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = valid
    }
}
