//
//  RegistrationProfileView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/17/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CropViewController

class RegistrationProfileView: UIViewController, DroppinView, DiImageLoader {
    
    let api: DroppinApi = DroppinApi()
    
    private var imagesController: ImagesPickerViewController?
    
    override func viewDidLoad() {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        super.viewDidLoad()
        
        if let imagesController = self.childViewControllers.first as? ImagesPickerViewController {
            self.imagesController = imagesController
            self.imagesController!.delegate = self
        }
        
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: Any) {
        DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Start")
        
        guard let imagesController = imagesController else { return }
        let filtered = imagesController.getImages().compactMap { $0 }
        if filtered.count == 0 {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: No image selected")
        }
        
        DiActivityIndicator.showActivityIndicator()
        uploadImages(images: filtered) { error in
            DiActivityIndicator.hide()
            
            if let error = error {
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Failure: \(error)")
                return
            }
            
            if let mainImage = imagesController.getMainImage() {
                UserDefaults.standard.set(mainImage, forKey: AccountSettings.profileImageData)
                UserDefaults.standard.synchronize()
            }
            
            self.performSegue(withIdentifier: "bioSegue", sender: sender)
        }
    }
}

extension RegistrationProfileView: ImagesPickerViewControllerDelegate {

    func imagesPicker(_ picturesCollection: UICollectionView, pickedAtLeastOne: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = pickedAtLeastOne
    }
}

