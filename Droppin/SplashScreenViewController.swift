//
//  SplashScreenViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 11/3/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        perform(Selector.init("showNavigationController:"), with: nil, afterDelay: 3)
        
        perform(#selector(showNavigationController), with: nil, afterDelay: 3)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @objc func showNavigationController(){
        performSegue(withIdentifier: "navigationControllerSegue", sender: self)
    }

}
