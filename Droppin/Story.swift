//
//  DroppinView.swift
//  Droppin
//
//  Created by Rene Reyes on 10/13/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

//import UIKit
//
//class DroppinView: UIViewController {
//
//    private var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//
//    func startActivityIndicator() {
//        self.activityIndicator.center = self.view.center
//        self.activityIndicator.hidesWhenStopped = true
//        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
//        view.addSubview(activityIndicator)
//        self.activityIndicator.startAnimating()
//        UIApplication.shared.beginIgnoringInteractionEvents()
//    }
//
//    func stopActivityIndicator(){
//        self.activityIndicator.stopAnimating()
//        UIApplication.shared.endIgnoringInteractionEvents()
//    }
//
//
//
//    enum InterestType : CustomStringConvertible {
//        case music
//        case outdoors
//
//        var description: String {
//            switch self{
//            case .music: return "Music"
//            case .outdoors: return "Outdoors"
//            }
//        }
//    }
//
//
//    struct Interest {
//        let isSelected: Bool?
//        let name: InterestType?
//        let id: Int?
//        let label: String?
//    }
//
//    let interests = [
//        Interest(isSelected: nil, name: InterestType.music, id: 0, label: InterestType.music.description),
//        Interest(isSelected: nil, name: InterestType.outdoors, id: 1, label: InterestType.outdoors.description)
//    ]
//
//
//
//
////    struct Interests{
////        let statusCode: StatusCode?
////        let message: String?
////        let metaData: [String:String]?
////
////        init(statusCode: StatusCode?, message: String?){
////            metaData = nil
////            self.statusCode = statusCode
////            self.message = message
////        }
////
////        init(statusCode: StatusCode?, message: String?, metaData: [String:String]?){
////            self.metaData = metaData
////            self.statusCode = statusCode
////            self.message = message
////        }
////    }
//
//
//}

