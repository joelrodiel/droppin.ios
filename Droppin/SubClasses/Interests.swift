//
//  Interests.swift
//  Droppin
//
//  Created by Rene Reyes on 10/30/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import TagListView

enum InterestType : CustomStringConvertible {
    
    case building
    case business
    case cars
    case chilling
    case creativeArts
    case fitness
    case gaming
    case health
    case literature
    case mastermind
    case music
    case newExperiences
    case outdoors
    case partying
    case people
    case politics
    case science
    case school
    case spirituality
    case sports
    case tech
    
    var description: String {
        switch self{
        case .building: return "Building"
        case .business: return "Business"
        case .cars: return "Cars"
        case .chilling: return "Chilling"
        case .creativeArts: return "Creative Arts"
        case .fitness: return "Fitness"
        case .gaming: return "Gaming"
        case .health: return "Health"
        case .literature: return "Literature"
        case .mastermind: return "Mastermind"
        case .music: return "Music"
        case .newExperiences: return "New experiences"
        case .outdoors: return "Outdoors"
        case .partying: return "Partying"
        case .people: return "People"
        case .politics: return "Politics"
        case .science: return "Science"
        case .school: return "School"
        case .spirituality: return "Spirituality"
        case .sports: return "Sports"
        case .tech: return "Tech"
        }
    }
}


struct Interest {
    var isSelected: Bool
    let name: InterestType
    let id: Int
    let label: String
    var tagView: TagView?
    
    init(isSelected: Bool, name: InterestType, id: Int, label: String) {
        self.isSelected = isSelected
        self.name = name
        self.id = id
        self.label = label
    }
}

func getInterests()->[Interest]{
    
    return [
        Interest(isSelected: false, name: InterestType.building, id: 0, label: InterestType.building.description),
        Interest(isSelected: false, name: InterestType.business, id: 1, label: InterestType.business.description),
        Interest(isSelected: false, name: InterestType.cars, id: 2, label: InterestType.cars.description),
        Interest(isSelected: false, name: InterestType.chilling, id: 3, label: InterestType.chilling.description),
        Interest(isSelected: false, name: InterestType.creativeArts, id: 4, label: InterestType.creativeArts.description),
        Interest(isSelected: false, name: InterestType.fitness, id: 5, label: InterestType.fitness.description),
        Interest(isSelected: false, name: InterestType.gaming, id: 6, label: InterestType.gaming.description),
        Interest(isSelected: false, name: InterestType.health, id: 7, label: InterestType.health.description),
        Interest(isSelected: false, name: InterestType.literature, id: 8, label: InterestType.literature.description),
        Interest(isSelected: false, name: InterestType.mastermind, id: 9, label: InterestType.mastermind.description),
        Interest(isSelected: false, name: InterestType.music, id: 10, label: InterestType.music.description),
        Interest(isSelected: false, name: InterestType.newExperiences, id: 11, label: InterestType.newExperiences.description),
        Interest(isSelected: false, name: InterestType.outdoors, id: 12, label: InterestType.outdoors.description),
        Interest(isSelected: false, name: InterestType.partying, id: 13, label: InterestType.partying.description),
        Interest(isSelected: false, name: InterestType.people, id: 14, label: InterestType.people.description),
        Interest(isSelected: false, name: InterestType.politics, id: 15, label: InterestType.politics.description),
        Interest(isSelected: false, name: InterestType.science, id: 16, label: InterestType.science.description),
        Interest(isSelected: false, name: InterestType.school, id: 17, label: InterestType.school.description),
        Interest(isSelected: false, name: InterestType.spirituality, id: 18, label: InterestType.spirituality.description),
        Interest(isSelected: false, name: InterestType.sports, id: 19, label: InterestType.sports.description),
        Interest(isSelected: false, name: InterestType.tech, id: 20, label: InterestType.tech.description),
    ]
}
