//
//  ApiPagination.swift
//  Droppin
//
//  Created by Isaias Garcia on 02/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation

struct ApiPagination {
    var page: Int = 0
    var total: Int?
    var pages: Int?
    
    var nextPage: Int {
        return page + 1
    }
    
    var hasNextPage: Bool {
        get {
            return pages == nil || nextPage <= pages!
        }
    }
}
