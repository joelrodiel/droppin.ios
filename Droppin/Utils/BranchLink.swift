//
//  BranchSharing.swift
//  Droppin
//
//  Created by Isaias Garcia on 25/06/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Branch

class BranchLink: BranchUniversalObject {
    
    private var _properties: BranchLinkProperties = BranchLinkProperties()
    
    var properties: BranchLinkProperties {
        set {
            self._properties = newValue
        }
        get {
            return self._properties
        }
    }
    
    init(title: String, description: String, pinId: String, imageURL: String?, campaign: String? = nil, tags: [String]? = nil) {
        super.init()
        
        // Set properties
        self.title = title
        self.contentDescription = description
        self.imageUrl = imageURL
        self.contentMetadata.customMetadata["pinId"] = pinId
        
        // Set default BranchLinkProperties
        self._properties.channel = "social-media"
        self._properties.feature = "pin"
        
        if let tags = tags {
            self._properties.tags = tags
        }
        
        if let campaign = campaign {
            self._properties.campaign = campaign
        }
    }
    
    func share(message: String, viewController: UIViewController, afterSharingAction action: ((Bool) -> Void)?) {
        self.showShareSheet(with: self.properties, andShareText: message, from: viewController) { (activityType, completed) in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: activityType ?? "No Type")
            action?(completed)
        }
    }
}
