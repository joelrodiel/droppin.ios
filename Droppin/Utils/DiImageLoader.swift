//
//  ImageLoader.swift
//  Droppin
//
//  Created by Isaias Garcia on 15/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import UIKit

protocol DiImageLoader: UserApi {
    var api: DroppinApi {get}
}

extension DiImageLoader {
    
    func uploadImages(images: [String]?, callback: @escaping (Error?) -> ()) {
        update(images) { (error, response) in
            callback(error)
        }
    }
    
    func loadImage(userId: String, callback: @escaping (String?)->()) {
        getUserImage(userId) { error, result in
            guard error == nil else {
                callback(nil)
                return
            }

            guard let data = result?.metaData else {
                callback(nil)
                return
            }

            let profileImage = data["profileImage"]
            callback(profileImage)
        }
    }
    
    func loadImages(userId: String, callback: @escaping ([String]?)->()) {
        getUserImages(userId) { error, result in
            guard error == nil else {
                callback(nil)
                return
            }

            guard let data = result?.metaData else {
                callback(nil)
                return
            }

            var images = [String]()
            for image in data {
                if let profileImage = image["profileImage"] {
                    images.append(profileImage)
                }
            }

            callback(images)
        }
    }
    
    func loadProfilePicture(for button: UIButton, userId: String) {
        loadImage(userId: userId) { image in
            guard let image = image else { return }
            button.cldSetImage(publicId: image, cloudinary: CDNManager.shared.cloudinary, forState: .normal)
        }
    }
    
    func loadProfilePicture(for imageView: UIImageView, userId: String) {
        loadImage(userId: userId) { image in
            guard let image = image else { return }
            imageView.cldSetImage(publicId: image, cloudinary: CDNManager.shared.cloudinary, signUrl: true)
        }
    }
}
