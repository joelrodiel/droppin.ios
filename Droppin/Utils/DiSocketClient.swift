//
//  SocketIOHelper.swift
//  Droppin
//
//  Created by Isaias Garcia on 03/07/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON

class DiSocketClient {
    let api = DroppinApi()
    let manager: SocketManager
    let socket: SocketIOClient
    
    init() {
        let url = URL(string: Config.env.baseURL)!
        let config: SocketIOClientConfiguration = [.log(false), .compress]
        
        self.manager = SocketManager(socketURL: url, config: config)
        self.socket = manager.defaultSocket
        
        socket.on(clientEvent: .connect) {data, ack in
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Socket connected")
        }
        
        socket.connect()
    }
    
    func listen(event: String, onEvent: @escaping ([Any])->()) {
        socket.on(event) {data, ack in
            onEvent(data)
        }
    }
    
    func emit(event: String, data: [Any]) {
        socket.emit(event, with: data)
    }
    
    func disconnect() {
        socket.disconnect()
    }
}
