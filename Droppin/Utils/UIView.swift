//
//  UIView.swift
//  Droppin
//
//  Created by Isaias Garcia on 16/08/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

extension UIView {
    
    func roundBorders() {
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true;
    }

    func addGradientLayer(with colors: [UIColor], alpha: CGFloat, isHorizontal: Bool = false) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map({ $0.cgColor })

        if isHorizontal {
            gradient.startPoint = CGPoint(x: 0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1, y: 0.5)
        }

        self.alpha = alpha
        self.layer.insertSublayer(gradient, at: 0)
    }
}
