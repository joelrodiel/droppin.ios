//
//  UIViewControllerExtensions.swift
//  Droppin
//
//  Created by Madson on 9/20/18.
//  Copyright © 2018 Mintz Holdings, LLC. All rights reserved.
//

import Foundation
import JGProgressHUD

extension UIViewController {
    func setupDefaultBackgroundGradient() {
        let topColor = UIColor.aquaMarine
        let bottomColor = UIColor.salmon
        self.view.addGradientLayer(with: [topColor, topColor, bottomColor], alpha: 1)
    }
    
    func removeNavigationBarTraits() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    func present(from source: UIViewController, completion: (() -> Void)? = nil) {
        source.present(self, animated: true, completion: completion)
    }
    
    func goBack(message: String? = nil, error: Error? = nil) {
        if let error = error {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Error: \(error)")
        }
        
        if let message = message {
            DiAlert.alertSorry(message: message, inViewController: self)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension UIViewController {
    
    func showOverlay(text: String? = nil, withLoading: Bool = true) {
        let overlay = UIView()
        
        overlay.backgroundColor = .white
        overlay.frame = self.view.frame
        
        if let text = text {
            let label = UILabel()
            
            label.text = text
            label.translatesAutoresizingMaskIntoConstraints = false
            overlay.addSubview(label)
            label.centerXAnchor.constraint(equalTo: overlay.centerXAnchor).isActive = true
            label.centerYAnchor.constraint(equalTo: overlay.centerYAnchor).isActive = true
        }
        
        self.view.addSubview(overlay)
        
        if withLoading {
            let hud = JGProgressHUD(style: .dark)
            
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
        }
    }
    
    func removeOverlay() {
        guard let views = self.view?.subviews.enumerated() else { return }
        
        for (index, element) in views {
            if index > 0 {
                element.removeFromSuperview()
            }
        }
    }
}
