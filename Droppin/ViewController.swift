//
//  ViewController.swift
//  Droppin
//
//  Created by Rene Reyes on 10/2/17.
//  Copyright © 2017 Mintz Holdings, LLC. All rights reserved.
//

import UIKit
import CoreLocation

struct PanGestureProperties {
    var fingerLocation: CGPoint?
    var fingerLocationAtBegan: CGPoint?
    var swipingDirection: SwipingDirection?
}

protocol ViewWithSliders {
    var panGestureProperties: PanGestureProperties { get set }
    func updateSwipingDirection(velocity: CGPoint)
    func isInsideTouchableArea(forSlider slider: DISlider) -> Bool
    func handlePanGesture(recognizer: UIPanGestureRecognizer, forSlider slider: DISlider)
}

class ViewController: UIViewController, DroppinView, UITextFieldDelegate, Observer {
    
    @IBOutlet weak var searchItem: UIBarButtonItem!
    @IBOutlet weak var messagesItem: BadgeBarButtonItem!

    private var preparedPin: String?
    
    var pinDetailsMasterViewController: PinDetailsMasterViewController!
    var mainMapVC: MainMapVC?
    var sideMenuViewController: SideMenuVC!
    var searchViewController: PinFilterViewController!
    
    var topSlider: DISlider!, bottomSlider: DISlider!
    let grayViewTagNumber: Int = 213
    var panGestureProperties = PanGestureProperties()
    var grayView: UIView!
    var range: RangeEnumeration?
    var newRequests: Int = 0
    var newPinData: [String : Any?]? // Example ["pin": Pin2, "zoom": RangeEnumeration]
    
    @IBAction func unwindToMainViewController(forZooming unwindSegue: UIStoryboardSegue) {
        guard let pinData = newPinData else { return }
        
        if let zoom = pinData["zoom"] as? RangeEnumeration, let mainMapVC = mainMapVC {
            mainMapVC.forceRefresh = true
            mainMapVC.range = zoom
        }

        if let pin = pinData["pin"] as? Pin2 {
            showCreationAlert(for: pin)
        }
    }
    
    enum SegueIdentifier: String {
        case mainMapViewSegue = "mainMapViewSegue"
    }
    
    fileprivate func initGrayView() {
        grayView = UIView(frame: view.frame)
        grayView.tag = grayViewTagNumber
        grayView.isHidden = true
        grayView.alpha = 0
        view.addSubview(grayView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let item = self.tabBarController?.navigationItem {
            item.title = MainTabsViewController.Tabs.home.rawValue
            item.leftBarButtonItem = searchItem
            item.rightBarButtonItem = messagesItem
        }
    }
    
    override func viewDidLoad() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        
        super.viewDidLoad()
        initGrayView()
        grayView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        initSlider(for: .TopSlider, identifier: "sbPinFilterView", visibleLength: 320, enableDragExpand: false) { (_ vc: UIViewController, _ diSlider: DISlider  ) in
            searchViewController = vc as? PinFilterViewController //@TODO not sure i like this.
            searchViewController.delegate = self
            topSlider = diSlider
            topSlider.attach(searchViewController)
        }
        
        initSlider(for: .BottomSlider, identifier: "pinDetailsMasterView", visibleLength: 0, enableDragExpand: false) { (_ vc: UIViewController, _ diSlider: DISlider  ) in
            pinDetailsMasterViewController = vc as? PinDetailsMasterViewController
            pinDetailsMasterViewController.delegate = self
            bottomSlider = diSlider
        }
        
        // Setup Observers
        topSlider.attach([bottomSlider, self])
        bottomSlider.attach([topSlider, self])
        
        if let mainMapVC = mainMapVC {
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            mainMapVC.view.addGestureRecognizer(tap)
        }

        // Added observer for updating badge number on messages button
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadge(_:)), name: .updateBadge, object: nil)
        
        // Added observer for showing quick pin detail view
        NotificationCenter.default.addObserver(self, selector: #selector(showQuickPin(_:)), name: .showQuickPin, object: nil)
        
        // Added observer for showing quick pin detail view
        NotificationCenter.default.addObserver(self, selector: #selector(prepareQuickPin(_:)), name: .prepareQuickPin, object: nil)
    }

    @objc private func updateBadge(_ notification: Notification) {
        messagesItem.badgeNumber = notification.object as? Int ?? 0
    }
    
    @objc private func showQuickPin(_ notification: Notification) {
        guard let pinId = notification.object as? String else { return }
        pinTouched(pinId: pinId)
    }
    
    @objc private func prepareQuickPin(_ notification: Notification) {
        guard let pinId = notification.object as? String else { return }
        preparedPin = pinId
    }
    
    @objc func handleTap() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
        
        topSlider.collapse()
        bottomSlider.collapse()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        super.viewDidAppear(animated)
        bottomSlider.updateViewProperties(parentViewSize: view.frame.size)
        topSlider.updateViewProperties(parentViewSize: view.frame.size)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
        guard (UserDefaults.standard.bool(forKey: UserPropertyKey.isAccountCreated.rawValue)) else {
            return self.performSegue(withIdentifier: "joinNow", sender: self)
        }
        
        if userId == nil {
            print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "No User found. PerformSeque to signInSeque"))
            return self.performSegue(withIdentifier: "signInSegue", sender: self)
        }
        
        if let pinId = preparedPin {
            self.pinTouched(pinId: pinId)
            self.preparedPin = nil
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        if (identifier == SegueIdentifier.mainMapViewSegue.rawValue) {
            let mongoIdLength = 24
            guard let userId = UserDefaults.standard.string(forKey: UserPropertyKey.currentUserId.rawValue), userId.count == mongoIdLength else {
                return false
            }
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        
        if (segue.identifier == SegueIdentifier.mainMapViewSegue.rawValue) {
            mainMapVC = segue.destination as? MainMapVC
            mainMapVC?.delegate = self
        }
    }
    
    @IBAction func unwindToViewController(forMyPins unwindSegue: UIStoryboardSegue) {
        print("[VC][unwindToMyEventsController][forMyPins] -> Strart")
        
        if let mainMapVC = mainMapVC {
            mainMapVC.forceRefresh = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    // Observable Slider will send an update here.
    func update(_ observableState: ObservableState, sliderType: SliderType) {
        //        print("[VC][update] -> " + String(describing: observableState))
        
        //        switch (observableState) {
        //        case .hasExpanded:
        //            grayView.isHidden = false
        //            UIView.animate(withDuration: 0.3, animations: {
        //                self.grayView.alpha = 0.6 } , completion: nil)
        //        case .hasCollapsed:
        //            UIView.animate(withDuration: 0.3, animations: {
        //                self.grayView.alpha = 0
        //            }, completion: {
        //                (value: Bool) in
        //                self.grayView.isHidden =
        //                true
        //
        //            })
        //
        //        default:
        //            return
        //        }
        
    }
    
    
    func initSlider(for type: SliderType, identifier: String, visibleLength: CGFloat, enableDragExpand: Bool = true, enableDragCollapse: Bool = true, completion: (_ vc: UIViewController, _ diSlider: DISlider) -> Void) {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController: UIViewController
        viewController = storyBoard.instantiateViewController(withIdentifier: identifier)
        view.addSubview(viewController.view)
        addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)
        var slider: DISlider
        slider = DISlider(type: type,
                          sliderView: viewController.view,
                          visibleLength: visibleLength,
                          animationTime: 0.5,
                          enableDragExpand: enableDragExpand,
                          parentViewSize: self.view.frame.size)
        
        completion(viewController, slider)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        //        let defaults = UserDefaults.standard
        //        defaults.set(false, forKey: "userLoggedIn")
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.currentUserId.rawValue)
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.isTester.rawValue)
        UserDefaults.standard.removeObject(forKey: UserPropertyKey.token.rawValue)
        self.performSegue(withIdentifier: "loginSegue", sender: self)
    }
    
    @IBAction func searchDisplay(_ sender: UIBarButtonItem) {
        // Check for location status
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Location not enabled")
                
                let alert = UIAlertController(title: "Location not enabled", message: "We need access to your location to show pins near to you", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Close", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse:
                print("[VC][searchDisplay]")
                topSlider.toggle()
            }
        } else {
            DiHelper.printLineDescription(of: self, andFunction: #function, withDescription: "Location services are not enabled")
            
            let alert = UIAlertController(title: "Location not available", message: "We need access to your location to show pins near to you", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ViewController: PinDetailsMasterViewControllerDelegate {
    func pinLoaded(visibleLength: CGFloat) {
        bottomSlider.refreshView(for: visibleLength)
        bottomSlider.expand()
    }
    
    func didTapRequest() {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "START"))
    }
}

extension ViewController: MainMapVCDelegate {
    
    func pinTouched(pinId: String) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        pinDetailsMasterViewController.refresh(pinId: pinId)
    }
}

extension ViewController: PinFilterViewControllerDelegate {
    func didTapConnect(range: RangeEnumeration, search: String?) {
        print(DiHelper.lineDescription(of: self, andFunction: #function, withDescription: "Start"))
        
        if let mainMapVC = mainMapVC {
            self.range = range
            mainMapVC.refreshPins(range: range, search: search)
            topSlider.toggle()
        }
    }
}

extension ViewController: ViewWithSliders {
    
    func handlePanGesture(recognizer: UIPanGestureRecognizer, forSlider slider: DISlider) {
        
        //change to swipable area
        guard  let swipingDirection = panGestureProperties.swipingDirection, slider.canSlide(to: swipingDirection), isInsideTouchableArea(forSlider: slider) else {
            return
        }
        
        if (recognizer.state == .ended) {
            slider.completeTransition()
        }
        
        guard recognizer.state == .changed else {
            return
        }
        
        slider.slide(translation: recognizer.translation(in: view))
        recognizer.setTranslation(CGPoint.zero, in: view)
    }
    
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        
        updateSwipingDirection(velocity: recognizer.velocity(in: self.view))
        panGestureProperties.fingerLocation = recognizer.location(in: self.view)
        
        switch recognizer.state {
        case .began:
            panGestureProperties.fingerLocationAtBegan = recognizer.location(in: self.view)
        case .changed, .ended:
            handlePanGesture(recognizer: recognizer, forSlider: bottomSlider)
            handlePanGesture(recognizer: recognizer, forSlider: topSlider)
        default:
            print("[handlePanGesture] ->unhandled state: " + String(describing: recognizer.state))
        }
    }
    
    
    func updateSwipingDirection(velocity: CGPoint) {
        guard velocity != CGPoint(x: 0, y: 0) else {
            panGestureProperties.swipingDirection = nil
            return
        }
        
        if (abs(velocity.y) > abs(velocity.x)) {
            panGestureProperties.swipingDirection = (velocity.y < 0) ? .up : .down
            //            print("dragging Direction: " + String(describing: panGestureProperties.swipingDirection))
        } else {
            //            print("dragging Direction: " + String(describing: panGestureProperties.swipingDirection))
            panGestureProperties.swipingDirection = (velocity.x < 0) ? .left : .right
        }
    }
    
    func isInsideTouchableArea(forSlider slider: DISlider) -> Bool {
        guard let locAtBegan = panGestureProperties.fingerLocationAtBegan else {
            return false
        }
        
        if (slider.isExpanded) {
            return true
        }
        
        switch (slider.type) {
        case .LeftSlider:
            return (locAtBegan.x <= (view.frame.width * 0.40))
        case .BottomSlider:
            return (locAtBegan.y >= (view.frame.height - view.frame.height * 0.40))
        case .TopSlider:
            return (locAtBegan.y <= (view.frame.height * 0.40))
        }
    }
}

extension ViewController {

    private func showCreationAlert(for pin: Pin2) {
        let isFirstTimeCreatingAPin = AccountSettings.get(property: .isFirstTimeCreatingAPin) as? Bool ?? true

        if isFirstTimeCreatingAPin {
            let confirmAction = {
                guard
                    let pinId = pin.pinId,
                    let hostId = pin.hostId?.userId,
                    let description = pin.description
                    else { return }

                let pinSharing = PinSharing(pinId, hostId, pin.title, description, pin.startDate, pin.endDate, pin.photo)

                pinSharing.share(in: self, and: nil, completion: nil)
            }

            DiAlert.confirmOrCancel(title: "Share your pin and get more people to come.", message: nil, confirmTitle: "Share", cancelTitle: "Cancel", inViewController: self, withConfirmAction: confirmAction, cancelAction: {})
        }
    }
}

extension Notification.Name {
    static let updateBadge = Notification.Name(rawValue: "updateBadge")
    static let showQuickPin = Notification.Name(rawValue: "showQuickPin")
    static let prepareQuickPin = Notification.Name(rawValue: "prepareQuickPin")
}
